<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* -------------------------------------------------------------------
* Constants & URLs are defined by user
* -------------------------------------------------------------------
*/

// The path to the "public" folder
define('PUBLICPATH', FCPATH.'public/');

// The path to the "scripts" folder
define('SCRIPTSPATH', FCPATH.'public/scripts/');

// The path to the "templates" folder
define('TEMPLATESPATH', FCPATH.'public/templates/');

// The path to the "files" folder
define('FILESPATH', FCPATH.'public/files/');

// The path to the "upload files" folder
define('UPLOADPATH', FCPATH.'public/upload/files/');


// The URL to the base folder
define('BASEURL', '/');

// The URL to the application folder
define('APPURL', BASEURL.'application/');

// The URL to the "public" folder
define('PUBLICURL', BASEURL.'public/');

// The URL to the "scripts" folder
define('SCRIPTSURL', BASEURL.'public/scripts/');

// The URL to the "templates" folder
define('TEMPLATESURL', BASEURL.'public/templates/');

// The URL to the "files" folder
define('FILESURL', BASEURL.'public/files/');

// The URL to the "upload files" folder
define('UPLOADURL', BASEURL.'public/upload/files/');

// The URL to the "images" folder
define('IMAGESURL', BASEURL.'public/images/');

// utilities
define('NO_IMAGE', BASEURL.'public/images/no-image.png');


//-------------------------------------------------------------------
// Config
//-------------------------------------------------------------------
define('DI_DONG_TRA_TRUOC', 'PW1101');
define('DI_DONG_TRA_SAU', 'PW1102');
define('MUA_THE_DIEN_THOAI', 'PW1103');
define('MUA_THE_GAME', 'PW1104');
define('NAP_TIEN_GAME', 'PW1105');
define('NAP_TIEN_TAI_KHOAN', 'PW1106');

define('GD_KHOI_TAO', 1);
define('GD_THANH_TOAN_TC', 2);
define('GD_THANH_TOAN_TB', 3);
define('GD_XU_LY_TC', 4);
define('GD_XU_LY_TB', 5);
define('GD_XAC_NHAN_TC', 6);
define('GD_XAC_NHAN_TB', 7);
define('GD_HOAN_TIEN_TC', 8);
define('GD_HOAN_TIEN_TB', 9);
define('GD_KO_XAC_DINH', 10);
define('GD_LOI', 11);

define('GD_MB_KHOI_TAO', 1);
define('GD_MB_HOAN_TAT', 2);
define('GD_MB_THAT_BAI', 3);
define('GD_MB_DA_HOAN_TIEN', 4);
define('GD_MB_HOAN_TIEN_TB', 5);
define('GD_MB_LOI', 6);

//-------------------------------------------------------------------
// Function
//-------------------------------------------------------------------

function array_to_object($array = array())
{
	return json_decode(json_encode($array), FALSE);
}

function debug($var)
{
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

function debug1($var)
{
  echo '<pre>';
  var_dump($var);
  echo '</pre>';
}
function debuge($var)
{
  echo '<pre>';
  print_r($var);
  echo '</pre>';
  exit;
}
function debuge1($var)
{
  echo '<pre>';
  var_dump($var);
  echo '</pre>';
  exit;
}