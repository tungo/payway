;(function($) {
  $.fn.extend({
    linkSelect: function(select_id, params, selected) {
      $(this).each(function() {
        var value = $(this).prop('value');
        $(select_id).removeOption(/.*/);
        if (params[value].length != 0)
          $(select_id).addOption(params[value], false);
        if (selected != '')
          $(select_id).find('option[value="'+selected+'"]').prop('selected', true);
      });
      $(this).change(function() {
        var value = $(this).prop('value');
        $(select_id).removeOption(/.*/);
        if (params[value].length != 0)
          $(select_id).addOption(params[value], false);
      });
    }
  });
})(jQuery);
