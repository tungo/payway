$(document).ready(function(){

	$("#backtop").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#backtop').fadeIn();
			} else {
				$('#backtop').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#backtop a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	$(':checkbox[name="check_all"]').on('change', function(){
		$(':checkbox[name="cid[]"]').prop('checked', $(this).is(':checked'));
	});
	$(':checkbox[name="cid[]"]').on('change', function() {
		if (! $(this).is(':checked'))
		{
			$(':checkbox[name="check_all"]').prop('checked', false);
		}
		else
		{
			if ($(':checkbox[name="cid[]"]:checked').length == $(':checkbox[name="cid[]"]').length)
			{
				$(':checkbox[name="check_all"]').prop('checked', true);
			}
		}
	});
	$('.manage-table tbody tr').on('click', function(event) {
		var $target = $(event.target);
		if($target.is('td'))
		{
			$(this).find(':checkbox[name="cid[]"]').click();
		}
	});
	$('#manage_form').submit(function() {
		if (! $(this).prop('action'))
		{
			alert('FU Hacker!!!');
			return false;
		}
	});

	$('#update_form').on('click', '.delete_image', function(){
		$(this).prevAll('img.old_image').prop('src', '/public/images/no-image.png');
		$(this).prevAll(':hidden[name^="delete_image"]').val('1');
	});
});
function submit_form(url, cf)
{
	if (! url)
	{
		return false;
	}
	if (! cf || (cf && confirm(cf)))
	{
		$('#manage_form').prop('action', url).submit();
		return false;
	}
	return false;
}

function submit_manage_form(url, cf)
{
	if ($(':checkbox[name="cid[]"]:checked').length == 0)
	{
		alert('Please select one or more rows.');
		return false;
	}
	if (! url)
	{
		return false;
	}
	if (! cf || (cf && confirm(cf)))
	{
		$('#manage_form').prop('action', url).submit();
		return false;
	}
	return false;
}

function clear_filter(url, name)
{
	if (! url || ! name)
	{
		return false;
	}
	$(':input[name="'+name+'"]').val('');
	$('#manage_form').prop('action', url).submit();
	return false;
}

function clear_search(url)
{
	if (! url)
	{
		return false;
	}
	$(':input[name="keyword"]').val('');
	$('#manage_form').prop('action', url).submit();
	return false;
}

function clear_show(url)
{
	if (! url)
	{
		return false;
	}
	$(':input[name="list"]').val('');
	$('#manage_form').prop('action', url).submit();
	return false;
}

function sort_list(url, name)
{
	if (! url || ! name)
	{
		return false;
	}
	$(':input[name="sort"]').val(name);
	$('#manage_form').prop('action', url).submit();
	return false;
}
function go_to_page(url)
{
	if (! url)
	{
		return false;
	}
	url = url + '/' + $(':input[name="page"]').val();
	$('#manage_form').prop('action', url).submit();
	return false;
}
function valid_update_form(rule)
{
	$('#update_form').validate({
		rules: rule,
		highlight: function(element) {
			// $(element).closest('.form-control').parent().parent().removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			// $(element).closest('.form-control').parent().parent().removeClass('has-error').addClass('has-success');
			$(element).remove();
		}
	});
}

