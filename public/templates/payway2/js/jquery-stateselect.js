/*
 * Simple State/Province Select plugin for jQuery
 *
 * Example:
 * $(document).ready(function() {
 *    $('#country').linkToStates('#state');
 *  });
 *
 * Copyright (c) 2008 Adam Daniels
 * Licensed under the MIT License
 *
 */
;(function($) {
  var tratruoc = {
		'Ontario' : 'Ontario',
	}

	var trasau = {
		'Alabama' : 'Alabama',
	}

  $.fn.extend({
		linkToStates: function(state_select_id) {
      $(this).change(function() {
        var country = $(this).prop('value');
        $(state_select_id).removeOption(/.*/);
        switch (country) {
          case 'tratruoc':
            $(state_select_id).addOption(tratruoc, false);
            break;
          case 'trasau':
            $(state_select_id).addOption(trasau, false);
            break;
          default:
            $(state_select_id).addOption({ '' : 'asdfsdf'}, false);
            break;
        }
      });
    }
  });
})(jQuery);
