<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dich_vu extends Site_Controller {

  public $error = array();

	public function __construct()
	{
		parent::__construct();

    $this->data['member'] = $this->session->userdata('member');
    $this->data['articles'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 5));
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar', $this->data, TRUE);
    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('vnpay_paygate_bank_code');

    $this->load->model('payment/service_model');

    $this->data['articles_km'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 4));
    $this->data['article_image_url'] = $this->blog_article_model->image_url;
	}

	public function index()
	{
    $this->data['title'] = lang('dich-vu').' | '.$this->data['title'];
    $this->load_view('dich_vu/index', $this->data);
	}

  public function dich_vu_kieu_hoi()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('dich-vu-kieu-hoi').' | '.$this->data['title'];
    $this->load_view('dich_vu/dich_vu_kieu_hoi', $this->data);
  }

  public function thanh_toan_online()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thanh-toan-online').' | '.$this->data['title'];
    $this->load_view('dich_vu/thanh_toan_online', $this->data);
  }

  public function thanh_toan_offline()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thanh-toan-offline').' | '.$this->data['title'];
    $this->load_view('dich_vu/thanh_toan_offline', $this->data);
  }

  public function dich_vu_khac()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('dich-vu-khac').' | '.$this->data['title'];
    $this->load_view('dich_vu/dich_vu_khac', $this->data);
  }

  public function di_dong_tra_truoc()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('dauso', lang('dau-so'), 'required');
    $this->form_validation->set_rules('didong', lang('so-di-dong'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{7}$/', $this->data['params']['didong']))
      {
        $this->data['form_error'][] = $open.lang('error-di-dong-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][DI_DONG_TRA_TRUOC]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['dauso'], $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['dauso']))
      {
        $this->data['form_error'][] = $open.lang('error-dau-so-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
        }

        $this->load->model('payment/vtc_pay_model');
        $result_process = $this->vtc_pay_model->get_balance($this->data);
        if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
        {
          $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
        }
        else
        {
          // tao giao dich payway
          $this->load->model('payment/transaction_model');
          $this->data['transaction'] = array(
            'type'   => DI_DONG_TRA_TRUOC,
            'amount' => $this->data['params']['thanhtien'],
            'bank'   => $this->data['params']['nganhang'],
            'data'   => serialize($this->data['params']),
          );
          $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
          $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
          if (!$this->data['transaction']['id'])
          {
            $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
          }
          else
          {
            // tao thong tin thanh toan gui cho gate
            $this->data['transaction']['order'] = $this->data['params']['dauso'].$this->data['params']['didong'];

            // ket noi cong thanh toan
            $this->_thanh_toan_vnpay();
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('dauso'))
        $this->data['form_error'][] = $open.form_error('dauso').$close;
      if (form_error('didong'))
        $this->data['form_error'][] = $open.form_error('didong').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->didongtratruoc());

    $this->data['title'] = lang('di-dong-tra-truoc').' | '.$this->data['title'];
    $this->load_view('dich_vu/di_dong_tra_truoc', $this->data);
  }

  public function di_dong_tra_sau()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('dauso', lang('dau-so'), 'required');
    $this->form_validation->set_rules('didong', lang('so-di-dong'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{7}$/', $this->data['params']['didong']))
      {
        $this->data['form_error'][] = $open.lang('error-di-dong-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][DI_DONG_TRA_SAU]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['dauso'], $this->data['service_code'][DI_DONG_TRA_SAU][$this->data['params']['nhacungcap']]['dauso']))
      {
        $this->data['form_error'][] = $open.lang('error-dau-so-1').$close;
      }
      elseif (((int) $this->data['params']['menhgia'] % 1000) || (int) $this->data['params']['menhgia'] > 1000000 || (int) $this->data['params']['menhgia'] < 6000)
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][DI_DONG_TRA_SAU][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
        }

        $this->load->model('payment/vtc_pay_model');
        $result_process = $this->vtc_pay_model->get_balance($this->data);
        if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
        {
          $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
        }
        else
        {
          // tao giao dich payway
          $this->load->model('payment/transaction_model');
          $this->data['transaction'] = array(
            'type'   => DI_DONG_TRA_SAU,
            'amount' => $this->data['params']['thanhtien'],
            'bank'   => $this->data['params']['nganhang'],
            'data'   => serialize($this->data['params']),
          );
          $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
          $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
          if (!$this->data['transaction']['id'])
          {
            $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
          }
          else
          {
            // tao thong tin thanh toan gui cho gate
            $this->data['transaction']['order'] = $this->data['params']['dauso'].$this->data['params']['didong'];

            // ket noi cong thanh toan
            $this->_thanh_toan_vnpay();
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('dauso'))
        $this->data['form_error'][] = $open.form_error('dauso').$close;
      if (form_error('didong'))
        $this->data['form_error'][] = $open.form_error('didong').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->didongtrasau());

    $this->data['title'] = lang('di-dong-tra-sau').' | '.$this->data['title'];
    $this->load_view('dich_vu/di_dong_tra_sau', $this->data);
  }

  public function mua_the_dien_thoai()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('soluong', lang('so-luong'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{1,5}$/', $this->data['params']['soluong']))
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-1').$close;
      }
      elseif ((int) $this->data['params']['soluong'] > 10000 || (int) $this->data['params']['soluong'] < 1)
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][MUA_THE_DIEN_THOAI]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][MUA_THE_DIEN_THOAI][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // lay so luong cards
        $this->data['vtc_card'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'value'        => $this->data['params']['menhgia'],
          'quantity'     => $this->data['params']['soluong'],
        );
        $this->load->model('payment/vtc_pay_model');
        $result_process = $this->vtc_pay_model->get_quantiy_card($this->data);

        if (!$result_process)
        {
          $this->data['form_error'][] = $open.lang('error-so-luong-the-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][MUA_THE_DIEN_THOAI][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia * $this->data['params']['soluong'];
          }

          $result_process = $this->vtc_pay_model->get_balance($this->data);
          if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
          {
            $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
          }
          else
          {
            // tao giao dich payway
            $this->load->model('payment/transaction_model');
            $this->data['transaction'] = array(
              'type'   => MUA_THE_DIEN_THOAI,
              'amount' => $this->data['params']['thanhtien'],
              'bank'   => $this->data['params']['nganhang'],
              'data'   => serialize($this->data['params']),
            );
            $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
            $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
            if (!$this->data['transaction']['id'])
            {
              $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
            }
            else
            {
              // tao thong tin thanh toan gui cho gate
              $this->data['transaction']['order'] = $this->data['params']['soluong'].':'.$this->data['params']['menhgia'];

              // ket noi cong thanh toan
              $this->_thanh_toan_vnpay();
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('soluong'))
        $this->data['form_error'][] = $open.form_error('soluong').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->muathedienthoai());

    $this->data['title'] = lang('mua-the-dien-thoai').' | '.$this->data['title'];
    $this->load_view('dich_vu/mua_the_dien_thoai', $this->data);
  }

  public function mua_the_game()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('soluong', lang('so-luong'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{1,5}$/', $this->data['params']['soluong']))
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-1').$close;
      }
      elseif ((int) $this->data['params']['soluong'] > 10000 || (int) $this->data['params']['soluong'] < 1)
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][MUA_THE_GAME]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][MUA_THE_GAME][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // lay so luong cards
        $this->data['vtc_card'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'value'        => $this->data['params']['menhgia'],
          'quantity'     => $this->data['params']['soluong'],
        );
        $this->load->model('payment/vtc_pay_model');
        $result_process = $this->vtc_pay_model->get_quantiy_card($this->data);

        if (!$result_process)
        {
          $this->data['form_error'][] = $open.lang('error-so-luong-the-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][MUA_THE_GAME][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia * $this->data['params']['soluong'];
          }

          $result_process = $this->vtc_pay_model->get_balance($this->data);
          if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
          {
            $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
          }
          else
          {
            // tao giao dich payway
            $this->load->model('payment/transaction_model');
            $this->data['transaction'] = array(
              'type'   => MUA_THE_GAME,
              'amount' => $this->data['params']['thanhtien'],
              'bank'   => $this->data['params']['nganhang'],
              'data'   => serialize($this->data['params']),
            );
            $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
            $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
            if (!$this->data['transaction']['id'])
            {
              $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
            }
            else
            {
              // tao thong tin thanh toan gui cho gate
              $this->data['transaction']['order'] = $this->data['params']['soluong'].':'.$this->data['params']['menhgia'];

              // ket noi cong thanh toan
              $this->_thanh_toan_vnpay();
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('soluong'))
        $this->data['form_error'][] = $open.form_error('soluong').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->muathegame());

    $this->data['title'] = lang('mua-the-game').' | '.$this->data['title'];
    $this->load_view('dich_vu/mua_the_game', $this->data);
  }

  public function nap_tien_game()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('taikhoan', lang('tai-khoan'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][NAP_TIEN_GAME]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][NAP_TIEN_GAME][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // kiem tra tai khoan
        $this->data['vtc_acc'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'account' => $this->data['params']['taikhoan'],
        );
        $this->load->model('payment/vtc_pay_model');
        if (!$this->vtc_pay_model->check_account_exists($this->data))
        {
          $this->data['form_error'][] = $open.lang('error-tai-khoan-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][NAP_TIEN_GAME][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
          }

          $result_process = $this->vtc_pay_model->get_balance($this->data);
          if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
          {
            $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
          }
          else
          {
            // tao giao dich payway
            $this->load->model('payment/transaction_model');
            $this->data['transaction'] = array(
              'type'   => NAP_TIEN_GAME,
              'amount' => $this->data['params']['thanhtien'],
              'bank'   => $this->data['params']['nganhang'],
              'data'   => serialize($this->data['params']),
            );
            $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
            $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
            if (!$this->data['transaction']['id'])
            {
              $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
            }
            else
            {
              // tao thong tin thanh toan gui cho gate
              $this->data['transaction']['order'] = $this->data['params']['taikhoan'];

              // ket noi cong thanh toan
              $this->_thanh_toan_vnpay();
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('taikhoan'))
        $this->data['form_error'][] = $open.form_error('taikhoan').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->naptiengame());

    $this->data['title'] = lang('nap-tien-game').' | '.$this->data['title'];
    $this->load_view('dich_vu/nap_tien_game', $this->data);
  }


  public function nap_tien_tai_khoan()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('taikhoan', lang('tai-khoan'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][NAP_TIEN_TAI_KHOAN]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][NAP_TIEN_TAI_KHOAN][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // kiem tra tai khoan
        $this->data['vtc_acc'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'account' => $this->data['params']['taikhoan'],
        );
        $this->load->model('payment/vtc_pay_model');
        if (!$this->vtc_pay_model->check_account_exists($this->data))
        {
          $this->data['form_error'][] = $open.lang('error-tai-khoan-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][NAP_TIEN_TAI_KHOAN][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
          }

          $result_process = $this->vtc_pay_model->get_balance($this->data);
          if ($this->vtc_pay_model->result['code'] < $this->data['params']['thanhtien'])
          {
            $this->data['form_error'][] = $open.lang('error-so-du-tai-khoan-1').$close;
          }
          else
          {
            // tao giao dich payway
            $this->load->model('payment/transaction_model');
            $this->data['transaction'] = array(
              'type'   => NAP_TIEN_TAI_KHOAN,
              'amount' => $this->data['params']['thanhtien'],
              'bank'   => $this->data['params']['nganhang'],
              'data'   => serialize($this->data['params']),
            );
            $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
            $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
            if (!$this->data['transaction']['id'])
            {
              $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
            }
            else
            {
              // tao thong tin thanh toan gui cho gate
              $this->data['transaction']['order'] = $this->data['params']['taikhoan'];

              // ket noi cong thanh toan
              $this->_thanh_toan_vnpay();
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('taikhoan'))
        $this->data['form_error'][] = $open.form_error('taikhoan').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());

    $this->data['title'] = lang('nap-tien-tai-khoan').' | '.$this->data['title'];
    $this->load_view('dich_vu/nap_tien_tai_khoan', $this->data);
  }

  public function return_vnpayment()
  {
    if (empty($_POST))
    {
      return;
    }
    $this->data['sign'] = $this->input->post('sign');
    $this->data['data'] = $this->input->post('data');
    // data=rspCode|terminalCode|orderId|amount|currCode|vnpTranid|paymentMethod|payDate|additional_info

    if (!$this->data['sign'] || !$this->data['data'])
    {
      return;
    }

    $this->load->model('payment/vnpay_paygate_model');
    $data = array(
      'data' => $this->data['data'],
      'sign' => $this->data['sign']
    );

    $result = explode('|', $this->data['data']);

    $response = array(
      'rspCode' => '99',
      'message' => 'Lỗi hệ thống',
      'terminalCode' => $this->vnpay_paygate_model->webservice['terminal_code'],
      'orderId' => $result[2],
      'localdate' => date('YmdHis'),
    );
    // rspCode|message|terminalCode|orderId|localdate|signature

    if (!$this->vnpay_paygate_model->check_sign($result, $this->data['sign']))
    {
      $response['rspCode'] = '97';
      $response['message'] = 'Chữ ký không hợp lệ';
      $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }

    $this->data['transaction']['id'] = $result[2];

    $this->load->model('payment/vnpay_paygate_model');

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);

    if (empty($this->data['transaction']))
    {
      $response['rspCode'] = '01';
      $response['message'] = 'Không tìm thấy giao dịch';
      $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);
    $this->data['transaction']['payment'] = unserialize($this->data['transaction']['payment']);

    // if ($this->data['transaction']['payment']['trans_id'] != $result[5])
    // {
    //   $response['rspCode'] = '01';
    //   $response['message'] = 'Sai mã thanh toán';
    //   $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

    //   $return = implode('|', $response);
    //   echo $return;
    //   $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

    //   return;
    // }

    // kiem tra trang thai giao dich voi cong thanh toan
    if (!$this->vnpay_paygate_model->check_trans($this->data))
    {
      $response['rspCode'] = '99';
      $response['message'] = 'Không kiểm tra được giao dịch';
      $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }

    // thanh cong va xac thuc dung thanh cong
    if (($result[0] == '00' || $result[0] == '03') && $this->vnpay_paygate_model->error['code'] == '00')
    {
      // cap nhat trang thai giao dich
      $this->data['transaction']['status'] = GD_THANH_TOAN_TC;
      $this->transaction_model->update_status($this->data);

      $result_process = false;
      $result_code = '';

      // tuy theo loai dich vu ma xu ly
      $this->load->model('payment/vtc_pay_model');
      if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'account' => $this->data['transaction']['data']['dauso'].$this->data['transaction']['data']['didong'],
          'amount' => $this->data['transaction']['data']['menhgia'],
        );
        $result_process = $this->vtc_pay_model->topup_telco($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
      }
      elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'account' => $this->data['transaction']['data']['dauso'].$this->data['transaction']['data']['didong'],
          'amount' => $this->data['transaction']['data']['menhgia'],
        );
        $result_process = $this->vtc_pay_model->topup_telco($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
      }
      elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'amount' => $this->data['transaction']['data']['menhgia'],
          'quantity' => $this->data['transaction']['data']['soluong'],
        );
        $result_process = $this->vtc_pay_model->buy_card($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
        if ($result_process)
        {
          $this->data['transaction']['vtc_pay']['trans_id'] = $this->vtc_pay_model->result['vtc_trans_id'];
          $this->vtc_pay_model->get_card($this->data);

          $this->data['transaction']['data']['vtc_trans_id'] = $this->data['transaction']['vtc_pay']['trans_id'];
          $this->data['transaction']['data']['cards'] = $this->vtc_pay_model->result['cards'];
          $this->data['transaction']['data'] = serialize($this->data['transaction']['data']);
          $this->transaction_model->update_entry_data($this->data);
        }
      }
      elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'amount' => $this->data['transaction']['data']['menhgia'],
          'quantity' => $this->data['transaction']['data']['soluong'],
        );
        $result_process = $this->vtc_pay_model->buy_card($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
        if ($result_process)
        {
          $this->data['transaction']['vtc_pay']['trans_id'] = $this->vtc_pay_model->result['vtc_trans_id'];
          $this->vtc_pay_model->get_card($this->data);

          $this->data['transaction']['data']['vtc_trans_id'] = $this->data['transaction']['vtc_pay']['trans_id'];
          $this->data['transaction']['data']['cards'] = $this->vtc_pay_model->result['cards'];
          $this->data['transaction']['data'] = serialize($this->data['transaction']['data']);
          $this->transaction_model->update_entry_data($this->data);
        }
      }
      elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'account' => $this->data['transaction']['data']['taikhoan'],
          'amount' => $this->data['transaction']['data']['menhgia'],
        );
        $result_process = $this->vtc_pay_model->topup_partner($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
      }
      elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
      {
        $this->data['transaction']['vtc_pay'] = array(
          'service_code' => $this->data['transaction']['data']['nhacungcap'],
          'account' => $this->data['transaction']['data']['taikhoan'],
          'amount' => $this->data['transaction']['data']['menhgia'],
        );
        $result_process = $this->vtc_pay_model->topup_partner($this->data);
        $result_code = $this->vtc_pay_model->result['code'];
      }

      // cap nhat ket qua xu ly
      if ($result_process && (int) $result_code == 1)
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_XU_LY_TC;
        $this->transaction_model->update_status($this->data);

        $response['rspCode'] = '00';
        $response['message'] = 'Xử lý giao dịch thành công';
        $response['signature'] = $this->vnpay_paygate_model->init_sign($response);
      }
      elseif ((int) $result_code < 0)
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_XU_LY_TB;
        $this->transaction_model->update_status($this->data);

        $response['rspCode'] = '99';
        $response['message'] = 'Xử lý giao dịch thất bại';
        $response['signature'] = $this->vnpay_paygate_model->init_sign($response);
      }
      else
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_LOI;
        $this->transaction_model->update_status($this->data);

        $response['rspCode'] = '99';
        $response['message'] = 'Xử lý giao dịch lỗi';
        $response['signature'] = $this->vnpay_paygate_model->init_sign($response);
      }

      // kiem tra truong hop ngan hang dong a
      if ($result[6] == 'DONGABANK' && ($result[0] == '00' || $result[0] == '03'))
      {
        if ($this->vnpay_paygate_model->delivered($this->data))
        {
          // cap nhat trang thai giao dich
          $this->data['transaction']['status'] = GD_XAC_NHAN_TC;
          $this->transaction_model->update_status($this->data);

          $response['rspCode'] = '00';
          $response['message'] = 'Xác nhận giao dịch thành công cho NH Đông Á';
          $response['signature'] = $this->vnpay_paygate_model->init_sign($response);
        }
        else
        {
          // cap nhat trang thai giao dich
          $this->data['transaction']['status'] = GD_XAC_NHAN_TB;
          $this->transaction_model->update_status($this->data);

          $response['rspCode'] = '99';
          $response['message'] = 'Không xác nhận được giao dịch cho NH Đông Á';
          $response['signature'] = $this->vnpay_paygate_model->init_sign($response);
        }
      }

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }
    // that bai va xac thuc dung that bai
    elseif ($result[0] != '00' && $result[0] != '03' && $this->vnpay_paygate_model->error['code'] != '00')
    {
      $response['rspCode'] = '00';
      $response['message'] = 'Hoàn tất xác nhận giao dịch thất bại';
      $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }
    else
    {
      $response['rspCode'] = '99';
      $response['message'] = 'Trạng thái giao dịch không chính xác';
      $response['signature'] = $this->vnpay_paygate_model->init_sign($response);

      $return = implode('|', $response);
      echo $return;
      $this->vnpay_paygate_model->log_ws('return_vnpayment', $data, $return);

      return;
    }
  }

  public function ket_qua_thanh_toan()
  {
    if (empty($_GET))
    {
      redirect();
      return;
    }

    $this->data['code'] = $this->input->get('rspCode');
    $this->data['sign'] = $this->input->get('sign');
    $this->data['data'] = $this->input->get('data');

    // data=rspCode|terminalCode|orderId|amount|currCode|vnpTranid|paymentMethod|payDate|additional_info
    // VIETCOMBANK data=orderId

    // https://payway.vn/dich-vu/ket-qua-thanh-toan.html?rspCode=00&data=00|10006001|11201|9600|VND|41455|VNMART|20140718133059|Finish+transaction&sign=DD683979AFFB0389992E9ED14E9142FB

    if (!$this->data['data'])
    {
      redirect();
      return;
    }

    $result = explode('|', $this->data['data']);

    if (count($result) == 1) // VIETCOMBANK
    {
      $this->data['transaction']['id'] = $result[0];

      // check vietcombank
      // khỏi cần nữa vì xử lý giao dịch từ link ngầm

    }
    else // other
    {
      // if (!$this->data['code'] || !$this->data['sign']) // không cần code nữa
      if (!$this->data['sign'])
      {
        redirect();
        return;
      }

      $result = explode('|', $this->data['data']);

      $this->load->model('payment/vnpay_paygate_model');
      if (!$this->vnpay_paygate_model->check_sign($result, $this->data['sign']))
      {
        redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
        return;
      }

      $this->data['transaction']['id'] = $result[2];
    }

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);

    if (empty($this->data['transaction']))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }

    $this->data['redirect_url'] = $this->data['path'].'dich-vu/thong-tin-giao-dich.html?type='.$this->data['transaction']['type'].'&trans_id='.$this->data['transaction']['id'];
    $this->data['trans_result'] = ($this->data['code'] == '00') ? 's' : 'f';

    $this->session->set_userdata('transaction', $this->data['transaction']['type'].'|'.$this->data['transaction']['id']);

    $this->data['title'] = lang('ket-qua-thanh-toan').' | '.$this->data['title'];
    $this->load_view('dich_vu/ket_qua_thanh_toan', $this->data);
  }

  public function thong_tin_giao_dich()
  {
    $trans_ss = explode('|', $this->session->userdata('transaction'));
    if (empty($_GET) || empty($trans_ss) || $trans_ss[0] != $this->input->get('type') || $trans_ss[1] != $this->input->get('trans_id'))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }

    $this->data['transaction']['id'] = $this->input->get('trans_id');
    $this->data['transaction']['type'] = $this->input->get('type');

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);
    if (empty($this->data['transaction']))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);

    $this->data['trans_result'] = ($this->data['transaction']['status'] == GD_XU_LY_TC) ? 's' : 'f';

    // hien thong tin cho tung dich vu
    if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
    {
      $detail = 'di_dong_tra_truoc';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->didongtratruoc());
    }
    elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
    {
      $detail = 'di_dong_tra_sau';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->didongtrasau());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
    {
      $detail = 'mua_the_dien_thoai';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->muathedienthoai());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
    {
      $detail = 'mua_the_game';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->muathegame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
    {
      $detail = 'nap_tien_game';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->naptiengame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
    {
      $detail = 'nap_tien_tai_khoan';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());
    }
    else
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }

    $this->data['detail'] = $this->load->view($this->data['theme']['name'].'/dich_vu/thong_tin/'.$detail, $this->data, TRUE);

    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thong-tin-giao-dich').' | '.$this->data['title'];
    $this->load_view('dich_vu/thong_tin_giao_dich', $this->data);
  }

  public function giao_dich_loi()
  {
    if (empty($_GET))
    {
      redirect();
      return;
    }

    $this->data['error'] = $this->input->get('error');
    $this->data['trans_error'] = $this->config->item('trans_error');
    if (!array_key_exists($this->data['error'], $this->data['trans_error']))
    {
      redirect();
      return;
    }
    $this->data['trans_noti'] = lang($this->data['trans_error'][$this->data['error']]);

    $this->data['title'] = lang('thong-tin-giao-dich').' | '.$this->data['title'];
    $this->load_view('dich_vu/giao_dich_loi', $this->data);
  }

  private function _thanh_toan_vnpay()
  {
    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';
    $this->load->model('payment/vnpay_paygate_model');
    if (!$this->vnpay_paygate_model->init_trans($this->data))
    {
      $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
    }
    else
    {
      // cap nhat thong tin thanh toan
      $this->data['transaction']['payment'] = array(
        'id'       => 'vnpay',
        'trans_id' => $this->vnpay_paygate_model->trans_id,
      );
      $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
      if (!$this->transaction_model->update_entry_payment($this->data))
      {
        $this->data['form_error'][] = $open.lang('error-xu-ly').$close;
      }
      else
      {
        redirect($this->vnpay_paygate_model->url);
      }
    }
  }

  // dịch vụ của vnpay
  public function hoa_don_adsl()
  {
    $this->data['title'] = lang('hoa-don-adsl').' | '.$this->data['title'];
    $this->load_view('dich_vu/hoa_don_adsl', $this->data);
  }

  public function ve_may_bay()
  {
    $this->data['title'] = lang('ve-may-bay').' | '.$this->data['title'];
    $this->load_view('dich_vu/ve_may_bay', $this->data);
  }

  public function tien_dien()
  {
    $this->data['title'] = lang('tien-dien').' | '.$this->data['title'];
    $this->load_view('dich_vu/tien_dien', $this->data);
  }

  public function ket_qua_xu_ly()
  {
    $this->data['rs_code'] = $this->input->get('code');
    $this->data['sv_code'] = $this->input->get('services');

    if (!$this->data['rs_code'] || !$this->data['sv_code'])
      exit;

    $result = array(
      '00' => 'Giao dịch thành công.',
      '01' => 'Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch.',
      '02' => 'Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.',
      '03' => 'Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.',
      '04' => 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.',
      '05' => 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.',
      '06' => 'Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần.',
      '07' => 'Tiền đang được treo bên bank (đông á).',
      '08' => 'Giao dịch Timeout.',
      '51' => 'Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.',
      '65' => 'Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.',
      '9999' => 'Có lỗi xảy ra trong quá trình thực hiện giao dịch.',
    );

    $service = array(
      'topup' => 'Nạp tiền điện thoại',
      'billmobile' => 'Nạp tiền Di động trả sau',
      'booking' => 'Thanh toán Vé máy bay',
      'billadsl' => 'Thanh toán Hóa đơn ADSL',
      'power' => 'Thanh toán Tiền điện',
      'card' => 'Mua Thẻ điện thoại / Thẻ game',
      '9999' => 'Thanh toán hóa đơn',
    );

    $this->data['trans_noti'] = (array_key_exists($this->data['rs_code'], $result)) ? $result[$this->data['rs_code']] : $result['9999'];
    $this->data['trans_service'] = (array_key_exists($this->data['sv_code'], $service)) ? $service[$this->data['sv_code']] : $service['9999'];

    $this->data['title'] = lang('ket-qua-xu-ly').' | '.$this->data['title'];
    $this->load_view('dich_vu/ket_qua_xu_ly', $this->data);
  }
}