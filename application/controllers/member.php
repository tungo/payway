<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends Site_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('TN_Auth');
    $this->check_login();

    $this->data['gender'] = array(lang('khac'), lang('nam'), lang('nu'));
    
    $this->data['articles_km'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 2));
  }

  public function index()
  {
    $this->load->model('database/user_account_model');
    $this->data['nations'] = $this->user_account_model->get_nation($this->data);
    $this->data['nations'][] = lang('khong');
    $this->data['questions'] = $this->user_account_model->get_security_question($this->data);
    $this->data['questions'][0] = lang('khong');

    $this->data['account'] = $this->user_account_model->get_entry_member(array('email' => $this->data['member']['email']));

    $this->load->model('payment/transaction_model');
    $this->data['transactions'] = $this->transaction_model->get_entries_member($this->data);

    $this->data['title'] = lang('thong-tin-tai-khoan').' | '.$this->data['title'];
    $this->load_view('member/index', $this->data);
  }

  public function history()
  {
    $this->load->library('TN_Pagination');
    $this->tn_pagination->initialize(array('layout' => 'trans'));

    $this->load->model('payment/transaction_model');
    // filter
    $this->data['filter'] = array();
    if (! $this->session->userdata($this->data['namespace']))
    {
      $this->data['filter'] = array(
        'keyword'   => '',
        'sort'      => 'id',
        'order'     => 'DESC',
        'page'      => 1,
        'per_page'  => $this->tn_pagination->per_page,
        'type'      => '',
        'status'    => '',
        'from_date' => '',
        'to_date'   => '',
      );
      $this->session->set_userdata($this->data['namespace'], $this->data['filter']);
    }
    else
    {
      $this->data['filter'] = $this->session->userdata($this->data['namespace']);
      if (isset($this->data['request']['trang']))
        $this->data['filter']['page'] = (int) $this->data['request']['trang'];
      if (isset($this->data['params']['keyword']))
        $this->data['filter']['keyword'] = $this->data['params']['keyword'];
      if (isset($this->data['params']['type']))
        $this->data['filter']['type'] = $this->data['params']['type'];
      if (isset($this->data['params']['status']))
        $this->data['filter']['status'] = $this->data['params']['status'];
      if (isset($this->data['params']['from_date']))
        $this->data['filter']['from_date'] = $this->data['params']['from_date'];
      if (isset($this->data['params']['to_date']))
        $this->data['filter']['to_date'] = $this->data['params']['to_date'];
      $this->session->set_userdata($this->data['namespace'], $this->data['filter']);
    }

    // pagination
    $this->tn_pagination->cur_page   = $this->data['filter']['page'];
    $this->tn_pagination->base_url   = base_url('member/history/');
    $this->tn_pagination->per_page   = $this->data['filter']['per_page'];
    $this->tn_pagination->total_rows = $this->transaction_model->count_entries_history($this->data);
    $this->tn_pagination->create_links();

    $this->data['data']['pagination'] = $this->tn_pagination;

    // get list
    $this->data['items'] = $this->transaction_model->get_entries_history($this->data);

    $this->data['service_code'] = array_keys($this->config->item('vtc_pay_service_code'));
    $this->data['select_type'] = array();
    foreach ($this->data['service_code'] as $val)
      $this->data['select_type'][$val] = lang($val);

    $this->data['trans_status'] = $this->config->item('trans_member_status');
    $this->data['select_status'] = array();
    foreach ($this->data['trans_status'] as $val)
      $this->data['select_status'][$val] = lang('trans_member_st'.$val);

    $this->data['title'] = lang('lich-su-giao-dich').' | '.$this->data['title'];
    $this->load_view('member/history', $this->data);
  }

  public function transaction()
  {
    $this->data['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_history_detail($this->data);
    if ( ! $this->data['transaction']['id'])
    {
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);
    $this->data['transaction']['payment'] = unserialize($this->data['transaction']['payment']);

    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('vnpay_paygate_bank_code');

    $this->load->model('payment/service_model');

    // hien thong tin cho tung dich vu
    if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
    {
      $detail = 'di_dong_tra_truoc';
      $this->data = array_merge($this->data, $this->service_model->didongtratruoc());
    }
    elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
    {
      $detail = 'di_dong_tra_sau';
      $this->data = array_merge($this->data, $this->service_model->didongtrasau());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
    {
      $detail = 'mua_the_dien_thoai';
      $this->data = array_merge($this->data, $this->service_model->muathedienthoai());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
    {
      $detail = 'mua_the_game';
      $this->data = array_merge($this->data, $this->service_model->muathegame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
    {
      $detail = 'nap_tien_game';
      $this->data = array_merge($this->data, $this->service_model->naptiengame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
    {
      $detail = 'nap_tien_tai_khoan';
      $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());
    }
    else
    {
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    $this->data['detail'] = $this->load->view($this->data['theme']['name'].'/member/thong_tin/'.$detail, $this->data, TRUE);

    $this->data['title'] = lang('chi-tiet-giao-dich').' | '.$this->data['title'];
    $this->load_view('member/transaction', $this->data);
  }

  public function edit($type = '')
  {
    if ($type == 'account')
      $this->_edit_account();
    else if ($type == 'password')
      $this->_edit_password();
    else
      redirect($this->data['path']);
  }

  private function _edit_account()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('ten', lang('ten'), 'required');
    $this->form_validation->set_rules('ho', lang('ho'), 'required');
    $this->form_validation->set_rules('gioitinh', lang('gioi-tinh'), 'required');
    $this->form_validation->set_rules('quocgia', lang('quoc-gia'), 'required');
    $this->form_validation->set_rules('diachi', lang('dia-chi'), 'required');
    $this->form_validation->set_rules('thongtinthem', lang('thong-tin-them'), '');
    $this->form_validation->set_rules('thanhpho', lang('thanh-pho'), 'required');
    $this->form_validation->set_rules('postcode', lang('post-code'), 'required');
    $this->form_validation->set_rules('ngaysinh', lang('date-ngay'), 'required');
    $this->form_validation->set_rules('thangsinh', lang('date-thang'), 'required');
    $this->form_validation->set_rules('namsinh', lang('date-nam'), 'required');
    $this->form_validation->set_rules('cauhoi', lang('cau-hoi-bao-mat'), '');
    $this->form_validation->set_rules('traloi', lang('cau-tra-loi'), '');
    $this->form_validation->set_rules('chungminhthu', lang('chung-minh-thu'), '');
    $this->form_validation->set_rules('dienthoai', lang('so-dien-thoai'), 'numeric');

    $this->load->model('database/user_account_model');
    $this->data['nations'] = $this->user_account_model->get_nation($this->data);
    $this->data['questions'] = $this->user_account_model->get_security_question($this->data);
    $this->data['account'] = $this->user_account_model->get_entry_member(array('email' => $this->data['member']['email']));

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];

      if (!checkdate($this->data['params']['thangsinh'], $this->data['params']['ngaysinh'], $this->data['params']['namsinh']))
      {
        $this->data['form_error'][] = $open.lang('ngay-sinh-khong-hop-le').$close;
      }
      else
      {
        if ($this->user_account_model->update_entry_member($this->data))
        {
          $this->tn_message->set_success('<p><i class="fa-check"></i>'.lang('sua-thong-tin-thanh-cong').'<a href="#" class="notification-close notification-close-success"><i class="fa-times"></i></a></p>');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error($open.lang('sua-thong-tin-that-bai').$close);
          $this->tn_message->save();
        }
        redirect($this->data['path'].'member/');
        return;
      }
    }
    else
    {
      if (form_error('email'))
        $this->data['form_error'][] = $open.form_error('email').$close;
      if (form_error('ten'))
        $this->data['form_error'][] = $open.form_error('ten').$close;
      if (form_error('ho'))
        $this->data['form_error'][] = $open.form_error('ho').$close;
      if (form_error('gioitinh'))
        $this->data['form_error'][] = $open.form_error('gioitinh').$close;
      if (form_error('ngaysinh'))
        $this->data['form_error'][] = $open.form_error('ngaysinh').$close;
      if (form_error('thangsinh'))
        $this->data['form_error'][] = $open.form_error('thangsinh').$close;
      if (form_error('namsinh'))
        $this->data['form_error'][] = $open.form_error('namsinh').$close;
      if (form_error('quocgia'))
        $this->data['form_error'][] = $open.form_error('quocgia').$close;
      if (form_error('diachi'))
        $this->data['form_error'][] = $open.form_error('diachi').$close;
      if (form_error('thongtinthem'))
        $this->data['form_error'][] = $open.form_error('thongtinthem').$close;
      if (form_error('thanhpho'))
        $this->data['form_error'][] = $open.form_error('thanhpho').$close;
      if (form_error('postcode'))
        $this->data['form_error'][] = $open.form_error('postcode').$close;
      if (form_error('cauhoi'))
        $this->data['form_error'][] = $open.form_error('cauhoi').$close;
      if (form_error('traloi'))
        $this->data['form_error'][] = $open.form_error('traloi').$close;
      if (form_error('chungminhthu'))
        $this->data['form_error'][] = $open.form_error('chungminhthu').$close;
      if (form_error('dienthoai'))
        $this->data['form_error'][] = $open.form_error('dienthoai').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('sua-thong-tin-tai-khoan').' | '.$this->data['title'];
    $this->load_view('member/edit_account', $this->data);
  }

  private function _edit_password()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('oldpass', lang('mat-khau-cu'), 'required');
    $this->form_validation->set_rules('newpass', lang('mat-khau-moi'), 'required|min_length[8]|matches[confirm]');
    $this->form_validation->set_rules('confirm', lang('xac-nhan-mat-khau'), 'required');

    $this->load->model('database/user_account_model');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];

      if (!$this->user_account_model->check_password($this->data))
      {
        $this->data['form_error'][] = $open.lang('sai-mat-khau-cu').$close;
      }
      else
      {
        if ($this->user_account_model->change_password($this->data))
        {
          $member = $this->user_account_model->get_entry_member(array('email' => $this->data['member']['email']));
          $this->data['member']['salt'] = $member['salt'];
          $this->session->set_userdata('member', $this->data['member']);

          $this->tn_message->set_success('<p><i class="fa-check"></i>'.lang('doi-mat-khau-thanh-cong').'<a href="#" class="notification-close notification-close-success"><i class="fa-times"></i></a></p>');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error($open.lang('doi-mat-khau-that-bai').$close);
          $this->tn_message->save();
        }
        redirect($this->data['path'].'member/');
        return;
      }
    }
    else
    {
      if (form_error('oldpass'))
        $this->data['form_error'][] = $open.form_error('oldpass').$close;
      if (form_error('newpass'))
        $this->data['form_error'][] = $open.form_error('newpass').$close;
      if (form_error('confirm'))
        $this->data['form_error'][] = $open.form_error('confirm').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('doi-mat-khau').' | '.$this->data['title'];
    $this->load_view('member/edit_password', $this->data);
  }

  public function login()
  {
    // if logged in
    if ($this->tn_auth->is_logged_in())
      redirect($this->data['path'].'member');

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
    $this->form_validation->set_rules('password', lang('password'), 'required');

    if ($this->form_validation->run() == TRUE)
    {
      // logged in successfully
      if ($this->tn_auth->login($this->data))
      {
        redirect($this->data['path']);
      }
      else
      {
        $this->data['form_error'][] = '<p><i class="icon_error-triangle_alt"></i>'.lang('sai-thong-tin-dang-nhap').'<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';
      }
    }
    else
    {
      if (form_error('email'))
        $this->data['form_error'][] = '<p><i class="icon_error-triangle_alt"></i>'.form_error('email').'<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';
      if (form_error('password'))
        $this->data['form_error'][] = '<p><i class="icon_error-triangle_alt"></i>'.form_error('password').'<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('dang-nhap').' | '.$this->data['title'];
    $this->load_view('member/login', $this->data);
  }

  // Log out page
  public function logout()
  {
    // $this->tn_auth->logout();

    $this->session->unset_userdata('member');
    $this->tn_message->set_success('Signed out successfully.');
    // $this->tn_message->save();

    redirect($this->data['path']);
  }

  public function register()
  {
    // if logged in
    if ($this->tn_auth->is_logged_in())
      redirect($this->data['path'].'member');

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
    $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|matches[confirm]');
    $this->form_validation->set_rules('confirm', lang('nhap-lai-mat-khau'), 'required');
    $this->form_validation->set_rules('ten', lang('ten'), 'required');
    $this->form_validation->set_rules('ho', lang('ho'), 'required');
    $this->form_validation->set_rules('gioitinh', lang('gioi-tinh'), 'required');
    $this->form_validation->set_rules('ngaysinh', lang('date-ngay'), 'required');
    $this->form_validation->set_rules('thangsinh', lang('date-thang'), 'required');
    $this->form_validation->set_rules('namsinh', lang('date-nam'), 'required');
    $this->form_validation->set_rules('quocgia', lang('quoc-gia'), 'required');
    $this->form_validation->set_rules('diachi', lang('dia-chi'), 'required');
    $this->form_validation->set_rules('thongtinthem', lang('thong-tin-them'), '');
    $this->form_validation->set_rules('thanhpho', lang('thanh-pho'), 'required');
    $this->form_validation->set_rules('postcode', lang('post-code'), 'required');
    $this->form_validation->set_rules('cauhoi', lang('cau-hoi-bao-mat'), 'required');
    $this->form_validation->set_rules('traloi', lang('cau-tra-loi'), 'required');
    $this->form_validation->set_rules('chungminhthu', lang('chung-minh-thu'), 'required');
    $this->form_validation->set_rules('dienthoai', lang('so-dien-thoai'), 'required|numeric');

    $this->load->library('Recaptcha');
    $this->data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html('', true);

    $this->load->model('database/user_account_model');
    $this->data['nations'] = $this->user_account_model->get_nation($this->data);
    $this->data['questions'] = $this->user_account_model->get_security_question($this->data);

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      $this->recaptcha->recaptcha_check_answer();

      if (!checkdate($this->data['params']['thangsinh'], $this->data['params']['ngaysinh'], $this->data['params']['namsinh']))
      {
        $this->data['form_error'][] = $open.lang('ngay-sinh-khong-hop-le').$close;
      }
      elseif (!$this->recaptcha->getIsValid())
      {
        $this->data['form_error'][] = $open.lang('sai-ma-xac-nhan').$close;
      }
      elseif ($this->user_account_model->check_email_register($this->data['params']))
      {
        $this->data['form_error'][] = $open.lang('error-email-da-su-dung').$close;
      }
      else
      {
        if ($this->user_account_model->register($this->data['params']))
        {
          $this->tn_message->set_success('<p><i class="fa-check"></i>'.lang('dang-ky-thanh-cong').'<a href="#" class="notification-close notification-close-success"><i class="fa-times"></i></a></p>');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error($open.lang('dang-ky-that-bai').$close);
          $this->tn_message->save();
        }
        redirect($this->data['path'].'member/login');
        return;
      }
    }
    else
    {
      if (form_error('email'))
        $this->data['form_error'][] = $open.form_error('email').$close;
      if (form_error('password'))
        $this->data['form_error'][] = $open.form_error('password').$close;
      if (form_error('confirm'))
        $this->data['form_error'][] = $open.form_error('confirm').$close;
      if (form_error('ten'))
        $this->data['form_error'][] = $open.form_error('ten').$close;
      if (form_error('ho'))
        $this->data['form_error'][] = $open.form_error('ho').$close;
      if (form_error('gioitinh'))
        $this->data['form_error'][] = $open.form_error('gioitinh').$close;
      if (form_error('ngaysinh'))
        $this->data['form_error'][] = $open.form_error('ngaysinh').$close;
      if (form_error('thangsinh'))
        $this->data['form_error'][] = $open.form_error('thangsinh').$close;
      if (form_error('namsinh'))
        $this->data['form_error'][] = $open.form_error('namsinh').$close;
      if (form_error('quocgia'))
        $this->data['form_error'][] = $open.form_error('quocgia').$close;
      if (form_error('diachi'))
        $this->data['form_error'][] = $open.form_error('diachi').$close;
      if (form_error('thongtinthem'))
        $this->data['form_error'][] = $open.form_error('thongtinthem').$close;
      if (form_error('thanhpho'))
        $this->data['form_error'][] = $open.form_error('thanhpho').$close;
      if (form_error('postcode'))
        $this->data['form_error'][] = $open.form_error('postcode').$close;
      if (form_error('cauhoi'))
        $this->data['form_error'][] = $open.form_error('cauhoi').$close;
      if (form_error('traloi'))
        $this->data['form_error'][] = $open.form_error('traloi').$close;
      if (form_error('chungminhthu'))
        $this->data['form_error'][] = $open.form_error('chungminhthu').$close;
      if (form_error('dienthoai'))
        $this->data['form_error'][] = $open.form_error('dienthoai').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('dang-ky').' | '.$this->data['title'];
    $this->load_view('member/register', $this->data);
  }

  public function reset_password()
  {
    // if logged in
    if ($this->tn_auth->is_logged_in())
      redirect($this->data['path'].'member');

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');

    $this->load->library('Recaptcha');
    $this->data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html('', true);

    $this->load->model('database/user_account_model');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      $this->recaptcha->recaptcha_check_answer();

      if (!$this->recaptcha->getIsValid())
      {
        $this->data['form_error'][] = $open.lang('sai-ma-xac-nhan').$close;
      }
      elseif (!$this->user_account_model->check_email_register($this->data['params']))
      {
        $this->data['form_error'][] = $open.lang('email-chua-duoc-dang-ky').$close;
      }
      else
      {
        $member = $this->user_account_model->get_entry_member(array('email' => $this->data['params']['email']));

        if (empty($member['salt']))
        {
          $this->tn_message->set_error($open.lang('lay-lai-mat-khau-that-bai').$close);
          $this->tn_message->save();
          redirect($this->data['path'].'member/login');
          return;
        }

        $link = base_url($this->data['path'].'member/renew_password/?email='.$this->data['params']['email'].'&key='.$member['salt']);

        $message = lang('email-tao-moi-mat-khau');
        $message = str_replace('[email]', $this->data['params']['email'], $message);
        $message = str_replace('[link]', $link, $message);

        $this->data['mail_to'] = $this->data['params']['email'];
        $this->data['mail_msg']['subject'] = lang('tieu-de-email-tao-moi-mat-khau');
        $this->data['mail_msg']['message'] = $message;

        $this->load->model('system/mail_model');

        if ($this->mail_model->send($this->data))
        {
          $this->tn_message->set_success('<p><i class="fa-check"></i>'.lang('lay-lai-mat-khau-thanh-cong').'<a href="#" class="notification-close notification-close-success"><i class="fa-times"></i></a></p>');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error($open.lang('lay-lai-mat-khau-that-bai').$close);
          $this->tn_message->save();
        }
        redirect($this->data['path'].'member/login');
        return;
      }
    }
    else
    {
      if (form_error('email'))
        $this->data['form_error'][] = $open.form_error('email').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('lay-lai-mat-khau').' | '.$this->data['title'];
    $this->load_view('member/reset_password', $this->data);
  }

  public function renew_password()
  {
    // if logged in
    if ($this->tn_auth->is_logged_in())
      redirect($this->data['path'].'member');

    $this->data['params']['email'] = $this->input->get('email');
    $this->data['params']['key'] = $this->input->get('key');

    if (empty($this->data['params']['email']) || empty($this->data['params']['key']))
    {
      redirect($this->data['path']);
      return;
    }

    $this->load->model('database/user_account_model');
    $member = $this->user_account_model->get_entry_member(array('email' => $this->data['params']['email'], 'salt' => $this->data['params']['key']));

    if (empty($member))
    {
      redirect($this->data['path']);
      return;
    }

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('newpass', lang('password'), 'required|min_length[8]|matches[confirm]');
    $this->form_validation->set_rules('confirm', lang('nhap-lai-mat-khau'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      if ($this->user_account_model->renew_password($this->data))
      {
        $this->tn_message->set_success('<p><i class="fa-check"></i>'.lang('doi-mat-khau-thanh-cong').'<a href="#" class="notification-close notification-close-success"><i class="fa-times"></i></a></p>');
        $this->tn_message->save();
      }
      else
      {
        $this->tn_message->set_error($open.lang('doi-mat-khau-that-bai').$close);
        $this->tn_message->save();
      }
      redirect($this->data['path'].'member/login');
      return;
    }
    else
    {
      if (form_error('newpass'))
        $this->data['form_error'][] = $open.form_error('newpass').$close;
      if (form_error('confirm'))
        $this->data['form_error'][] = $open.form_error('confirm').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data['title'] = lang('tao-moi-mat-khau').' | '.$this->data['title'];
    $this->load_view('member/renew_password', $this->data);
  }

  public function check_login()
  {
    // redirect if not login
    if (!$this->tn_auth->is_logged_in())
    {
      // exclude login page
      if ($this->data['method'] != 'login' &&
        $this->data['method'] != 'register' &&
        $this->data['method'] != 'reset_password' &&
        $this->data['method'] != 'renew_password')
      {
        redirect($this->data['path'].'member/login');
      }
      else
      {

      }
    }
    else
    {
      if ($this->tn_auth->check_login())
        $this->data['member'] = $this->session->userdata('member');
      else
        redirect($this->data['path'].'member/login');
    }
  }
}