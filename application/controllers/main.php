<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Site_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->data['member'] = $this->session->userdata('member');

    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('vnpay_paygate_bank_code');

    $this->data['articles_km'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 2));
  }

  public function index()
  {
    $this->data['article_image_url'] = $this->blog_article_model->image_url;

    $this->data['articles'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 2));

    $this->load->model('database/module_partner_model');
    $this->data['partner_image_url'] = $this->module_partner_model->image_url;
    $this->data['partners'] = $this->module_partner_model->get_entries_site_home($this->data['params']);

    if ($this->data['version'] != 'mobile')
    {
      // dich vu
      $this->load->model('payment/service_model');
      $this->data['didongtratruoc'] = $this->service_model->didongtratruoc();
      $this->data['didongtrasau'] = $this->service_model->didongtrasau();
      $this->data['muathedienthoai'] = $this->service_model->muathedienthoai();
      $this->data['muathegame'] = $this->service_model->muathegame();
      $this->data['naptiengame'] = $this->service_model->naptiengame();
      $this->data['naptientaikhoan'] = $this->service_model->naptientaikhoan();

      // view cua dich vu
      $this->data['view_didongtratruoc'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/di_dong_tra_truoc', $this->data, TRUE);
      $this->data['view_didongtrasau'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/di_dong_tra_sau', $this->data, TRUE);
      $this->data['view_muathedienthoai'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/mua_the_dien_thoai', $this->data, TRUE);
      $this->data['view_muathegame'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/mua_the_game', $this->data, TRUE);
      $this->data['view_naptiengame'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/nap_tien_game', $this->data, TRUE);
      $this->data['view_naptientaikhoan'] = $this->load->view($this->data['theme']['name'].'/main/dich_vu/nap_tien_tai_khoan', $this->data, TRUE);
    }

    $this->data['title'] = lang('trang-chu').' | '.$this->data['title'];
    $this->load_view('main/index', $this->data);
  }

  public function language()
  {
    debug($this->data['language_session']);
    $this->session->set_userdata($this->data['language_session'], $this->data['request']['id']);
    redirect($this->input->server('HTTP_REFERER'));
  }

  public function contact()
  {
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
      if (! isset($this->data['params']['senderName']) || ! $this->data['params']['senderName']
       || ! isset($this->data['params']['senderPhone']) || ! $this->data['params']['senderPhone']
       || ! isset($this->data['params']['senderEmail']) || ! $this->data['params']['senderEmail']
       || ! isset($this->data['params']['message']) || ! $this->data['params']['message'])
      {
        echo '0';
        exit;
      }

      $this->data['params']['name'] = $this->data['params']['senderName'];
      $this->data['params']['phone'] = $this->data['params']['senderPhone'];
      $this->data['params']['email'] = $this->data['params']['senderEmail'];
      $this->data['params']['message'] = $this->data['params']['message'];

      $this->load->model('database/website_contact_model');
      if ($this->website_contact_model->add_entry($this->data['params']))
        echo '1';
      else
        echo '0';
      exit;
    }

    $this->data['title'] = lang('lien-he').' | '.$this->data['title'];
    $this->load_view('main/contact', $this->data);
  }

  public function category()
  {
    $url = $this->uri->segment(1);
    if (! $url)
    {
      $this->error_404();
      return;
    }

    $this->data['params']['seo_name'] = $url;
    $id = $this->blog_category_model->get_id_from_seo($this->data['params']);

    if (! $id || $id == 1001)
    {
      $this->error_404();
      return;
    }

    if (! isset($this->data['category_list'][$id]))
    {
      $this->error_404();
      return;
    }

    $this->data['params']['category_id'] = $id;
    if ($this->uri->segment(2) && $this->uri->segment(2) != 'trang')
    {
      $this->article();
      return;
    }
    $this->data['category'] = $this->data['category_list'][$id];

    $this->data['article_image_url'] = $this->blog_article_model->image_url;

    $this->data['request'] = $this->uri->uri_to_assoc(2);
    $this->data['request']['trang'] = isset($this->data['request']['trang']) ? (int) $this->data['request']['trang'] : 1;

    $this->load->library('TN_Pagination');
    $this->tn_pagination->initialize(array('layout' => 'site'));
    // pagination
    $this->tn_pagination->cur_page   = $this->data['request']['trang'];
    $this->tn_pagination->base_url   = base_url($this->uri->segment(1));
    $this->tn_pagination->total_rows = $this->blog_article_model->count_entries_site_category($this->data['params']);
    $this->tn_pagination->create_links();

    $this->data['params']['pagination'] = $this->tn_pagination;

    $this->data['articles'] = $this->blog_article_model->get_entries_site_category($this->data['params']);

    $this->data['category_news'] = $this->blog_category_model->get_entries_site_news($this->data['params']);
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/main/sidebar_news', $this->data, TRUE);

    $this->data['title'] = $this->data['category']['name'].' | '.$this->data['title'];
    $this->load_view('main/category', $this->data);
  }

  public function article()
  {
    if (! $this->uri->segment(1) || ! $this->uri->segment(2))
    {
      $this->error_404();
      return;
    }

    $this->data['params']['category_seo_name'] = $this->uri->segment(1);
    $this->data['params']['seo_name'] = $this->uri->segment(2);
    $id = $this->blog_article_model->get_id_from_seo($this->data['params']);

    if (! $id)
    {
      $this->error_404();
      return;
    }
    $this->data['params']['id'] = $id;

    $this->data['article_image_url'] = $this->blog_article_model->image_url;
    $this->data['article'] = $this->blog_article_model->get_entries_site_article($this->data['params']);

    if (empty($this->data['article']))
    {
      $this->error_404();
      return;
    }

    $this->data['params']['category_id'] = $this->data['article']['category_id'];
    $this->data['category'] = $this->data['category_list'][$this->data['params']['category_id']];

    $this->data['params']['created_time'] = $this->data['article']['created_time'];
    $this->data['article_more'] = $this->blog_article_model->get_entries_site_article_more($this->data['params']);

    $this->data['category_news'] = $this->blog_category_model->get_entries_site_news($this->data['params']);
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/main/sidebar_news', $this->data, TRUE);

    if ($this->data['article']['category_id'] != 1001)
      $this->data['title'] = $this->data['category']['name'].' | '.$this->data['title'];

    $this->data['title'] = $this->data['article']['name'].' | '.$this->data['title'];
    $this->load_view('main/article', $this->data);
  }

  public function tim_kiem()
  {
    if (empty($_GET['keyword']))
    {
      redirect(base_url(''));
      return;
    }
    $this->data['params']['keyword'] = $_GET['keyword'];

    $this->data['article_image_url'] = $this->blog_article_model->image_url;

    $this->data['trang'] = isset($_GET['trang']) ? (int) $_GET['trang'] : 1;

    $this->load->library('TN_Pagination');
    $this->tn_pagination->initialize(array('layout' => 'site_search'));

    // pagination
    $this->tn_pagination->cur_page   = $this->data['trang'];
    $this->tn_pagination->base_url   = site_url($this->uri->segment(1)).'?keyword='.$_GET['keyword'];
    $this->tn_pagination->total_rows = $this->blog_article_model->count_entries_site_search($this->data['params']);
    $this->tn_pagination->create_links();

    $this->data['params']['pagination'] = $this->tn_pagination;

    $this->data['articles'] = $this->blog_article_model->get_entries_site_search($this->data['params']);

    $this->data['category_news'] = $this->blog_category_model->get_entries_site_news($this->data['params']);
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/main/sidebar_news', $this->data, TRUE);

    $this->data['title'] = lang('ket-qua-tim-kiem').' | '.$this->data['title'];
    $this->load_view('main/search', $this->data);
  }

  public function partner()
  {
    $this->data['title'] = lang('doi-tac-dau-tu').' | '.$this->data['title'];
    $this->load_view('main/partner', $this->data);
  }

  public function gioi_thieu()
  {
    $this->data['title'] = lang('gioi-thieu').' | '.$this->data['title'];
    $this->load_view('main/gioi_thieu', $this->data);
  }

  public function ho_tro()
  {
    $this->data['title'] = lang('ho-tro').' | '.$this->data['title'];
    $this->load_view('main/ho_tro', $this->data);
  }

  public function chi_ho_vnpost()
  {
    if (!$_GET)
      return;

    $tinh = (isset($_GET['tinh'])) ? (int) $_GET['tinh'] : 0;
    $ma = (isset($_GET['ma'])) ? (int) $_GET['ma'] : 0;

    if ($ma)
    {

      $this->load->library('PHPExcel');

      $objPHPExcel = PHPExcel_IOFactory::load(FILESPATH.'chi_ho_vnpost.xls');
      $rows = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      $output = array();
      foreach ($rows as $row)
      {
        if ($ma != $row['C'])
          continue;
        $output = array(
          'tinh' => $row['A'],
          'stt' => $row['B'],
          'ma' => $row['C'],
          'ten' => $row['D'],
          'cap' => $row['E'],
          'diachi' => $row['F'],
          'dienthoai' => $row['G'],
          'fax' => $row['H'],
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
    elseif ($tinh)
    {
      $this->load->library('PHPExcel');

      $objPHPExcel = PHPExcel_IOFactory::load(FILESPATH.'chi_ho_vnpost.xls');
      $rows = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      array_shift($rows);
      $danhsach = array();
      $output = array();
      $i = 0;
      foreach ($rows as $row)
      {
        if (in_array($row['A'], $danhsach))
        {
          if ($i == $tinh)
          {
            $output[] = array(
              'tinh' => $row['A'],
              'stt' => $row['B'],
              'ma' => $row['C'],
              'ten' => $row['D'],
              'cap' => $row['E'],
              'diachi' => $row['F'],
              'dienthoai' => $row['G'],
              'fax' => $row['H'],
            );
          }
        }
        else
        {
          $i++;
          if ($i > $tinh)
            break;
          $danhsach[$i] = $row['A'];
          if ($i == $tinh)
          {
            $output[] = array(
              'tinh' => $row['A'],
              'stt' => $row['B'],
              'ma' => $row['C'],
              'ten' => $row['D'],
              'cap' => $row['E'],
              'diachi' => $row['F'],
              'dienthoai' => $row['G'],
              'fax' => $row['H'],
            );
          }
        }
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
  }

  public function newsletter()
  {
    if (!$_POST)
    {
      redirect(base_url());
      return;
    }

    $this->load->library('form_validation');
    $this->form_validation->set_rules('nl_email', '', 'required|valid_email');

    if ($this->form_validation->run() != TRUE)
    {
      redirect(base_url());
      return;
    }

    $email = trim($this->data['params']['nl_email']);
    $filename = FCPATH.'newsletter68.txt';

    $fcontent = file_get_contents($filename);
    $fcontent = str_replace("\r\n", "\n", $fcontent);
    $result = explode("\n", $fcontent);

    if (!in_array($email, $result))
    {
      $fhandle = fopen($filename, 'ab');
      fwrite($fhandle, $email."\n");
      fclose($fhandle);
    }

    redirect(base_url());
    return;
  }

  public function exchange_rate()
  {
    $source = 'http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx';
    $fcontent = file_get_contents($source);
    $this->data['source'] = json_decode(json_encode((array) simplexml_load_string($fcontent)), 1);

    $this->data['title'] = lang('ty-gia-ngoai-te').' | '.$this->data['title'];
    $this->load_view('main/exchange_rate', $this->data);
  }

  public function error_404()
  {
    $this->data['title'] = 'Error 404 | '.$this->data['title'];
    $this->load_view('main/error_404', $this->data);
  }

  public function version($vs = '')
  {
    $this->session->set_userdata('version', $vs);
    redirect(base_url());
  }

  public function sitemap()
  {
    $this->config->load('sitemap');
    $this->data['sitemap'] = $this->config->item('sitemap');

    $this->load->model('system/sitemap_model');
    $this->data['sitemap'] = array_merge($this->data['sitemap'], $this->sitemap_model->get_entries_site($this->data));

    $this->data['sitemap'] = array_unique($this->data['sitemap']);

    header("Content-Type: text/xml;charset=iso-8859-1");
    $this->load->view($this->data['theme']['name'].'/main/sitemap', $this->data);
  }

  public function tn_test_28101988($debug = '')
  {
    // if ($debug == 'run48944') {
    //   // lay so luong cards
    //   $this->data['vtc_card'] = array(
    //     'service_code' => 'VTC0319',
    //     'value' => '50000',
    //   );
    //   $this->load->model('payment/vtc_pay_model');
    //   $result_process = $this->vtc_pay_model->get_balance($this->data);
    //   debug($this->vtc_pay_model->result);
    //   var_dump($result_process);
    //   exit;
    // }
  }
}