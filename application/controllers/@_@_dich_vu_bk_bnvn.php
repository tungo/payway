<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dich_vu extends Site_Controller {

  public $error = array();

	public function __construct()
	{
		parent::__construct();

    $this->data['member'] = $this->session->userdata('member');
    $this->data['articles'] = $this->blog_article_model->get_entries_site_home($this->data['params'], array('limit' => 5));
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar', $this->data, TRUE);
    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('bnvn_paygate_bank_code');

    $this->load->model('payment/service_model');
	}

	public function index()
	{
    $this->data['title'] = lang('dich-vu').' | '.$this->data['title'];
    $this->load_view('dich_vu/index', $this->data);
	}

  public function dich_vu_kieu_hoi()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('dich-vu-kieu-hoi').' | '.$this->data['title'];
    $this->load_view('dich_vu/dich_vu_kieu_hoi', $this->data);
  }

  public function thanh_toan_online()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thanh-toan-online').' | '.$this->data['title'];
    $this->load_view('dich_vu/thanh_toan_online', $this->data);
  }

  public function thanh_toan_offline()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thanh-toan-offline').' | '.$this->data['title'];
    $this->load_view('dich_vu/thanh_toan_offline', $this->data);
  }

  public function dich_vu_khac()
  {
    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('dich-vu-khac').' | '.$this->data['title'];
    $this->load_view('dich_vu/dich_vu_khac', $this->data);
  }

  public function di_dong_tra_truoc()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('dauso', lang('dau-so'), 'required');
    $this->form_validation->set_rules('didong', lang('so-di-dong'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{7}$/', $this->data['params']['didong']))
      {
        $this->data['form_error'][] = $open.lang('error-di-dong-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][DI_DONG_TRA_TRUOC]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['dauso'], $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['dauso']))
      {
        $this->data['form_error'][] = $open.lang('error-dau-so-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][DI_DONG_TRA_TRUOC][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
        }

        // tao giao dich payway
        $this->load->model('payment/transaction_model');
        $this->data['transaction'] = array(
          'type'   => DI_DONG_TRA_TRUOC,
          'amount' => $this->data['params']['thanhtien'],
          'bank'   => $this->data['params']['nganhang'],
          'data'   => serialize($this->data['params']),
        );
        $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
        $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
        if (!$this->data['transaction']['id'])
        {
          $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
        }
        else
        {
          // tao thong tin thanh toan gui cho gate
          $this->data['transaction']['order'] = $this->data['params']['dauso'].$this->data['params']['didong'];

          // $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
          // $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.DI_DONG_TRA_TRUOC.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.DI_DONG_TRA_TRUOC.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

          // ket noi cong thanh toan
          $this->load->model('payment/bnvn_paygate_model');
          if (!$this->bnvn_paygate_model->send_order($this->data))
          {
            $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
          }
          else
          {
            // cap nhat thong tin thanh toan
            $this->data['transaction']['payment'] = array(
              'id'       => 'bnvn',
              'trans_id' => $this->bnvn_paygate_model->trans_id,
            );
            $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
            if (!$this->transaction_model->update_entry_payment($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              redirect($this->bnvn_paygate_model->url);
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('dauso'))
        $this->data['form_error'][] = $open.form_error('dauso').$close;
      if (form_error('didong'))
        $this->data['form_error'][] = $open.form_error('didong').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->didongtratruoc());

    $this->data['title'] = lang('di-dong-tra-truoc').' | '.$this->data['title'];
    $this->load_view('dich_vu/di_dong_tra_truoc', $this->data);
  }

  public function di_dong_tra_sau()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('dauso', lang('dau-so'), 'required');
    $this->form_validation->set_rules('didong', lang('so-di-dong'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{7}$/', $this->data['params']['didong']))
      {
        $this->data['form_error'][] = $open.lang('error-di-dong-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][DI_DONG_TRA_SAU]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['dauso'], $this->data['service_code'][DI_DONG_TRA_SAU][$this->data['params']['nhacungcap']]['dauso']))
      {
        $this->data['form_error'][] = $open.lang('error-dau-so-1').$close;
      }
      elseif (((int) $this->data['params']['menhgia'] % 1000) || (int) $this->data['params']['menhgia'] > 1000000 || (int) $this->data['params']['menhgia'] < 6000)
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][DI_DONG_TRA_SAU][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
        }

        // tao giao dich payway
        $this->load->model('payment/transaction_model');
        $this->data['transaction'] = array(
          'type'   => DI_DONG_TRA_SAU,
          'amount' => $this->data['params']['thanhtien'],
          'bank'   => $this->data['params']['nganhang'],
          'data'   => serialize($this->data['params']),
        );
        $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
        $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
        if (!$this->data['transaction']['id'])
        {
          $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
        }
        else
        {
          // tao thong tin thanh toan gui cho gate
          $this->data['transaction']['order'] = $this->data['params']['dauso'].$this->data['params']['didong'];
          // $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
          // $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.DI_DONG_TRA_SAU.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.DI_DONG_TRA_SAU.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

          // ket noi cong thanh toan
          $this->load->model('payment/bnvn_paygate_model');
          if (!$this->bnvn_paygate_model->send_order($this->data))
          {
            $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
          }
          else
          {
            // cap nhat thong tin thanh toan
            $this->data['transaction']['payment'] = array(
              'id'       => 'bnvn',
              'trans_id' => $this->bnvn_paygate_model->trans_id,
            );
            $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
            if (!$this->transaction_model->update_entry_payment($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              redirect($this->bnvn_paygate_model->url);
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('dauso'))
        $this->data['form_error'][] = $open.form_error('dauso').$close;
      if (form_error('didong'))
        $this->data['form_error'][] = $open.form_error('didong').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->didongtrasau());

    $this->data['title'] = lang('di-dong-tra-sau').' | '.$this->data['title'];
    $this->load_view('dich_vu/di_dong_tra_sau', $this->data);
  }

  public function mua_the_dien_thoai()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('soluong', lang('so-luong'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{1,5}$/', $this->data['params']['soluong']))
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-1').$close;
      }
      elseif ((int) $this->data['params']['soluong'] > 10000 || (int) $this->data['params']['soluong'] < 1)
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][MUA_THE_DIEN_THOAI]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][MUA_THE_DIEN_THOAI][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // lay so luong cards
        // $this->data['vtc_card'] = array(
        //   'service_code' => $this->data['params']['nhacungcap'],
        // );
        // $this->load->model('payment/vtc_pay_model');
        // $result_process = $this->vtc_pay_model->get_quantiy_card($this->data);

        // if ()
        // {

        // }

        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][MUA_THE_DIEN_THOAI][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia * $this->data['params']['soluong'];
        }

        // tao giao dich payway
        $this->load->model('payment/transaction_model');
        $this->data['transaction'] = array(
          'type'   => MUA_THE_DIEN_THOAI,
          'amount' => $this->data['params']['thanhtien'],
          'bank'   => $this->data['params']['nganhang'],
          'data'   => serialize($this->data['params']),
        );
        $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
        $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
        if (!$this->data['transaction']['id'])
        {
          $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
        }
        else
        {
          // tao thong tin thanh toan gui cho gate
          $this->data['transaction']['order'] = $this->data['params']['soluong'].':'.$this->data['params']['menhgia'];
          // $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
          // $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.MUA_THE_DIEN_THOAI.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.MUA_THE_DIEN_THOAI.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

          // ket noi cong thanh toan
          $this->load->model('payment/bnvn_paygate_model');
          if (!$this->bnvn_paygate_model->send_order($this->data))
          {
            $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
          }
          else
          {
            // cap nhat thong tin thanh toan
            $this->data['transaction']['payment'] = array(
              'id'       => 'bnvn',
              'trans_id' => $this->bnvn_paygate_model->trans_id,
            );
            $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
            if (!$this->transaction_model->update_entry_payment($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              redirect($this->bnvn_paygate_model->url);
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('soluong'))
        $this->data['form_error'][] = $open.form_error('soluong').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->muathedienthoai());

    $this->data['title'] = lang('mua-the-dien-thoai').' | '.$this->data['title'];
    $this->load_view('dich_vu/mua_the_dien_thoai', $this->data);
  }

  public function mua_the_game()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('soluong', lang('so-luong'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!preg_match('/^[0-9]{1,5}$/', $this->data['params']['soluong']))
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-1').$close;
      }
      elseif ((int) $this->data['params']['soluong'] > 10000 || (int) $this->data['params']['soluong'] < 1)
      {
        $this->data['form_error'][] = $open.lang('error-so-luong-2').$close;
      }
      elseif (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][MUA_THE_GAME]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][MUA_THE_GAME][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // lay so luong cards
        // $this->data['vtc_card'] = array(
        //   'service_code' => $this->data['params']['nhacungcap'],
        // );
        // $this->load->model('payment/vtc_pay_model');
        // $result_process = $this->vtc_pay_model->get_quantiy_card($this->data);

        // if ()
        // {

        // }

        //********** chinh gia tien khuyen mai tai day **********
        $giamgia = $this->data['service_code'][MUA_THE_GAME][$this->data['params']['nhacungcap']]['giamgia'];
        if ($giamgia)
        {
          $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia * $this->data['params']['soluong'];
        }

        // tao giao dich payway
        $this->load->model('payment/transaction_model');
        $this->data['transaction'] = array(
          'type'   => MUA_THE_GAME,
          'amount' => $this->data['params']['thanhtien'],
          'bank'   => $this->data['params']['nganhang'],
          'data'   => serialize($this->data['params']),
        );
        $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
        $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
        if (!$this->data['transaction']['id'])
        {
          $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
        }
        else
        {
          // tao thong tin thanh toan gui cho gate
          $this->data['transaction']['order'] = $this->data['params']['soluong'].':'.$this->data['params']['menhgia'];
          
          // $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
          // $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.MUA_THE_GAME.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.MUA_THE_GAME.'&trans_id='.$this->data['transaction']['id']);
          // $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

          // ket noi cong thanh toan
          $this->load->model('payment/bnvn_paygate_model');
          if (!$this->bnvn_paygate_model->send_order($this->data))
          {
            $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
          }
          else
          {
            // cap nhat thong tin thanh toan
            $this->data['transaction']['payment'] = array(
              'id'       => 'bnvn',
              'trans_id' => $this->bnvn_paygate_model->trans_id,
            );
            $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
            if (!$this->transaction_model->update_entry_payment($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              redirect($this->bnvn_paygate_model->url);
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('soluong'))
        $this->data['form_error'][] = $open.form_error('soluong').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->muathegame());

    $this->data['title'] = lang('mua-the-game').' | '.$this->data['title'];
    $this->load_view('dich_vu/mua_the_game', $this->data);
  }

  public function nap_tien_game()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('taikhoan', lang('tai-khoan'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][NAP_TIEN_GAME]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][NAP_TIEN_GAME][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // kiem tra tai khoan
        $this->data['vtc_acc'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'account' => $this->data['params']['taikhoan'],
        );
        $this->load->model('payment/vtc_pay_model');
        if (!$this->vtc_pay_model->check_account_exists($this->data))
        {
          $this->data['form_error'][] = $open.lang('error-tai-khoan-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][NAP_TIEN_GAME][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
          }

          // tao giao dich payway
          $this->load->model('payment/transaction_model');
          $this->data['transaction'] = array(
            'type'   => NAP_TIEN_GAME,
            'amount' => $this->data['params']['thanhtien'],
            'bank'   => $this->data['params']['nganhang'],
            'data'   => serialize($this->data['params']),
          );
          $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
          $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
          if (!$this->data['transaction']['id'])
          {
            $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
          }
          else
          {
            // tao thong tin thanh toan gui cho gate
            $this->data['transaction']['good_code'] = $this->data['params']['taikhoan'];
            $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
            $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.NAP_TIEN_GAME.'&trans_id='.$this->data['transaction']['id']);
            $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.NAP_TIEN_GAME.'&trans_id='.$this->data['transaction']['id']);
            $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

            // ket noi cong thanh toan
            $this->load->model('payment/bnvn_paygate_model');
            if (!$this->bnvn_paygate_model->send_order($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              // cap nhat thong tin thanh toan
              $this->data['transaction']['payment'] = array(
                'id'       => 'bnvn',
                'trans_id' => $this->bnvn_paygate_model->trans_id,
              );
              $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
              if (!$this->transaction_model->update_entry_payment($this->data))
              {
                $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
              }
              else
              {
                redirect($this->bnvn_paygate_model->url);
              }
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('taikhoan'))
        $this->data['form_error'][] = $open.form_error('taikhoan').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->naptiengame());

    $this->data['title'] = lang('nap-tien-game').' | '.$this->data['title'];
    $this->load_view('dich_vu/nap_tien_game', $this->data);
  }


  public function nap_tien_tai_khoan()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('nhacungcap', lang('nha-cung-cap'), 'required');
    $this->form_validation->set_rules('taikhoan', lang('tai-khoan'), 'required');
    $this->form_validation->set_rules('menhgia', lang('menh-gia'), 'required');
    $this->form_validation->set_rules('nganhang', lang('ngan-hang'), 'required');

    $open = '<p><i class="icon_error-triangle_alt"></i>';
    $close = '<a href="#" class="notification-close notification-close-error"><i class="fa-times"></i></a></p>';

    if ($this->form_validation->run() == TRUE)
    {
      // kiem tra du lieu
      if (!array_key_exists($this->data['params']['nhacungcap'], $this->data['service_code'][NAP_TIEN_TAI_KHOAN]))
      {
        $this->data['form_error'][] = $open.lang('error-nha-cung-cap-1').$close;
      }
      elseif (!in_array($this->data['params']['menhgia'], $this->data['service_code'][NAP_TIEN_TAI_KHOAN][$this->data['params']['nhacungcap']]['menhgia']))
      {
        $this->data['form_error'][] = $open.lang('error-menh-gia-1').$close;
      }
      elseif (!array_key_exists($this->data['params']['nganhang'], $this->data['bank_code']))
      {
        $this->data['form_error'][] = $open.lang('error-ngan-hang-1').$close;
      }
      else
      {
        // kiem tra tai khoan
        $this->data['vtc_acc'] = array(
          'service_code' => $this->data['params']['nhacungcap'],
          'account' => $this->data['params']['taikhoan'],
        );
        $this->load->model('payment/vtc_pay_model');
        if (!$this->vtc_pay_model->check_account_exists($this->data))
        {
          $this->data['form_error'][] = $open.lang('error-tai-khoan-1').$close;
        }
        else
        {
          //********** chinh gia tien khuyen mai tai day **********
          $giamgia = $this->data['service_code'][NAP_TIEN_TAI_KHOAN][$this->data['params']['nhacungcap']]['giamgia'];
          if ($giamgia)
          {
            $this->data['params']['thanhtien'] = $this->data['params']['menhgia'] * $giamgia;
          }

          // tao giao dich payway
          $this->load->model('payment/transaction_model');
          $this->data['transaction'] = array(
            'type'   => NAP_TIEN_TAI_KHOAN,
            'amount' => $this->data['params']['thanhtien'],
            'bank'   => $this->data['params']['nganhang'],
            'data'   => serialize($this->data['params']),
          );
          $this->data['transaction']['member_id'] = (!empty($this->data['member'])) ? $this->data['member']['id'] : 0;
          $this->data['transaction']['id'] = $this->transaction_model->add_entry($this->data);
          if (!$this->data['transaction']['id'])
          {
            $this->data['form_error'][] = $open.lang('error-giao-dich-1').$close;
          }
          else
          {
            // tao thong tin thanh toan gui cho gate
            $this->data['transaction']['good_code'] = $this->data['params']['taikhoan'];
            $this->data['transaction']['net_cost'] = $this->data['transaction']['amount'];
            $this->data['transaction']['url_success'] = base_url('dich-vu/payment-success?type='.NAP_TIEN_TAI_KHOAN.'&trans_id='.$this->data['transaction']['id']);
            $this->data['transaction']['url_fail'] = base_url('dich-vu/payment-fail?type='.NAP_TIEN_TAI_KHOAN.'&trans_id='.$this->data['transaction']['id']);
            $this->data['transaction']['selected_bank'] = $this->data['params']['nganhang'];

            // ket noi cong thanh toan
            $this->load->model('payment/bnvn_paygate_model');
            if (!$this->bnvn_paygate_model->send_order($this->data))
            {
              $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
            }
            else
            {
              // cap nhat thong tin thanh toan
              $this->data['transaction']['payment'] = array(
                'id'       => 'bnvn',
                'trans_id' => $this->bnvn_paygate_model->trans_id,
              );
              $this->data['transaction']['payment'] = serialize($this->data['transaction']['payment']);
              if (!$this->transaction_model->update_entry_payment($this->data))
              {
                $this->data['form_error'][] = $open.lang('error-cong-thanh-toan-1').$close;
              }
              else
              {
                redirect($this->bnvn_paygate_model->url);
              }
            }
          }
        }
      }
    }
    else
    {
      if (form_error('nhacungcap'))
        $this->data['form_error'][] = $open.form_error('nhacungcap').$close;
      if (form_error('taikhoan'))
        $this->data['form_error'][] = $open.form_error('taikhoan').$close;
      if (form_error('menhgia'))
        $this->data['form_error'][] = $open.form_error('menhgia').$close;
      if (form_error('nganhang'))
        $this->data['form_error'][] = $open.form_error('nganhang').$close;
    }
    init_form_error($this->data['form_error'], '');

    $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());

    $this->data['title'] = lang('nap-tien-tai-khoan').' | '.$this->data['title'];
    $this->load_view('dich_vu/nap_tien_tai_khoan', $this->data);
  }

  public function payment_success()
  {
    if (empty($_GET))
    {
      redirect();
      return;
    }
    $this->data['transaction']['id'] = $this->input->get('trans_id');
    $this->data['transaction']['type'] = $this->input->get('type');
    $this->data['transaction']['status'] = GD_KHOI_TAO;

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);

    if (empty($this->data['transaction']))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);
    $this->data['transaction']['payment'] = unserialize($this->data['transaction']['payment']);

    // kiem tra trang thai giao dich voi banknet
    $this->load->model('payment/bnvn_paygate_model');
    if (!$this->bnvn_paygate_model->check_status($this->data))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1012'));
      return;
    }
    if ($this->bnvn_paygate_model->error['code'] != '210')
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1013'));
      return;
    }

    // cap nhat trang thai giao dich
    $this->data['transaction']['status'] = GD_THANH_TOAN_TC;
    $this->transaction_model->update_status($this->data);

    $result_process = false;
    $result_code = '';

    // tuy theo loai dich vu ma xu ly
    if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'account' => $this->data['transaction']['data']['dauso'].$this->data['transaction']['data']['didong'],
        'amount' => $this->data['transaction']['data']['menhgia'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->topup_telco($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
    }
    elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'account' => $this->data['transaction']['data']['dauso'].$this->data['transaction']['data']['didong'],
        'amount' => $this->data['transaction']['data']['menhgia'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->topup_telco($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'amount' => $this->data['transaction']['data']['menhgia'],
        'quantity' => $this->data['transaction']['data']['soluong'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->buy_card($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
      if ($result_process)
      {
        $this->data['transaction']['vtc_pay']['trans_id'] = $this->vtc_pay_model->result['vtc_trans_id'];
        $this->vtc_pay_model->get_card($this->data);

        $this->data['transaction']['data']['vtc_trans_id'] = $this->data['transaction']['vtc_pay']['trans_id'];
        $this->data['transaction']['data']['cards'] = $this->vtc_pay_model->result['cards'];
        $this->data['transaction']['data'] = serialize($this->data['transaction']['data']);
        $this->transaction_model->update_entry_data($this->data);
      }
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'amount' => $this->data['transaction']['data']['menhgia'],
        'quantity' => $this->data['transaction']['data']['soluong'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->buy_card($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
      if ($result_process)
      {
        $this->data['transaction']['vtc_pay']['trans_id'] = $this->vtc_pay_model->result['vtc_trans_id'];
        $this->vtc_pay_model->get_card($this->data);

        $this->data['transaction']['data']['vtc_trans_id'] = $this->data['transaction']['vtc_pay']['trans_id'];
        $this->data['transaction']['data']['cards'] = $this->vtc_pay_model->result['cards'];
        $this->data['transaction']['data'] = serialize($this->data['transaction']['data']);
        $this->transaction_model->update_entry_data($this->data);
      }
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'account' => $this->data['transaction']['data']['taikhoan'],
        'amount' => $this->data['transaction']['data']['menhgia'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->topup_partner($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
    {
      $this->data['transaction']['vtc_pay'] = array(
        'service_code' => $this->data['transaction']['data']['nhacungcap'],
        'account' => $this->data['transaction']['data']['taikhoan'],
        'amount' => $this->data['transaction']['data']['menhgia'],
      );
      $this->load->model('payment/vtc_pay_model');
      $result_process = $this->vtc_pay_model->topup_partner($this->data);
      $result_code = $this->vtc_pay_model->result['code'];
    }

    // cap nhat ket qua xu ly
    if ($result_process && (int) $result_code == 1)
    {
      // cap nhat trang thai giao dich
      $this->data['transaction']['status'] = GD_XU_LY_TC;
      $this->transaction_model->update_status($this->data);

      // xac nhan giao dich cho banknet
      $this->data['transaction']['result'] = '0';
      if (!$this->bnvn_paygate_model->confirm_transaction($this->data))
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_XAC_NHAN_TB;
        $this->transaction_model->update_status($this->data);
      }
      else
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_XAC_NHAN_TC;
        $this->transaction_model->update_status($this->data);
      }

      // luu session de hien ket qua
      $this->session->set_userdata('transaction', 's|'.$this->data['transaction']['type'].'|'.$this->data['transaction']['id']);
    }
    elseif ((int) $result_code < 0)
    {
      // cap nhat trang thai giao dich
      $this->data['transaction']['status'] = GD_XU_LY_TB;
      $this->transaction_model->update_status($this->data);

      // xac nhan giao dich cho banknet
      $this->data['transaction']['result'] = '1';
      if (!$this->bnvn_paygate_model->confirm_transaction($this->data))
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_HOAN_TIEN_TB;
        $this->transaction_model->update_status($this->data);
      }
      else
      {
        // cap nhat trang thai giao dich
        $this->data['transaction']['status'] = GD_HOAN_TIEN_TC;
        $this->transaction_model->update_status($this->data);
      }

      // luu session de hien ket qua
      $this->session->set_userdata('transaction', 'f|'.$this->data['transaction']['type'].'|'.$this->data['transaction']['id']);
    }
    else
    {
      // cap nhat trang thai giao dich
      $this->data['transaction']['status'] = GD_LOI;
      $this->transaction_model->update_status($this->data);

      // khong xac nhan giao dich cho banknet
      $this->data['transaction']['result'] = '-1';

      // luu session de hien ket qua
      $this->session->set_userdata('transaction', 'f|'.$this->data['transaction']['type'].'|'.$this->data['transaction']['id']);
    }

    redirect(base_url($this->data['path'].'dich-vu/thong-tin-giao-dich.html?type='.$this->data['transaction']['type'].'&trans_id='.$this->data['transaction']['id']));
    return;
  }

  public function payment_fail()
  {
    if (empty($_GET))
    {
      redirect();
      return;
    }
    $this->data['transaction']['id'] = $this->input->get('trans_id');
    $this->data['transaction']['type'] = $this->input->get('type');
    $this->data['transaction']['status'] = GD_KHOI_TAO;

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);

    if (empty($this->data['transaction']))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);
    $this->data['transaction']['payment'] = unserialize($this->data['transaction']['payment']);

    // kiem tra trang thai giao dich voi banknet
    $this->load->model('payment/bnvn_paygate_model');
    if (!$this->bnvn_paygate_model->check_status($this->data))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1012'));
      return;
    }
    if ($this->bnvn_paygate_model->error['code'] == '210')
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1013'));
      return;
    }

    // cap nhat trang thai giao dich
    $this->data['transaction']['status'] = GD_THANH_TOAN_TB;
    $this->transaction_model->update_status($this->data);

    // luu session de hien ket qua
    $this->session->set_userdata('transaction', 'f|'.$this->data['transaction']['type'].'|'.$this->data['transaction']['id']);

    redirect(base_url($this->data['path'].'dich-vu/thong-tin-giao-dich.html?type='.$this->data['transaction']['type'].'&trans_id='.$this->data['transaction']['id']));
  }

  public function thong_tin_giao_dich()
  {
    $trans_ss = explode('|', $this->session->userdata('transaction'));
    if (empty($_GET) || empty($trans_ss) || $trans_ss[1] != $this->input->get('type') || $trans_ss[2] != $this->input->get('trans_id') || ($trans_ss[0] != 's' && $trans_ss[0] != 'f'))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }

    $this->data['transaction']['id'] = $this->input->get('trans_id');
    $this->data['transaction']['type'] = $this->input->get('type');
    $this->data['trans_result'] = $trans_ss[0];

    // kiem tra & lay giao dich
    $this->load->model('payment/transaction_model');
    $this->data['transaction'] = $this->transaction_model->get_entry_payment($this->data);
    if (empty($this->data['transaction']))
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);

    // hien thong tin cho tung dich vu
    if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
    {
      $detail = 'di_dong_tra_truoc';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->didongtratruoc());
    }
    elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
    {
      $detail = 'di_dong_tra_sau';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->didongtrasau());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
    {
      $detail = 'mua_the_dien_thoai';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->muathedienthoai());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
    {
      $detail = 'mua_the_game';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->muathegame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
    {
      $detail = 'nap_tien_game';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->naptiengame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
    {
      $detail = 'nap_tien_tai_khoan';
      $this->data['trans_noti'] = ($this->data['trans_result'] == 's') ? lang('noti-success-1') : lang('noti-fail-1');

      $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());
    }
    else
    {
      redirect(base_url($this->data['path'].'dich-vu/giao-dich-loi.html?error=1011'));
      return;
    }

    $this->data['detail'] = $this->load->view($this->data['theme']['name'].'/dich_vu/thong_tin/'.$detail, $this->data, TRUE);

    $this->data['sidebar'] = $this->load->view($this->data['theme']['name'].'/dich_vu/sidebar2', $this->data, TRUE);

    $this->data['title'] = lang('thong-tin-giao-dich').' | '.$this->data['title'];
    $this->load_view('dich_vu/thong_tin_giao_dich', $this->data);
  }

  public function giao_dich_loi()
  {
    if (empty($_GET))
    {
      redirect();
      return;
    }

    $this->data['error'] = $this->input->get('error');
    $this->data['trans_error'] = $this->config->item('trans_error');
    if (!array_key_exists($this->data['error'], $this->data['trans_error']))
    {
      redirect();
      return;
    }
    $this->data['trans_noti'] = lang($this->data['trans_error'][$this->data['error']]);

    $this->data['title'] = lang('thong-tin-giao-dich').' | '.$this->data['title'];
    $this->load_view('dich_vu/giao_dich_loi', $this->data);
  }

}