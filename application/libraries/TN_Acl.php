<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Acl {

	public $CI;

	function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->model('system/acl_model');
	}

	// initialize acl
	public function init_acl($group_id = '')
	{
    if (!$group_id)
      return array();

		return $this->CI->acl_model->get_acl($group_id);
	}

	// Check privilege of user
	public function check_privilege($dispatch = '', $acl = array())
	{
		if (!is_array($acl))
			return false;

		return in_array($dispatch, $acl);
	}

}