<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Recursive {

  public $CI;

  // nguon du lieu lay tu dabase
  public $source = array();

  // cay da duoc xay dung hoan chinh tu $source
  public $tree = array();

  // mang ket qua se dc tra ve khi lay cay/mang theo tham so id
  public $result = array();

  function __construct($source = array())
  {
    $this->CI =& get_instance();

    $this->initialize($source);
  }

  // ham khoi tao cho lop
  // khi co du lieu tu database se tien hanh tao cay de xu ly
  public function initialize($source = array())
  {
    if (is_array($source) && count($source))
    {
      $this->tree = array();
      $this->result = array();
      $this->source = $source;
      $this->build_tree($this->source);
    }
  }

  // tao cay day du. cau truc xem o cuoi trang
  // public function build_tree($items = array())
  // {
  //  if (! count($items))
  //  {
  //    return FALSE;
  //  }

  //  foreach($items as $item)
  //  {
  //    $this->tree[$item->parent_id][] = $item;
  //  }
  //  foreach($items as $item)
  //  {
  //    if (isset($this->tree[$item->id]))
  //    {
  //      $item->children = $this->tree[$item->id];
  //    }
  //  }
  // }

  // version for array
  public function build_tree($items = array())
  {
    if (! count($items))
    {
      return FALSE;
    }

    foreach ($items as &$item)
    {
      $this->tree[$item['parent_id']][] = &$item;
    }
    unset($item);

    foreach ($items as &$item)
    {
      if (isset($this->tree[$item['id']]))
      {
        $item['children'] = $this->tree[$item['id']];
      }
    }
  }

  // lay mang cay theo parent id
  public function get($parent_id = 0)
  {
    $parent_id = (int) $parent_id;
    if ($parent_id < 0)
    {
      $parent_id = 0;
    }

    if (! isset($this->tree[$parent_id]))
    {
      return array();
    }

    $this->recursive($this->tree[$parent_id]);
    return $this->result;
  }

  // de quy cho ham get
  // public function recursive($tree = array(), $depth = 1)
  // {
  //  $depth = (int) $depth;

  //  if (count($tree))
  //  {
  //    foreach($tree as $val)
  //    {
  //      $val->depth = $depth;
  //      if (isset($val->children))
  //      {
  //        $val->children_amount = count($val->children);
  //        $this->result[] = $val;
  //        $this->recursive($val->children, $depth + 1);
  //      }
  //      else
  //      {
  //        $val->children_amount = 0;
  //        $this->result[] = $val;
  //      }
  //    }
  //  }
  // }

  // version for array
  public function recursive($tree = array(), $depth = 1)
  {
    $depth = (int) $depth;

    if (count($tree))
    {
      foreach($tree as $val)
      {
        $val['depth'] = $depth;
        if (isset($val['children']))
        {
          $val['children_amount'] = count($val['children']);
          $this->result[] = $val;
          $this->recursive($val['children'], $depth + 1);
        }
        else
        {
          $val['children_amount'] = 0;
          $this->result[] = $val;
        }
      }
    }
  }

  public function get_parent()
  {

  }

}

// $tree = array(
//  'id1' => array(
//    'value1' => 'something1',
//    'children' => array(
//      'id2' => array(
//        'value2' => 'something2',
//        'children' => array(
//          'id3' => array(
//            'value3' => 'something3',
//            'children' => 'more children',
//          ),
//        ),
//      ),
//      'id4' => array(
//        'value4' => 'something4',
//        'children' => array(
//          'id5' => array(
//            'value5' => 'something5',
//            'children' => 'more children',
//          ),
//        ),
//      ),
//      'other id' => 'other value',
//    ),
//  ),
//  'id2' => array(
//    'value2' => 'something2',
//    'children' => array(
//      'id3' => array(
//        'value3' => 'something3',
//        'children' => 'more children',
//      ),
//    ),
//  ),
//  'id3' => array(
//    'value3' => 'something3',
//    'children' => 'more children',
//  ),
//  'other id' => 'other children'
// );