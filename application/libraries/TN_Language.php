<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Language {

	public $CI;

	function __construct($params = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->model('system/language_model');
	}

	public function initialize($session = '')
	{
		$language_id = $this->CI->session->userdata($session);
		if (! $language_id || ! $this->CI->language_model->check($language_id))
		{
			$language_id = $this->CI->language_model->get_default();
			$this->CI->session->set_userdata($session, $language_id);
		}
		return $this->CI->language_model->get();
	}
}