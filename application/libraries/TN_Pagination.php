<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Pagination {

  // object CI
  public $CI;

  // file config library (in 'application/config')
  public $config_file  = 'tn_pagination';

  // config pagination
  public $layout       = '';
  public $layouts      = array();

  public $base_url     = ''; // the page we are linking to
  public $links        = ''; // page links are returned

  public $cur_page     = 0; // the current page being viewed
  public $total_rows   = 0; // total number of items (database results)

  public $per_page     = 10; // Max number of items you want shown per page
  public $num_links    = 2; // Number of "digit" links to show before/after the currently viewed page
  public $page_query   = FALSE; // TRUE: segment, FALSE: param get method
  public $page_string  = 'page'; // Name of page param get method

  public $total_pages  = 0; // total pages
  public $offset       = 0; // offset for sql limit
  public $start_row    = 0; // first row of current page
  public $end_row      = 0; // last row of current page

  public $show_disable = TRUE; // show disable button (first, prev, next, last)

  public $full_tag_open           = '<ul class="pagination">';
  public $full_tag_close          = '</ul>';

  public $first_link              = '&laquo;';
  public $first_link_attr         = '';
  public $first_tag_open          = '<li>';
  public $first_tag_close         = '</li>';
  public $first_disable_tag_open  = '<li class="disabled">';
  public $first_disable_tag_close = '</li>';

  public $prev_link               = '&lsaquo;';
  public $prev_link_attr          = '';
  public $prev_tag_open           = '<li>';
  public $prev_tag_close          = '</li>';
  public $prev_disable_tag_open   = '<li class="disabled">';
  public $prev_disable_tag_close  = '</li>';

  public $next_link               = '&rsaquo;';
  public $next_link_attr          = '';
  public $next_tag_open           = '<li>';
  public $next_tag_close          = '</li>';
  public $next_disable_tag_open   = '<li class="disabled">';
  public $next_disable_tag_close  = '</li>';

  public $last_link               = '&raquo;';
  public $last_link_attr          = '';
  public $last_tag_open           = '<li>';
  public $last_tag_close          = '</li>';
  public $last_disable_tag_open   = '<li class="disabled">';
  public $last_disable_tag_close  = '</li>';

  public $cur_link                = ''; // after current link
  public $cur_link_attr           = '';
  public $cur_tag_open            = '<li class="active"><a>';
  public $cur_tag_close           = '</a></li>';

  public $num_link                = ''; // after number link
  public $num_link_attr           = '';
  public $num_tag_open            = '<li>';
  public $num_tag_close           = '</li>';

  function __construct($params = array())
  {
    $this->CI =& get_instance();
    $this->CI->config->load($this->config_file);

    $this->config = $this->CI->config->item('tn_pagination');

    // get config file before get params
    $this->initialize($this->config);

    if (count($params) > 0)
    {
      $this->initialize($params);
    }
  }

  // trong config file se co cac thiet ke layouts [layouts][ten layout] va layout duoc chon [layout] = 'ten layout'
  // ham init se luu lai cac thiet ke layouts do trong thuoc tinh $this->layouts[ten layout]
  // va luu ten layout dc chon trong thuoc tinh $this->layout

  // dong thoi ham init se lay mang cua layouts duoc chon do gan vao cac thuoc tinh cua lop de thuc hien tao link.
  // => trong config file lay [layouts][ten layout][thuoc tinh] = 'thiet ke html'
  // => trong lop se la $this->thuoc_tinh = 'thiet ke html'

  // khac voi lop tn_message vi thiet ke it (4 html cho 4 loai thong bao) nen tn_message se su dung cac thiet ke html ngay trong thuoc tinh $this->layouts[layout dc chon][loai thong bao] = 'thiet ke html'

  // khi tao lop co truyen tham so hoac goi ham init tu controller van thuc hien tuong tu

  public function initialize($params = array())
  {
    if (count($params) > 0)
    {
      // duyet tham so
      foreach ($params as $key => $val)
      {
        // neu tham so chinh xac (duoc khai bao nhu thuoc tinh cua lop)
        if (isset($this->$key))
        {
          // gan tham so vao thuoc tinh
          $this->$key = $val;
        }
      }

      // dung layout dc chon de gan vao thuoc tinh de su dung
      foreach ($this->layouts[$this->layout] as $key => $val)
      {
        if (isset($this->$key))
        {
          $this->$key = $val;
        }
      }
    }
  }

  public function create_links()
  {
    // validate
    $this->cur_page   = (int) $this->cur_page;
    $this->per_page   = (int) $this->per_page;
    $this->total_rows = (int) $this->total_rows;
    $this->num_links  = (int) $this->num_links;

    // Calculate the total number of pages
    $this->total_pages = ceil($this->total_rows / $this->per_page);
    $this->total_pages = ($this->total_pages) ? $this->total_pages : 1;

    if ($this->cur_page <= 0)
    {
      $this->cur_page = 1;
    }
    elseif ($this->cur_page > $this->total_pages)
    {
      $this->cur_page = $this->total_pages;
    }

    // process for sql
    $this->offset = ($this->per_page * ($this->cur_page - 1));
    $this->start_row = $this->offset + 1;
    $this->end_row = (($this->offset + $this->per_page) <= $this->total_rows) ? ($this->offset + $this->per_page) : $this->total_rows;

    // If our item count or per-page total is zero there is no need to continue.
    if ($this->total_rows <= 0 OR $this->per_page <= 0 || $this->num_links <= 0)
    {
      return FALSE;
    }

    // Is there only one page? Hm... nothing more to do here then.
    if ($this->total_pages == 1)
    {
      return FALSE;
    }

    // Calculate the start and end numbers. These determine
    // which number to start and end the digit links with
    $this->start_page = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
    $this->end_page   = (($this->cur_page + $this->num_links) < $this->total_pages) ? ($this->cur_page + $this->num_links) : $this->total_pages;

    $this->base_url = rtrim($this->base_url, '/');
    if ($this->page_query)
    {
      if (preg_match('/\?/', $this->base_url))
      {
        $this->base_url .= '&'.$this->page_string.'=';
      }
      else
      {
        $this->base_url .= '?'.$this->page_string.'=';
      }
    }
    else
    {
      if ($this->page_string)
      {
        $this->base_url .= '/'.$this->page_string.'/';
      }
      else
      {
        $this->base_url .= '/';
      }
    }

    // And here we go...
    $this->links = '';

    // Render the "First" link
    if  ($this->first_link !== FALSE AND $this->cur_page > ($this->num_links + 1))
    {
      $i = 1;
      $this->links .= $this->first_tag_open.'<a '.$this->first_link_attr.' href="'.$this->base_url.$i.'">'.$this->first_link.'</a>'.$this->first_tag_close;
    }
    elseif ($this->show_disable)
    {
      $this->links .= $this->first_disable_tag_open.$this->first_link.$this->first_disable_tag_close;
    }

    // Render the "previous" link
    if  ($this->prev_link !== FALSE AND $this->cur_page != 1)
    {
      $i = $this->cur_page - 1;
      $this->links .= $this->prev_tag_open.'<a '.$this->prev_link_attr.' href="'.$this->base_url.$i.'">'.$this->prev_link.'</a>'.$this->prev_tag_close;
    }
    elseif ($this->show_disable)
    {
      $this->links .= $this->prev_disable_tag_open.$this->prev_link.$this->prev_disable_tag_close;
    }

    // Write the digit links
    for ($i = $this->start_page - 1; $i <= $this->end_page; $i++)
    {
      if ($i >= 1)
      {
        // current page
        if ($this->cur_page == $i)
        {
          $this->links .= $this->cur_tag_open.$i.$this->cur_tag_close;
          // $this->links .= $this->cur_tag_open.'<a '.$this->cur_link_attr.'>'.$i.$this->cur_link.'</a>'.$this->cur_tag_close;
        }
        // number page
        else
        {
          $n = ($i == 1) ? 1 : $i;
          $this->links .= $this->num_tag_open.'<a '.$this->num_link_attr.' href="'.$this->base_url.$n.'">'.$i.'</a>'.$this->num_tag_close;
        }
      }
    }

    // Render the "next" link
    if ($this->next_link !== FALSE AND $this->cur_page < $this->total_pages)
    {
      $i = $this->cur_page + 1;
      $this->links .= $this->next_tag_open.'<a '.$this->next_link_attr.' href="'.$this->base_url.$i.'">'.$this->next_link.'</a>'.$this->next_tag_close;
    }
    elseif ($this->show_disable)
    {
      $this->links .= $this->next_disable_tag_open.$this->next_link.$this->next_disable_tag_close;
    }

    // Render the "Last" link
    if ($this->last_link !== FALSE AND ($this->cur_page + $this->num_links) < $this->total_pages)
    {
      $i = $this->total_pages;
      $this->links .= $this->last_tag_open.'<a '.$this->last_link_attr.' href="'.$this->base_url.$i.'">'.$this->last_link.'</a>'.$this->last_tag_close;
    }
    elseif ($this->show_disable)
    {
      $this->links .= $this->last_disable_tag_open.$this->last_link.$this->last_disable_tag_close;
    }

    // Kill double slashes.  Note: Sometimes we can end up with a double slash
    // in the penultimate link so we'll kill all double slashes.
    $this->links = preg_replace("#([^:])//+#", "\\1/", $this->links);

    // Add the wrapper HTML if exists
    $this->links = $this->full_tag_open.$this->links.$this->full_tag_close;

    return $this->links;
  }
}