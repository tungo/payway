<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Auth {

	public $CI;

	function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->model('system/auth_model');
	}

	// auto-login the user if they are remembered
	public function login_remembered_user($data = array())
	{
		if (!$this->CI->input->cookie('identity') || !$this->CI->input->cookie('remember_code'))
			return false;

		$member = $this->CI->auth_model->login_remembered_user();
		if (!empty($member))
		{
			$this->CI->session->set_userdata('member', $member);
			return true;
		}
		return false;
	}

	// Log in
	public function login($data = array())
	{
		if (empty($data['params']['email']) || empty($data['params']['password']))
			return false;

		$member = $this->CI->auth_model->login($data['params']);
		if (!empty($member))
		{
			$this->CI->session->set_userdata('member', $member);
			return true;
		}
		return false;
	}

	// Log out
	public function logout()
	{
		// destroy member session & remember cookie
		$this->CI->session->unset_userdata('member');

		$this->CI->input->set_cookie('identity', '', '');
		$this->CI->input->set_cookie('remember_code', '', '');

		//Recreate the session
		$this->CI->session->sess_destroy();
		$this->CI->session->sess_create();

		return true;
	}

	// check login session
	public function check_login()
	{
		$member = $this->CI->session->userdata('member');
		if (empty($member['email']) || empty($member['salt']))
		{
			$this->logout();
			return false;
		}

		$member = $this->CI->auth_model->check_login($member);
		if (!empty($member))
		{
			$this->CI->session->set_userdata('member', $member);
			return true;
		}
		$this->logout();
		return false;
	}

	// get account information from session
	public function get_info()
	{
		$identity = $this->CI->input->cookie('identity');
		$remember_code = $this->CI->input->cookie('remember_code');
		return !empty($identity) && !empty($remember_code);
	}

	/*
	* Check whether remembered login
	*
	* @return bool true: remembered
	*/
	public function is_remembered()
	{
		$identity = $this->CI->input->cookie('identity');
		$remember_code = $this->CI->input->cookie('remember_code');
		return !empty($identity) && !empty($remember_code);
	}

	/*
	* Check whether logged in
	*
	* @return bool true: logged in
	*/
	public function is_logged_in()
	{
		$member = $this->CI->session->userdata('member');
		return !empty($member);
	}

	/*
	* Check whether user can access system
	*
	* @return bool true: can access
	*/
	public function check_access()
	{
		$member = $this->CI->session->userdata('member');
		if (!empty($member))
			return (bool) $member['group_admin'];
		return false;
	}

}