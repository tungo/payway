<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TN_Message {

	// object CI
	public $CI;

	// file config library (in 'application/config')
	public $config_file = 'tn_message';

	// messages in session
	public $messages    = array();

	// 0 (default): warning, 1: error, 2: success, 3: info
	public $type        = array('warning', 'error', 'success', 'info');

	// $config[$config_file] in config file
	public $config      = array();

	public $flash       = TRUE;
	public $layout      = 'default';
	public $layouts     = array();

	public $default_layout = array(
		'<div class="alert alert-dismissable alert-warning">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			[message]
		</div>',
		'<div class="alert alert-dismissable alert-danger">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			[message]
		</div>',
		'<div class="alert alert-dismissable alert-success">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			[message]
		</div>',
		'<div class="alert alert-dismissable alert-info">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			[message]
		</div>'
	);

	function __construct($params = array())
	{
		$this->layouts['default'] = $this->default_layout;

		$this->CI =& get_instance();
		$this->CI->config->load($this->config_file);
		$this->config = $this->CI->config->item('tn_message');

		$this->CI->load->library('session');

		// get config file before get params
		$this->initialize($this->config);

		if (count($params) > 0)
		{
			$this->initialize($params);
		}
	}

	// *** chu y layout so it va layouts so nhieu
	// trong config file se co cac thiet ke layouts [layouts][ten layout] va layout duoc chon [layout] = 'ten layout'
	// ham init se luu lai cac thiet ke layouts do trong thuoc tinh $this->layouts[ten layout]
	// va luu ten layout dc chon trong thuoc tinh $this->layout

	// khi thuc hien tao thong bao thi se dung thiet ke html cua layout duoc chon
	// => trong lop se la $this->layouts[layout dc chon][loai thong bao] = 'thiet ke html'

	// khi tao lop co truyen tham so hoac goi ham init tu controller van thuc hien tuong tu

	public function initialize($params = array())
	{
		if (count($params) > 0)
		{
			// duyet tham so
			foreach ($params as $key => $val)
			{
				// neu tham so chinh xac (duoc khai bao nhu thuoc tinh cua lop)
				if (isset($this->$key))
				{
					// neu khai bao do la ve cac thiet ke layouts
					if ($key == 'layouts')
					{
						// thuc hien them tung thiet ke layouts de giu lai layout default
						// va neu layout dc them tu controller thi van giu cac layout trong config file
						foreach ($val as $k => $v)
						{
							$this->layouts[$k] = $v;
						}
					}
					else
					{
						$this->$key = $val;
					}
				}
			}
		}
		if ($this->flash === TRUE)
		{
			$this->flash();
		}
	}

	public function renew()
	{
		$this->messages = array();
		return $this;
	}

	public function flash()
	{
		// get session messages from previous page
		if ($this->CI->session->flashdata('messages'))
		{
			$this->messages = $this->CI->session->flashdata('messages');
		}
		return $this;
	}

	public function save()
	{
		$this->CI->session->set_flashdata('messages', $this->messages);
	}

	public function get()
	{
		if (! isset($this->messages) OR ! is_array($this->messages))
		{
			return FALSE;
		}

		$html = '';
		foreach ($this->messages as $message)
		{
			if ($message['content'] != '')
			{
				$html .= str_replace('[message]', $message['content'], $this->layouts[$this->layout][$message['type']]);
			}
		}
		return $html;
	}

	// $message = array('content' => '', 'type' => '')
	public function set($message = array())
	{
		if (! isset($message) OR ! is_array($message))
		{
			return FALSE;
		}

		$message['content'] = isset($message['content']) ? $message['content'] : '';
		$message['type'] = (int) $message['type'];

		$this->messages[] = $message;
		return TRUE;
	}

	public function set_content($content = '', $type = 0)
	{
		$message = array(
			'content' => $content,
			'type' => $type
		);
		return $this->set($message);
	}

	public function set_warning($content = '')
	{
		return $this->set_content($content, 0);
	}
	public function set_error($content = '')
	{
		return $this->set_content($content, 1);
	}
	public function set_success($content = '')
	{
		return $this->set_content($content, 2);
	}
	public function set_info($content = '')
	{
		return $this->set_content($content, 3);
	}

	public function set_multi($messages = array())
	{
		if (! isset($messages) OR ! is_array($messages))
		{
			return FALSE;
		}

		foreach ($messages as $message)
		{
			$message['content'] = isset($message['content']) ? $message['content'] : '';
			$message['type'] = (int) $message['type'];

			$this->messages[] = $message;
		}
		return TRUE;
	}

	public function set_multi_content($contents = array(), $type = 0)
	{
		if (! isset($contents) OR ! is_array($contents))
		{
			return FALSE;
		}

		foreach ($contents as $content)
		{
			$message['content'] = $content;
			$message['type'] = $type;

			$messages[] = $message;
		}
		return $this->set_multi($messages);
	}

	public function set_multi_warning($contents = array())
	{
		$this->set_multi_content($contents, 0);
	}

	public function set_multi_error($contents = array())
	{
		$this->set_multi_content($contents, 1);
	}

	public function set_multi_success($contents = array())
	{
		$this->set_multi_content($contents, 2);
	}

	public function set_multi_info($contents = array())
	{
		$this->set_multi_content($contents, 3);
	}

	// PROFESSION FOR CONFIG FILE
	// but $config is more simple than $tn_message
	//-----
	// private function _load_config_file()
	// {
	// 	// include config file
	// 	if (defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/'.$this->config_file.'.php'))
	// 	{
	// 		include(APPPATH.'config/'.ENVIRONMENT.'/'.$this->config_file.'.php');
	// 	}
	// 	elseif (file_exists(APPPATH.'config/'.$this->config_file.'.php'))
	// 	{
	// 		include(APPPATH.'config/'.$this->config_file.'.php');
	// 	}
	// 	else
	// 	{
	// 		return FALSE;
	// 	}

	// 	// check $message (config variable)
	// 	if (isset($tn_messenge) AND is_array($tn_message))
	// 	{
	// 		$this->config = $tn_messenge;
	// 		return TRUE;
	// 	}

	// 	return FALSE;
	// }
}