<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
* Button for toolbar
*
* @param string name
* @param string link
* @param array config
* 	type: link or submit form
* 	img: image of button
* 	msg: message for submit form type
*/
if ( ! function_exists('cms_tool_button'))
{
	function cms_tool_button($name = '', $value = '', $options = array())
	{
		$type = isset($options['type']) ? $options['type'] : 'link';

		$img = isset($options['img']) ? '<img src="'.$options['img'].'" alt="'.$name.'"><br>' : '';

		$msg = isset($options['msg']) ? $options['msg'] : '';

		$aTag = ($type == 'link') ? 'href="'.$value.'"' : ((!$msg) ? 'href="#" onclick="onSubmitForm(\''.$value.'\');"' : 'href="#" onclick="onSubmitForm(\''.$value.'\', \''.$msg.'\');"');

		$xhtml = '<li><a class="shortcut-button" '.$aTag.' title="'.$name.'"><span>'.$img.$name.'</span></a></li>';
		return $xhtml;
	}
}

/*
* Icon for list table
*
* @param string name
* @param string image source
* @param array config
* @param string name
* 	link: link for icon
* 	msg: message for delete button
*/
if ( ! function_exists('cms_icon_button'))
{
	function cms_icon_button($name = '', $value = '', $options = array())
	{
		$link = isset($options['link']) ? $options['link'] : '';

		$msg = isset($options['msg']) ? $options['msg'] : '';

		$xhtml = (!$link) ? '<img src="'.$value.'" alt="'.$name.'">' : ((!$msg) ? '<a href="'.$link.'" title="'.$name.'"><img src="'.$value.'" alt="'.$name.'"></a>' : '<a href="'.$link.'" onclick="return confirm(\''.$msg.'\');" title="'.$name.'"><img src="'.$value.'" alt="'.$name.'"></a>');
		return $xhtml;
	}
}

/*
* CKEditor for textarea
*
* @param string name
* @param string value
* @param array config
* 	width, height: number | percent demension
* 	toolbar: toolbar name
*/
if ( ! function_exists('cms_editor'))
{
	function cms_editor($name = '', $value, $options = array())
	{
		require_once (SCRIPTSPATH . 'fckeditor/fckeditor_php5.php');
		$oFCKeditor = new FCKeditor($name);
		$oFCKeditor->BasePath = SCRIPTSURL . 'fckeditor/';
		$oFCKeditor->Value = $value;

		$oFCKeditor->Width = (isset($options['width'])) ? $options['width'] : '100%';
		$oFCKeditor->Height = (isset($options['height'])) ? $options['height'] : '500';
		$oFCKeditor->ToolbarSet = (isset($options['toolbar'])) ? $options['toolbar'] : 'Default';
		$oFCKeditor->Config['DefaultLanguage'] = (isset($options['DefaultLanguage'])) ? $options['DefaultLanguage'] : 'en';

		return $oFCKeditor->Create();
	}
}

/*
* Replace string for ckeditor
*/
if ( ! function_exists('cms_replace_string'))
{
	function cms_replace_string($string = '', $options = array())
	{
		$str = str_replace('\"', '"', $string);
		$str = str_replace("\'", "'", $str);
		return $str;
	}
}

/*
* Alert message
*
* @param array messages: type, content
* @param array options: config
* type: success, error, warn, info
*/
if ( ! function_exists('cms_messages'))
{
	function cms_messages($msg = array(), $options = array())
	{
		$xhtml = '';
		if (is_array($msg))
		{
			$CI =& get_instance();
			$CI->load->helper('url');

			foreach ($msg as $value)
			{
				$xhtml .= '<div class="ad-notif-'.$value['type'].' grid_12"><p>'.$value['content'].'</p></div>';
			}
		}
		return $xhtml;
	}
}
