<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// load config cho module
if (! function_exists("init_module_config"))
{
  function init_module_config($params = array(), &$data = array())
  {
    if (count($params) > 0)
      foreach ($params as $key => $val)
        $data[$key] = $val;
  }
}

if ( ! function_exists('lang'))
{
  function lang($line)
  {
    $CI =& get_instance();
    return $CI->lang->line($line);
  }
}

if ( ! function_exists('elang'))
{
  function elang($line)
  {
    echo lang($line);
  }
}

if ( ! function_exists('init_form_error'))
{
  function init_form_error($form_error = array(), $glue = '<br>')
  {
    $form_error = array_filter($form_error, 'strlen');
    if (!empty($form_error))
    {
      $form_error = implode($glue, $form_error);
      $CI =& get_instance();
      $CI->tn_message->set_error($form_error);
    }
  }
}

// bo dau unicode
if (! function_exists("remove_sign"))
{
  function remove_sign($str = '')
  {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);

    $str = str_replace(".", "-", $str);
    $str = str_replace(" ", "-", str_replace("&*#39;","",$str));
    return $str;

    // $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ","ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ","ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ỳ","ý","ỵ","ỷ","ỹ","đ","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ","Ì","Í","Ị","Ỉ","Ĩ","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ","Ỳ","Ý","Ỵ","Ỷ","Ỹ","Đ","ê","ù","à");
    // $khongDau=array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","E","E","E","E","E","E","E","E","E","E","E","I","I","I","I","I","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","U","U","U","U","U","U","U","U","U","U","U","Y","Y","Y","Y","Y","D","e","u","a");
    // return str_replace($coDau,$khongDau,$str);

// $str = str_replace("&Agrave;", "À", $str);
// $str = str_replace("&Egrave;", "È", $str);
// $str = str_replace("&Igrave;", "Ì", $str);
// $str = str_replace("&Ograve;", "Ò", $str);
// $str = str_replace("&Ugrave;", "Ù", $str);
// $str = str_replace("&agrave;", "à", $str);
// $str = str_replace("&egrave;", "è", $str);
// $str = str_replace("&igrave;", "ì", $str);
// $str = str_replace("&ograve;", "ò", $str);
// $str = str_replace("&ugrave;", "ù", $str);
// $str = str_replace("&Aacute;", "Á", $str);
// $str = str_replace("&Eacute;", "É", $str);
// $str = str_replace("&Iacute;", "Í", $str);
// $str = str_replace("&Oacute;", "Ó", $str);
// $str = str_replace("&Uacute;", "Ú", $str);
// $str = str_replace("&Yacute;", "Ý", $str);
// $str = str_replace("&aacute;", "á", $str);
// $str = str_replace("&eacute;", "é", $str);
// $str = str_replace("&iacute;", "í", $str);
// $str = str_replace("&oacute;", "ó", $str);
// $str = str_replace("&uacute;", "ú", $str);
// $str = str_replace("&yacute;", "ý", $str);
// $str = str_replace("&Acirc;", "Â", $str);
// $str = str_replace("&Ecirc;", "Ê", $str);
// $str = str_replace("&Icirc;", "Î", $str);
// $str = str_replace("&Ocirc;", "Ô", $str);
// $str = str_replace("&Ucirc;", "Û", $str);
// $str = str_replace("&acirc;", "â", $str);
// $str = str_replace("&ecirc;", "ê", $str);
// $str = str_replace("&icirc;", "î", $str);
// $str = str_replace("&ocirc;", "ô", $str);
// $str = str_replace("&ucirc;", "û", $str);
// $str = str_replace("&Atilde;", "Ã", $str);
// $str = str_replace("&Ntilde;", "Ñ", $str);
// $str = str_replace("&Otilde;", "Õ", $str);
// $str = str_replace("&atilde;", "ã", $str);
// $str = str_replace("&ntilde;", "ñ", $str);
// $str = str_replace("&otilde;", "õ", $str);
// $str = str_replace("&Auml;", "Ä", $str);
// $str = str_replace("&Euml;", "Ë", $str);
// $str = str_replace("&Iuml;", "Ï", $str);
// $str = str_replace("&Ouml;", "Ö", $str);
// $str = str_replace("&Uuml;", "Ü", $str);
// $str = str_replace("&Yuml;", "Ÿ", $str);
// $str = str_replace("&auml;", "ä", $str);
// $str = str_replace("&euml;", "ë", $str);
// $str = str_replace("&iuml;", "ï", $str);
// $str = str_replace("&ouml;", "ö", $str);
// $str = str_replace("&uuml;", "ü", $str);
// $str = str_replace("&yuml;", "ÿ", $str);

  }
}

if (! function_exists("remove_non_utf8_char"))
{
  function remove_non_utf8_char($str = '')
  {
    //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
    $str = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
     '|[\x00-\x7F][\x80-\xBF]+'.
     '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
     '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
     '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
     '?', $str);

    //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
    $str = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
     '|\xED[\xA0-\xBF][\x80-\xBF]/S', '?', $str);

    return $str;
  }
}


if (! function_exists("furl"))
{
  function furl($str = '')
  {
    return mb_strtolower(url_title(remove_sign(trim($str))));
  }
}

if (! function_exists("recursive_category"))
{
  function recursive_category($source = array())
  {
    if (! is_array($source))
    {
      return FALSE;
    }

    // dung cho theme a2
    $html = '';
    foreach ($source as $item)
    {
      $html .= '<li data="'.$item['id'].'"><a href="'.site_url('danh-muc/'.$item['id'].'-'.furl($item['name'])).'">'.$item['name'].'</a>';
      if (isset($item['children'][0]))
      {

        $html .= '<ul class="nav nav-list subnav">'.
          recursive_category($item['children']).
        '</ul>';
      }
      else
      {
        $html .= '</li>';
      }
    }
    return $html;
  }
}

if (! function_exists("recursive_category_menu"))
{
  function recursive_category_menu($source = array())
  {
    if (! is_array($source))
    {
      return FALSE;
    }

    $html = '';
    foreach ($source as $item)
    {
      $link = (! empty($item['link'])) ? $item['link'] : site_url($item['seo_name']);
      $html .= '<li><a href="'.$link.'">'.$item['name'].'</a>';
      if (isset($item['children'][0]))
      {
        $html .= '<ul>'.
          recursive_category_menu($item['children']).
        '</ul>';
      }
      $html .= '</li>';
    }
    return $html;
  }
}

if (! function_exists("recursive_category_menu_mobile"))
{
  function recursive_category_menu_mobile($source = array())
  {
    if (! is_array($source))
    {
      return FALSE;
    }

    $html = '';
    foreach ($source as $item)
    {
      $link = (! empty($item['link'])) ? $item['link'] : site_url($item['seo_name']);
      $html .= '<div class="navigation-item">';
      if (isset($item['children'][0]))
      {
        $html .= '<a href="'.$link.'" class="nav-item submenu-deploy">'.$item['name'].'<em class="dropdown-item"></em></a>';
        $html .= '<div class="nav-submenu">';
        foreach ($item['children'] as $child)
        {
          $child_link = (! empty($child['link'])) ? $child['link'] : site_url($child['seo_name']);
          $html .= '<a href="'.$child_link.'">'.$child['name'].'<em class="unselected-item"></em></a>';
        }
        $html .= '</div>';
      }
      else
      {
        $html .= '<a href="'.$link.'" class="nav-item">'.$item['name'].'<em class="unselected-item"></em></a>';
      }
      $html .= '</div><div class="sidebar-decoration"></div>';
    }
    return $html;
  }
}

if ( ! function_exists('tn_date'))
{
  function tn_date($sqldate)
  {
    return date('d/m/Y', strtotime($sqldate));
  }
}

if ( ! function_exists('tn_datetime'))
{
  function tn_datetime($sqldate)
  {
    return date('d/m/Y H:i:s', strtotime($sqldate));
  }
}

if ( ! function_exists('tn_hide'))
{
  function tn_hide($str = '')
  {
    $length = mb_strlen($str, 'UTF-8');

    if ($length == 0)
      return '';
    if ($length <= 8)
      return str_repeat("*", $length);

    $star = str_repeat("*", $length-3);
    $head = mb_substr($str, 0, 1, 'UTF-8');
    $foot = mb_substr($str, -2, 2, 'UTF-8');
    return $head.$star.$foot;
  }
}

if ( ! function_exists('tn_limit'))
{
  function tn_limit($str = '', $limit = 110)
  {
    $length = mb_strlen($str, 'UTF-8');

    if ($length <= $limit)
      return $str;

    $str = mb_substr($str, 0, $limit, 'UTF-8');
    $str = preg_replace('/(\s+)(\S*)$/', '', $str);
    return $str.'...';
  }
}


if ( ! function_exists('tn_money_format'))
{
  function tn_money_format($number = 0)
  {
    return number_format($number, 0, ',', '.');
  }
}

if ( ! function_exists('tn_check_browser'))
{
  function tn_is_mobile()
  {
    $useragent = (!empty($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
// newline php trong html
if (! function_exists("newline_html"))
{
  function newline_html($str = '')
  {
    $str = str_replace("\r\n", "\n", $str);
    // $str = str_replace("\n\r", "\n", $str);
    $str = str_replace("\r", "", $str);
    $str = str_replace("\n", "<br>", $str);
    return $str;
  }
}
