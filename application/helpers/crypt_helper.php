<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists("sign_data"))
{
  function sign_data($data = '', $private_key)
  {
    // kí điện tử $data lưu vào $encrypted
    if (openssl_sign($data, $encrypted, $private_key))
    {
      // mã hóa base64 $encrypted
      return base64_encode($encrypted);
    }
    return '';
  }
}

if (! function_exists("verify_data"))
{
  function verify_data($data = '', $sign = '', $public_key)
  {
    if(openssl_verify($data, base64_decode($sign), $public_key))
      return true;
    return false;
  }
}

if (! function_exists("encrypt_triple_des"))
{
  function encrypt_triple_des($input, $key_seed)
  {
    $input = trim($input);
    $block = mcrypt_get_block_size('tripledes', 'ecb');
    $len = strlen($input);
    $padding = $block - ($len % $block);
    $input .= str_repeat(chr($padding), $padding);
    // generate a 24 byte key from the md5 of the seed
    $key = substr(md5($key_seed), 0, 24);
    $iv_size = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    // encrypt
    $encrypted_data = mcrypt_encrypt(MCRYPT_TRIPLEDES, $key, $input,
    MCRYPT_MODE_ECB, $iv);
    // clean up output and return base64 encoded
    return base64_encode($encrypted_data);
  }
}

if (! function_exists("decrypt_triple_des"))
{
  function decrypt_triple_des($input, $key_seed)
  {
    $input = base64_decode($input);
    $key = substr(md5($key_seed), 0, 24);
    $text = mcrypt_decrypt(MCRYPT_TRIPLEDES, $key, $input, MCRYPT_MODE_ECB, '12345678');
    $block = mcrypt_get_block_size('tripledes', 'ecb');
    $packing = ord($text{strlen($text) - 1});
    if($packing and ($packing < $block))
    {
      for($P = strlen($text) - 1; $P >= strlen($text) - $packing; $P--)
      {
        if(ord($text{$P}) != $packing)
         $packing = 0;
      }
    }
    $text = substr($text, 0, strlen($text) - $packing);
    return $text;
  }
}