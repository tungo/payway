<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// sort button
if ( ! function_exists('cms_sort'))
{
  function cms_sort($name = '', $value = '', $filter = array(), $options = array())
  {
    if ($filter['sort'] == $value)
    {
      $html = '<a onclick="return sort_list(\''.$options['link'].'\', \''.$value.'\');" title="Sort">'.$name.$options['image'].'</a>';
    }
    else
    {
      $html = '<a onclick="return sort_list(\''.$options['link'].'\', \''.$value.'\');" title="Sort">'.$name.'</a>';
    }
    return $html;
  }
}

// // fckeditor
// if ( ! function_exists('cms_editor'))
// {
//  function cms_editor($name = '', $value, $options = array())
//  {
//    require_once (SCRIPTSPATH . 'fckeditor269/fckeditor_php5.php');
//    $oFCKeditor = new FCKeditor($name);
//    $oFCKeditor->BasePath = SCRIPTSURL . 'fckeditor269/';
//    $oFCKeditor->Value = $value;

//    $oFCKeditor->Width = (isset($options['width'])) ? $options['width'] : '100%';
//    $oFCKeditor->Height = (isset($options['height'])) ? $options['height'] : '320';
//    $oFCKeditor->ToolbarSet = (isset($options['toolbar'])) ? $options['toolbar'] : 'Default';
//    $oFCKeditor->Config['DefaultLanguage'] = (isset($options['DefaultLanguage'])) ? $options['DefaultLanguage'] : 'en';

//    return $oFCKeditor->Create();
//  }
// }

// CKEditor
// if ( ! function_exists('cms_editor'))
// {
//  function cms_editor($inputs = array(), $options = array())
//  {
//    $html = '<script src="'.SCRIPTSURL.'ckeditor432/ckeditor.js"></script>';
//    $html .= '<script src="'.SCRIPTSURL.'ckfinder241/ckfinder.js"></script>';

//    $html .= '<script>';
//    foreach ($inputs as $i => $input)
//    {
//      $html .= "var editor_{$i} = CKEDITOR.replace('{$input}');";
//      $html .= "CKFinder.setupCKEditor(editor_{$i}, '".SCRIPTSURL."ckfinder241/');";
//    }
//    $html .= '</script>';

//    return $html;
//  }
// }

// TinyMCE 4
if ( ! function_exists('cms_editor'))
{
  function cms_editor($inputs = array(), $options = array())
  {
    $html = '<script>';
    foreach ($inputs as $i => $input)
    {
      $html .= 'tinymce.init({
        selector: "'.$input.'",
        fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "styleselect | fontselect | fontsizeselect | print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        height : 300
      });';
    }
    $html .= '</script>';

    return $html;
  }
}

// Replace string for fckeditor
if ( ! function_exists('cms_replace_string'))
{
  function cms_replace_string($string = '', $options = array())
  {
    $str = str_replace('\"', '"', $string);
    $str = str_replace("\'", "'", $str);
    return $str;
  }
}
