<?php
// ---------- ADMIN ----------

// trang thai giao dich
$lang['atrans_st1']  = 'Khởi tạo';
$lang['atrans_st2']  = 'Thanh toán thành công';
$lang['atrans_st3']  = 'Thanh toán thất bại';
$lang['atrans_st4']  = 'Xử lý thành công';
$lang['atrans_st5']  = 'Xử lý thất bại';
$lang['atrans_st6']  = 'Xác nhận thành công';
$lang['atrans_st7']  = 'Xác nhận thất bại';
$lang['atrans_st8']  = 'Hoàn tiền thành công';
$lang['atrans_st9']  = 'Hoàn tiền thất bại';
$lang['atrans_st10'] = 'Không xác định';
$lang['atrans_st11'] = 'Lỗi';

// phan quyen nhom nguoi dung
$lang['blog']                        = 'Blog';
$lang['blog_article view']           = 'View article';
$lang['blog_article manage']         = 'Manage article';
$lang['blog_category view']          = 'View category';
$lang['blog_category manage']        = 'Manage category';
$lang['user']                        = 'User';
$lang['user_account view']           = 'View account';
$lang['user_account manage']         = 'Manage account';
$lang['user_group view']             = 'View group';
$lang['user_group manage']           = 'Manage group';
$lang['administration']              = 'Administration';
$lang['module_partner view']         = 'View partner';
$lang['module_partner manage']       = 'Manage partner';
$lang['website_contact view']        = 'View contact';
$lang['website_contact manage']      = 'Manage contact';
$lang['payment']                     = 'Payment';
$lang['payment_transaction view']    = 'View transaction';
$lang['payment_transaction manage']  = 'Manage transaction';
$lang['payment_transaction balance'] = 'Balance transaction';


// ---------- SITE ----------

// tu thong dung
$lang['chao'] 			= 'Welcome';
$lang['trang-chu']    		= 'Trang Chủ';
$lang['thong-tin']    		= 'Thông Tin';
$lang['ve-chung-toi'] 		= 'Về Chúng Tôi';
$lang['gioi-thieu']   		= 'Giới Thiệu';
$lang['dich-vu']      		= 'Dịch Vụ';
$lang['tin-tuc']      		= 'Tin Tức';
$lang['ho-tro']       		= 'Hỗ Trợ';
$lang['lien-he']      		= 'Liên Hệ';
$lang['hotline']      		= 'Hotline';
$lang['dang-nhap']    		= 'Đăng Nhập';
$lang['dang-ky']      		= 'Đăng Ký';
$lang['danh-muc']     		= 'Danh Mục';
$lang['thanh-vien']   		= 'Thành Viên';
$lang['doi-tac']   	  	= 'ĐỐI TÁC';
$lang['email']   	  	= 'Email';
$lang['password']     		= 'Mật Khẩu';
$lang['tai-khoan']    		= 'Tài Khoản';
$lang['thoat']   		= 'Thoát';
$lang['nhap-lai']   		= 'Nhập Lại';
$lang['ten']   		  	= 'Tên';
$lang['ho']   		  	= 'Họ';
$lang['sua-thong-tin-tai-khoan']= 'Sửa Thông Tin Tài Khoản';
$lang['doi-mat-khau'] 		= 'Đổi Mật Khẩu';
$lang['lich-su-giao-dich']   	= 'Lịch Sử Giao Dịch';
$lang['nhap-lai-mat-khau']   	= 'Nhập Lại Mật Khẩu';
$lang['ngay-sinh']   		= 'Ngày Sinh';
$lang['dia-chi']        	= 'Địa Chỉ';
$lang['xem-chi-tiet'] 		= 'Xem Chi Tiết';
$lang['ngon-ngu'] 		= 'Ngôn Ngữ';
$lang['tong-so'] 		= 'Tổng số';
$lang['thanh-toan'] 		= 'Thanh Toán';
$lang['ngay'] 			= 'Ngày';
$lang['thanh-cong'] = 'Thành công';
$lang['that-bai'] = 'Thất bại';

// thong tin
$lang['dia-chi-1']        	= ' 401 Huỳnh Văn Bánh, Phường 11, Quận Phú Nhuận, Tp.HCM';
$lang['footer-contact']  	= 'Công ty Cổ Phần Nhất Phương<br>
Trụ sở: 154/15 Nguyễn Trọng Tuyển, P8, Q.Phú Nhuận<br>
ĐC giao dịch: 401 Huỳnh Văn Bánh, P.11, Q.Phú Nhuận<br>MST: 0305221500<br>
Số ĐKKD 4103007887 do SKH& ĐT cấp ngày 22/9/2007 <br>
Giấy phép số 973/ GP-HCM do Ngân Hàng Nhà Nước CN Tp.HCM cấp ngày 04/07/2014<br>HOTLINE: (84-8)- 6686 5378 / 0919 89 19 09<br>EMAIL: lienhe.payway@gmail.com - WEBSITE: www.payway.vn';
$lang['footer-aboutus']   	= 'Pay Way Chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ vào tài khoản ngân hàng<br>Nhận tiền trên 5000 điểm tại 63 tỉnh và Phương thức Thanh toán hóa đơn đa dạng... <a href="gioi-thieu/ve-chung-toi.html">(Xem thêm)</a>';
$lang['footer-copyright'] 	= '© 2013 Payway. All Rights Reserved.';

// banner 1
$lang['B1-S1'] = 'MONEY TRANSFER';
$lang['B1-S2'] = 'Chuyển Tiền Toàn Cầu Về Việt Nam.<br>AN TOÀN - TIỆN LỢI - NHANH CHÓNG';
$lang['B1-S3'] = 'Nhận Tiền Vào Tài Khoản';
$lang['B1-S4'] = 'Nhận Tiền Tại Nhà';
$lang['B1-S5'] = 'Nhận Tiền Tại Quầy';
// banner 2
$lang['B2-S1'] = 'BILL COLLECTION - PAY @ POST';
$lang['B2-S2'] = 'THANH TOÁN HÓA ĐƠN</strong><br>TẠI TRÊN <strong style="font-size:40px;color:#ff0;line-height: 65px;">5000</strong> BƯU CỤC TOÀN QUỐC';
$lang['B2-S3'] = 'Giải Pháp Thanh Toán Tại Bưu Điện Cho Khách Hàng Của Bạn';
// banner 3
$lang['B3-S1'] = 'PAYMENT GATEWAY';
$lang['B3-S2'] = 'THANH TOÁN HÓA ĐƠN TRỰC TUYẾN<br>AN TOÀN - TIỆN LỢI - NHANH NHẤT';
$lang['B3-S3'] = 'Nạp Tiền Điện Thoại';
$lang['B3-S4'] = 'Điện Thoại Trả Sau';
$lang['B3-S5'] = 'Hoa Đơn Tiền Điện';
$lang['B3-S6'] = 'Thanh Toán ADSL';
$lang['B3-S7'] = 'Mua Vé Máy Bay';
$lang['B3-S8'] = 'Thẻ Game / Thẻ Điện Thoại';

// contact
$lang['contact-ghi-chu'] 	= 'Vui lòng nhập thông tin liên hệ với chúng tôi theo mẫu bên dưới.';
$lang['contact-info'] 		= ' <strong>Địa chỉ: </strong>401 Huỳnh Văn Bánh, Phường 11, Quận Phú Nhuận, TpHCM<br><strong>MST:</strong> 0305221500<br>
Số ĐKKD 4103007887 do SKH& ĐT cấp ngày 22/9/2007 <br>
Giấy phép số 973/ GP-HCM do Ngân Hàng Nhà Nước CN Tp.HCM cấp ngày 04/07/2014<br>
<strong>HOTLINE: </strong>(84-8)- 6686 5378 / 0919 89 19 09<br><strong>FAX: </strong>08 629 09 340<br><strong>Email: </strong>lienhe.payway@gmail.com<br><strong>Website:</strong> www.payway.vn<br>';

// error 404
$lang['error-404-info'] 	= 'PAGE NOT FOUND. <small>The page you are looking for might have been removed, had its name changed.</small>';
$lang['ve-trang-chu']   	= 'Back To Homepage';

// tu khac
$lang['chap-nhan-thanh-toan']   = 'Chấp Nhận Thanh Toán';
$lang['nha-cung-cap-dich-vu']   = 'Nhà Cung Cấp Dịch Vụ';
$lang['payway-hoat-dong-nhu-the-nao'] 		= 'How Pay Way Works?';
$lang['video']                        		= 'Video Galary';
$lang['huong-dan-su-dung-dich-vu-nhanh'] 	= 'HƯỚNG DẪN DỊCH VỤ';
$lang['cach-chuyen-tien']                	= 'CÁCH CHUYỂN TIỀN';
$lang['cach-nhan-tien']                  	= 'CÁCH NHẬN TIỀN';
$lang['thanh-toan-hoa-don-online']       	= 'THANH TOÁN HÓA ĐƠN ONLINE';
$lang['thanh-toan-hoa-don-offline']      	= 'THANH TOÁN HÓA ĐƠN OFFLINE';
$lang['bieu-phi-dich-vu']  			= 'BIỂU PHÍ DỊCH VỤ';
$lang['dia-diem-chuyen-tien']  			= 'ĐỊA ĐIỂM CHUYỂN TIỀN';
$lang['dia-diem-nhan-tien'] 			= 'ĐỊA ĐIỂM NHẬN TIỀN';
$lang['dieu-khoan-bao-mat']    			= 'Điều Khoản Bảo Mật';
$lang['cau-hoi-thuong-gap']    			= 'Câu Hỏi Thường Gặp';
$lang['chinh-sach-quy-dinh']     		= 'Chính Sách Quy Định';
$lang['quy-trinh-giao-dich']   			= 'Quy Trình Giao Dịch';
$lang['huong-dan-nghiep-vu']   			= 'Hướng Dẫn Nghiệp Vụ';
$lang['huong-dan-su-dung']   			= 'Hướng Dẫn Dịch Vụ Kiều Hối';
$lang['ty-gia-ngoai-te']           		= 'Tỷ Giá Ngoại Tệ';
$lang['ho-tro-truc-tuyen'] 			= 'Hỗ trợ trực tuyến';
$lang['cac-bai-viet-khac'] 			= 'Các Bài Viết Khác';
$lang['chua-co-thong-tin'] 			= 'CHƯA CÓ THÔNG TIN';
$lang['xem-them']          			= 'Xem thêm &gt;';
$lang['nhan-email'] 				= 'Nhập địa chỉ email của bạn để nhận thông tin từ website';
$lang['ket-qua-tim-kiem'] 			= "Kết Quả Tìm Kiếm";

// dang ky tai khoan
$lang['chung-minh-thu'] 			= 'Chứng Minh Thư';
$lang['agreement']      			= 'Xác nhận đăng ký đồng nghĩa với việc bạn đã chấp nhận quy định của chúng tôi!';
$lang['error-email-da-su-dung']  		= 'Email đã được sử dụng.';
$lang['dang-ky-thanh-cong']      		= 'Đăng ký thành công.';
$lang['dang-ky-that-bai']        		= 'Đăng ký thất bại.';
$lang['sai-thong-tin-dang-nhap'] 		= 'Email hoặc mật khẩu không chính xác.';
$lang['quen-mat-khau']				= 'Quên Mật Khẩu?';
$lang['khong-phai-thanh-vien']			= 'Bạn không phải là thành viên?';
$lang['tao-tai-khoan-moi']			= 'Tạo một tài khoản mới dễ dàng và mất ít hơn một phút.';
$lang['dang-ky-tai-khoan']			= 'Đăng ký tài khoản mới';
$lang['thong-tin-tai-khoan']    		= 'Thông Tin Tài Khoản';
$lang['thong-tin-chung']    			= 'Thông Tin Chung';
$lang['thong-tin-them']    			= 'Thông Tin Thêm';
$lang['thong-tin-bao-mat']    			= 'Thông Tin Bảo Mật';
$lang['gioi-tinh']    				= 'Giới Tính';
$lang['nam']    				= 'Nam';
$lang['nu']    					= 'Nữ';
$lang['khac']    				= 'Khác';
$lang['chon']    				= 'Chọn';
$lang['quoc-gia']   			 	= 'Quốc Gia';
$lang['post-code']    				= 'Postal Code';
$lang['so-dien-thoai']    			= 'Số Điện Thoại';
$lang['ma-xac-nhan']    			= 'Mã Xác Nhận';
$lang['cau-hoi-bao-mat']    			= 'Câu Hỏi Bảo Mật';
$lang['cau-tra-loi']    			= 'Câu Trả Lời';

// lich su giao dich 
$lang['ma-giao-dich']    			= 'Mã Giao Dịch';
$lang['thoi-gian']    				= 'Thời Gian';
$lang['so-tien']    				= 'Số Tiền';
$lang['trang-thai']    				= 'Trạng Thái';

// doi mat khau
$lang['mat-khau-cu']    			= 'Mật Khẩu Cũ';
$lang['mat-khau-moi']    			= 'Mật Khẩu Mới';
$lang['xac-nhan-mat-khau']    			= 'Xác Nhận Mật Khẩu';
$lang['luu']   					= 'Lưu';
$lang['tim-kiem']    				= 'Tìm kiếm';
$lang['tu-ngay']    				= 'Từ Ngày';
$lang['den-ngay']    				= 'Đến Ngày';

// dang ky 
$lang['loi-ich-dang-ky']    			= 'Lợi Ích Khi Bạn Đăng Ký Thành Viên';
$lang['kiem-tra-giao-dich']    			= 'Kiểm Tra Lịch Sử Giao Dịch Bất Cứ Lúc Nào';
$lang['su-dung-dich-vu']    			= 'Sử Dụng Các Dịch Vụ Của PAYWAY';
$lang['ban-da-co-tai-khoan']    		= 'Bạn Đã Có Tài Khoản Vui Lòng';
$lang['bam-vao-day']    			= 'Bấm Vào Đây';
$lang['date-ngay']  				='Ngày';
$lang['date-thang']  				='Tháng';
$lang['date-nam']  				='Năm';

// bat loi
$lang['ngay-sinh-khong-hop-le']  		='Ngày Sinh Không Hợp Lệ';
$lang['sai-ma-xac-nhan']  			='Mã xác nhận không chính xác';
$lang['thanh-pho'] 				= 'Thành Phố';
$lang['doi-mat-khau-that-bai'] 			= 'Đổi Mật Khẩu Thất Bại';
$lang['doi-mat-khau-thanh-cong'] 		= 'Đổi Mật Khẩu Thành Công';
$lang['sai-mat-khau-cu'] 			= 'Sai Mật Khẩu Cũ';
$lang['sua-thong-tin-thanh-cong'] 		= 'Sửa Thông Tin Thành Công';
$lang['sua-thong-tin-that-bai'] 		= 'Sửa Thông Tin Thất Bại';
$lang['doi-tac-dau-tu'] 			= 'Đối Tác - Đầu Tư';
$lang['noi-dung-doi-tac-dau-tu'] 		= 'Lợi ích khi hợp tác cùng Pay Way';
$lang['mail-tu-payway'] 			= 'Nhận Mail Từ PAYWAY';
$lang['ten-newsletter'] 			= 'Tên';
$lang['mail-newsletter'] 			= 'Email';
$lang['theo-doi'] 				= 'Xác Nhận';
$lang['lay-lai-mat-khau'] 			= 'Lấy Lại Mật Khẩu';
$lang['email-chua-duoc-dang-ky'] 		= 'Email Chưa Được Đăng Ký';
$lang['lay-lai-mat-khau-thanh-cong'] 		= 'Lấy Lại Mật Khẩu Thành Công Vui Lòng Kiểm Tra Email Của Bạn.';
$lang['lay-lai-mat-khau-that-bai'] 		= 'Lấy Lại Mật Khẩu Thất Bại';
$lang['email-tao-moi-mat-khau'] 		= 'Bạn hoặc ai đó đã yêu cầu tạo mới mật khẩu trên website payway.vn.
Nếu bạn chưa từng thực hiện yêu cầu này vui lòng bỏ qua email.

Tài khoản của bạn: [email]
Vào đây để tạo mật khẩu: [link]

Xin cám ơn.';
$lang['tieu-de-email-tao-moi-mat-khau'] 	= 'Tạo mới mật khẩu trang payway.vn';
$lang['dai-ly'] 				= 'ĐẠI LÝ';
$lang['nha-dau-tu'] 				= 'NHÀ ĐẦU TƯ';
$lang['ho-ten'] 				= 'Họ & Tên';
$lang['thuoc-don-vi'] 				= 'Thuộc Đơn Vị';
$lang['nganh-nghe-kinh-doanh'] 			= 'Ngành Nghề Kinh Doanh';
$lang['chuc-vu-hien-tai'] 			= 'Chức Vụ Hiện Tại';
$lang['noi-dung-can-trao-doi'] 			= 'Nội Dung Cần Trao Đổi';
$lang['gui'] 					= 'Gửi';
$lang['thong-bao'] 				= 'Thông Báo';

// gioi thieu 
$lang['gioi-thieu-noi-dung-ngan'] 		= 'Công ty Cổ Phần Nhất Phương, tên quốc tế là Best Way Corp, viết tắt là Eway, tiền thân là Công ty tư vấn và quản lý King Horse. Ra đời từ 2007, để thành công trong một môi trường kinh doanh đặc trưng và liên tục thay đổi, Eway tự hào là một trong những nhà tiên phong hoạt động TMĐT thành công tại Việt Nam. Điều quan trọng để luôn luôn ghi nhớ rằng uy tín đã tạo cho chúng tôi các giá trị đặc biệt. Hành động phù hợp với thị trường toàn cầu sẽ giữ chúng tôi đi đúng hướng, và sẽ tiếp tục gia tăng sức mạnh của chúng tôi.';
$lang['gioi-thieu-dich-vu-kieu-hoi'] = 'Pay Way là nhà cung cấp dịch vụ nhận tiền kiều hối chuyên nghiệp, đã hợp tác với nhiều tập đoàn chuyển tiền toàn cầu từ trên 150 quốc gia và vùng lãnh thổ.';
$lang['gioi-thieu-dich-vu-thanh-toan-online'] 	= 'Thanh toán trực tuyến một cách nhanh chóng, tiện lợi như hóa đơn tiền điện, nước, di động trả sau, mua thẻ game, điện thoại, vé máy bay,… ';
$lang['gioi-thieu-dich-vu-thanh-toan-offline'] 	= 'Pay Way mang đến thêm một kênh thanh toán cho khách hàng của đối tác là thanh toán tại bưu điện. Tốc độ thanh toán rất nhanh, an toàn do không cần kết nối.';
$lang['gioi-thieu-dich-vu-khac'] 		= 'ĐANG HOÀN THIỆN';

// dich vu 
$lang['dich-vu-kieu-hoi']   			= 'Dịch vụ chuyển tiền';
$lang['thanh-toan-online']  			= 'Thanh toán online';
$lang['thanh-toan-offline'] 			= 'Thanh toán offline';
$lang['chuyen-tien-dien-thoai'] 		= 'Chuyển Tiền Điện Thoại';
$lang['dich-vu-khac']       			= 'Dịch vụ khác';
$lang['mua-the-game'] 				= 'Mua Thẻ Game';
$lang['thanh-toan-hoa-don'] 			= 'Thanh Toán Hoá Đơn';
$lang['di-dong-tra-truoc'] 			= 'Di Động Trả Trước';
$lang['mua-the-dien-thoai'] 			= 'Mua Thẻ Điện Thoại';
$lang['di-dong-tra-sau'] 			= 'Di Động Trả Sau';
$lang['nap-tien-tai-khoan'] 			= 'Nạp Tiền Tài Khoản';
$lang['nap-tien-game'] 				= 'Nạp Tiền Game';
$lang['ve-may-bay'] 				= 'Vé Máy Bay';
$lang['hoa-don-adsl'] 				= 'Hóa Đơn ADSL';
$lang['tien-dien'] 				= 'Tiền Điện';

// noi dung dich vu 
$lang['noi-dung-mua-the-game'] 			= 'Mua thẻ game Vcoin, Zing Xu, Thẻ Sò, Thẻ Gate, Thẻ Oncash...';
$lang['noi-dung-di-dong-tra-truoc'] 		= 'Nạp tiền di động online cho, Vinaphone, Mobifone, Viettel...';
$lang['noi-dung-mua-the-dien-thoai'] 		= 'Mua thẻ điện thoại cho Vinaphone, Mobifone, Viettel...';
$lang['noi-dung-di-dong-tra-sau'] 		= 'Thanh toán tiền di động trả sau cho nhà cung cấp Mobifone.';
$lang['noi-dung-nap-tien-tai-khoan'] 		= 'Nạp tiền vào tài khoản, hocmai.vn, Elean, Tiếng Anh Ibest..';
$lang['noi-dung-nap-tien-game'] 		= 'Nạp tiền Vcoin, Zing Xu, GG, Bạc, Scoin, @Point vào tài khoản. ';
$lang['noi-dung-tien-dien'] 		= 'Tương tác linh hoạt, kết nối chặt chẽ giữa người nhà cung ứng và người dùng.';
$lang['noi-dung-hoa-don-adsl'] 		= 'Thanh toán tiền internet adsl của Viettel, VNPT, FPT.';
$lang['noi-dung-ve-may-bay'] 		= 'Mua vé trực tuyến với 3 hãng hàng không quốc nội và 27 hãng hàng không quốc tế.';

// dich vu homepage
$lang['home-info-1'] = 'Đăng ký trong 10 giây với bước thao tác thân thiện nhất.';
$lang['home-info-2'] = 'Quy trình tích hợp đơn giản với 1 bước, chỉ trong 3 phút.';
$lang['home-info-3'] = 'Kết nối trực tuyến với 3 hãng hàng không quốc nội và 27 hãng hàng không quốc tế.';
$lang['home-info-4'] = 'Bảo mật hoàn hảo mọi tài khoản và giao dịch theo tiêu chuẩn bảo mật thông tin.';

// dich vu thanh toan 
$lang['nha-cung-cap'] 	= 'Nhà Cung Cấp';
$lang['so-di-dong'] 	= 'Số Di Động';
$lang['menh-gia'] 	= 'Mệnh Giá';
$lang['thanh-tien'] 	= 'Thành Tiền';
$lang['ngan-hang'] 	= 'Ngân Hàng';
$lang['so-luong'] 	= 'Số Lượng';
$lang['ma-the'] 	= 'Mã Thẻ';
$lang['so-seri'] 	= 'Số Seri';
$lang['han-su-dung'] 	= 'Hạn Sử Dụng';

//bat loi dich vu di dong tra truoc
$lang['error-so-du-tai-khoan-1'] 		= 'Dịch vụ đang bảo trì. Vui lòng quay lại sau vài phút, Xin lỗi về sự bất tiện này.';
$lang['error-di-dong-1'] 		= 'Số Điện Thoại Không Đúng';
$lang['error-nha-cung-cap-1'] 		= 'Nhà Cung Cấp Không Đúng';
$lang['error-dau-so-1'] 		= 'Đầu Số Không Đúng';
$lang['error-menh-gia-1'] 		= 'Mệnh Giá Không Đúng';
$lang['error-cong-thanh-toan-1'] 	= 'Lỗi thực hiện giao dịch tại cổng thanh toán.';
$lang['noti-fail-1'] 			= 'Thực hiện giao dịch thất bại. Vui lòng thực hiện lại quá trình thanh toán.';
$lang['noti-success-1'] 		= 'Thực hiện giao dịch thành công. Xin cám ơn bạn đã sử dụng dịch vụ PayWay.';
$lang['error-so-luong-1'] 		= 'Số Lượng Không Đúng';
$lang['error-tai-khoan-1'] 		= 'Tài Khoản Không Đúng';
$lang['error-so-luong-2'] 		= 'Vui lòng nhập số lượng không quá 10.000';
$lang['error-menh-gia-2'] 		= 'Mệnh giá nạp phải từ 6.000 đến 1.000.000 và phải chẵn nghìn';
$lang['error-so-luong-the-1']   = 'Số lượng thẻ không đủ để thực hiện giao dịch. Vui lòng chọn mệnh giá khác';

$lang['giao-dich-thanh-cong'] 	= 'Giao Dịch Thành Công';
$lang['giao-dich-that-bai'] 	= 'Giao Dịch Thất Bại';

$lang['thanh-toan-thanh-cong'] = 'Thanh Toán Thành Công';
$lang['thanh-toan-that-bai'] = 'Thanh Toán Thất Bại';

$lang['xu-ly-giao-dich'] = 'Xử Lý Giao Dịch';
$lang['xu-ly-that-bai'] = 'Xử Lý Giao Dịch Thất Bại';

$lang['noi-dung-xu-ly-giao-dich'] = 'Vui lòng chờ xử lý giao dịch';
$lang['noi-dung-xu-ly-that-bai'] = 'Xử Lý Giao Dịch Thất Bại. Vui lòng thử lại';
//
$lang['giao-dich-loi'] 	= 'Giao Dịch Lỗi';
$lang['xin-thu-lai'] 	= 'Vui lòng thực hiện lại giao dịch.';

//thong bao loi
$lang['khong-tim-thay-giao-dich'] 			= 'Không tìm thấy giao dịch';
$lang['khong-the-kiem-tra-trang-thai-giao-dich'] 	= 'không thể kiểm tra trạng thái giao dịch';
$lang['khong-dung-trang-giao-dich'] 			= 'Không đúng trạng thái giao dịch';


$lang['chi-tiet-giao-dich'] 	= 'Chi Tiết Giao Dịch';
$lang['thong-tin-giao-dich'] 	= 'Thông Tin Giao Dịch';
$lang['thong-tin-dich-vu'] 	= 'Thông Tin Dịch Vụ';
$lang['ket-qua-thanh-toan'] = 'Kết Quả Thanh Toán';

// cac loai dich vu
$lang['PW1101'] = 'Di động trả trước';
$lang['PW1102'] = 'Di động trả sau';
$lang['PW1103'] = 'Mua thẻ điện thoại';
$lang['PW1104'] = 'Mua thẻ game';
$lang['PW1105'] = 'Nạp tiền game';
$lang['PW1106'] = 'Nạp tiền tài khoản';

// trang thai giao dich
$lang['trans_st1']  = 'Khởi tạo';
$lang['trans_st2']  = 'Thất bại';
$lang['trans_st3']  = 'Thất bại';
$lang['trans_st4']  = 'Hoàn tất';
$lang['trans_st5']  = 'Thất bại';
$lang['trans_st6']  = 'Hoàn tất';
$lang['trans_st7']  = 'Hoàn tất';
$lang['trans_st8']  = 'Đã hoàn tiền';
$lang['trans_st9']  = 'Hoàn tiền thất bại';
$lang['trans_st10'] = 'Không xác định';
$lang['trans_st11'] = 'Lỗi';

// trang thai giao dich cho thanh vien xem
$lang['trans_member_st1']  = 'Khởi tạo';
$lang['trans_member_st2']  = 'Hoàn tất';
$lang['trans_member_st3']  = 'Thất bại';
$lang['trans_member_st4']  = 'Đã hoàn tiền';
$lang['trans_member_st5']  = 'Hoàn tiền thất bại';
$lang['trans_member_st6']  = 'Lỗi';

//ho tro 
$lang['ho-tro-cau-hoi-thuong-gap'] 	= 'Pay Way là gì?<br>Làm thế nào để tìm địa điểm chuyển tiền của Pay Way/ Đối tác của Pay Way?<br>Đối tượng sử dụng dịch vụ của Pay Way?';
$lang['ho-tro-huong-dan-nghiep-vu'] 	= '1. SMS Banking<br>2. Ứng dụng Mobile Banking<br>3. Thanh toán hóa đơn qua ngân hàng<br>';
$lang['ho-tro-ty-gia-ngoai-te'] 	= 'Tỷ giá ngoại tệ USD, AUD, HKD, EUR, JPY, RUB, THB.... Được cập nhật theo tỷ giá của ngân hàng Vietcombank';

// ty gia ngoai te
$lang['ma-ngoai-te'] 			= 'Mã Ngoại Tệ';
$lang['ten-ngoai-te'] 			= 'Tên Ngoại Tệ';
$lang['mua-ngoai-te'] 			= 'Mua Tiền Mặt';
$lang['chuyen-ngoai-te'] 		= 'Chuyển Khoản';
$lang['ban-ngoai-te'] 			= 'Bán';
$lang['ty-gia-ngoai-te-vietcombank'] 	= 'Tỷ giá ngoại tệ Vietcombank';

//dich vu chuyen tien 
$lang['noi-dung-dich-vu-kieu-hoi'] = 'Pay Way cung cấp dịch vụ nhận tiền kiều hối chuyên nghiệp thông qua việc hợp tác với nhiều công ty chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ. Dịch vụ nhanh chóng, an toàn và nhiều sự chọn lựa hoàn hảo nhất.';
$lang['dich-vu-kieu-hoi-nhan-tai-quay'] = 'Nhận tiền tại quầy';
$lang['dich-vu-kieu-hoi-nhan-vao-tai-khoan'] = 'Chuyển tiền vào tài khoản / Thẻ';
$lang['dich-vu-kieu-hoi-nhan-tai-nha'] = 'Nhận tiền tại nhà';
$lang['dich-vu-kieu-hoi-nhan-bang-dien-thoai'] = 'Nhận tiền bằng điện thoại';

$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay'] = 'Nhận tiền một cách dễ dàng và thuận tiện chỉ trong vài phút tại trên 5,000 điểm chi trả của Bưu điện Việt Nam và hệ thống ngân hàng trên toàn quốc.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan'] = 'Quý khách có tài khoản ngân hàng hoặc thẻ trong thành viên ngân hàng liên kết với Pay Way (Xem danh sách – Hầu hết tất cả các ngân hàng tại Việt Nam với trên 50 triệu tài khoản và trên 30 triệu thẻ), chuyển tiền về Việt Nam là thời gian thật, tiền sẽ báo có ngay khi người gửi xác nhận giao dịch. Khi có yêu cầu, hệ thống tin nhắn SMS của chúng tôi sẽ thông báo cho quý vị về giao dịch này.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha'] = 'Nhân viên phụ trách dịch vụ chuyển tiền tận nhà của chúng tôi hoặc của đối tác sẽ liên lạc với người thụ hưởng để hẹn trước việc giao dịch. Nhân viên của chúng tôi sẽ xác thực giấy tờ tùy thân hợp lệ và các thông tin về giao dịch này. Quý khách kí xác nhận và không phải trả bất kì thêm phí giao dịch nào. (ngoại trừ giao dịch có yêu cầu đặc biệt)';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai'] = 'Chuyển tiền bằng điện thoại là dịch vụ chuyển tiền trong tương lai vì ưu điểm nhanh, thuận tiện, an toàn, minh bạch và ít tốn kém. Nền tảng của Pay Way cho phép giao dịch kiều hối linh hoạt với chi phí thấp và nó mang đến cơ hội tiềm năng mới trong kinh doanh.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-1'] = 'Quý khách có thể nhận tiền từ trụ sở, chi nhánh của Pay Way và tại mạng lưới trên toàn quốc của đối tác Pay Way là Bưu điện và ngân hàng';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-2'] = 'Quý khách mang theo giấy tờ hợp lệ (CMND hay Passport), biết được số tiền dự kiến nhận và mã số cuộc chuyển tiền hay các thông tin cần thiết khác.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-3'] = 'Hoàn tất mẫu nhận tiền có thông tin của quý khách và người gửi. Như Tên người gửi, số tiền, nước gửi, mã số chuyển tiền, giấy tờ tùy thân người thụ hưởng hay các thông tin cần thiết và Cung cấp cho nhân viên của chúng tôi.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-4'] = 'Quý khách sẽ nhận được tiền trong vòng 5-15 phút khi mọi thông tin mà Quý vị cung cấp là chính xác. Hệ thống chăm sóc khách hàng của chúng tôi và/ hoặc của đối tác sẽ thông báo đến người gửi về việc hoàn tất giao dịch này.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-1'] = 'Đến tại công ty, chi nhánh, đại lý là đối tác của Pay Way trên toàn thế giới để thực hiện việc chuyển tiền. Hay Quý khách cũng có thể chuyển trực tiếp từ tài khoản, thẻ, điện thoại (tùy theo nước/ vùng/ đối tác)';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-2'] = 'Pay Way cung cấp dịch vụ nhận tiền kiều hối chuyên nghiệp thông qua việc hợp tác với nhiều công ty chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ. Và chi trả trên toàn lãnh thổ Việt Nam. ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-3'] = 'Pay Way đã tích hợp với mạng lưới bưu điện và hầu hết các ngân hàng trong nước với trên 50 triệu tài khoản và trên 30 triệu thẻ cho việc chuyển tiền kiều hối theo thời gian thật.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-4'] = 'Số tiền sẽ được chuyển trực tiếp vào tài khoản hay thẻ của người thụ hưởng chỉ trong giây lác ngay khi người gửi vừa hoàn tất giao dịch. Hệ thống chăm sóc khách hàng của chúng tôi và/ hoặc của đối tác sẽ thông báo đến người gửi về việc hoàn tất giao dịch này.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-1'] = 'Đến tại công ty, chi nhánh, đại lý là đối tác của Pay Way trên toàn thế giới để thực hiện việc chuyển tiền. Hay Quý khách cũng có thể chuyển trực tiếp từ tài khoản, thẻ, điện thoại (tùy theo nước/ vùng/ đối tác)';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-2'] = 'Pay Way cung cấp dịch vụ nhận tiền kiều hối chuyên nghiệp thông qua việc hợp tác với nhiều công ty chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ. Và chi trả trên toàn lãnh thổ Việt Nam. ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-3'] = 'Pay Way hợp tác với các các công ty giao nhận chuyển tiền trên toàn quốc tại 63 tỉnh thành. Thời gian giao nhận tùy thuộc vào từng khu vực địa lý là trung tâm hay ngoại ô. Thời gian có thể từ 3 giờ đến 12 giờ và có khi đến 48 giờ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-4'] = 'Tiền của Quý khách sẽ được giao đến tận nhà của người thụ hưởng.  Hệ thống chăm sóc khách hàng của chúng tôi và/ hoặc của đối tác sẽ thông báo đến người gửi về việc hoàn tất giao dịch này.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-1'] = 'Đến tại công ty, chi nhánh, đại lý là đối tác của Pay Way trên toàn thế giới để thực hiện việc chuyển tiền. Hay Quý khách cũng có thể chuyển trực tiếp từ tài khoản, thẻ, điện thoại (tùy theo nước/ vùng/ đối tác)';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-2'] = 'Pay Way cung cấp dịch vụ nhận tiền kiều hối chuyên nghiệp thông qua việc hợp tác với nhiều công ty chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ. Và chi trả trên toàn lãnh thổ Việt Nam. ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-3'] = 'Với giải pháp chuyển tiền bằng di động cho phép bất kỳ nhà bán lẻ, đại lý, vv là có thể thực hiện giao dịch chuyển tiền không dựa trên nền tản truyền thống của giao dịch ngân hàng, nên sẽ tạo ra nhiều cơ hội kinh doanh mới.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-4'] = 'Nền tảng chuyển tiền bằng di động của Pay Way cho phép rút tiền qua tài khoản ví điện thoại, tại đại lý đã được ủy quyền, tại ATM hay tại điểm chuyển tiền kiều hối của bưu điện.  Hệ thống chăm sóc khách hàng của chúng tôi và/ hoặc của đối tác sẽ thông báo đến người gửi về việc hoàn tất giao dịch này.';
// Dai Ly
$lang['noi-dung-dai-ly'] = '<h3>Tiêu chí trở thành đại lý của chúng tôi: </h3>
Với mục tiêu tiếp cận khách hàng khắp mọi miền đất nước nhằm mang lại những dịch vụ tiện ích. Pay Way chào đón các cá nhân, pháp nhân trở thành đại lý của chúng tôi trên tinh thần xây dựng mối quan hệ hợp tác đôi bên cùng có lợi.
<br><br>
<h3>Lợi ích hấp dẫn của đại lý</h3>
<ul>
    <li><strong>CHIẾT KHẤU CAO:</strong> Sử dụng dịch vụ tại Payway.vn Thanh Toán Hóa Đơn đảm bảo sẽ mang lại hiệu quả về doanh thu và lợi nhuận cho đôi bên cùng có lợi.</li>
    <li><strong>AN TOÀN VÀ ỔN ĐỊNH:</strong> Dịch vụ ổn định và đảm bảo an toàn. Kết nối không chỉ toàn quốc mà mở rộng trên toàn cầu từ tất cả các nhà cung cấp dịch vụ viễn thông, dịch vụ kiều hối, thanh toán trực tuyến,…</li>
    <li><strong>TĂNG KHẢ NĂNG CẠNH TRANH:</strong> Hưởng mức chiết khấu cao hơn mức chiết khấu chung toàn hệ thống, theo chính sách thưởng doanh thu. </li>
</ul>
<strong>Nếu quý khách quan tâm trở thành đại lý của Pay Way hoặc cần biết thêm chi tiết vui lòng cung cấp thông tin theo form dưới đây:</strong><br>
';
$lang['noi-dung-doi-tac'] = '<h3>Hợp tác với Pay Way, Các đơn vị cung ứng dịch vụ thông qua Pay Way cũng được hưởng lợi ích như: </h3>
<ul>
    <li>Các chương trình Co-Branding đồng thương hiệu sẽ do Pay Way thực hiện. Đặc biệt, các chuyên viên marketing chuyên nghiệp sẽ thực hiện E-marketing hay các tương tác từ các mạng xã hội.</li>
    <li>Giảm chi phí công việc hành chính về xử lý thanh toán, chuyển tiền và nâng cao dữ liệu thông tin hơn phương pháp truyền thống </li>
    <li>Cải thiện mối quan hệ với khách hàng bằng cách cung cấp thêm lựa chọn dịch vụ cho họ.</li>
    <li>Hỗ trợ tăng doanh số bán, gia tăng lợi nhuận, thu hút khách hàng và phát triển doanh nghiệp.</li>
    <li>Triển khai các chương trình thưởng cho đại lý.</li>
    <li>Triển khai các chương trình khuyến mãi và cùng quảng bá thương hiệu</li>
</ul>
<h3>Liên hệ để trở thành đối tác Payway.vn</h3><br>
<font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
MST: 0305221500<br>
H: 08 6686 5378 – 091 989 1909<br>
P: 08 3602 8733 - F: 08 629 09 340<br>
E: info@eway.com.vn<br>
URL: www.Payway.vn<br>';
$lang['noi-dung-nha-dau-tu'] = '<h3>Tổng quan về Pay Way</h3>
Pay Way là nhà cung cấp dịch vụ thanh toán hóa đơn và chuyển tiền kiều hối chuyên nghiệp, đã hợp tác với nhiều tập đoàn chuyển tiền toàn cầu từ trên 150 quốc gia và vùng lãnh thổ. Chúng tôi cung cấp chuyển tiền nhanh, an toàn và đa dạng đảm bảo cho khách hàng của mình sự chọn lựa hoàn hảo nhất.
<h3>Vì sao chọn Pay Way?</h3>
<ul>
    <li>Uy tín – Đối tác của Pay Way là Tập đoàn bưu điện Việt Nam, Công ty Chuyển mạch tài chính quốc gia Việt Nam và các tập đoàn chuyển tiền uy tín toàn cầu.</li>
    <li>Đa dạng – Quý khách có thể chọn lựa nhiều kênh như nhận tiền mặt tại bưu điện, người thân chuyển tiền vào tài khoản hay thẻ ATM của Quý khách, và dịch vụ giao tiền tận nhà trên toàn quốc.</li>
    <li>Nhanh chóng – Tiền được chuyển vào tài khỏan trong giây lác, khi người thân vừa gửi là Quý khách sẽ có tiền ngay; nhận tiền tại quầy chỉ trong 5 phút và thời gian giao tiền tại nhà từ 3-48 giờ tùy theo từng khu vực.</li>
    <li>Thuận tiện – Với mạng lưới trên 100,000 điểm giao dịch trên toàn thế giới thông qua đối tác của Pay Way </li>
    <li>Dễ dàng – Trung tâm dịch vụ khách hàng 24/7 của chúng tôi sẽ luôn hổ trợ Quý vị</li>
</ul>
<h3>Hợp tác đầu tư:</h3>
<ul>
    <li>Chiến lược liên minh, hợp tác được mở rộng ngoài ranh giới quốc gia, với tiêu chí www (Bạn Win – Khách hàng Win và Chúng tôi Win). Điều này sẽ góp phần làm nâng cao sức mạnh của chúng tôi nhằm đóng góp tích cực các sản phẩm, dịch vụ mới hoàn hảo cung cấp cho cộng đồng.</li>
    <li>Với định hướng là công chúng hóa sở hữu nhằm mở rộng quy mô hoạt động và phát triển Pay Way. Chúng tôi luôn trân trọng đón nhận sự hợp tác từ khách hàng, đối tác, quỹ đầu tư, vv.</li>
</ul>
<h3>Mọi chi tiết liên hệ:</h3><br>
<font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
MST: 0305221500<br>
H: 08 6686 5378 – 091 989 1909<br>
P: 08 3602 8733 - F: 08 629 09 340<br>
E: info@eway.com.vn<br>
URL: www.Payway.vn<br>
';
$lang['chuong-trinh-khuyen-mai'] = 'Khuyến Mãi';