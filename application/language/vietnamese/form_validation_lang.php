<?php

$lang['required']           = "Thông tin %s phải được nhập.";
$lang['isset']              = "Thông tin %s phải có giá trị.";
$lang['valid_email']        = "Thông tin %s phải là một email hợp lệ.";
$lang['valid_emails']       = "Thông tin %s phải là các email hợp lệ.";
$lang['valid_url']          = "Thông tin %s phải là một đường dẫn hợp lệ.";
$lang['valid_ip']           = "Thông tin %s phải là một IP hợp lệ.";
$lang['min_length']         = "Thông tin %s phải có ít nhất %s ký tự.";
$lang['max_length']         = "Thông tin %s không thể có quá %s ký tự.";
$lang['exact_length']       = "Thông tin %s phải có đúng %s ký tự.";
$lang['alpha']              = "Thông tin %s chỉ có thể là các chữ cái.";
$lang['alpha_numeric']      = "Thông tin %s chỉ có thể là các chữ cái và con số.";
$lang['alpha_dash']         = "Thông tin %s chỉ có thể là các chữ cái, con số, dấu gạch dưới \"_\" và dấu gạch ngang \"-\".";
$lang['numeric']            = "Thông tin %s chỉ có thể là các số.";
$lang['is_numeric']         = "Thông tin %s chỉ có thể là các ký tự số.";
$lang['integer']            = "Thông tin %s phải là một số nguyên.";
$lang['regex_match']        = "Thông tin %s không đúng định dạng.";
$lang['matches']            = "Thông tin %s không đúng với thông tin %s.";
$lang['is_unique']          = "Thông tin %s phải là một giá trị duy nhất.";
$lang['is_natural']         = "Thông tin %s phải là một số tự nhiên.";
$lang['is_natural_no_zero'] = "Thông tin %s phải là một số tự nhiên lớn hơn số 0.";
$lang['decimal']            = "Thông tin %s phải là một số thập phân.";
$lang['less_than']          = "Thông tin %s phải là một số nhỏ hơn số %s.";
$lang['greater_than']       = "Thông tin %s phải là một số lơn hơn số %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */