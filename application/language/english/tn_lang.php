﻿<?php
// ---------- ADMIN ----------

// trang thai giao dich
$lang['atrans_st1']  = 'Created';
$lang['atrans_st2']  = 'Successful payment';
$lang['atrans_st3']  = 'Payment failed';
$lang['atrans_st4']  = 'Successful handling';
$lang['atrans_st5']  = 'Handling failed';
$lang['atrans_st6']  = 'Successful confirmation';
$lang['atrans_st7']  = 'Confirmation failed';
$lang['atrans_st8']  = 'Successful refunding';
$lang['atrans_st9']  = 'Refunding failed';
$lang['atrans_st10'] = 'Unidentified';
$lang['atrans_st11'] = 'Error';

// phan quyen nhom nguoi dung
$lang['blog']                       = 'Blog';
$lang['blog_article view']          = 'View article';
$lang['blog_article manage']        = 'Manage article';
$lang['blog_category view']         = 'View category';
$lang['blog_category manage']       = 'Manage category';
$lang['user']                       = 'User';
$lang['user_account view']          = 'View account';
$lang['user_account manage']        = 'Manage account';
$lang['user_group view']            = 'View group';
$lang['user_group manage']          = 'Manage group';
$lang['administration']             = 'Administration';
$lang['module_partner view']        = 'View partner';
$lang['module_partner manage']      = 'Manage partner';
$lang['module_banner view']         = 'View banner';
$lang['module_banner manage']       = 'Manage banner';
$lang['website_contact view']       = 'View contact';
$lang['website_contact manage']     = 'Manage contact';
$lang['payment']                    = 'Payment';
$lang['payment_transaction view']   = 'View transaction';
$lang['payment_transaction manage'] = 'Manage transaction';


// ---------- SITE ----------

// tu thong dung
$lang['Xin chao'] = 'Welcome';
$lang['trang-chu']    = 'Homepage';
$lang['thong-tin']    = 'Information';
$lang['ve-chung-toi'] = 'About Us';
$lang['gioi-thieu']   = 'About Us';
$lang['dich-vu']      = 'Services';
$lang['tin-tuc']      = 'News';
$lang['ho-tro']       = 'Support';
$lang['lien-he']      = 'Contact';
$lang['hotline']      = 'Hotline';
$lang['dang-nhap']    = 'Login';
$lang['dang-ky']      = 'Register';
$lang['danh-muc']     = 'Category';
$lang['thanh-vien']   = 'Members';
$lang['doi-tac']   	  = 'Partners';
$lang['email']   	  = 'Email';
$lang['Password']     = 'Password';
$lang['tai-khoan']    = 'Account';
$lang['thoat']   		  = 'Log out';
$lang['nhap-lai']   		  = 'Reset';
$lang['ten']   		  = 'First Name';
$lang['ho']   		  = 'Surname';
$lang['sua-thong-tin-tai-khoan']   		      = 'Edit Information';
$lang['doi-mat-khau'] = 'Edit Password';
$lang['lich-su-giao-dich']   		  = 'Transaction History';
$lang['nhap-lai-mat-khau']   		  = 'Confirm password';
$lang['ngay-sinh']   		  = 'Birthday';
$lang['dia-chi']   		  = 'Address';
$lang['xem-chi-tiet'] = 'View Detail';
$lang['ngon-ngu'] = 'Language';
$lang['tong-so'] = 'Total';
$lang['thanh-toan'] = 'Accept';
$lang['ngay'] = 'Date';
$lang['thanh-cong'] = 'Success';
$lang['that-bai'] = 'Fail';

// thong tin
$lang['dia-chi-1']        = ' 401 Huynh Van Banh St, Ward 11, Phu Nhuan District, HCMC, Vietnam';
$lang['footer-contact']   = 'Best Way Corp (Eway)<br>Office: 154/15 Nguyen Trong Tuyen St., Ward 8, Phu Nhuan Dist., HCMC<br>
Transaction Add: 401 Huynh Van Banh, Ward 11, Phu Nhuan Dist., HCMC<br>
MST: 0305221500<br>
The License No. 973/ GP-HCM ; issued on July 04th,2014 by The State Bank of Vietnam, Ho Chi Minh Branch<br>HOTLINE: (84-8)- 6686 5378 / 0919 89 19 09<br>EMAIL: lienhe.payway@gmail.com - WEBSITE: www.payway.vn';
$lang['footer-aboutus']   = 'Pay Way money transfer services from more than 150 countries and territories on bank accounts<br>Pick-up money at over 3600 locations in 63 provinces and variable bill payment services... <a href="gioi-thieu/ve-chung-toi.html">(View more)</a>';
$lang['footer-copyright'] = '© 2014 Pay Way. All Rights Reserved.';

// banner 1
$lang['B1-S1'] = 'MONEY TRANSFER';
$lang['B1-S2'] = 'Global Money Transfer to Vietnam.<br>SAFE - CONVENIENT - FAST';
$lang['B1-S3'] = 'Get money to your account';
$lang['B1-S4'] = 'Get Money at Home';
$lang['B1-S5'] = 'Get Money at Counter';
// banner 2
$lang['B2-S1'] = 'BILL COLLECTION - PAY @ POST';
$lang['B2-S2'] = 'Bill Payment</strong><br>AT <strong style="font-size:40px;color:#ff0;line-height: 65px;">5000</strong> NATIONAL POSTS';
$lang['B2-S3'] = 'Payment Solutions - your customers have more options to pay you at post office';
// banner 3
$lang['B3-S1'] = 'PAYMENT GATEWAY';
$lang['B3-S2'] = 'ONLINE BILL PAYMENT<br>SAFE - CONVENIENCE - FAST';
$lang['B3-S3'] = 'Top Up';
$lang['B3-S4'] = 'Postpaid Mobile';
$lang['B3-S5'] = 'Electric Bills';
$lang['B3-S6'] = 'ADSL Bills';
$lang['B3-S7'] = 'Air Ticket';
$lang['B3-S8'] = 'Game Card / Mobile Card';

// contact
$lang['contact-ghi-chu'] = 'Please enter information to contact us at the form below.';
$lang['contact-info'] = 'Address: <strong>401 Huynh Van Banh St, Ward 11, Phu Nhuan District, HCMC</strong><br>HOTLINE: <strong>(84-8)- 6686 5378 / 0919 89 19 09</strong><br>FAX: <strong>08 629 09 340</strong><br>Email: <strong>lienhe.payway@gmail.com</strong><br>Website: <strong>www.payway.vn</strong><br>';

// error 404
$lang['error-404-info'] = 'PAGE NOT FOUND. <small>The page you are looking for might have been removed, had its name changed.</small>';
$lang['ve-trang-chu']   = 'Back To Homepage';

// tu khac
$lang['chap-nhan-thanh-toan']   = 'Accept Payment';
$lang['nha-cung-cap-dich-vu']   = 'Service Providers';
$lang['payway-hoat-dong-nhu-the-nao'] = 'How Pay Way Works?';
$lang['video']                        = 'Video Gallery';
$lang['huong-dan-su-dung-dich-vu-nhanh'] = 'REMITTANCE SERVICE GUIDE';
$lang['cach-chuyen-tien']                = 'HOW TO SEND MONEY';
$lang['cach-nhan-tien']                  = 'HOW TO RECEIVE MONEY';
$lang['thanh-toan-hoa-don-online']       = 'ONLINE BILL PAYMENT';
$lang['thanh-toan-hoa-don-offline']      = 'OFFLINE BILL PAYMENT';
$lang['bieu-phi-dich-vu']  = 'SERVICE FEES';
$lang['dia-diem-chuyen-tien']  = 'MONEY TRANSFER LOCATIONS LIST';
$lang['dia-diem-nhan-tien'] = 'RECEIVE MONEY LOCATIONS LIST';
$lang['dieu-khoan-bao-mat']    = 'Privacy Terms';
$lang['cau-hoi-thuong-gap']    = 'FAQs';
$lang['chinh-sach-quy-dinh']     = 'Policy - Regulations';
$lang['quy-trinh-giao-dich']   = 'Transaction Process  ';
$lang['huong-dan-nghiep-vu']   = 'Professional Guidance';
$lang['huong-dan-su-dung']   = 'Remittance Service Guide';
$lang['ty-gia-ngoai-te']           = 'Dashboard FX';
$lang['ho-tro-truc-tuyen'] = 'Live Support';
$lang['cac-bai-viet-khac'] = 'Other Articles';
$lang['chua-co-thong-tin'] = 'No Information';
$lang['xem-them']          = 'View more &gt;';
$lang['nhan-email'] = 'Submit your email for signing up Pay Way newsletter';
$lang['ket-qua-tim-kiem'] = "Searching result";

// dang ky tai khoan
$lang['chung-minh-thu'] = 'ID card';
$lang['agreement']      = 'Confirming account registration means that you accepted our rules!';
$lang['error-email-da-su-dung']  = 'Email was used.';
$lang['dang-ky-thanh-cong']      = 'Successful registration.';
$lang['dang-ky-that-bai']        = 'Registration failed.';
$lang['sai-thong-tin-dang-nhap'] = 'Email or password is incorrect.';
$lang['quen-mat-khau']      = 'Forgot Your Password?';
$lang['khong-phai-thanh-vien']  = 'You are not a member?';
$lang['tao-tai-khoan-moi']    = 'Create a new account easily and takes less than a minute.';
$lang['dang-ky-tai-khoan']    = 'Register a new account';
$lang['thong-tin-tai-khoan']    = 'Account Information';
$lang['thong-tin-chung']    = 'General Information';
$lang['thong-tin-them']    = 'Additional Info';
$lang['thong-tin-bao-mat']    = 'Security Information';
$lang['gioi-tinh']    = 'Gender';
$lang['nam']    = 'Male';
$lang['nu']    = 'Female';
$lang['khac']    = 'Other';
$lang['chon']    = 'Select';
$lang['quoc-gia']    = 'Country';
$lang['post-code']    = 'Postal Code';
$lang['so-dien-thoai']    = 'Phone Number';
$lang['ma-xac-nhan']    = 'Security Code';
$lang['cau-hoi-bao-mat']    = 'Choose a Security Question';
$lang['cau-tra-loi']    = 'Security Answer';

// lich su giao dich 
$lang['ma-giao-dich']    = 'Transaction Code';
$lang['thoi-gian']    = 'Time';
$lang['so-tien']    = 'Amount';
$lang['trang-thai']    = 'Status';

// doi mat khau
$lang['mat-khau-cu']    = 'Old Password';
$lang['mat-khau-moi']    = 'New Password';
$lang['xac-nhan-mat-khau']    = 'Confirm Password';
$lang['luu']    = 'Save';
$lang['tim-kiem']    = 'Search';
$lang['tu-ngay']    = 'From Date';
$lang['den-ngay']    = 'To Date';

// dang ky 
$lang['loi-ich-dang-ky']    = 'Benefits when you register ';
$lang['kiem-tra-giao-dich']    = 'Checking transaction';
$lang['su-dung-dich-vu']    = 'Using services';
$lang['ban-da-co-tai-khoan']    = 'You had account. Please';
$lang['bam-vao-day']    = 'click here';
$lang['date-ngay']  ='Date';
$lang['date-thang']  ='Month';
$lang['date-nam']  ='Year';

// bat loi
$lang['ngay-sinh-khong-hop-le']  ='Invalid Birthday';
$lang['sai-ma-xac-nhan']  ='Captcha Incorrect';
$lang['thanh-pho'] = 'City';
$lang['doi-mat-khau-that-bai'] = 'Change Password Failed';
$lang['doi-mat-khau-thanh-cong'] = 'Password Changed Successfully';
$lang['sai-mat-khau-cu'] = 'Old Password Incorrect';
$lang['sua-thong-tin-thanh-cong'] = 'Edit Infomation Successfully ';
$lang['sua-thong-tin-that-bai'] = ' Edit Infomation Failed ';
$lang['doi-tac-dau-tu'] = 'BECOME A PARTNER';
$lang['noi-dung-doi-tac-dau-tu'] = 'Benefits of cooperation with Pay Way';
$lang['mail-tu-payway'] = 'Newsletter';
$lang['ten-newsletter'] = 'Name';
$lang['mail-newsletter'] = 'Email';
$lang['theo-doi'] = 'Subscribe';
$lang['lay-lai-mat-khau'] = 'Reset Password';
$lang['email-chua-duoc-dang-ky'] = 'Email unregistered';
$lang['lay-lai-mat-khau-thanh-cong'] = 'reset your password successfully, please check your email.';
$lang['lay-lai-mat-khau-that-bai'] = 'Reset Password Failure';
$lang['email-tao-moi-mat-khau'] = 'You or someone else has asked to create new password on website payway.vn 
If you\'ve never done this request please ignore this email.

Your account: [email]
Click here to create a password: [link]

Xin cám ơn. Thank you';
$lang['tieu-de-email-tao-moi-mat-khau'] = 'Renew passwords on payway.vn';
$lang['dai-ly'] = 'AGENT';
$lang['nha-dau-tu'] = 'INVESTORS';
$lang['ho-ten'] = 'LAST NAME & FIRST NAME';
$lang['thuoc-don-vi'] = 'Professional';
$lang['nganh-nghe-kinh-doanh'] = 'Industries';
$lang['chuc-vu-hien-tai'] = 'Current Position';
$lang['noi-dung-can-trao-doi'] = 'Exchange Content ';
$lang['gui'] = 'Send';
$lang['thong-bao'] = 'Notice';

// gioi thieu 
$lang['gioi-thieu-noi-dung-ngan'] = 'eWay is abbreviation of The Best Way Corporation Join Stock Company. eWay was established in 2007. It was formerly known as management consulting by King Horse brand. To succeed in a characterized and constantly changing business environment, Eway is proud to be one of the pioneers operate successfully in the e-commerce in Vietnam. It is important to always keep in mind that reputation has made us special values​​. Action in accordance with the global market, will keep us moving in the right direction, and will continue to increase our strength. ';
$lang['gioi-thieu-dich-vu-kieu-hoi'] = 'Pay Way is a professional remittance service provider, has integrated with many global transfer money from corporations over 150 countries and territories.....';
$lang['gioi-thieu-dich-vu-thanh-toan-online'] = 'With the foundation of advanced and modern technology systems, Payway.vn always brings the fastest online services which have been......';
$lang['gioi-thieu-dich-vu-thanh-toan-offline'] = 'Nowadays, modern and advanced technology, payment solutions is increasing popularly and help to save time......';
$lang['gioi-thieu-dich-vu-khac'] = 'UNDER CONSTRUCTION';

// dich vu 
$lang['dich-vu-kieu-hoi']   = 'Remittance Services';
$lang['thanh-toan-online']  = 'Online Payment';
$lang['thanh-toan-offline'] = 'Offline Payment';
$lang['chuyen-tien-dien-thoai'] = 'Mobile Money Transfer';
$lang['dich-vu-khac']       = 'Other Services';
$lang['mua-the-game'] = 'Buy Game Cards';
$lang['thanh-toan-hoa-don'] = 'Bill Pay';
$lang['di-dong-tra-truoc'] = 'Prepaid phones';
$lang['mua-the-dien-thoai'] = 'Buy phone cards';
$lang['di-dong-tra-sau'] = 'Postpaid phones';
$lang['nap-tien-tai-khoan'] = 'Top-up to accounts';
$lang['nap-tien-game'] = 'Top-up to Games';
$lang['ve-may-bay'] 				= 'Ari Ticker';
$lang['hoa-don-adsl'] 				= 'ADSL Bills';
$lang['tien-dien'] 				= 'Electric Bills';

// noi dung dich vu 
$lang['noi-dung-mua-the-game'] = 'Buy game cards as Vcoin, Zing Xu, Shell card, Gate card, Oncash card, ...';
$lang['noi-dung-di-dong-tra-truoc'] = 'Phone bill online payment as Vinaphone, Mobifone, Viettel...';
$lang['noi-dung-mua-the-dien-thoai'] = 'Buy phone cards for Vinaphone, Mobifone, Viettel...';
$lang['noi-dung-di-dong-tra-sau'] = 'Pay postpaid bill for Mobifone service provider.';
$lang['noi-dung-nap-tien-tai-khoan'] = 'Top-up to accounts as hocmai.vn, Elean, English Ibest..';
$lang['noi-dung-nap-tien-game'] = 'Top-up Vcoin, Zing Xu, GG, Silver, Scoin, @Point to accounts. ';
$lang['noi-dung-tien-dien'] 		= 'Flexible interaction close connecting between suppliers and users.';
$lang['noi-dung-hoa-don-adsl'] 		= 'Paying bills such as internet ADSL of Viettel, VNPT, FPT...';
$lang['noi-dung-ve-may-bay'] 		= 'Buying air tickets online with 3 domestic and 27 international airlines .';

// dich vu homepage
$lang['home-info-1'] = 'Sign up in 10 seconds with the most friendly steps.';
$lang['home-info-2'] = 'Integrated process within 3 minutes in 1 simple step.';
$lang['home-info-3'] = 'Online integrated with 3 domestic and 27 international Airlines.';
$lang['home-info-4'] = 'All privacy information of accounts and transactions are secured perfectly.';

// dich vu thanh toan 
$lang['nha-cung-cap'] = 'Service Provider';
$lang['so-di-dong'] = 'Mobile number';
$lang['menh-gia'] = 'Price';
$lang['thanh-tien'] = 'Total';
$lang['ngan-hang'] = 'Bank';
$lang['so-luong'] = 'Quantity';
$lang['ma-the'] = 'Code card';
$lang['so-seri'] = 'Seri number';
$lang['han-su-dung'] = 'Term of use';

//bat loi dich vu di dong tra truoc
$lang['error-so-du-tai-khoan-1'] 		= 'Dịch vụ đang bảo trì. Vui lòng quay lại sau vài phút, Xin lỗi về sự bất tiện này.';
$lang['error-di-dong-1'] = 'Incorrect phone number';
$lang['error-nha-cung-cap-1'] = 'Incorrect service provider';
$lang['error-dau-so-1'] = 'Incorrect pre-fix number';
$lang['error-menh-gia-1'] = 'Incorrect value';
$lang['error-cong-thanh-toan-1'] = 'Error transaction in payment gateway.';
$lang['noti-fail-1'] = 'Transaction failed. Please follow the payment process.';
$lang['noti-success-1'] = 'Successful transaction. Thank you for using the services of Pay Way.';
$lang['error-so-luong-1'] = 'Wrong quantity';
$lang['error-tai-khoan-1'] = 'Wrong account';
$lang['error-so-luong-2'] = 'Please enter a quantity not exceeding 10.000';
$lang['error-menh-gia-2'] = 'Face value to be top-up from 6.000 to 1.000.000 and even thousands';

$lang['error-so-luong-the-1']   = 'Số lượng thẻ không đủ để thực hiện giao dịch. Vui lòng chọn mệnh giá khác';

$lang['giao-dich-thanh-cong'] = 'Successful transaction';
$lang['giao-dich-that-bai'] = 'Transaction failed';

$lang['thanh-toan-thanh-cong'] = 'Successful payment';
$lang['thanh-toan-that-bai'] = 'Payment failed';

$lang['xu-ly-giao-dich'] = 'Transaction processing';
$lang['xu-ly-that-bai'] = 'Failure processing';

$lang['noi-dung-xu-ly-giao-dich'] = 'Please keep waiting for transaction processing ';
$lang['noi-dung-xu-ly-that-bai'] = 'Failure processing. Failure processing';

//
$lang['giao-dich-loi'] = 'Error transaction';
$lang['xin-thu-lai'] = 'Please perform the transaction.';

//thong bao loi
$lang['khong-tim-thay-giao-dich'] = 'Transaction not found';
$lang['khong-the-kiem-tra-trang-thai-giao-dich'] = 'Cannot check the status of transaction';
$lang['khong-dung-trang-giao-dich'] = 'Wrong status of transaction';


$lang['chi-tiet-giao-dich'] = 'Transaction details';
$lang['thong-tin-giao-dich'] = 'Transaction information';
$lang['thong-tin-dich-vu'] = 'Service information';
$lang['ket-qua-thanh-toan'] = 'Payment result';

// cac loai dich vu
$lang['PW1101'] = 'Prepaid mobile phone';
$lang['PW1102'] = 'Postpaid mobile phone';
$lang['PW1103'] = 'Buy phone card';
$lang['PW1104'] = 'Buy game card';
$lang['PW1105'] = 'Game top-up';
$lang['PW1106'] = 'Top-up to account';

// trang thai giao dich
$lang['trans_st1']  = 'Create';
$lang['trans_st2']  = 'Fail';
$lang['trans_st3']  = 'Fail';
$lang['trans_st4']  = 'Complete';
$lang['trans_st5']  = 'Fail';
$lang['trans_st6']  = 'Complete';
$lang['trans_st7']  = 'Complete';
$lang['trans_st8']  = 'Completed';
$lang['trans_st9']  = 'Failed refunding';
$lang['trans_st10'] = 'Unidentified';
$lang['trans_st11'] = 'Error';

// trang thai giao dich cho thanh vien xem
$lang['trans_member_st1']  = 'Create';
$lang['trans_member_st2']  = 'Complete';
$lang['trans_member_st3']  = 'Fail';
$lang['trans_member_st4']  = 'Completed refund';
$lang['trans_member_st5']  = 'Failed refunding';
$lang['trans_member_st6']  = 'Error';

//ho tro 
$lang['ho-tro-cau-hoi-thuong-gap'] = 'What is Pay Way?<br>How to find money transfer locations of Pay Way or Pay Way’s partners?<br>Who can use Pay Way services?';
$lang['ho-tro-huong-dan-nghiep-vu'] = '1. SMS Banking<br>2. Use Mobile Banking<br>3. Pay bill at banks<br>';
$lang['ho-tro-ty-gia-ngoai-te'] = 'Foreign exchange rate of USD, AUD, HKD, EUR, JPY, RUB, THB.... are updated from Vietcombank';

// ty gia ngoai te
$lang['ma-ngoai-te'] = 'Foreign currency codes';
$lang['ten-ngoai-te'] = 'Name of foreign currency';
$lang['mua-ngoai-te'] = 'Buying price';
$lang['chuyen-ngoai-te'] = 'Account transfer';
$lang['ban-ngoai-te'] = 'Selling price';
$lang['ty-gia-ngoai-te-vietcombank'] = 'Foreign exchange rate of Vietcombank';

//dich vu chuyen tien 
$lang['noi-dung-dich-vu-kieu-hoi'] = 'Pay Way is a professional remittance service provider, has integrated with many global transfer money corporations from over 150 countries and territories. We provide fast, safe and diverse money transfer, ensure our customers the most perfect choice.';
$lang['dich-vu-kieu-hoi-nhan-tai-quay'] = 'Cash Pick-up';
$lang['dich-vu-kieu-hoi-nhan-vao-tai-khoan'] = 'Deposit Account / Cards';
$lang['dich-vu-kieu-hoi-nhan-tai-nha'] = 'Home delivery';
$lang['dich-vu-kieu-hoi-nhan-bang-dien-thoai'] = 'Mobile Remittance';

$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay'] = 'Receiving money at the counter:  With over 5,000 locations of post offices and banks in whole country, customers will be easy & convenient to get the money there in minutes.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan'] = 'Customers have the bank account associated with Pay Way (bank list – Almost all banks in VN with over 50,000,000 accounts and over 30,000,000 cards) the money will be credited into your account /cards immediately after your senders who carried out orders and notifies SMS to you (if required).';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha'] = 'Door to Door delivery staff will contact the beneficiary to arrange the delivering time. Recipients only need to provide valid identification card with photo for double check. Recipients sign, receive money and there’s no extra fee required (except any special requirement).';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai'] = 'Mobile Remittance is the future of remittance services as it is fast, convenient, secure, transparent and significantly less expensive. Pay Way’s platform enables a low cost and flexible remittance service offering enormous potential new business opportunities.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-1'] = 'You can collect your money from any Pay Way’s office, branches,  and partners as post offices or banks network around the country.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-2'] = 'Make sure that you carry a valid identification card with photo (ID Card, Passport), know the amount of money to be received and Money Transfer Control Numer or needed information.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-3'] = 'Complete the remittance receiving form mentioning your personal and sender’s details. Handover the duly filled form (sender name, amount sent and country sent & MTCN  and ID Card or other info (if any) to the center personnel.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-4'] = 'Your remittance amount will be provided to you within 05-15 minutes once the given details are verified. And the notification will be updated in our CRM system to the sender.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-1'] = 'Visit any one of the Pay Way’s principal partners, branches or agents network around the world to send your money. Or direct from sender’s account, card, mobile (depending on countries, areas, companies)';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-2'] = 'Pay Way is a remittance service professional provider by integrated with many global transfer money corporations from over 150 countries and territories. And pay out whole Vietnam country.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-3'] = 'Pay Way is integrated with all post network and switching almost 45 local banks with over 50,000,000 accounts and over 30,000,000 cards for Real Time transfer';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-4'] = 'Your remittance amount will be provided directly to beneficiary’s cards or accounts in Real Time as the sender submit the order. And the notification will be updated in our CRM system to the sender.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-1'] = 'Visit any one of the Pay Way’s principal partners, branches or agents network around the world to send your money. Or direct from sender’s account, card, mobile';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-2'] = 'Pay Way is a remittance service professional provider by integrated with many global transfer money corporations from over 150 countries and territories. ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-3'] = 'Pay Way is integrated with door to door services to delivery money in whole country – 63 provinces. The time of delivery is depending on geographic areas which in downtown or remote locations. That’s ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-4'] = 'Your remittance amount will be delivered to beneficiary’s house. And the notification will be updated in our CRM system to the sender.';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-1'] = 'Visit any one of the Pay Way’s principal partners, branches or agents network around the world to send your money. Or direct from sender’s account, card, mobile';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-2'] = 'Pay Way is a remittance service professional provider by integrated with many global transfer money corporations from over 150 ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-3'] = 'With Pay Way’s m-Remittance solution, any retailer with a mobile phone can become a Remittance Agent and no traditional banking infrastructure is required that offering enormous potential to ';
$lang['noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-4'] = 'Pay Way’s Mobile Remittance platform to cash out from their mobile wallet account or an authorized agent or an ATM, or remittance retail outlet of post office.  And the notification ';
// Dai Ly
$lang['noi-dung-dai-ly'] = '<h3>Standards to become our agent: </h3>
With the aim to reach customers across the country to provide utility services. Pay Way welcomes individuals and legal entities to become our agent on constructive cooperative relationship of mutual be nefit.<br><br>
<h3>Benefits of gravitational agents:</h3>
<ul>
    <li><strong>HIGH DISCOUNTS:</strong> Use services at Payway.vn, we ensure effective sales and profit for mutual benefit.
</li>
    <li><strong>SAFETY AND STABILITY:</strong> Services to ensure stability and safety. Connect not only nationally but globally expanding from all providers of telecommunications services, remittance services, online payment, ...</li>
    <li><strong>INCREASING COMPETITIVENESS:</strong> To enjoy a higher discount discount general system-wide, according to the usual policy of revenue.</li>
</ul>
<strong>If you are interested in becoming agents of Pay Way or need more information please provide information in the form below:</strong><br>
';
$lang['noi-dung-doi-tac'] = '<h3>Cooperation with Pay Way, Providers through Pay Way take benefit as well:</h3>
<ul>
    <li>The Co-Branding Program will be done by Pay Way. In particular, marketing professionals will perform professional E-marketing or the interaction of social networks.</li>
    <li>Reducing the cost of administrative payments processing, transfer money and improve data communication over traditional methods.</li>
    <li>Improving relationships with customers by providing more choices for their services.</li>
    <li>Supporting sales, increase profits, attract customers and grow the business.</li>
    <li>Deploying the agent reward program.</li>
    <li>Deploying the same promotions and branding</li>
</ul>
<h3>Contact</h3><br>
<font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
MST: 0305221500<br>
H: 08 6686 5378 – 091 989 1909<br>
P: 08 3602 8733 - F: 08 629 09 340<br>
E: info@eway.com.vn<br>
URL: www.Payway.vn<br>';
$lang['noi-dung-nha-dau-tu'] = '<h3>Overview of Pay Way</h3>
Pay Way is a professional remittance service provider, has integrated with many global transfer money from corporations over 150 countries and territories. We provide fast, safe and diverse money transfer, ensure our customers the most perfect choice.
<h3>Why choose Pay Way?</h3>
<ul>
    <li>Prestige - Pay Way’s partners are Vietnam post group, Vietnam national switching corporation, VNPay and reputable global money transfer organizations.
</li>
    <li>Diversity - You can select multiple channels such as pick-up cash at post offices, deposit service/ wire transfer, ATM or cards, and nationwide home delivery services. And mobile money transfer is on processing of integration.</li>
    <li>Quickly - Funds are transferred to account in second strabismus, the beneficiary receives right away after the sender enter the transaction (real time). It just takes 10 minutes for cash pick-up at counter and from 3-48 hours for door to door services that depending on the geography regions.</li>
    <li>Convenience - With a pay in network of over 100,000 outlets worldwide through partnership of Pay Way. And over 5,000 pay out locations of post in whole country and by millions accounts from almost banks in VN. And mobile money transfer is on processing of integration. 
    </li>
    <li>Easy – Just take a few simple steps and with the 24/7 customer service center that will always support you.</li>
</ul>
<h3>Investment cooperation:</h3>
<ul>
    <li>Strategic alliances and cooperation extended beyond national boundaries, the www criterion (You Win - Customers Win and We Win). This will contribute to improve our power to make a positive contribution products, excellent services provided to the community.</li>
    <li>With cultural orientation is public property in order to expand the scale of operation and Development Pay Way. We appreciate the cooperation received from customers, partners, investment funds, etc..</li>
</ul>
<h3>Contact:</h3><br>
<font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
MST: 0305221500<br>
H: 08 6686 5378 – 091 989 1909<br>
P: 08 3602 8733 - F: 08 629 09 340<br>
E: info@eway.com.vn<br>
URL: www.Payway.vn<br>
';
$lang['chuong-trinh-khuyen-mai'] = 'Promotions';