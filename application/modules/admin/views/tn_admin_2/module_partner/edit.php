<?php
foreach($languages as $language)
{
  $name = form_input('description['.$language['id'].'][name]', set_value('description['.$language['id'].'][name]', $data['item']['description'][$language['id']]['name']), 'class="form-control"');
  $synopsis = form_textarea('description['.$language['id'].'][synopsis]', set_value('description['.$language['id'].'][synopsis]', $data['item']['description'][$language['id']]['synopsis']), 'class="form-control"');
  $description = form_textarea('description['.$language['id'].'][description]', set_value('description['.$language['id'].'][description]', $data['item']['description'][$language['id']]['description']), 'class="form-control"');

  $cms_editor[] = "textarea[name='description[{$language['id']}][description]']";

  $parser = array(
    'data' => array(
      array(
        'name'  => 'description['.$language['id'].'][name]',
        'label' => 'Name',
        'field' => $name,
        'help'  => ''
      ),
      array(
        'name'  => 'description['.$language['id'].'][synopsis]',
        'label' => 'Synopsis',
        'field' => $synopsis,
        'help'  => ''
      ),
      array(
        'name'  => 'description['.$language['id'].'][description]',
        'label' => 'Description',
        'field' => $description,
        'help'  => ''
      )
    )
  );

  $tab1_{$language['id']} = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);
}

if ($data['item']['image'] != '')
{
	$image = form_upload('image')
		.'<span class="help-block">'
			.form_hidden('old_image', $data['item']['image'])
			.form_hidden('delete_image', '0')
			.'<img src="'.$image_url.$data['item']['image'].'" class="img-thumbnail old_image">
			<br>
			<button class="btn btn-danger btn-xs delete_image" type="button"><span class="glyphicon glyphicon-ban-circle"></span> Delete image</button>
		</span>';
}
else
{
	$image = form_upload('image')
		.'<span class="help-block">'.
			form_hidden('old_image', '')
			.'<img src="'.base_url('/public/images/no-image.png').'" class="img-thumbnail old_image">
		</span>';
}

$link = form_input('link', set_value('link', $data['item']['link']), 'class="form-control"');

$status = '
	<label class="radio-inline">
		<input type="radio" name="status" value="1" '.(($data['item']['status'] == 1) ? 'checked="checked"' : '').'> Enable
	</label>
	<label class="radio-inline">
		<input type="radio" name="status" value="0" '.(($data['item']['status'] == 0) ? 'checked="checked"' : '').'> Disable
	</label>
';
$position = form_input('position', set_value('position', $data['item']['position']), 'class="form-control"');

$parser = array(
	'data' => array(
		array(
			'name'  => 'image',
			'label' => 'Image',
			'field' => $image,
			'help'  => ''
		),
    array(
      'name'  => 'link',
      'label' => 'Link',
      'field' => $link,
      'help'  => ''
    ),
		array(
			'name'  => 'status',
			'label' => 'Status',
			'field' => $status,
			'help'  => ''
		),
		array(
			'name'  => 'position',
			'label' => 'Position',
			'field' => $position,
			'help'  => ''
		)
	)
);

$tab2 = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);

?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
  <li><a href="#tab-data" data-toggle="tab">Data</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <ul class="nav nav-pills language">
      <?php foreach ($languages as $language)
      {
        $active = ($language_id == $language['id']) ? 'class="active"' : '';
        echo '<li '.$active.'>
          <a href="#tab-'.$language['code'].'" data-toggle="tab">
            <img class="flag" src="'.base_url('/public/images/flags/'.$language['image']).'"> '
            .$language['name'].
          '</a>
        </li>';
      }
      ?>
      </ul>

      <div class="clearfix"></div>

      <div class="tab-content">
      <?php foreach ($languages as $language)
      {
        $active = ($language_id == $language['id']) ? 'active' : '';
        echo '<div class="tab-pane '.$active.'" id="tab-'.$language['code'].'">
          <fieldset>'
            .$tab1_{$language['id']}.
          '</fieldset>
        </div>';
      }
      ?>
      </div>
    </div>

    <div class="tab-pane" id="tab-data">
      <fieldset>
        <?php echo $tab2; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
				<button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
				<a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
      </div>
    </div>

  </div>
</form>

<script type="text/javascript" src="<?php echo SCRIPTSURL.'tinymce4020/tinymce.min.js'; ?>"></script>
<?php echo cms_editor($cms_editor); ?>

<script>
// $(document).ready(function(){
// 	var rule = {
// 		name: {
// 			required: true
// 		}
// 	}
// 	valid_update_form(rule);
// });
</script>