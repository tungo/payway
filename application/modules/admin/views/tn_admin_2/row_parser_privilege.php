{row}
  <div class="row">
  {panel}
    <div class="col-xs-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">{title}</h3>
        </div>
        <div class="panel-body">
          {checkbox}
            <div class="checkbox">
              <label>
                {field} {name}
              </label>
            </div>
          {/checkbox}
        </div>
      </div>
    </div>
  {/panel}
  </div>
{/row}