{data}
<div class="form-group">
	<label class="col-lg-2 control-label" for="{name}">{label}</label>
	<div class="col-lg-10">
		{field}
		{help}
	</div>
</div>
{/data}