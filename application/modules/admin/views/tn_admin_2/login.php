<form class="form-signin" method="post" action="">
  <h2 class="form-signin-heading">Sign in</h2>
  <input type="text" class="form-control" placeholder="Email" name="email" autofocus<?php if (isset($params['email'])) echo ' value="'.$params['email'].'"'; ?>>
  <input type="password" class="form-control" placeholder="Password" name="password">
  <label class="checkbox">
    <input type="checkbox" name="remember" value="remember"<?php if ($params['remember']) echo ' checked="checked"'; ?>> Remember me
  </label>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>

  <?php echo $this->tn_message->get(); ?>
</form>
