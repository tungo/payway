<!-- alert -->
<?php echo $this->tn_message->get(); ?>
<!-- /alert -->

<div class="row">

  <div class="col-xs-3">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Admin system</h3>
      </div>
      <div class="panel-body">
        PayWay's BackEnd System
      </div>
    </div>
  </div>

  <div class="col-xs-3">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">Transactions</h3>
      </div>
      <div class="panel-body">
        <div>Total: <?php echo $transactions['total']; ?></div>
        <div>Success: <?php echo $transactions['success']; ?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-3">
    <div class="panel panel-warning">
      <div class="panel-heading">
        <h3 class="panel-title">Quick links</h3>
      </div>
      <div class="panel-body">
        <a href="<?php echo site_url($path.'blog_article/add'); ?>">Add Blog Article</a><br>
        <a href="<?php echo site_url($path.'blog_category/add'); ?>">Add Blog Category</a><br>
        <a href="<?php echo site_url($path.'user_account/add'); ?>">Add User Account</a><br>
        <a href="<?php echo site_url($path.'user_group/add'); ?>">Add User Group</a><br>
        <a href="<?php echo site_url($path.'module_banner/add'); ?>">Add Banner</a><br>
        <a href="<?php echo site_url($path.'payment_transaction'); ?>">Manage Transaction</a><br>
      </div>
    </div>
  </div>

  <div class="col-xs-3">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h3 class="panel-title">Sitemap</h3>
      </div>
      <div class="panel-body">
        <ul class="sitemap">
          <li>Dashboard</li>
          <li>Blog
            <ul>
              <li>Category</li>
              <li>Article</li>
            </ul>
          </li>
          <li>Website
            <ul>
              <li>Contact</li>
            </ul>
          </li>
          <li>Module
            <ul>
              <li>Partner</li>
            </ul>
          </li>
          <li>User
            <ul>
              <li>Account</li>
              <li>Group</li>
            </ul>
          </li>
          <li>Payment
            <ul>
              <li>Transaction</li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

</div>
