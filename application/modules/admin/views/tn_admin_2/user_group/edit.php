<?php
$name = form_input('name', set_value('name', $data['item']['name']), 'class="form-control"');

$status = '
  <label class="radio-inline">
    <input type="radio" name="status" value="1" '.(($data['item']['status'] == 1) ? 'checked="checked"' : '').'> Enable
  </label>
  <label class="radio-inline">
    <input type="radio" name="status" value="0" '.(($data['item']['status'] == 0) ? 'checked="checked"' : '').'> Disable
  </label>
';
$position = form_input('position', set_value('position', $data['item']['position']), 'class="form-control"');

$parser = array(
  'data' => array(
    array(
      'name'  => 'name',
      'label' => 'Name',
      'field' => $name,
      'help'  => ''
    ),
    array(
      'name'  => 'status',
      'label' => 'Status',
      'field' => $status,
      'help'  => ''
    ),
    array(
      'name'  => 'position',
      'label' => 'Position',
      'field' => $position,
      'help'  => ''
    )
  )
);

$tab1 = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);

$panels = array();
$i = 0;
foreach ($privileges as $pvl_title => $prvl_checkbox)
{
  $panels[$i] = array(
    'title' => lang($pvl_title),
    'checkbox' => array(),
  );

  foreach ($prvl_checkbox as $j => $pvl_name)
  {
    $checked = '';
    if (in_array($pvl_name, $data['item']['privilege']))
      $checked = ' checked="checked"';

    $panels[$i]['checkbox'][] = array(
      'name' => lang($pvl_name),
      'field' => '<input type="checkbox" name="privilege['.(int) ($i.$j).']" value="'.$pvl_name.'"'.$checked.'>',
    );
  }

  $i++;
}

$rows = array();
$i = 0;
foreach ($panels as $j => $panel)
{
  $rows[$i]['panel'][] = $panel;

  if ($j % 4 == 3)
    $i++;
}
$parser['row'] = $rows;

$tab2 = $this->parser->parse('tn_admin_2/row_parser_privilege', $parser, TRUE);

$check_all_privilege = '<div class="form-group">
  <label class="col-lg-2 control-label"></label>
  <div class="col-lg-10">
    <div class="checkbox">
      <label>
        <input type="checkbox" name="check_all_privilege" checked="checked"> Check all privileges
      </label>
    </div>
  </div>
</div>';

$tab2 = $check_all_privilege.$tab2;

?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
  <li><a href="#tab-privilege" data-toggle="tab">Privilege</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <fieldset>
        <?php echo $tab1; ?>
      </fieldset>
    </div>

    <div class="tab-pane" id="tab-privilege">
      <fieldset>
        <?php echo $tab2; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
        <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
        <a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
      </div>
    </div>

  </div>
</form>

<script>
// $(document).ready(function(){
//   var rule = {
//     name: {
//       required: true
//     }
//   }
//   valid_update_form(rule);
// });
$(document).ready(function(){
  $(':checkbox[name="check_all_privilege"]').on('change', function(){
    $(':checkbox[name^="privilege"]').prop('checked', $(this).is(':checked'));
  });
  $(':checkbox[name^="privilege"]').on('change', function() {
    if (! $(this).is(':checked'))
    {
      $(':checkbox[name="check_all_privilege"]').prop('checked', false);
    }
    else
    {
      if ($(':checkbox[name^="privilege"]:checked').length == $(':checkbox[name^="privilege"]').length)
      {
        $(':checkbox[name="check_all_privilege"]').prop('checked', true);
      }
    }
  });
  $(':checkbox[name^="privilege"]').each(function(index) {
    if (! $(this).is(':checked'))
    {
      $(':checkbox[name="check_all_privilege"]').prop('checked', false);
    }
  });
});
</script>