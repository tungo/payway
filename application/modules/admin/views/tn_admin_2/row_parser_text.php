{data}
<div class="form-group">
	<label class="col-lg-2 control-label">{label}</label>
	<div class="col-lg-10 control-text">{text}</div>
</div>
{/data}