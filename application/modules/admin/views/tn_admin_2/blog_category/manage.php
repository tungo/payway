<p class="lead"><?php echo $title; ?></p>

<div class="clearfix"></div>

<?php echo $this->tn_message->get(); ?>

<form id="manage_form" name="manage_form" method="post" action="">

  <div class="toolbar">
    <a href="<?php echo site_url($current_controller.'add'); ?>" class="btn btn-success" title="Add"><span class="glyphicon glyphicon-plus"></span> Add</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'multi_copy'); ?>');" class="btn btn-success" title="Copy"><span class="glyphicon glyphicon-th-large"></span> Copy</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'multi_delete'); ?>', 'Are you sure you wish to delete selected rows?');" class="btn btn-danger" title="Delete"><span class="glyphicon glyphicon-remove"></span> Delete</a>
    <a onclick="return submit_form('<?php echo site_url($current_controller.'save'); ?>');" class="btn btn-primary" title="Save"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'active'); ?>');" class="btn btn-success" title="Enable"><span class="glyphicon glyphicon-ok-sign"></span> Enable</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'disable'); ?>');" class="btn btn-danger" title="Disable"><span class="glyphicon glyphicon-remove-sign"></span> Disable</a>
  </div><!-- /.toolbar -->

  <div class="clearfix"></div>

  <table class="table table-bordered manage-table">
    <thead>
      <tr>
        <th width="20" class="text-center"><input type="checkbox" name="check_all"></th>
        <th>Name</th>
        <th class="text-center">Status</th>
        <th class="text-center">Position</th>
        <th class="text-center">Id</th>
        <th class="text-center"></th>
      </tr>
    </thead>

    <tbody>

<?php
foreach($data['items'] as $row):

  $cid = '<input type="checkbox" value="'.$row['id'].'" name="cid[]">
    <input type="hidden" value="'.$row['id'].'" name="hid[]">';

  $name = '<span class="glyphicon glyphicon-plus"></span> '.$row['name'].' <span class="badge">'.$row['article_amount'].'</span>';
  for($i = 1; $i < $row['depth']; $i++)
  {
    $name = '<span class="depth">'.$name.'</span>';
  }

  $status = ($row['status'])
  ? '<a onclick="location.href = \''.site_url($current_controller.'status/id/'.$row['id'].'/status/0').'\'" class="btn btn-success btn-xs" title="Change status"><span class="glyphicon glyphicon-ok-sign"></span></a>'
  : '<a onclick="location.href = \''.site_url($current_controller.'status/id/'.$row['id'].'/status/1').'\'" class="btn btn-danger btn-xs" title="Change status"><span class="glyphicon glyphicon-remove-sign"></span></a>';

  $position = '<input type="text" value="'.$row['position'].'" class="form-control input-sm input-text text-center" name="position['.$row['id'].']">';

  $control = '
  <a onclick="location.href = \''.site_url($current_controller.'edit/id/'.$row['id']).'\'" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
  <a onclick="if (confirm(\'Are you sure you wish to delete this row?\')) location.href = \''.site_url($current_controller.'delete/id/'.$row['id']).'\';" class="btn btn-danger btn-xs" title="Delete"><span class="glyphicon glyphicon-remove"></span></a>
  ';
?>

      <tr data-id="<?php echo $row['id'];?>" data-parent="<?php echo $row['parent_id'];?>" data-children="0">
        <td class="text-center"><?php echo $cid; ?></td>
        <td><?php echo $name; ?></td>
        <td class="text-center"><?php echo $status; ?></td>
        <td class="text-center"><?php echo $position; ?></td>
        <td class="text-center"><?php echo $row['id']; ?></td>
        <td class="text-center"><?php echo $control; ?></td>
      </tr>

<?php endforeach; ?>

    </tbody>

  </table>
  <!-- /manage-table -->

  <div class="clearfix"></div>

  <?php if (count($data['items'])): ?>
    <div class="text-info entries-info pull-left">
      Showing <?php echo $data['total_rows']; ?>
      entries
    </div>
  <?php else: ?>
    <div class="text-info entries-info">
      There is no information
    </div>
  <?php endif; ?>

</form><!-- /#manage_form -->

<script>
jQuery(document).ready(function($) {
  $('a.collapse_row').click(function(){
    //alert('tr[data="'+$(this).attr('data')+'"]');
    // $('tr[data="'+$(this).attr('data')+'"]').toggle();
    // collapse_row($(this));
  });
  $('tr[data-children="0"]').each(function(){
    //$('tr[data-parent="'+$(this).attr('data-id')+'"]').addClass('inactive');
  });
});
function collapse_children(obj){
  $('tr[data-parent="'+obj.attr('data-parent')+'"]').toggle();
}
// function collapse_children1(obj){
//   var child = $('tr[data="'+$(this).attr('data')+'"]');
//   // if (child.attr('data-status') == 'i')
//   //   child.
// }
</script>