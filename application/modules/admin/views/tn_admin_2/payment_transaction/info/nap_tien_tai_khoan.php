<?php
$parser = array(
  'data' => array(
    array(
      'label' => lang('nha-cung-cap'),
      'text' => $nhacungcap[$transaction['data']['nhacungcap']]
    ),
    array(
      'label' => lang('menh-gia'),
      'text' => $menhgia[$transaction['data']['nhacungcap']][$transaction['data']['menhgia']]
    ),
    array(
      'label' => lang('tai-khoan'),
      'text' => $transaction['data']['taikhoan']
    ),
  )
);
$tab = $this->parser->parse('tn_admin_2/row_parser_text', $parser, TRUE);
echo $tab;
?>