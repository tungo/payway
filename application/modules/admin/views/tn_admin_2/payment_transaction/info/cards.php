<table class="table table-bordered manage-table">
  <thead>
    <tr>
      <th class="text-center">No.</th>
      <th class="text-center">Code</th>
      <th class="text-center">Serial</th>
      <th class="text-center">Expire</th>
    </tr>
  </thead>

  <tbody>

<?php
$i = 0;
foreach ($transaction['data']['cards'] as $card):
  $i++;
?>

    <tr>
      <td class="text-center"><?php echo $i; ?></td>
      <td class="text-center"><?php echo $card['code']; ?></td>
      <td class="text-center"><?php echo $card['serial']; ?></td>
      <td class="text-center"><?php echo $card['date']; ?></td>
    </tr>

<?php endforeach; ?>

  </tbody>

</table><!-- /.manage-table -->
