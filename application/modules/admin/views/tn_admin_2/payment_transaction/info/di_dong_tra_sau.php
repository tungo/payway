<?php
$parser = array(
  'data' => array(
    array(
      'label' => lang('nha-cung-cap'),
      'text' => $nhacungcap[$transaction['data']['nhacungcap']]
    ),
    array(
      'label' => lang('menh-gia'),
      'text' => $transaction['data']['menhgia']
    ),
    array(
      'label' => lang('so-di-dong'),
      'text' => $transaction['data']['dauso'].$transaction['data']['didong']
    )
  )
);
$tab = $this->parser->parse('tn_admin_2/row_parser_text', $parser, TRUE);
echo $tab;
?>