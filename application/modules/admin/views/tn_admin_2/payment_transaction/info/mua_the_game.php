<?php
$parser = array(
  'data' => array(
    array(
      'label' => lang('nha-cung-cap'),
      'text' => $nhacungcap[$transaction['data']['nhacungcap']]
    ),
    array(
      'label' => lang('menh-gia'),
      'text' => $menhgia[$transaction['data']['nhacungcap']][$transaction['data']['menhgia']]
    ),
    array(
      'label' => lang('so-luong'),
      'text' => $transaction['data']['soluong']
    )
  )
);
$tab = $this->parser->parse('tn_admin_2/row_parser_text', $parser, TRUE);
echo $tab;
?>