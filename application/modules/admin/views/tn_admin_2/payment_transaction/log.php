<?php
foreach($transaction['log'] as $log)
{
  $log['data'] = unserialize($log['data']);
  if (!is_array($log['data']['data']))
    $log['data']['data'] = unserialize($log['data']['data']);

$trans_data = '<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$log['id'].'">
          Data
        </a>
      </h4>
    </div>
    <div id="collapse'.$log['id'].'" class="panel-collapse collapse">
      <div class="panel-body">
        <pre>'.print_r($log['data'], true).'</pre>
      </div>
    </div>
  </div>
</div>';

$parser = array(
  'data' => array(
    array(
      'label' => 'Id',
      'text' => $log['id']
    ),
    array(
      'label' => 'Type',
      'text' => $log['type']
    ),
    array(
      'label' => 'Time',
      'text' => $log['time']
    ),
    array(
      'label' => 'Result',
      'text' => $log['result'].' ('.lang('atrans_st'.$log['result']).')'
    ),
    array(
      'label' => 'Data',
      'text' => $trans_data
    )
  )
);

  $tab = $this->parser->parse('tn_admin_2/row_parser_text', $parser, TRUE);
  echo $tab.'<hr>';
}
?>