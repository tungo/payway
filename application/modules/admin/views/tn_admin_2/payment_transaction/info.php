<?php

$parser = array(
  'data' => array(
    array(
      'label' => 'Id',
      'text' => $transaction['id']
    ),
    array(
      'label' => 'Email',
      'text' => $transaction['email']
    ),
    array(
      'label' => 'Type',
      'text' => $select_type[$transaction['type']]
    ),
    array(
      'label' => 'Amount',
      'text' => tn_money_format($transaction['amount'])
    ),
    array(
      'label' => 'Payment Id',
      'text' => $transaction['payment']['trans_id']
    ),
    array(
      'label' => 'Bank',
      'text' => $bank_code[$transaction['bank']]
    ),
    array(
      'label' => 'Status',
      'text' => $select_status[$transaction['status']]
    ),
    array(
      'label' => 'Created time',
      'text' => $transaction['created_time']
    ),
    array(
      'label' => 'Completed time',
      'text' => $transaction['completed_time']
    )
  )
);

$tab1 = $this->parser->parse('tn_admin_2/row_parser_text', $parser, TRUE);
?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
  <li><a href="#tab-data" data-toggle="tab">Data</a></li>
  <?php if ($tab_cards): ?>
  <li><a href="#tab-cards" data-toggle="tab">Cards</a></li>
  <?php endif; ?>
  <li><a href="#tab-log" data-toggle="tab">Log</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <fieldset>
      <?php echo $tab1; ?>
      </fieldset>
    </div>

    <div class="tab-pane" id="tab-data">
      <fieldset>
      <?php echo $tab_data; ?>
      </fieldset>
    </div>

    <?php if ($tab_cards): ?>
    <div class="tab-pane" id="tab-cards">
      <fieldset>
      <?php echo $tab_cards; ?>
      </fieldset>
    </div>
    <?php endif; ?>

    <div class="tab-pane" id="tab-log">
      <fieldset>
      <?php echo $log; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
				<a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-info"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
      </div>
    </div>

  </div>
</form>

<script>
// $(document).ready(function(){
// 	var rule = {
// 		name: {
// 			required: true
// 		}
// 	}
// 	valid_update_form(rule);
// });
</script>