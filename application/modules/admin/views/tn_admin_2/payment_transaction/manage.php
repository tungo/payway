<p class="lead"><?php echo $title; ?></p>

<div class="clearfix"></div>

<?php echo $this->tn_message->get(); ?>

<form id="manage_form" name="manage_form" method="post" action="">

  <div class="toolbar">
    <!-- <a href="<?php echo site_url($current_controller.'balance'); ?>" class="btn btn-info" title="Balance"><span class="glyphicon glyphicon-usd"></span> Balance</a> -->
    <a href="<?php echo site_url($current_controller.'excel'); ?>" class="btn btn-success" title="Excel"><span class="glyphicon glyphicon-file"></span> Excel</a>
  </div><!-- /.toolbar -->

  <div class="clearfix"></div>

  <div class="utility">

    <div class="pull-left">

      <div class="input-group search-tool">
        <input type="text" class="form-control input-small" name="keyword" placeholder="<?php echo $params['filter']['keyword'] ? $params['filter']['keyword'] : 'Enter keywords'; ?>" value="">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'search'); ?>');" class="btn btn-default" title="Search"><span class="glyphicon glyphicon-search"></span></a>
        </span>
      </div>

      <div class="input-group input-small filter-tool">
        <select name="type_filter" class="form-control">
          <option value="">All type</option>
        <?php
        $option = '';
        foreach($select_type as $key => $val)
        {
          $selected = '';
          if ((string) $params['filter']['type'] === (string) $key)
          {
            $selected = 'selected="selected"';
          }
          $option .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
        }
        echo $option;
        ?>
        </select>
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'filter'); ?>');" class="btn btn-default" title="Type"><span class="glyphicon glyphicon-filter"></span></a>
        </span>
      </div>

      <div class="input-group input-small filter-tool">
        <select name="status_filter" class="form-control">
          <option value="">All status</option>
        <?php
        $option = '';
        foreach($select_status as $key => $val)
        {
          $selected = '';
          if ((string) $params['filter']['status'] === (string) $key)
          {
            $selected = 'selected="selected"';
          }
          $option .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
        }
        echo $option;
        ?>
        </select>
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'filter'); ?>');" class="btn btn-default" title="Status"><span class="glyphicon glyphicon-filter"></span></a>
        </span>
      </div>

      <div class="input-group input-small filter-tool">
        <select name="bank_filter" class="form-control">
          <option value="">All bank</option>
        <?php
        $option = '';
        foreach($bank_code as $key => $val)
        {
          $selected = '';
          if ((string) $params['filter']['bank'] === (string) $key)
          {
            $selected = 'selected="selected"';
          }
          $option .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
        }
        echo $option;
        ?>
        </select>
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'filter'); ?>');" class="btn btn-default" title="Bank"><span class="glyphicon glyphicon-filter"></span></a>
        </span>
      </div>

      <div class="clearfix"></div>

      <div class="input-group" style="width:500px;">
        <span class="input-group-addon">From</span>
        <input type='text' name="from_date" class="form-control" id="datetimepicker1" data-date-format="YYYY/MM/DD" value="<?php echo $params['filter']['from_date']; ?>">
        <span class="input-group-addon">To</span>
        <input type='text' name="to_date" class="form-control" id="datetimepicker2" data-date-format="YYYY/MM/DD" value="<?php echo $params['filter']['to_date']; ?>">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'filter'); ?>');" class="btn btn-default" title="Date"><span class="glyphicon glyphicon-calendar"></span></a>
        </span>
      </div>

    </div><!-- /.pull-left -->

    <div class="pull-right">

      <div class="input-group entry-tool">
        <input type="text" class="form-control" name="list" placeholder="list" value="<?php echo $params['filter']['per_page']; ?>">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'show'); ?>');" class="btn btn-default" title="List"><span class="glyphicon glyphicon-list"></span></a>
        </span>
      </div>

      <div class="input-group page-tool">
        <input type="text" class="form-control" name="page" placeholder="page" value="<?php echo $params['pagination']->cur_page; ?>">
        <span class="input-group-btn">
          <a onclick="return go_to_page('<?php echo base_url($main_method.'/page'); ?>');" class="btn btn-default" title="Go"><span class="glyphicon glyphicon-play"></span></a>
        </span>
      </div>

    </div><!-- /.pull-right -->

  </div><!-- /.utility -->

  <div class="clearfix"></div>

  <?php
    echo '<input type="hidden" value="" name="sort">';
    $options = array(
      'link'  => site_url($current_controller.'sort'),
      'image' => ($params['filter']['order'] == 'DESC')
        ? ' <span class="glyphicon glyphicon-sort-by-attributes-alt"></span>'
        : ' <span class="glyphicon glyphicon-sort-by-attributes"></span>'
    );

    $id             = cms_sort('Id', 't.id', $params['filter'], $options);
    $email          = cms_sort('Email', 'ua.email', $params['filter'], $options);
    $type           = cms_sort('Type', 't.type', $params['filter'], $options);
    $amount         = cms_sort('Amount', 't.amount', $params['filter'], $options);
    $bank           = cms_sort('Bank', 't.bank', $params['filter'], $options);
    $status         = cms_sort('Status', 't.status', $params['filter'], $options);
    $created_time   = cms_sort('Created', 't.created_time', $params['filter'], $options);
    $completed_time = cms_sort('Completed', 't.completed_time', $params['filter'], $options);
  ?>

  <table class="table table-bordered manage-table">
    <thead>
      <tr>
        <th class="text-center"><?php echo $id; ?></th>
        <th><?php echo $email; ?></th>
        <th class="text-center"><?php echo $type; ?></th>
        <th class="text-center"><?php echo $amount; ?></th>
        <th class="text-center"><?php echo $bank; ?></th>
        <th class="text-center"><?php echo $status; ?></th>
        <th class="text-center"><?php echo $created_time; ?></th>
        <th>Data</th>
        <th width="15%">Note</th>
        <!-- <th class="text-center"><?php echo $completed_time; ?></th> -->
        <th></th>
      </tr>
    </thead>

    <tbody>

<?php
foreach($data['items'] as $row):
  $control = '
    <a onclick="location.href = \''.site_url($current_controller.'edit/id/'.$row['id']).'\'" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
    <a onclick="location.href = \''.site_url($current_controller.'info/id/'.$row['id']).'\'" class="btn btn-info btn-xs" title="Edit"><span class="glyphicon glyphicon-info-sign"></span></a>
  ';
  if ($row['status'] == GD_XAC_NHAN_TB || $row['status'] == GD_HOAN_TIEN_TB)
  $control .= '
    <a onclick="location.href = \''.site_url($current_controller.'finish/id/'.$row['id']).'\'" class="btn btn-success btn-xs" title="Finish"><span class="glyphicon glyphicon-ok"></span></a>
  ';
?>

      <tr>
        <td class="text-center"><?php echo $row['id']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td class="text-center"><?php echo $select_type[$row['type']]; ?></td>
        <td class="text-center"><?php echo tn_money_format($row['amount']); ?></td>
        <td class="text-center"><?php echo $bank_code[$row['bank']]; ?></td>
        <td class="text-center"><?php echo $select_status[$row['status']]; ?></td>
        <td class="text-center"><?php echo $row['created_time']; ?></td>
        <!-- <td class="text-center"><?php echo $row['completed_time']; ?></td> -->
        <td>
<?php
if(!empty($row['data']))
{
  $row['data'] = unserialize($row['data']);
  if ($row['type'] == DI_DONG_TRA_TRUOC)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$didongtratruoc['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$didongtratruoc['menhgia'][$row['data']['nhacungcap']][$row['data']['menhgia']].'</b></div>';
    echo '<div><i>'.lang('so-di-dong').'</i>: <b>'.$row['data']['dauso'].$row['data']['didong'].'</b></div>';
  }
  elseif ($row['type'] == DI_DONG_TRA_SAU)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$didongtrasau['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$row['data']['menhgia'].'</b></div>';
    echo '<div><i>'.lang('so-di-dong').'</i>: <b>'.$row['data']['dauso'].$row['data']['didong'].'</b></div>';
  }
  elseif ($row['type'] == MUA_THE_DIEN_THOAI)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$muathedienthoai['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$muathedienthoai['menhgia'][$row['data']['nhacungcap']][$row['data']['menhgia']].'</b></div>';
    echo '<div><i>'.lang('so-luong').'</i>: <b>'.$row['data']['soluong'].'</b></div>';
  }
  elseif ($row['type'] == MUA_THE_GAME)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$muathegame['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$muathegame['menhgia'][$row['data']['nhacungcap']][$row['data']['menhgia']].'</b></div>';
    echo '<div><i>'.lang('so-luong').'</i>: <b>'.$row['data']['soluong'].'</b></div>';
  }
  elseif ($row['type'] == NAP_TIEN_GAME)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$naptiengame['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$naptiengame['menhgia'][$row['data']['nhacungcap']][$row['data']['menhgia']].'</b></div>';
    echo '<div><i>'.lang('tai-khoan').'</i>: <b>'.$row['data']['taikhoan'].'</b></div>';
  }
  elseif ($row['type'] == NAP_TIEN_TAI_KHOAN)
  {
    echo '<div><i>'.lang('nha-cung-cap').'</i>: <b>'.$naptientaikhoan['nhacungcap'][$row['data']['nhacungcap']].'</b></div>';
    echo '<div><i>'.lang('menh-gia').'</i>: <b>'.$naptientaikhoan['menhgia'][$row['data']['nhacungcap']][$row['data']['menhgia']].'</b></div>';
    echo '<div><i>'.lang('tai-khoan').'</i>: <b>'.$row['data']['taikhoan'].'</b></div>';
  }
}
?>
        </td>
        <td class="text-justify"><?php echo newline_html($row['note']); ?></td>
        <td class="text-center"><?php echo $control; ?></td>
      </tr>

<?php endforeach; ?>

    </tbody>

  </table><!-- /.manage-table -->

  <div class="clearfix"></div>

  <?php if (count($data['items'])): ?>

    <div class="text-info entries-info pull-left">
      Showing <?php echo $params['pagination']->start_row; ?>
      to <?php echo $params['pagination']->end_row; ?>
      of <?php echo $params['pagination']->total_rows; ?>
      entries
      <br>
      Page <?php echo $params['pagination']->cur_page.' of '.$params['pagination']->total_pages; ?>
    </div>

    <div class="pull-right">
      <?php echo $params['pagination']->links; ?>
    </div>

  <?php else: ?>
    <div class="text-info entries-info">
      There is no information
    </div>
  <?php endif; ?>

</form><!-- /#manage_form -->

<link href="<?php echo base_url($theme['css'].'bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">

<script src="<?php echo base_url($theme['js'].'moment.min.js'); ?>"></script>
<script src="<?php echo base_url($theme['js'].'bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">
  $(function () {
    $('#datetimepicker1').datetimepicker({
      pickTime: false
    });
    $('#datetimepicker2').datetimepicker({
      pickTime: false
    });
  });
</script>