<?php
$note = form_textarea('note', set_value('note', $data['item']['note']), 'class="form-control"');

$parser = array(
  'data' => array(
    array(
      'name'  => 'note',
      'label' => 'Note',
      'field' => $note,
      'help'  => ''
    ),
  ),
);

$tab1 = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);
?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <fieldset>
      <?php echo $tab1; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
        <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
        <a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
      </div>
    </div>

  </div>
</form>

<script>
// $(document).ready(function(){
// 	var rule = {
// 		name: {
// 			required: true
// 		}
// 	}
// 	valid_update_form(rule);
// });
</script>