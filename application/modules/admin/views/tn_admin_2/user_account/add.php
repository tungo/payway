<?php
$group_id = '<select name="group_id" class="form-control">';
foreach($select_group as $val)
{
  $group_id .= '<option value="'.$val['id'].'">'.$val['name'].'</option>';
}
$group_id .= '</select>';

$email = form_input('email', set_value('email'), 'class="form-control"');
$password = form_password('password', '', 'class="form-control"');
$confirm = form_password('confirm', '', 'class="form-control"');
$firstname = form_input('firstname', set_value('firstname'), 'class="form-control"');
$lastname = form_input('lastname', set_value('lastname'), 'class="form-control"');

$status = '
  <label class="radio-inline">
    <input type="radio" name="status" value="1" checked="checked"> Enable
  </label>
  <label class="radio-inline">
    <input type="radio" name="status" value="0"> Disable
  </label>
';

$parser = array(
  'data' => array(
    array(
      'name'  => 'group_id',
      'label' => 'Group',
      'field' => $group_id,
      'help'  => ''
    ),
    array(
      'name'  => 'email',
      'label' => 'Email',
      'field' => $email,
      'help'  => ''
    ),
    array(
      'name'  => 'password',
      'label' => 'Password',
      'field' => $password,
      'help'  => ''
    ),
    array(
      'name'  => 'confirm',
      'label' => 'Password Confirm',
      'field' => $confirm,
      'help'  => ''
    ),
    array(
      'name'  => 'firstname',
      'label' => 'Firstname',
      'field' => $firstname,
      'help'  => ''
    ),
    array(
      'name'  => 'lastname',
      'label' => 'Lastname',
      'field' => $lastname,
      'help'  => ''
    ),
    array(
      'name'  => 'status',
      'label' => 'Status',
      'field' => $status,
      'help'  => ''
    ),
  )
);

$tab1 = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);

?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <fieldset>
        <?php echo $tab1; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
        <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
        <a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
      </div>
    </div>

  </div>
</form>

<script>
// $(document).ready(function(){
//   var rule = {
//     name: {
//       required: true
//     }
//   }
//   valid_update_form(rule);
// });
</script>