<form class="form-signin" method="" action="">
  <h2 class="form-signin-heading">Access retricted</h2>
  <p class="text-danger">Please sign out and try another account.</p>
  <a class="btn btn-lg btn-primary btn-block" href="<?php echo site_url($path.'auth/logout'); ?>" title="Log out">Log out</a>
  <?php echo $this->tn_message->get(); ?>
</form>
