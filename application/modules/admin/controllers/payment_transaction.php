<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_transaction extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('payment/transaction_model');

    $this->data['title'] = 'Payment : Transaction';

    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('vnpay_paygate_bank_code');
    $this->data['trans_status'] = $this->config->item('trans_status');
  }

  public function index()
  {
    $this->load->library('TN_Pagination');
    // filter
    $this->data['params']['filter'] = array();
    if (! $this->session->userdata($this->data['namespace']))
    {
      $this->data['params']['filter'] = array(
        'keyword'  => '',
        'sort'     => 't.id',
        'order'    => 'DESC',
        'page'     => 1,
        'per_page' => $this->tn_pagination->per_page,
        'type'      => '',
        'status'    => '',
        'bank'      => '',
        'from_date' => '',
        'to_date'   => '',
      );
      $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
    }
    else
    {
      $this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
      if (isset($this->data['request']['page']))
      {
        $this->data['params']['filter']['page'] = (int) $this->data['request']['page'];
        $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
      }
    }

    // pagination
    $this->tn_pagination->cur_page   = $this->data['params']['filter']['page'];
    $this->tn_pagination->base_url   = base_url($this->data['main_method']);
    $this->tn_pagination->per_page   = $this->data['params']['filter']['per_page'];
    $this->tn_pagination->total_rows = $this->transaction_model->count_entries_admin($this->data['params']);
    $this->tn_pagination->create_links();

    $this->data['params']['pagination'] = $this->tn_pagination;

    // get list
    $this->data['data']['items'] = $this->transaction_model->get_entries_admin($this->data['params']);

    $this->load->model('payment/service_model');
    $this->data['didongtratruoc'] = $this->service_model->didongtratruoc();
    $this->data['didongtrasau'] = $this->service_model->didongtrasau();
    $this->data['muathedienthoai'] = $this->service_model->muathedienthoai();
    $this->data['muathegame'] = $this->service_model->muathegame();
    $this->data['naptiengame'] = $this->service_model->naptiengame();
    $this->data['naptientaikhoan'] = $this->service_model->naptientaikhoan();

    $service_code = array_keys($this->data['service_code']);
    $this->data['select_type'] = array();
    foreach ($service_code as $val)
      $this->data['select_type'][$val] = lang($val);

    $this->data['select_status'] = array();
    foreach ($this->data['trans_status'] as $val)
      $this->data['select_status'][$val] = lang('atrans_st'.$val);

    $this->data['title'] = $this->data['title'].' : Manage';
    $this->load_view('payment_transaction/manage', $this->data);
  }

  public function excel()
  {
    // filter
    $this->data['params']['filter'] = array();
    if (! $this->session->userdata($this->data['namespace']))
    {
      $this->data['params']['filter'] = array(
        'keyword'  => '',
        'sort'     => 't.id',
        'order'    => 'DESC',
        'page'     => 1,
        'per_page' => $this->tn_pagination->per_page,
        'type'      => '',
        'status'    => '',
        'bank'      => '',
        'from_date' => '',
        'to_date'   => '',
      );
      $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
    }
    else
    {
      $this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
    }

    // get list
    $this->data['data']['items'] = $this->transaction_model->get_entries_admin($this->data['params']);

    $this->load->model('payment/service_model');
    $this->data['didongtratruoc'] = $this->service_model->didongtratruoc();
    $this->data['didongtrasau'] = $this->service_model->didongtrasau();
    $this->data['muathedienthoai'] = $this->service_model->muathedienthoai();
    $this->data['muathegame'] = $this->service_model->muathegame();
    $this->data['naptiengame'] = $this->service_model->naptiengame();
    $this->data['naptientaikhoan'] = $this->service_model->naptientaikhoan();

    $service_code = array_keys($this->data['service_code']);
    $this->data['select_type'] = array();
    foreach ($service_code as $val)
      $this->data['select_type'][$val] = lang($val);

    $this->data['select_status'] = array();
    foreach ($this->data['trans_status'] as $val)
      $this->data['select_status'][$val] = lang('atrans_st'.$val);

    $file = $this->transaction_model->export_excel($this->data);
    redirect($file);
    return;
  }

  public function info()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->data['transaction'] = $this->transaction_model->get_entry($this->data['params']);
    if ( ! $this->data['transaction']['id'])
    {
      $this->tn_message->set_warning('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    $this->data['transaction']['data'] = unserialize($this->data['transaction']['data']);
    $this->data['transaction']['payment'] = unserialize($this->data['transaction']['payment']);

    $this->data['transaction']['log'] = $this->transaction_model->get_log_admin($this->data['params']);
    $this->data['log'] = $this->load->view($this->data['theme']['name'].'/payment_transaction/log', $this->data, true);

    $service_code = array_keys($this->data['service_code']);
    $this->data['select_type'] = array();
    foreach ($service_code as $val)
      $this->data['select_type'][$val] = lang($val);

    $this->data['select_status'] = array();
    foreach ($this->data['trans_status'] as $val)
      $this->data['select_status'][$val] = lang('atrans_st'.$val);

    $this->load->model('payment/service_model');
    if ($this->data['transaction']['type'] == DI_DONG_TRA_TRUOC)
    {
      $detail = 'di_dong_tra_truoc';
      $this->data = array_merge($this->data, $this->service_model->didongtratruoc());
    }
    elseif ($this->data['transaction']['type'] == DI_DONG_TRA_SAU)
    {
      $detail = 'di_dong_tra_sau';
      $this->data = array_merge($this->data, $this->service_model->didongtrasau());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_DIEN_THOAI)
    {
      $detail = 'mua_the_dien_thoai';
      $this->data = array_merge($this->data, $this->service_model->muathedienthoai());
    }
    elseif ($this->data['transaction']['type'] == MUA_THE_GAME)
    {
      $detail = 'mua_the_game';
      $this->data = array_merge($this->data, $this->service_model->muathegame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_GAME)
    {
      $detail = 'nap_tien_game';
      $this->data = array_merge($this->data, $this->service_model->naptiengame());
    }
    elseif ($this->data['transaction']['type'] == NAP_TIEN_TAI_KHOAN)
    {
      $detail = 'nap_tien_tai_khoan';
      $this->data = array_merge($this->data, $this->service_model->naptientaikhoan());
    }

    $this->data['tab_data'] = $this->load->view($this->data['theme']['name'].'/payment_transaction/info/'.$detail, $this->data, true);

    $this->data['tab_cards'] = '';
    if (!empty($this->data['transaction']['data']['cards']))
    {
      $this->data['tab_cards'] = $this->load->view($this->data['theme']['name'].'/payment_transaction/info/cards', $this->data, true);
    }

    $this->data['title'] = $this->data['title'].' : Info';
    $this->load->library('parser');
    $this->load_view('payment_transaction/info', $this->data);
  }

  public function edit()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->data['data']['item'] = $this->transaction_model->get_entry($this->data['params']);
    if ( ! $this->data['data']['item']['id'])
    {
      $this->tn_message->set_warning('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    // $this->form_validation->set_rules('note', 'Note', 'required');

    // if ($this->form_validation->run() == TRUE)
    if (!empty($_POST))
    {
      $this->data['params']['method'] = $this->data['method'];
      if ($this->transaction_model->update_entry($this->data['params']))
      {
        $this->tn_message->set_success('Edited successfully.');
        $this->tn_message->save();
      }
      else
      {
        $this->tn_message->set_error('Failed to edit.');
        $this->tn_message->save();
      }
      redirect($this->data['current_controller']);
      return;
    }
    else
    {
      // $this->data['form_error'][] = form_error('note');
    }
    init_form_error($this->data['form_error']);

    $this->data['title'] = $this->data['title'].' : Edit';
    $this->load->library('parser');
    $this->load_view('payment_transaction/edit', $this->data);
  }

  public function finish()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->data['transaction'] = $this->transaction_model->get_entry($this->data['params']);
    if (empty($this->data['transaction']['id']))
    {
      $this->tn_message->set_warning('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->data['transaction']['status'] == GD_XAC_NHAN_TB)
      $this->data['transaction']['status'] = GD_XAC_NHAN_TC;
    elseif ($this->data['transaction']['status'] == GD_HOAN_TIEN_TB)
      $this->data['transaction']['status'] = GD_HOAN_TIEN_TC;
    else
    {
      $this->tn_message->set_warning('Information is wrong.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->transaction_model->update_status($this->data))
    {
      $this->tn_message->set_success('Finish successfully.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_error('Failed to finish.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function search()
  {
    if ($_POST)
    {
      $keyword = isset($this->data['params']['keyword']) ? $this->data['params']['keyword'] : '';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['keyword'] = $keyword;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function show()
  {
    if ($_POST)
    {
      $per_page = isset($this->data['params']['list']) ? $this->data['params']['list'] : 10;
      if ($per_page < 1) $per_page = 10;

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['per_page'] = $per_page;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function sort()
  {
    if ($_POST)
    {
      $sort = isset($this->data['params']['sort']) ? $this->data['params']['sort'] : $this->transaction_model->table.'.id';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['sort'] = $sort;
      $filter['order'] = ($filter['order'] == 'DESC') ? 'ASC' : 'DESC';
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function filter()
  {
    if ($_POST)
    {
      $filter = $this->session->userdata($this->data['namespace']);
      $filter['type'] = (isset($this->data['params']['type_filter'])) ? $this->data['params']['type_filter'] : '';
      $filter['status'] = (isset($this->data['params']['status_filter'])) ? $this->data['params']['status_filter'] : '';
      $filter['bank'] = (isset($this->data['params']['bank_filter'])) ? $this->data['params']['bank_filter'] : '';
      $filter['from_date'] = (isset($this->data['params']['from_date'])) ? $this->data['params']['from_date'] : '';
      $filter['to_date'] = (isset($this->data['params']['to_date'])) ? $this->data['params']['to_date'] : '';
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  // ----- Kiểm tra số dư tài khoản -----
  // public function balance()
  // {
  //   $this->load->model('payment/vtc_pay_model');
  //   $result_process = $this->vtc_pay_model->get_balance($this->data);
  //   $this->data['balance'] = ($result_process) ? number_format($this->vtc_pay_model->result['code'], 0, ',', '.').' đ' : 'Lỗi kết nối';

  //   $this->data['title'] = $this->data['title'].' : Balance';
  //   $this->load->library('parser');
  //   $this->load_view('payment_transaction/balance', $this->data);
  // }
}