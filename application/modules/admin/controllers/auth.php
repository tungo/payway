<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->data['theme']['layout'] = 'login';
	}

	// Alert access restrict at auth/index
	public function index()
	{
		// if user can access system
		if ($this->tn_auth->check_access())
			redirect($this->data['path']);

		$this->tn_message->set_error('Your account can not access this page.');

		$this->data['title'] = 'Admin';
		$this->load_view('auth', $this->data);
	}

	// Log in page
	public function login()
	{
		// if logged in
		if ($this->tn_auth->is_logged_in())
			redirect($this->data['path']);

		$this->load->library('form_validation');
		$this->load->helper('form');
    $this->data['form_error'] = array();

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		$this->data['params']['remember'] = (bool) $this->input->post('remember');

		if ($this->form_validation->run() == TRUE)
		{
			if (!$this->tn_auth->login($this->data))
			{
        $this->data['form_error'][] = 'Email or Password is incorrect.';
			}
			else
			{
				if ($this->data['params']['remember'] && !$this->input->cookie('remember_code'))
					$this->tn_message->set_warning('Can not remember your account on this computer.');

				$this->tn_message->set_info('<strong>Welcome to Admin!</strong> You can dismiss the alert by clicking X button!');
				$this->tn_message->save();

				redirect($this->data['path']);
			}
		}
		else
		{
      $this->data['form_error'][] = form_error('email');
      $this->data['form_error'][] = form_error('password');
		}
    init_form_error($this->data['form_error']);

		$this->data['title'] = 'Login';
		$this->load_view('login', $this->data);
	}

	// Log out page
	public function logout()
	{
		$this->tn_auth->logout();

		$this->tn_message->set_success('Signed out successfully.');
		$this->tn_message->save();

		redirect($this->data['path'].'auth/login');
	}
}