<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_article extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('database/blog_article_model');

    $this->data['image_url'] = $this->blog_article_model->image_url;

    $this->data['params']['languages'] = $this->data['languages'];
    $this->data['params']['language_id'] = $this->data['language_id'];

    $this->data['title'] = 'Blog : Article';
  }

  public function index()
  {
    $this->load->library('TN_Pagination');
    // filter
    $this->data['params']['filter'] = array();
    if (! $this->session->userdata($this->data['namespace']))
    {
      $this->data['params']['filter'] = array(
        'keyword'  => '',
        'sort'     => 'ba.id',
        'order'    => 'DESC',
        'page'     => 1,
        'per_page' => $this->tn_pagination->per_page,
        'category' => ''
      );
      $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
    }
    else
    {
      $this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
      if (isset($this->data['request']['page']))
      {
        $this->data['params']['filter']['page'] = (int) $this->data['request']['page'];
        $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
      }
    }

    // pagination
    $this->tn_pagination->cur_page   = $this->data['params']['filter']['page'];
    $this->tn_pagination->base_url   = base_url($this->data['main_method']);
    $this->tn_pagination->per_page   = $this->data['params']['filter']['per_page'];
    $this->tn_pagination->total_rows = $this->blog_article_model->count_entries_admin($this->data['params']);
    $this->tn_pagination->create_links();

    $this->data['params']['pagination'] = $this->tn_pagination;

    // get list
    $this->data['data']['items'] = $this->blog_article_model->get_entries_admin($this->data['params']);

    $this->load->model('database/blog_category_model');
    $this->data['select_category'] = $this->blog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'article-filter'));

    $this->data['title'] = $this->data['title'].' : Manage';
    $this->load_view('blog_article/manage', $this->data);
  }

  public function add()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('status', 'Status', 'required');

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      $this->data['params']['image'] = $this->blog_article_model->upload_file($this->data['params']);
      if ($this->data['params']['image'] === FALSE)
      {
        $this->data['form_error'][] = $this->upload->display_errors();
      }
      else
      {
        if (! $this->data['params']['seo_name'])
        {
          $this->data['params']['seo_name'] = furl($this->data['params']['description'][$this->data['params']['language_id']]['name']);
        }

        if ($this->blog_article_model->add_entry($this->data['params']))
        {
          $this->tn_message->set_success('Added successfully.');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error('Failed to add.');
          $this->tn_message->save();
        }
        redirect($this->data['current_controller']);
        return;
      }
    }
    else
    {
      $this->data['form_error'][] = form_error('status');
    }
    init_form_error($this->data['form_error']);

    $this->load->model('database/blog_category_model');
    $this->data['select_category'] = $this->blog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'article-admin'));

    $this->data['title'] = $this->data['title'].' : Add';
    $this->load->library('parser');
    $this->load_view('blog_article/add', $this->data);
  }

  public function edit()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->data['data']['item'] = $this->blog_article_model->get_entry($this->data['params']);
    if ( ! $this->data['data']['item']['id'])
    {
      $this->tn_message->set_warning('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('status', 'Status', 'required');

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      $this->data['params']['image'] = $this->blog_article_model->upload_file($this->data['params']);
      if ($this->data['params']['image'] === FALSE)
      {
        $this->data['form_error'][] = $this->upload->display_errors();
      }
      else
      {
        if (! $this->data['params']['seo_name'])
        {
          $this->data['params']['seo_name'] = furl($this->data['params']['description'][$this->data['params']['language_id']]['name']);
        }

        if ($this->blog_article_model->update_entry($this->data['params']))
        {
          $this->tn_message->set_success('Edited successfully.');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error('Failed to edit.');
          $this->tn_message->save();
        }
        redirect($this->data['current_controller']);
        return;
      }
    }
    else
    {
      $this->data['form_error'][] = form_error('status');
    }
    init_form_error($this->data['form_error']);

    $this->load->model('database/blog_category_model');
    $this->data['select_category'] = $this->blog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'article-admin'));

    $this->data['title'] = $this->data['title'].' : Edit';
    $this->load->library('parser');
    $this->load_view('blog_article/edit', $this->data);
  }

  public function multi_copy()
  {
    if ($_POST)
    {
      if ($this->blog_article_model->copy_multi_entries($this->data['params']))
      {
        $this->tn_message->set_success('Coped successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to copy.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function delete()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    if ( ! $this->blog_article_model->check_entry($this->data['params']))
    {
      $this->tn_message->set_error('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->blog_article_model->delete_entry($this->data['params']))
    {
      $this->tn_message->set_success('Deleted successfully.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_error('Failed to delete.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function multi_delete()
  {
    if ($_POST)
    {
      if ($this->blog_article_model->delete_multi_entries($this->data['params']))
      {
        $this->tn_message->set_success('Deleted successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to delete.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function save()
  {
    if ($_POST)
    {
      if ($this->blog_article_model->save_entries($this->data['params']))
      {
        $this->tn_message->set_success('Updated successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to update.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function status()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;
    $this->data['params']['status'] = isset($this->data['request']['status']) ? (int) $this->data['request']['status'] : 0;

    if ( ! $this->blog_article_model->check_entry($this->data['params']))
    {
      $this->tn_message->set_error('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->blog_article_model->change_status_entry($this->data['params']))
    {
      $this->tn_message->set_success('Changed status successfully.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_error('Failed to change status.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function active()
  {
    if ($_POST)
    {
      if ($this->blog_article_model->active_entries($this->data['params']))
      {
        $this->tn_message->set_success('Active successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to active.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function disable()
  {
    if ($_POST)
    {
      if ($this->blog_article_model->disable_entries($this->data['params']))
      {
        $this->tn_message->set_success('Disable successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to disable.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function search()
  {
    if ($_POST)
    {
      $keyword = isset($this->data['params']['keyword']) ? $this->data['params']['keyword'] : '';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['keyword'] = $keyword;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function show()
  {
    if ($_POST)
    {
      $per_page = isset($this->data['params']['list']) ? $this->data['params']['list'] : 10;
      if ($per_page < 1) $per_page = 10;

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['per_page'] = $per_page;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function sort()
  {
    if ($_POST)
    {
      $sort = isset($this->data['params']['sort']) ? $this->data['params']['sort'] : $this->blog_article_model->table.'.id';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['sort'] = $sort;
      $filter['order'] = ($filter['order'] == 'DESC') ? 'ASC' : 'DESC';
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function filter()
  {
    if ($_POST)
    {
      $category_filter = '';
      if (isset($this->data['params']['category_filter']))
      {
        $category_filter = ($this->data['params']['category_filter'] !== '')
          ? (int) $this->data['params']['category_filter']
          : '';
      }

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['category'] = $category_filter;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }
}