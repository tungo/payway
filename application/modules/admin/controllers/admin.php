<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->data['request'] = $this->uri->uri_to_assoc(3);
	}

	public function index()
	{
    $this->load->model('payment/transaction_model');
		$this->data['transactions']['total'] = $this->transaction_model->count_entries_admin($this->data['params']);
		$this->data['params']['filter']['status'] = 4;
		$this->data['transactions']['success'] = $this->transaction_model->count_entries_admin($this->data['params']);

		$this->data['title'] = 'Dashboard';
		$this->load_view('dashboard', $this->data);
	}

	public function language()
	{
		$this->session->set_userdata($this->data['language_session'], $this->data['request']['id']);
		redirect($this->input->server('HTTP_REFERER'));
	}
}