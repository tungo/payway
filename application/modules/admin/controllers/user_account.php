<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_account extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('database/user_account_model');

    $this->data['title'] = 'User : Account';
  }

  public function index()
  {
    $this->load->library('TN_Pagination');
    // filter
    $this->data['params']['filter'] = array();
    if (! $this->session->userdata($this->data['namespace']))
    {
      $this->data['params']['filter'] = array(
        'keyword'  => '',
        'sort'     => 'ua.id',
        'order'    => 'DESC',
        'page'     => 1,
        'per_page' => $this->tn_pagination->per_page,
        'group' => '',
      );
      $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
    }
    else
    {
      $this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
      if (isset($this->data['request']['page']))
      {
        $this->data['params']['filter']['page'] = (int) $this->data['request']['page'];
        $this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
      }
    }

    // pagination
    $this->tn_pagination->cur_page   = $this->data['params']['filter']['page'];
    $this->tn_pagination->base_url   = base_url($this->data['main_method']);
    $this->tn_pagination->per_page   = $this->data['params']['filter']['per_page'];
    $this->tn_pagination->total_rows = $this->user_account_model->count_entries_admin($this->data['params']);
    $this->tn_pagination->create_links();

    $this->data['params']['pagination'] = $this->tn_pagination;

    // get list
    $this->data['data']['items'] = $this->user_account_model->get_entries_admin($this->data['params']);

    $this->load->model('database/user_group_model');
    $this->data['select_group'] = $this->user_group_model->get_entries_select_admin($this->data['params'], array('method' => 'account-filter'));

    $this->data['title'] = $this->data['title'].' : Manage';
    $this->load_view('user_account/manage', $this->data);
  }

  public function add()
  {
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('group_id', 'Group', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'required|matches[confirm]');
    $this->form_validation->set_rules('confirm', 'Password Confirm', 'required');
    $this->form_validation->set_rules('firstname', 'Firstname', 'required');
    $this->form_validation->set_rules('lastname', 'Lastname', 'required');

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      if ($this->user_account_model->check_email($this->data['params']))
      {
        $this->data['form_error'][] = 'Email is already used!';
      }
      else
      {
        if ($this->user_account_model->add_entry($this->data['params']))
        {
          $this->tn_message->set_success('Added successfully.');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error('Failed to add.');
          $this->tn_message->save();
        }
        redirect($this->data['current_controller']);
        return;
      }
    }
    else
    {
      $this->data['form_error'][] = form_error('group_id');
      $this->data['form_error'][] = form_error('email');
      $this->data['form_error'][] = form_error('password');
      $this->data['form_error'][] = form_error('confirm');
      $this->data['form_error'][] = form_error('firstname');
      $this->data['form_error'][] = form_error('lastname');
    }
    init_form_error($this->data['form_error']);

    $this->load->model('database/user_group_model');
    $this->data['select_group'] = $this->user_group_model->get_entries_select_admin($this->data['params'], array('method' => 'account-admin'));

    $this->data['title'] = $this->data['title'].' : Add';
    $this->load->library('parser');
    $this->load_view('user_account/add', $this->data);
  }

  public function edit()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    $this->data['data']['item'] = $this->user_account_model->get_entry($this->data['params']);
    if ( ! $this->data['data']['item']['id'])
    {
      $this->tn_message->set_warning('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->data['form_error'] = array();

    $this->form_validation->set_rules('group_id', 'Group', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'matches[confirm]');
    $this->form_validation->set_rules('firstname', 'Firstname', 'required');
    $this->form_validation->set_rules('lastname', 'Lastname', 'required');

    if ($this->form_validation->run() == TRUE)
    {
      $this->data['params']['method'] = $this->data['method'];
      if ($this->user_account_model->check_email($this->data['params']))
      {
        $this->data['form_error'][] = 'Email is already used!';
      }
      elseif ($this->data['params']['id'] == 1 && $this->data['params']['password'] && $this->data['member']['id'] != 1)
      {
        $this->data['form_error'][] = 'You can not change pass this account!';
      }
      else
      {
        if ($this->user_account_model->update_entry($this->data['params']))
        {
          $this->tn_message->set_success('Edited successfully.');
          $this->tn_message->save();
        }
        else
        {
          $this->tn_message->set_error('Failed to edit.');
          $this->tn_message->save();
        }
        redirect($this->data['current_controller']);
        return;
      }
    }
    else
    {
      $this->data['form_error'][] = form_error('group_id');
      $this->data['form_error'][] = form_error('email');
      $this->data['form_error'][] = form_error('password');
      $this->data['form_error'][] = form_error('firstname');
      $this->data['form_error'][] = form_error('lastname');
    }
    init_form_error($this->data['form_error']);

    $this->load->model('database/user_group_model');
    $this->data['select_group'] = $this->user_group_model->get_entries_select_admin($this->data['params'], array('method' => 'account-admin'));

    $this->data['title'] = $this->data['title'].' : Edit';
    $this->load->library('parser');
    $this->load_view('user_account/edit', $this->data);
  }

  public function delete()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;

    if ( ! $this->user_account_model->check_entry($this->data['params']))
    {
      $this->tn_message->set_error('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->user_account_model->delete_entry($this->data['params']))
    {
      $this->tn_message->set_success('Deleted successfully.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_error('Failed to delete.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function multi_delete()
  {
    if ($_POST)
    {
      if ($this->user_account_model->delete_multi_entries($this->data['params']))
      {
        $this->tn_message->set_success('Deleted successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to delete.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function status()
  {
    $this->data['params']['id'] = isset($this->data['request']['id']) ? (int) $this->data['request']['id'] : 0;
    $this->data['params']['status'] = isset($this->data['request']['status']) ? (int) $this->data['request']['status'] : 0;

    if ( ! $this->user_account_model->check_entry($this->data['params']))
    {
      $this->tn_message->set_error('Information is not founded.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }

    if ($this->user_account_model->change_status_entry($this->data['params']))
    {
      $this->tn_message->set_success('Changed status successfully.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_error('Failed to change status.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function active()
  {
    if ($_POST)
    {
      if ($this->user_account_model->active_entries($this->data['params']))
      {
        $this->tn_message->set_success('Active successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to active.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function disable()
  {
    if ($_POST)
    {
      if ($this->user_account_model->disable_entries($this->data['params']))
      {
        $this->tn_message->set_success('Disable successfully.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
      else
      {
        $this->tn_message->set_error('Failed to disable.');
        $this->tn_message->save();
        redirect($this->input->server('HTTP_REFERER'));
        return;
      }
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function search()
  {
    if ($_POST)
    {
      $keyword = isset($this->data['params']['keyword']) ? $this->data['params']['keyword'] : '';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['keyword'] = $keyword;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function show()
  {
    if ($_POST)
    {
      $per_page = isset($this->data['params']['list']) ? $this->data['params']['list'] : 10;
      if ($per_page < 1) $per_page = 10;

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['per_page'] = $per_page;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function sort()
  {
    if ($_POST)
    {
      $sort = isset($this->data['params']['sort']) ? $this->data['params']['sort'] : $this->user_account_model->table.'.id';

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['sort'] = $sort;
      $filter['order'] = ($filter['order'] == 'DESC') ? 'ASC' : 'DESC';
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }

  public function filter()
  {
    if ($_POST)
    {
      $group_filter = '';
      if (isset($this->data['params']['group_filter']))
      {
        $group_filter = ($this->data['params']['group_filter'] !== '')
          ? (int) $this->data['params']['group_filter']
          : '';
      }

      $filter = $this->session->userdata($this->data['namespace']);
      $filter['group'] = $group_filter;
      $this->session->set_userdata($this->data['namespace'], $filter);

      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
    else
    {
      $this->tn_message->set_warning('Access restricted.');
      $this->tn_message->save();
      redirect($this->input->server('HTTP_REFERER'));
      return;
    }
  }
}