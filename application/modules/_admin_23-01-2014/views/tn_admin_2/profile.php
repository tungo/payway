<?php
$username  = '<span class="text-block">'.$data['item']->username.'</span>';
$password  = form_password('password', '', 'class="input-block-level"');
$confirm   = form_password('confirm', '', 'class="input-block-level"');
$firstname = form_input('firstname', set_value('firstname', $data['item']->firstname), 'class="input-block-level"');
$lastname  = form_input('lastname', set_value('lastname', $data['item']->lastname), 'class="input-block-level"');

$parser1 = array(
	'data' => array(
		array(
			'name'  => 'username',
			'label' => 'Username',
			'field' => $username,
			'help'  => ''
		),
		array(
			'name'  => 'password',
			'label' => 'Pasword',
			'field' => $password,
			'help'  => ''
		),
		array(
			'name'  => 'confirm',
			'label' => 'Confirm',
			'field' => $confirm,
			'help'  => ''
		),
		array(
			'name'  => 'firstname',
			'label' => 'Firstname',
			'field' => $firstname,
			'help'  => ''
		),
		array(
			'name'  => 'lastname',
			'label' => 'Lastname',
			'field' => $lastname,
			'help'  => ''
		)
	)
);

$tab1_legend1 = $this->parser->parse('row_paser', $parser1, TRUE);

// error
$this->form_validation->set_error_delimiters('', '<br>');

$form_error = array(
	(form_error('password') || form_error('confirm') ) ? 'The Confirm field does not match the Password field.<br>' : '',
	form_error('firstname'),
	form_error('lastname')
);
$form_error = implode('', $form_error);

if ($form_error)
{
	$this->tn_message->set_error('<h4>Error</h4>'.$form_error);
}

?>

<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	</ul>
	<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">
				<fieldset>
					<legend>Information</legend>
					<?php echo $tab1_legend1; ?>
				</fieldset>
			</div>
			<div class="form-actions">
				<button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Ok</button>
				<button class="btn btn-warning" type="reset"><i class="icon-refresh icon-white"></i> Reset</button>
				<a href="<?php echo site_url($path); ?>" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	var rule = {
		firstname: {
			required: true
		},
		lastname: {
			required: true
		}
	}
	valid_update_form(rule);
});
</script>