<?php
$name    = form_input('name', set_value('name', $data['item']->name), 'class="input-block-level"');
$account = form_input('account', set_value('account', $data['item']->account), 'class="input-block-level"');
$type    = form_dropdown('type', array('1' => 'Yahoo', '2' => 'Skype'), set_value('type', $data['item']->type), 'class="input-block-level"');

$status = '
	<label class="radio inline">
		<input type="radio" name="status" value="1" '.set_radio('status', '1', ($data['item']->status == 1) ? TRUE : FALSE).'> Enable
	</label>
	<label class="radio inline">
		<input type="radio" name="status" value="0" '.set_radio('status', '0', ($data['item']->status == 0) ? TRUE : FALSE).'> Disable
	</label>
';
$position = form_input('position', set_value('position', $data['item']->position), '');

$parser1 = array(
	'data' => array(
		array(
			'name'  => 'name',
			'label' => 'Name',
			'field' => $name,
			'help'  => ''
		),
		array(
			'name'  => 'account',
			'label' => 'Account',
			'field' => $account,
			'help'  => ''
		),
		array(
			'name'  => 'type',
			'label' => 'Type',
			'field' => $type,
			'help'  => ''
		),
		array(
			'name'  => 'status',
			'label' => 'Status',
			'field' => $status,
			'help'  => ''
		),
		array(
			'name'  => 'position',
			'label' => 'Position',
			'field' => $position,
			'help'  => ''
		)
	)
);

$tab1_legend1 = $this->parser->parse('row_paser', $parser1, TRUE);

// error
$this->form_validation->set_error_delimiters('', '<br>');

$form_error = array(
	form_error('name'),
	form_error('account')
);
$form_error = implode('', $form_error);

if ($form_error)
{
	$this->tn_message->set_error('<h4>Error</h4>'.$form_error);
}

?>

<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	</ul>
	<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">
				<fieldset>
					<legend>Information</legend>
					<?php echo $tab1_legend1; ?>
				</fieldset>
			</div>
			<div class="form-actions">
				<button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Ok</button>
				<button class="btn btn-warning" type="reset"><i class="icon-refresh icon-white"></i> Reset</button>
				<a href="<?php echo site_url($current_controller); ?>" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	var rule = {
		name: {
			required: true
		},
		account: {
			required: true
		}
	}
	valid_update_form(rule);
});
</script>