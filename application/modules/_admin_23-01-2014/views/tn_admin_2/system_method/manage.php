<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<!-- manage-form -->
<form id="manage_form" name="manage_form" method="post" action="">

	<!-- toolbar -->
	<div class="toolbar">
		<a href="<?php echo site_url($current_controller.'/add'); ?>" class="btn btn-success" title="Add"><i class="icon-plus icon-white"></i> Add</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_copy'); ?>');" class="btn btn-success" title="Copy"><i class="icon-th-large icon-white"></i> Copy</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_delete'); ?>', 'Are you sure you wish to delete selected rows?');" class="btn btn-danger" title="Delete"><i class="icon-remove icon-white"></i> Delete</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/save'); ?>');" class="btn btn-primary" title="Save"><i class="icon-ok icon-white"></i> Save</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/active'); ?>');" class="btn btn-success" title="Enable"><i class="icon-ok-circle icon-white"></i> Enable</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/disable'); ?>');" class="btn btn-danger" title="Disable"><i class="icon-ban-circle icon-white"></i> Disable</a>
	</div>
	<!-- /toolbar -->

	<div class="row-fluid">

		<!-- utility -->
		<div class="utility">
			<div class="input-prepend input-append search-tool">
				<a onclick="return clear_search('<?php echo base_url($current_controller.'/search'); ?>');" class="btn" title="Clear"><i class="icon-remove"></i></a>
				<input type="text" class="input-small" name="keyword" placeholder="search" value="<?php echo $params['filter']['keyword']; ?>">
				<a onclick="return submit_form('<?php echo base_url($current_controller.'/search'); ?>');" class="btn" title="Search"><i class="icon-search"></i></a>
			</div>
			<div class="input-prepend input-append entry-tool">
				<a onclick="return clear_show('<?php echo base_url($current_controller.'/show'); ?>');" class="btn" title="Default"><i class="icon-refresh"></i></a>
				<input type="text" class="input-mini" name="list" placeholder="list" value="<?php echo $params['filter']['per_page']; ?>">
				<a onclick="return submit_form('<?php echo base_url($current_controller.'/show'); ?>');" class="btn" title="List"><i class="icon-list"></i></a>
			</div>
			<div class="input-prepend input-append page-tool">
				<a onclick="location.href = '<?php echo site_url($current_controller); ?>';" class="btn" title="First"><i class="icon-backward"></i></a>
				<input type="text" class="input-mini" name="page" placeholder="page" value="<?php echo $params['pagination']->cur_page; ?>">
				<a onclick="return go_to_page('<?php echo base_url($main_method); ?>');" class="btn" title="Go"><i class="icon-play"></i></a>
			</div>
		</div>
		<!-- /utility -->

	</div>

<?php
if (count($data['items'])):

	echo '<input type="hidden" value="" name="sort">';
	$options = array(
		'link'  => site_url($current_controller.'/sort'),
		'image' => ($params['filter']['order'] == 'DESC')
			? ' <i class="icon-chevron-up">'
			: ' <i class="icon-chevron-down">'
	);

	$name     = cms_sort('Name', 'name', $params['filter'], $options);
	$status   = cms_sort('Status', 'status', $params['filter'], $options);
	$position = cms_sort('Position', 'position', $params['filter'], $options);
	$id       = cms_sort('Id', 'id', $params['filter'], $options);
?>

	<!-- manage-table -->
	<table class="table table-bordered manage-table">
		<thead>
			<tr>
				<th width="20" class="text-center"><input type="checkbox" name="check_all"></th>
				<th><?php echo $name; ?></th>
				<th class="text-center"><?php echo $status; ?></th>
				<th class="text-center"><?php echo $position; ?></th>
				<th class="text-center"><?php echo $id; ?></th>
				<th></th>
			</tr>
		</thead>

		<tbody>

<?php
foreach($data['items'] as $row):

	$cid = '<input type="checkbox" value="'.$row->id.'" name="cid[]">';

	$status = ($row->status)
	? '<a onclick="location.href = \''.site_url($current_controller.'/status/'.$row->id.'/0').'\'" class="btn btn-success btn-icon" title="Change status"><i class="icon-ok-circle icon-white"></i></a>'
	: '<a onclick="location.href = \''.site_url($current_controller.'/status/'.$row->id.'/1').'\'" class="btn btn-danger btn-icon" title="Change status"><i class="icon-ban-circle icon-white"></i></a>';

	$position = '<input type="text" value="'.$row->position.'" class="input-mini text-center" name="position['.$row->id.']">';

	$control = '
	<a onclick="location.href = \''.site_url($current_controller.'/edit/'.$row->id).'\'" class="btn btn-primary btn-icon" title="Edit"><i class="icon-pencil icon-white"></i></a>
	<a onclick="if (confirm(\'Are you sure you wish to delete this row?\')) location.href = \''.site_url($current_controller.'/delete/'.$row->id).'\';" class="btn btn-danger btn-icon" title="Delete"><i class="icon-remove icon-white"></i></a>
	';
?>

			<tr>
				<td class="text-center"><?php echo $cid; ?></td>
				<td><?php echo $row->name; ?></td>
				<td class="text-center"><?php echo $status; ?></td>
				<td class="text-center"><?php echo $position; ?></td>
				<td class="text-center"><?php echo $row->id; ?></td>
				<td><?php echo $control; ?></td>
			</tr>

			<?php endforeach; ?>

		</tbody>

	</table>
	<!-- /manage-table -->

	<div class="row-fluid">

		<!-- entries-info -->
		<div class="entries-info">
			<p class="text-info">
				Showing <?php echo $params['pagination']->start_row; ?>
				to <?php echo $params['pagination']->end_row; ?>
				of <?php echo $params['pagination']->total_rows; ?>
				entries
			</p>
		</div>
		<!-- /entries-info -->

		<!-- pagination -->
		<div class="pagination pagination-right">
			<?php echo $params['pagination']->links; ?>
		</div>
		<!-- /pagination -->

	</div>

<?php else: ?>

	<h4 class="text-center text-info">There is no information</h4>

<?php endif; ?>

</form>
<!-- /manage-form -->