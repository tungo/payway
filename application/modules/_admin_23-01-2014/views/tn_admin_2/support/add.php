<?php
$name        = form_input('name', set_value('name'), 'class="text-input medium-input"');
$yahoo       = form_input('yahoo', set_value('yahoo'), 'class="text-input medium-input"');
$position       = form_input('position', set_value('position', 0), 'class="text-input small-input"');


$name_error  = form_error('name');
$yahoo_error = form_error('yahoo');
$position_error = form_error('position');

$parser =array(
	'data' => array(
		array(
			'label' => 'Name: *',
			'field' => $name,
			'error' => $name_error,
			'desc'  => ''
		),
		array(
			'label' => 'Yahoo: *',
			'field' => $yahoo,
			'error' => $yahoo_error,
			'desc'  => ''
		),
		array(
			'label' => 'Position:',
			'field' => $position,
			'error' => $position_error,
			'desc'  => ''
		)
	)
);

$form = form_open_multipart('', array('name' => 'appForm', 'id' => 'appForm')).
		$this->parser->parse('row_add', $parser, TRUE).
		form_close();
?>

<!-- Page Head -->
<h2><?php echo $title; ?></h2>
<p id="page-intro"></p>

<?php echo $this->load->view('content/toolbar'); ?>
<div class="clear"></div>

<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
		<h3>Information</h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->

	<div class="content-box-content">
		<div class="tab-content default-tab">
			<?php echo $form; ?>
			 <!--
			 <fieldset> Set class to "column-left" or "column-right" on fieldsets to divide the form into columns
			</fieldset>
			-->
			<div class="clear"></div><!-- End .clear -->
		</div> <!-- End tab -->
	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
