<?php
$id = (isset($params['id'])) ? $params['id'] : 0;

$btnDelete = cms_tool_button(
	'Delete selected',
	site_url($current_controller.'/multi_delete'),
	array(
		'img' => base_url('public/templates/admin/images/icons/delete-48.png'),
		'type' => 'submit',
		'msg' => 'Are you sure you want to delete selected entries?'
	)
);
$btnAdd = cms_tool_button(
	'Add new',
	site_url($current_controller.'/add'),
	array('img' => base_url('public/templates/admin/images/icons/plus-48.png'))
);
$save = ($method == 'add') ? $current_controller.'/add' : $current_controller.'/edit/'.$id;
$btnSave = cms_tool_button(
	'Save',
	site_url($save),
	array(
		'img' => base_url('public/templates/admin/images/icons/save-48.png'),
		'type' => 'submit'
	)
);
$btnCancel = cms_tool_button(
	'Cancel',
	site_url($current_controller),
	array('img' => base_url('public/templates/admin/images/icons/block-48.png'))
);
$btnEdit = cms_tool_button(
	'Edit',
	site_url($current_controller.'/edit/'.$id),
	array('img' => base_url('public/templates/admin/images/icons/pencil-48.png'))
);
$btnBack = cms_tool_button(
	'Back',
	site_url($current_controller),
	array('img' => base_url('public/templates/admin/images/icons/left-48.png'))
);
$btnAccept = cms_tool_button(
	'Accept',
	site_url($current_controller.'/delete/'.$id),
	array(
		'img' => base_url('public/templates/admin/images/icons/tick-48.png'),
		'type' => 'submit'
	)
);

switch($method)
{
	case 'index':
		$strButton = $btnAdd.$btnDelete;
		break;
	case 'add':
		$strButton = $btnSave.$btnCancel;
		break;
	case 'info':
		$strButton = $btnEdit.$btnBack;
		break;
	case 'edit':
		$strButton = $btnSave.$btnCancel;
		break;
	case 'delete':
		$strButton = $btnAccept.$btnCancel;
		break;
	default:
		$strButton = '';
}
?>
<ul class="shortcut-buttons-set">
	<?php echo $strButton; ?>
</ul>