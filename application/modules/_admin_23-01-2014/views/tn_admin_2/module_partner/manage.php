<p class="lead"><?php echo $title; ?></p>

<div class="clearfix"></div>

<?php echo $this->tn_message->get(); ?>

<form id="manage_form" name="manage_form" method="post" action="">

  <div class="toolbar">
    <a href="<?php echo site_url($current_controller.'/add'); ?>" class="btn btn-success" title="Add"><span class="glyphicon glyphicon-plus"></span> Add</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_copy'); ?>');" class="btn btn-success" title="Copy"><span class="glyphicon glyphicon-th-large"></span> Copy</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_delete'); ?>', 'Are you sure you wish to delete selected rows?');" class="btn btn-danger" title="Delete"><span class="glyphicon glyphicon-remove"></span> Delete</a>
    <a onclick="return submit_form('<?php echo site_url($current_controller.'/save'); ?>');" class="btn btn-primary" title="Save"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/active'); ?>');" class="btn btn-success" title="Enable"><span class="glyphicon glyphicon-ok-sign"></span> Enable</a>
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/disable'); ?>');" class="btn btn-danger" title="Disable"><span class="glyphicon glyphicon-remove-sign"></span> Disable</a>
  </div><!-- /.toolbar -->

  <div class="clearfix"></div>

  <div class="utility">

    <div class="pull-left">

      <div class="input-group search-tool">
        <input type="text" class="form-control input-small" name="keyword" placeholder="<?php echo $params['filter']['keyword'] ? $params['filter']['keyword'] : 'Enter keywords'; ?>" value="">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'/search'); ?>');" class="btn btn-default" title="Search"><span class="glyphicon glyphicon-search"></span></a>
        </span>
      </div>

    </div><!-- /.pull-left -->

    <div class="pull-right">

      <div class="input-group entry-tool">
        <input type="text" class="form-control" name="list" placeholder="list" value="<?php echo $params['filter']['per_page']; ?>">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'/show'); ?>');" class="btn btn-default" title="List"><span class="glyphicon glyphicon-list"></span></a>
        </span>
      </div>

      <div class="input-group page-tool">
        <input type="text" class="form-control" name="page" placeholder="page" value="<?php echo $params['pagination']->cur_page; ?>">
        <span class="input-group-btn">
          <a onclick="return go_to_page('<?php echo base_url($main_method.'/page'); ?>');" class="btn btn-default" title="Go"><span class="glyphicon glyphicon-play"></span></a>
        </span>
      </div>

    </div><!-- /.pull-right -->

  </div><!-- /.utility -->

  <div class="clearfix"></div>

  <?php
    echo '<input type="hidden" value="" name="sort">';
    $options = array(
      'link'  => site_url($current_controller.'/sort'),
      'image' => ($params['filter']['order'] == 'DESC')
        ? ' <span class="glyphicon glyphicon-sort-by-attributes-alt"></span>'
        : ' <span class="glyphicon glyphicon-sort-by-attributes"></span>'
    );

    $name     = cms_sort('Name', 'mp.name', $params['filter'], $options);
    $image    = cms_sort('Image', 'mp.image', $params['filter'], $options);
    $status   = cms_sort('Status', 'mp.status', $params['filter'], $options);
    $position = cms_sort('Position', 'mp.position', $params['filter'], $options);
    $id       = cms_sort('Id', 'mp.id', $params['filter'], $options);
  ?>

  <table class="table table-bordered manage-table">
    <thead>
      <tr>
        <th width="20" class="text-center"><input type="checkbox" name="check_all"></th>
        <th><?php echo $name; ?></th>
        <th class="text-center"><?php echo $image; ?></th>
        <th class="text-center"><?php echo $status; ?></th>
        <th class="text-center"><?php echo $position; ?></th>
        <th class="text-center"><?php echo $id; ?></th>
        <th></th>
      </tr>
    </thead>

    <tbody>

<?php
foreach($data['items'] as $row):

  $cid = '<input type="checkbox" value="'.$row['id'].'" name="cid[]">
    <input type="hidden" value="'.$row['id'].'" name="hid[]">';

  $image = ($row['image'] != '') ? '<img src="'.$image_url.$row['image'].'" class="img-thumbnail image">' : '<img src="'.base_url('/public/images/no-image.png').'" class="img-thumbnail image">';

  $status = ($row['status'])
    ? '<a onclick="location.href = \''.site_url($current_controller.'/status/id/'.$row['id'].'/status/0').'\'" class="btn btn-success btn-xs" title="Change status"><span class="glyphicon glyphicon-ok-sign"></span></a>'
    : '<a onclick="location.href = \''.site_url($current_controller.'/status/id/'.$row['id'].'/status/1').'\'" class="btn btn-danger btn-xs" title="Change status"><span class="glyphicon glyphicon-remove-sign"></span></a>';

  $position = '<input type="text" value="'.$row['position'].'" class="form-control input-sm input-text text-center" name="position['.$row['id'].']">';

  $control = '
    <a onclick="location.href = \''.site_url($current_controller.'/edit/id/'.$row['id']).'\'" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
    <a onclick="if (confirm(\'Are you sure you wish to delete this row?\')) location.href = \''.site_url($current_controller.'/delete/id/'.$row['id']).'\';" class="btn btn-danger btn-xs" title="Delete"><span class="glyphicon glyphicon-remove"></span></a>
  ';
?>

      <tr>
        <td class="text-center"><?php echo $cid; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td class="text-center"><?php echo $image; ?></td>
        <td class="text-center"><?php echo $status; ?></td>
        <td class="text-center"><?php echo $position; ?></td>
        <td class="text-center"><?php echo $row['id']; ?></td>
        <td><?php echo $control; ?></td>
      </tr>

<?php endforeach; ?>

    </tbody>

  </table><!-- /.manage-table -->

  <div class="clearfix"></div>

  <?php if (count($data['items'])): ?>

    <div class="text-info entries-info pull-left">
      Showing <?php echo $params['pagination']->start_row; ?>
      to <?php echo $params['pagination']->end_row; ?>
      of <?php echo $params['pagination']->total_rows; ?>
      entries
    </div>

    <div class="pull-right">
      <?php echo $params['pagination']->links; ?>
    </div>

  <?php else: ?>
    <div class="text-info entries-info">
      There is no information
    </div>
  <?php endif; ?>

</form><!-- /#manage_form -->