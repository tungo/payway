<?php
$name = form_input('name', set_value('name'), 'class="input-block-level"');

$category_id = '<select name="category_id" class="input-block-level">';
foreach($select_category as $val)
{
	$prefix = '';
	for($i = 1; $i < $val->depth; $i++)
	{
		$prefix .= '¦&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	$category_id .= '<option value="'.$val->id.'">'.$prefix.$val->name.'</option>';
}
$category_id .= '</select>';

$image = form_upload('image')
	.'<span class="help-block">
		<img src="'.base_url('/public/images/no-image.png').'" class="img-polaroid old_image">
	</span>';

$price = form_input('price', set_value('price', 0), '');
$hot = '
	<label class="radio inline">
		<input type="radio" name="hot" value="1"> Yes
	</label>
	<label class="radio inline">
		<input type="radio" name="hot" value="0" checked="checked"> No
	</label>
';

$description = cms_editor('description', set_value('description'));

$status = '
	<label class="radio inline">
		<input type="radio" name="status" value="1" checked="checked"> Enable
	</label>
	<label class="radio inline">
		<input type="radio" name="status" value="0"> Disable
	</label>
';
$position = form_input('position', set_value('position', 0), '');

$parser1 = array(
	'data' => array(
		array(
			'name'  => 'name',
			'label' => 'Name',
			'field' => $name,
			'help'  => ''
		),
		array(
			'name'  => 'category_id',
			'label' => 'Category',
			'field' => $category_id,
			'help'  => ''
		),
		array(
			'name'  => 'image',
			'label' => 'Image',
			'field' => $image,
			'help'  => ''
		),
		array(
			'name'  => 'price',
			'label' => 'Price',
			'field' => $price,
			'help'  => ''
		),
		array(
			'name'  => 'hot',
			'label' => 'Hot',
			'field' => $hot,
			'help'  => ''
		),
		array(
			'name'  => 'description',
			'label' => 'Description',
			'field' => $description,
			'help'  => ''
		),
		array(
			'name'  => 'status',
			'label' => 'Status',
			'field' => $status,
			'help'  => ''
		),
		array(
			'name'  => 'position',
			'label' => 'Position',
			'field' => $position,
			'help'  => ''
		)
	)
);

$tab1_legend1 = $this->parser->parse('row_paser', $parser1, TRUE);

$parser2 = array(
	'data' => array(
		array(
			'name'  => '',
			'label' => '',
			'field' => '<button class="btn btn-success btn-icon" name="add-image" type="button"><i class="icon-plus icon-white"></i></button>',
			'help'  => ''
		)
	)
);

$tab2_legend1 = $this->parser->parse('row_paser', $parser2, TRUE);

// error
$this->form_validation->set_error_delimiters('', '<br>');

$image_image_error = '';
if (! empty($product_image_image_error))
{
	foreach ($product_image_image_error as $key => $error)
	{
		$image_image_error .= '<li>Image '.($key+1).': '.$error.'</li>';
	}
}

$form_error = array(
	form_error('name'),
	(empty($image_error)) ? '' : 'The Image field: '.$image_error,
	(empty($image_image_error)) ? '' : 'Product image error:<ul>'.$image_image_error.'</ul>'
);
$form_error = implode('', $form_error);

if ($form_error)
{
	$this->tn_message->set_error('<h4>Error</h4>'.$form_error);
}

?>

<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		<li><a href="#tab-image" data-toggle="tab">Image</a></li>
		<li><a href="#tab-seo" data-toggle="tab">Seo</a></li>
	</ul>
	<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">
				<fieldset>
					<legend>Information</legend>
					<?php echo $tab1_legend1; ?>
				</fieldset>
			</div>
			<div class="tab-pane" id="tab-image">
				<fieldset>
					<legend>Image</legend>
					<?php echo $tab2_legend1; ?>
				</fieldset>
			</div>
			<div class="tab-pane" id="tab-seo">
				<p>SEO</p>
			</div>
			<div class="form-actions">
				<button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Ok</button>
				<button class="btn btn-warning" type="reset"><i class="icon-refresh icon-white"></i> Reset</button>
				<a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	var rule = {
		name: {
			required: true
		}
	}
	valid_update_form(rule);

	$('#tab-image').on('click', ':button[name="add-image"]:not(.disabled)', function(){
		$(this).parents('.control-group').after('<div class="control-group"><label class="control-label"></label><div class="controls"><input type="file" name="product_image_image[]" value=""> <span class="help-inline"><button class="btn btn-success btn-icon" name="add-image" type="button"><i class="icon-plus icon-white"></i></button> <button class="btn btn-danger btn-icon" name="delete-image" type="button"><i class="icon-minus icon-white"></i></button></span></div></div>');
	});

	$('#tab-image').on('click', ':button[name="delete-image"]:not(.disabled)', function(){
		$(this).parents('.control-group').remove();
	});
});
</script>