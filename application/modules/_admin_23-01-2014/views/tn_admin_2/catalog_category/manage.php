<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<!-- manage-form -->
<form id="manage_form" name="manage_form" method="post" action="">

	<!-- toolbar -->
	<div class="toolbar">
		<a href="<?php echo site_url($current_controller.'/add'); ?>" class="btn btn-success" title="Add"><i class="icon-plus icon-white"></i> Add</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_copy'); ?>');" class="btn btn-success" title="Copy"><i class="icon-th-large icon-white"></i> Copy</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_delete'); ?>', 'Are you sure you wish to delete selected rows?');" class="btn btn-danger" title="Delete"><i class="icon-remove icon-white"></i> Delete</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/save'); ?>');" class="btn btn-primary" title="Save"><i class="icon-ok icon-white"></i> Save</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/active'); ?>');" class="btn btn-success" title="Enable"><i class="icon-ok-circle icon-white"></i> Enable</a>
		<a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/disable'); ?>');" class="btn btn-danger" title="Disable"><i class="icon-ban-circle icon-white"></i> Disable</a>
	</div>
	<!-- /toolbar -->

<?php
if (count($data['items'])):
?>

	<!-- manage-table -->
	<table class="table table-bordered manage-table">
		<thead>
			<tr>
				<th width="20" class="text-center"><input type="checkbox" name="check_all"></th>
				<th>Name</th>
				<th class="text-center">Status</th>
				<th class="text-center">Position</th>
				<th class="text-center">Id</th>
				<th></th>
			</tr>
		</thead>

		<tbody>

<?php
foreach($data['items'] as $row):

	$cid = '<input type="checkbox" value="'.$row->id.'" name="cid[]">';

	$name = $row->name;
	for($i = 1; $i < $row->depth; $i++)
	{
		$name = '-<span class="depth">'.$name.'</span>';
	}

	$status = ($row->status)
	? '<a onclick="location.href = \''.site_url($current_controller.'/status/'.$row->id.'/0').'\'" class="btn btn-success btn-icon" title="Change status"><i class="icon-ok-circle icon-white"></i></a>'
	: '<a onclick="location.href = \''.site_url($current_controller.'/status/'.$row->id.'/1').'\'" class="btn btn-danger btn-icon" title="Change status"><i class="icon-ban-circle icon-white"></i></a>';

	$position = '<input type="text" value="'.$row->position.'" class="input-mini text-center" name="position['.$row->id.']">';

	$control = '
	<a onclick="location.href = \''.site_url($current_controller.'/edit/'.$row->id).'\'" class="btn btn-primary btn-icon" title="Edit"><i class="icon-pencil icon-white"></i></a>
	<a onclick="if (confirm(\'Are you sure you wish to delete this row?\')) location.href = \''.site_url($current_controller.'/delete/'.$row->id).'\';" class="btn btn-danger btn-icon" title="Delete"><i class="icon-remove icon-white"></i></a>
	';
?>

			<tr>
				<td class="text-center"><?php echo $cid; ?></td>
				<td><?php echo $name; ?></td>
				<td class="text-center"><?php echo $status; ?></td>
				<td class="text-center"><?php echo $position; ?></td>
				<td class="text-center"><?php echo $row->id; ?></td>
				<td><?php echo $control; ?></td>
			</tr>

			<?php endforeach; ?>

		</tbody>

	</table>
	<!-- /manage-table -->

	<div class="row-fluid">

		<!-- entries-info -->
		<div class="entries-info">
			<p class="text-info">
				Showing <?php echo $data['total_rows']; ?>
				entries
			</p>
		</div>
		<!-- /entries-info -->

	</div>

<?php else: ?>

	<h4 class="text-center text-info">There is no information</h4>

<?php endif; ?>

</form>
<!-- /manage-form -->