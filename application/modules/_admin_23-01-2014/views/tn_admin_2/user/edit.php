<?php
$username          = form_input('username', set_value('username', $data['item']->username), 'class="text-input medium-input"');
$password          = form_password('password', '', 'class="text-input medium-input"');
$firstname          = form_input('firstname', set_value('firstname', $data['item']->firstname), 'class="text-input medium-input"');
$lastname          = form_input('lastname', set_value('lastname', $data['item']->lastname), 'class="text-input medium-input"');

$username_error    = form_error('username');
$password_error      = form_error('password');
$firstname_error    = form_error('firstname');
$lastname_error    = form_error('lastname');

$parser =array(
	'data' => array(
		array(
			'label' => 'Username: *',
			'field' => $username,
			'error' => $username_error,
			'desc'  => ''
		),
		array(
			'label' => 'Password:',
			'field' => $password,
			'error' => $password_error,
			'desc'  => ''
		),
		array(
			'label' => 'Firstname:',
			'field' => $firstname,
			'error' => $firstname_error,
			'desc'  => ''
		),
		array(
			'label' => 'Lastname:',
			'field' => $lastname,
			'error' => $lastname_error,
			'desc'  => ''
		)
	)
);

$form = form_open_multipart('', array('name' => 'appForm', 'id' => 'appForm')).
		$this->parser->parse('row_add', $parser, TRUE).
		form_close();
?>

<!-- Page Head -->
<h2><?php echo $title; ?></h2>
<p id="page-intro"></p>

<?php echo $this->load->view('content/toolbar'); ?>
<div class="clear"></div>

<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
		<h3>Information</h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->

	<div class="content-box-content">
		<div class="tab-content default-tab">
			<?php echo $form; ?>
			 <!--
			 <fieldset> Set class to "column-left" or "column-right" on fieldsets to divide the form into columns
			</fieldset>
			-->
			<div class="clear"></div><!-- End .clear -->
		</div> <!-- End tab -->
	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
