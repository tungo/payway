<!-- Page Head -->
<h2><?php echo $title; ?></h2>
<!-- <p id="page-intro"></p> -->

<?php echo cms_messages($this->session->flashdata('messages')); ?>

<?php echo $this->load->view('content/toolbar'); ?>
<div class="clear"></div>

<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
		<h3>List</h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->

	<div class="content-box-content">
		<div class="tab-content default-tab">
			<form id="appForm" name="appForm" method="post" action="" enctype="multipart/form-data">

<?php if (count($data['items'])): ?>

	<div class="pagination">
		<?php echo $params['pagination']->links; ?>
	</div>

	<div class="align-left">
		Show
		<select onchange="document.location.href = '<?php echo base_url($current_controller.'/per_page'); ?>/'+this.value;">
			<?php
			foreach ($params['pagination']->show_entries as $value)
			{
				$select = ($params['pagination']->per_page == $value) ? ' selected="selected"' : '';
				echo '<option value="'.$value.'"'.$select.'>'.$value.'</option>';
			}
			?>
		</select>
		entries
	</div>

<table class="list">

	<thead>
		<tr>
			<th width="4%"><input class="check-all" type="checkbox"></th>
			<th>Username</th>
			<th width="30%">Firstname</th>
			<th width="30%">Lastname</th>
			<th width="8%" class="text-center">Control</th>
			<th width="5%" class="text-center">ID</th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="5">
				Showing <?php echo $params['pagination']->start_row; ?>
				to <?php echo $params['pagination']->end_row; ?>
				of <?php echo $params['pagination']->total_rows; ?>
				entries
			</td>
		</tr>
	</tfoot>
	<tbody>
<?php
	foreach($data['items'] as $row):
	$cid = form_checkbox('cid[]', $row->id);

	$btnInfo = cms_icon_button(
		'Info',
		base_url('public/templates/admin/images/icons/info-16.png'),
		array('link' => site_url($current_controller.'/info/'.$row->id))
	);
	$btnEdit = cms_icon_button(
		'Edit',
		base_url('public/templates/admin/images/icons/pencil-16.png'),
		array('link' => site_url($current_controller.'/edit/'.$row->id))
	);
	$btnDelete = cms_icon_button(
		'Delete',
		base_url('public/templates/admin/images/icons/delete-16.png'),
		array(
			'link' => site_url($current_controller.'/delete/'.$row->id),
			'msg' => 'Are you sure you delete this entry?'
		)
	);
	$btnControl = $btnInfo.' '.$btnEdit.' '.$btnDelete;
?>
		<tr>
			<td><?php echo $cid; ?></td>
			<td><?php echo $row->username; ?></td>
			<td><?php echo $row->firstname; ?></td>
			<td><?php echo $row->lastname; ?></td>
			<td class="text-center"><?php echo $btnControl; ?></td>
			<td class="text-center"><?php echo $row->id; ?></td>
		</tr>
<?php endforeach; ?>

	</tbody>
</table>

	<div class="pagination">
		<?php echo $params['pagination']->links; ?>
	</div>

<?php else: ?>

	<div class="notification information png_bg">
		<div>Empty</div>
	</div>

<?php endif; ?>

			</form>
		</div> <!-- End tab -->

	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->