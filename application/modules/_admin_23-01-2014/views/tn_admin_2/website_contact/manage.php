<p class="lead"><?php echo $title; ?></p>

<div class="clearfix"></div>

<?php echo $this->tn_message->get(); ?>

<form id="manage_form" name="manage_form" method="post" action="">

  <div class="toolbar">
    <a onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_delete'); ?>', 'Are you sure you wish to delete selected rows?');" class="btn btn-danger" title="Delete"><span class="glyphicon glyphicon-remove"></span> Delete</a>
  </div><!-- /.toolbar -->

  <div class="clearfix"></div>

  <div class="utility">

    <div class="pull-left">

      <div class="input-group search-tool">
        <input type="text" class="form-control input-small" name="keyword" placeholder="<?php echo $params['filter']['keyword'] ? $params['filter']['keyword'] : 'Enter keywords'; ?>" value="">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'/search'); ?>');" class="btn btn-default" title="Search"><span class="glyphicon glyphicon-search"></span></a>
        </span>
      </div>

    </div><!-- /.pull-left -->

    <div class="pull-right">

      <div class="input-group entry-tool">
        <input type="text" class="form-control" name="list" placeholder="list" value="<?php echo $params['filter']['per_page']; ?>">
        <span class="input-group-btn">
          <a onclick="return submit_form('<?php echo base_url($current_controller.'/show'); ?>');" class="btn btn-default" title="List"><span class="glyphicon glyphicon-list"></span></a>
        </span>
      </div>

      <div class="input-group page-tool">
        <input type="text" class="form-control" name="page" placeholder="page" value="<?php echo $params['pagination']->cur_page; ?>">
        <span class="input-group-btn">
          <a onclick="return go_to_page('<?php echo base_url($main_method.'/page'); ?>');" class="btn btn-default" title="Go"><span class="glyphicon glyphicon-play"></span></a>
        </span>
      </div>

    </div><!-- /.pull-right -->

  </div><!-- /.utility -->

  <div class="clearfix"></div>

  <?php
    echo '<input type="hidden" value="" name="sort">';
    $options = array(
      'link'  => site_url($current_controller.'/sort'),
      'image' => ($params['filter']['order'] == 'DESC')
        ? ' <span class="glyphicon glyphicon-sort-by-attributes-alt"></span>'
        : ' <span class="glyphicon glyphicon-sort-by-attributes"></span>'
    );

    $email   = cms_sort('Email', 'wc.email', $params['filter'], $options);
    $name    = cms_sort('Name', 'wc.name', $params['filter'], $options);
    $phone   = cms_sort('Phone', 'wc.phone', $params['filter'], $options);
    $created_time = cms_sort('Time', 'wc.created_time', $params['filter'], $options);
    $message = cms_sort('Message', 'wc.message', $params['filter'], $options);
    $id      = cms_sort('Id', 'ba.id', $params['filter'], $options);
  ?>

  <table class="table table-bordered manage-table">
    <thead>
      <tr>
        <th width="20" class="text-center"><input type="checkbox" name="check_all"></th>
        <th><?php echo $email; ?></th>
        <th><?php echo $name; ?></th>
        <th><?php echo $phone; ?></th>
        <th><?php echo $created_time; ?></th>
        <th><?php echo $message; ?></th>
        <th class="text-center"><?php echo $id; ?></th>
        <th></th>
      </tr>
    </thead>

    <tbody>

<?php
foreach($data['items'] as $row):

  $cid = '<input type="checkbox" value="'.$row['id'].'" name="cid[]">
    <input type="hidden" value="'.$row['id'].'" name="hid[]">';

  $control = '
    <a onclick="if (confirm(\'Are you sure you wish to delete this row?\')) location.href = \''.site_url($current_controller.'/delete/id/'.$row['id']).'\';" class="btn btn-danger btn-xs" title="Delete"><span class="glyphicon glyphicon-remove"></span></a>
  ';
?>

      <tr>
        <td class="text-center"><?php echo $cid; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['phone']; ?></td>
        <td><?php echo $row['created_time']; ?></td>
        <td><?php echo $row['message']; ?></td>
        <td class="text-center"><?php echo $row['id']; ?></td>
        <td><?php echo $control; ?></td>
      </tr>

<?php endforeach; ?>

    </tbody>

  </table><!-- /.manage-table -->

  <div class="clearfix"></div>

  <?php if (count($data['items'])): ?>

    <div class="text-info entries-info pull-left">
      Showing <?php echo $params['pagination']->start_row; ?>
      to <?php echo $params['pagination']->end_row; ?>
      of <?php echo $params['pagination']->total_rows; ?>
      entries
    </div>

    <div class="pull-right">
      <?php echo $params['pagination']->links; ?>
    </div>

  <?php else: ?>
    <div class="text-info entries-info">
      There is no information
    </div>
  <?php endif; ?>

</form><!-- /#manage_form -->