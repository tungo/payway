<?php
$name        = form_input('name', set_value('name', $data['item']->name), 'class="text-input medium-input"');
$content = (isset($params['content'])) ? cms_replace_string($params['content']) : $data['item']->content;
	$content = cms_editor('content', $content);

$image       =  form_upload('image');
$position       = form_input('position', set_value('position', $data['item']->position), 'class="text-input small-input"');

$old_image = $data['item']->image;
if ($old_image != '')
{
	$old_image = '<img src="'.base_url('public/files/service/'.$old_image).'" style="height:200px;">';
}
$old_image .= form_hidden('old_image', $data['item']->image);

$name_error  = form_error('name');
$content_error = form_error('content');
$image_error = (empty($image_error)) ? '' : $image_error;
$position_error = form_error('position');

$parser =array(
	'data' => array(
		array(
			'label' => 'Name: *',
			'field' => $name,
			'error' => $name_error,
			'desc'  => ''
		),
		array(
			'label' => 'Image:',
			'field' => $image,
			'error' => $image_error,
			'desc'  => 'square'
		),
		array(
			'label' => '',
			'field' => $old_image,
			'error' => '',
			'desc'  => ''
		),
		array(
			'label' => 'Content:',
			'field' => $content,
			'error' => $content_error,
			'desc'  => ''
		),
		array(
			'label' => 'Position:',
			'field' => $position,
			'error' => $position_error,
			'desc'  => ''
		)
	)
);

$form = form_open_multipart('', array('name' => 'appForm', 'id' => 'appForm')).
		$this->parser->parse('row_add', $parser, TRUE).
		form_close();
?>

<!-- Page Head -->
<h2><?php echo $title; ?></h2>
<p id="page-intro"></p>

<?php echo $this->load->view('service/toolbar'); ?>
<div class="clear"></div>

<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
		<h3>Information</h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->

	<div class="content-box-content">
		<div class="tab-content default-tab">
			<?php echo $form; ?>
			 <!--
			 <fieldset> Set class to "column-left" or "column-right" on fieldsets to divide the form into columns
			</fieldset>
			-->
			<div class="clear"></div><!-- End .clear -->
		</div> <!-- End tab -->
	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
