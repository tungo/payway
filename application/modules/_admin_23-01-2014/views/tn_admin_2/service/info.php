<?php
$name  = $data['item']->name;
$content = $data['item']->content;
$image = $data['item']->image;
if ($image != '')
	$image = '<img src="'.base_url('public/files/service/'.$image).'">';
$position = $data['item']->position;

$parser =array(
	'data' => array(
		array(
			'label' => 'Name:',
			'field' => $name,
			'desc'  => ''
		),
		array(
			'label' => 'Content:',
			'field' => $content,
			'desc'  => ''
		),
		array(
			'label' => 'Image:',
			'field' => $image,
			'desc'  => ''
		),
		array(
			'label' => 'Position:',
			'field' => $position,
			'desc'  => ''
		)
	)
);

$form = form_open_multipart('', array('name' => 'appForm', 'id' => 'appForm')).
		$this->parser->parse('row_info', $parser, TRUE).
		form_close();
?>

<!-- Page Head -->
<h2><?php echo $title; ?></h2>
<p id="page-intro"></p>

<?php echo $this->load->view('content/toolbar'); ?>
<div class="clear"></div>

<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
		<h3>Information</h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->

	<div class="content-box-content">
		<div class="tab-content default-tab">
			<?php echo $form; ?>
			 <!--
			 <fieldset> Set class to "column-left" or "column-right" on fieldsets to divide the form into columns
			</fieldset>
			-->
			<div class="clear"></div><!-- End .clear -->
		</div> <!-- End tab -->
	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
