<?php
$name    = form_input('name', set_value('name', $data['item']->name), 'class="required"');
$note    = form_input('note', set_value('note', $data['item']->note), '');
$picture = form_upload('picture');
$content = form_textarea('content', set_value('content', $data['item']->content), '');
//$content = (isset($params['content'])) ? cms_replace_string($params['content']) : $data['item']->content;
//$content = cms_editor('content', $content);

$current_picture = $data['item']->picture;
if ($current_picture != '')
{
	$current_picture = '<br><img src="'.base_url('public/files/web_content/original/'.$current_picture).'" style="height:200px;">';
}
$current_picture .= form_hidden('current_picture', $data['item']->picture);

$name_error    = form_error('name');
$note_error    = form_error('note');
$picture_error = (empty($picture_error)) ? '' : $picture_error;
$content_error = form_error('content');

$parser =array(
	'data' => array(
		array(
			'label' => 'Name: *',
			'field' => $name,
			'error' => $name_error,
			'desc'  => ''
		),
		array(
			'label' => 'Note:',
			'field' => $note,
			'error' => $note_error,
			'desc'  => ''
		),
		array(
			'label' => 'Picture:',
			'field' => $picture,
			'error' => $picture_error,
			'desc'  => $current_picture
		),
		array(
			'label' => 'Content:',
			'field' => $content,
			'error' => $content_error,
			'desc'  => ''
		)
	)
);

$form = $this->parser->parse('row_paser', $parser, TRUE);
?>

<div class="header-box grid_12">
	<span>
		<h2><?php echo $title; ?></h2>
	</span>
</div>

<form id="update_form" name="update_form" action="" method="post" enctype="multipart/form-data">

	<div class="box grid_12">
		<div class="box-head"><h2>Information</h2></div>
		<div class="box-content">
			<?php echo $form; ?>
			<div class="form-row">
				<input type="submit" class="button green" value="Ok">
				<input type="reset" class="button yellow" value="Reset">
				<input type="button" class="button yellow" value="Clear" onclick="$('#update_form').find(':input').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');">
				<input type="button" class="button red" value="Cancel" onclick="location.href='<?php echo site_url($current_controller); ?>'">
			</div>
			<div class="clear"></div>
		</div>
	</div>

</form>

<script type="text/javascript" src="<?php echo base_url('public/templates/admin/js/forms/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#update_form').validate();
});
</script>