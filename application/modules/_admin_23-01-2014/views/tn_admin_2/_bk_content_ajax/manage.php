<div class="header-box grid_12">
	<span>
		<h2><?php echo $title; ?></h2>
	</span>
</div>

<?php echo cms_messages($this->session->flashdata('messages')); ?>

<div class="box grid_12">
	<div class="box-head"><h2>List</h2></div>
	<div class="box-content no-pad">
		<form id="manage_form" name="manage_form" method="post" action="">

<!-- Toolbar -->
<ul class="table-toolbar">
	<li>
		<a href="<?php echo site_url($current_controller.'/add'); ?>" title="">
			<img src="<?php echo base_url('public/templates/admin/img/icons/basic/plus-16.png'); ?>" alt=""> Add
		</a>
	</li>
	<li>
		<a href="#" onclick="return submit_manage_form('<?php echo site_url($current_controller.'/multi_delete'); ?>', 'Are you sure you wish to delete selected rows?')" title="">
			<img src="<?php echo base_url('public/templates/admin/img/icons/basic/delete-16.png'); ?>" alt=""> Delete
		</a>
	</li>
	<li>
		<a href="#" onclick="return submit_manage_form('<?php echo site_url($current_controller.'/save'); ?>')" title="">
			<img src="<?php echo base_url('public/templates/admin/img/icons/basic/save-16.png'); ?>" alt=""> Save
		</a>
	</li>
	<li>
		<a href="#" onclick="return submit_manage_form('<?php echo site_url($current_controller.'/active'); ?>')" title="">
			<img src="<?php echo base_url('public/templates/admin/img/icons/basic/tick-16.png'); ?>" alt=""> Active
		</a>
	</li>
	<li>
		<a href="#" onclick="return submit_manage_form('<?php echo site_url($current_controller.'/disable'); ?>')" title="">
			<img src="<?php echo base_url('public/templates/admin/img/icons/basic/block-16.png'); ?>" alt=""> Disable
		</a>
	</li>
</ul><!-- End Toolbar -->

<!-- DataTable -->
<table class="display" id="manage_table">
	<thead>
		<tr>
			<th class="no_sort" width="3%"><input type="checkbox" name="check_all"></th>
			<th>Name</th>
			<th>Note</th>
			<th width="7%">Status</th>
			<th width="9%">Position</th>
			<th width="7%">Id</th>
			<th class="no_sort" width="7%"></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</tfoot>
</table><!-- End DataTable -->

		</form>
	</div>
</div>

<script src="<?php echo base_url('public/templates/admin/js/tn-dataTables.plugin.js'); ?>"></script>
<script src="<?php echo base_url('public/templates/admin/js/jquery.dataTables.columnFilter.js'); ?>"></script>
<script type="text/javascript">
var oManageTable;

function initTable()
{
	oManageTable = $('#manage_table')
		.bind('sort',   function () { reset_manage_table(); })
		.bind('filter', function () { reset_manage_table(); })
		.bind('page',   function () { reset_manage_table(); })
		.dataTable({
		"bProcessing": true,
		"sAjaxSource": "<?php echo site_url($current_controller.'/entries'); ?>",
		"bDeferRender": true,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"bStateSave": true,
		"aoColumns": [
			{
				"sClass": "center", "bSortable": false, "bSearchable": false,
				"mRender": function(data, type, full) {
					if (type === "display")
						return '<input type="checkbox" name="cid[]"" value="'+data+'">';
					return data;
				}
			},
			null,
			null,
			{
				"sClass": "center", "bSearchable": false,
				"mRender": function(data, type, full) {
					if (type === "display"){
						return '<a href="#" onclick="return process_entry(\''+data[2]+'\');" title="Change status"><img src="'+data[3]+'" alt="'+data[1]+'"></a>';
					}
					return data[1];
				}
			},
			{
				"sClass": "center", "sType": "numeric",
				"mRender": function(data, type, full) {
					if (type === "display"){
						return '<input type="text" value="'+data+'" data="'+data+'" class="manage_input text-right remember center" name="position[]">';
					}
					return data;
				}
			},
			{"sClass": "center", "sType": "numeric"},
			{
				"sClass": "center", "bSortable": false, "bSearchable": false,
				"mRender": function(data, type, full) {
					if (type === "display"){
						return '<a href="'+data[1]+'" title="Edit"><img src="'+data[2]+'" alt="Edit"></a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="return delete_entry(\''+data[3]+'\', \'Are you sure you wish to delete this row?\');" title="Delete"><img src="'+data[4]+'" alt="Delete"></a>';
					}
					return data[0];
				}
			}
		]
	})
	.columnFilter({
		aoColumns: [
			null,
			{"type": "text"},
			{"type": "text"},
			{"type": "select", values: ['Active', 'Disable']},
			{"type": "text"},
			{"type": "text"},
			null
		]
	});
}
</script>
<script src="<?php echo base_url('public/templates/admin/js/tn-manage.js'); ?>"></script>