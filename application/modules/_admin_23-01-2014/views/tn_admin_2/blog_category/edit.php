<?php
foreach($languages as $language)
{
  $name = form_input('description['.$language['id'].'][name]', set_value('description['.$language['id'].'][name]', $data['item']['description'][$language['id']]['name']), 'class="form-control"');
  $link = form_input('description['.$language['id'].'][link]', set_value('description['.$language['id'].'][link]', $data['item']['description'][$language['id']]['link']), 'class="form-control"');

	$description = (isset($params['description['.$language['id'].'][description]'])) ? cms_replace_string($params['description['.$language['id'].'][description]']) : $data['item']['description'][$language['id']]['description'];
	$description = cms_editor('description['.$language['id'].'][description]', $description);

  $parser = array(
    'data' => array(
      array(
        'name'  => 'description['.$language['id'].'][name]',
        'label' => 'Name',
        'field' => $name,
        'help'  => ''
      ),
      array(
        'name'  => 'description['.$language['id'].'][link]',
        'label' => 'Link',
        'field' => $link,
        'help'  => ''
      ),
      array(
        'name'  => 'description['.$language['id'].'][description]',
        'label' => 'Description',
        'field' => $description,
        'help'  => ''
      )
    )
  );

  $tab1_{$language['id']} = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);
}

$parent_id = '<select name="parent_id" class="form-control">';
foreach($select_category as $val)
{
	$selected = ($val['id'] == $data['item']['parent_id']) ? 'selected="selected"' : '';
	$prefix = '';
	for($i = 1; $i < $val['depth']; $i++)
	{
		$prefix .= '¦&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	$parent_id .= '<option value="'.$val['id'].'" '.$selected.'>'.$prefix.$val['name'].'</option>';
}
$parent_id .= '</select>';

$seo_name = form_input('seo_name', set_value('seo_name', $data['item']['seo_name']), 'class="form-control"');

$status = '
	<label class="radio-inline">
		<input type="radio" name="status" value="1" '.set_radio('status', '1', ($data['item']['status'] == 1) ? TRUE : FALSE).'> Enable
	</label>
	<label class="radio-inline">
		<input type="radio" name="status" value="0" '.set_radio('status', '0', ($data['item']['status'] == 0) ? TRUE : FALSE).'> Disable
	</label>
';
$position = form_input('position', set_value('position', $data['item']['position']), 'class="form-control"');

$parser = array(
  'data' => array(
    array(
      'name'  => 'parent_id',
      'label' => 'Parent Category',
      'field' => $parent_id,
      'help'  => ''
    ),
    array(
      'name'  => 'seo_name',
      'label' => 'SEO Name',
      'field' => $seo_name,
      'help'  => ''
    ),
    array(
      'name'  => 'status',
      'label' => 'Status',
      'field' => $status,
      'help'  => ''
    ),
    array(
      'name'  => 'position',
      'label' => 'Position',
      'field' => $position,
      'help'  => ''
    )
  )
);

$tab2 = $this->parser->parse('tn_admin_2/row_parser', $parser, TRUE);

// error
$this->form_validation->set_error_delimiters('', '<br>');

$form_error = array(
	form_error('parent_id'),
);
$form_error = implode('', $form_error);

if ($form_error)
{
	$this->tn_message->set_error('<h4>Error</h4>'.$form_error);
}

?>

<p class="lead"><?php echo $title; ?></p>

<?php echo $this->tn_message->get(); ?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
  <li><a href="#tab-data" data-toggle="tab">Data</a></li>
</ul>

<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
  <div class="tab-content">

    <div class="tab-pane active" id="tab-general">
      <ul class="nav nav-pills language">
      <?php foreach ($languages as $language)
      {
        $active = ($language_id == $language['id']) ? 'class="active"' : '';
        echo '<li '.$active.'>
          <a href="#tab-'.$language['code'].'" data-toggle="tab">
            <img class="flag" src="/public/images/flags/'.$language['image'].'"> '
            .$language['name'].
          '</a>
        </li>';
      }
      ?>
      </ul>

      <div class="clearfix"></div>

      <div class="tab-content">
      <?php foreach ($languages as $language)
      {
        $active = ($language_id == $language['id']) ? 'active' : '';
        echo '<div class="tab-pane '.$active.'" id="tab-'.$language['code'].'">
          <fieldset>'
            .$tab1_{$language['id']}.
          '</fieldset>
        </div>';
      }
      ?>
      </div>
    </div>

    <div class="tab-pane" id="tab-data">
      <fieldset>
        <?php echo $tab2; ?>
      </fieldset>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-1 col-lg-11">
				<button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
				<a onclick="location.href = '<?php echo site_url($current_controller); ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
      </div>
    </div>

  </div>
</form>

<script>
// $(document).ready(function(){
// 	var rule = {
// 		name: {
// 			required: true
// 		}
// 	}
// 	valid_update_form(rule);
// });
</script>