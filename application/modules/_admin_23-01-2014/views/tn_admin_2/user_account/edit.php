<?php
$name    = form_input('name', set_value('name', $data['item']->name), 'class="input-block-level"');

$category_id = '<select name="category_id" class="input-block-level">';
foreach($select_category as $val)
{
	$selected = ($val->id == $data['item']->category_id) ? 'selected="selected"' : '';
	$prefix = '';
	for($i = 1; $i < $val->depth; $i++)
	{
		$prefix .= '¦&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	$category_id .= '<option value="'.$val->id.'" '.$selected.'>'.$prefix.$val->name.'</option>';
}
$category_id .= '</select>';

if ($data['item']->image != '')
{
	$image = form_upload('image')
		.'<span class="help-block">'
			.form_hidden('old_image', $data['item']->image)
			.form_hidden('delete_image', '0')
			.'<img src="'.$image_url.$data['item']->image.'" class="img-polaroid old_image">
			<br>
			<button class="btn btn-danger delete_image" type="button"><i class="icon-remove icon-white"></i> Delete image</button>
		</span>';
}
else
{
	$image = form_upload('image')
		.'<span class="help-block">'
			.form_hidden('old_image', '')
			.'<img src="'.base_url('/public/images/no-image.png').'" class="img-polaroid old_image">
		</span>';
}

$price = form_input('price', set_value('price', $data['item']->price), '');
$hot = '
	<label class="radio inline">
		<input type="radio" name="hot" value="1" '.set_radio('hot', '1', ($data['item']->hot == 1) ? TRUE : FALSE).'> Yes
	</label>
	<label class="radio inline">
		<input type="radio" name="hot" value="0" '.set_radio('hot', '0', ($data['item']->hot == 0) ? TRUE : FALSE).'> No
	</label>
';

$description = (isset($params['description'])) ? cms_replace_string($params['description']) : $data['item']->description;
$description = cms_editor('description', $description);

$status = '
	<label class="radio inline">
		<input type="radio" name="status" value="1" '.set_radio('status', '1', ($data['item']->status == 1) ? TRUE : FALSE).'> Enable
	</label>
	<label class="radio inline">
		<input type="radio" name="status" value="0" '.set_radio('status', '0', ($data['item']->status == 0) ? TRUE : FALSE).'> Disable
	</label>
';
$position = form_input('position', set_value('position', $data['item']->position), '');


$parser1 = array(
	'data' => array(
		array(
			'name'  => 'name',
			'label' => 'Name',
			'field' => $name,
			'help'  => ''
		),
		array(
			'name'  => 'category_id',
			'label' => 'Category',
			'field' => $category_id,
			'help'  => ''
		),
		array(
			'name'  => 'image',
			'label' => 'Image',
			'field' => $image,
			'help'  => ''
		),
		array(
			'name'  => 'price',
			'label' => 'Price',
			'field' => $price,
			'help'  => ''
		),
		array(
			'name'  => 'hot',
			'label' => 'Hot',
			'field' => $hot,
			'help'  => ''
		),
		array(
			'name'  => 'description',
			'label' => 'Description',
			'field' => $description,
			'help'  => ''
		),
		array(
			'name'  => 'status',
			'label' => 'Status',
			'field' => $status,
			'help'  => ''
		),
		array(
			'name'  => 'position',
			'label' => 'Position',
			'field' => $position,
			'help'  => ''
		)
	)
);

$tab1_legend1 = $this->parser->parse('row_paser', $parser1, TRUE);

$parser2 = array(
	'data' => array(
		array(
			'name'  => '',
			'label' => '',
			'field' => '<button class="btn btn-success btn-icon" name="add-image" type="button"><i class="icon-plus icon-white"></i></button>',
			'help'  => ''
		)
	)
);

$old_product_image = '';

if (count($data['product_image']))
{
	foreach ($data['product_image'] as $product_image)
	{
		$old_product_image .= form_hidden('old_product_image['.$product_image->id.']', $product_image->image);
		$parser2['data'][] = array(
			'name'  => '',
			'label' => '',
			'field' => form_upload('product_image_image[]')
				.form_hidden('old_product_image_image[]', $product_image->id),
			'help'  => '<span class="help-inline">
					<button class="btn btn-success btn-icon" name="add-image" type="button"><i class="icon-plus icon-white"></i></button>
					<button class="btn btn-danger btn-icon" name="delete-image" type="button"><i class="icon-minus icon-white"></i></button>
				</span>
				<span class="help-block">
					<img src="'.$image_url.'image/'.$product_image->image.'" class="img-polaroid old_image">
				</span>'
		);
	}
}

$tab2_legend1 = $this->parser->parse('row_paser', $parser2, TRUE);

// error
$this->form_validation->set_error_delimiters('', '<br>');

$image_image_error = '';
if (! empty($product_image_image_error))
{
	foreach ($product_image_image_error as $key => $error)
	{
		$image_image_error .= '<li>Image '.($key+1).': '.$error.'</li>';
	}
}

$form_error = array(
	form_error('name'),
	(empty($image_error)) ? '' : 'The Image field: '.$image_error,
	(empty($image_image_error)) ? '' : 'Product image error:<ul>'.$image_image_error.'</ul>'
);
$form_error = implode('', $form_error);

if ($form_error)
{
	$this->tn_message->set_error('<h4>Error</h4>'.$form_error);
}

?>

<h3><?php echo $title; ?></h3>

<?php echo $this->tn_message->get(); ?>

<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		<li><a href="#tab-image" data-toggle="tab">Image</a></li>
		<li><a href="#tab-seo" data-toggle="tab">Seo</a></li>
	</ul>
	<form id="update_form" name="update_form" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">
				<fieldset>
					<legend>Information</legend>
					<?php echo $tab1_legend1; ?>
				</fieldset>
			</div>
			<div class="tab-pane" id="tab-image">
				<fieldset>
					<legend>Image</legend>
					<?php
					echo $old_product_image;
					echo $tab2_legend1;
					?>
				</fieldset>
			</div>
			<div class="tab-pane" id="tab-seo">
				SEO
			</div>
			<div class="form-actions">
				<button class="btn btn-success" type="submit"><i class="icon-ok icon-white"></i> Ok</button>
				<button class="btn btn-warning" type="reset"><i class="icon-refresh icon-white"></i> Reset</button>
				<a href="<?php echo site_url($current_controller); ?>" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	var rule = {
		name: {
			required: true
		}
	}
	valid_update_form(rule);

	$('#tab-image').on('click', ':button[name="add-image"]:not(.disabled)', function(){
		$(this).parents('.control-group').after('<div class="control-group"><label class="control-label"></label><div class="controls"><input type="file" name="product_image_image[]" value=""><input type="hidden" value="" name="old_product_image_image[]"> <span class="help-inline"><button class="btn btn-success btn-icon" name="add-image" type="button"><i class="icon-plus icon-white"></i></button> <button class="btn btn-danger btn-icon" name="delete-image" type="button"><i class="icon-minus icon-white"></i></button></span></div></div>');
	});

	$('#tab-image').on('click', ':button[name="delete-image"]:not(.disabled)', function(){
		$(this).parents('.control-group').remove();
	});
});
</script>