<!-- alert -->
<div class="alert alert-info alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Welcome to Admin!</strong> You can dismiss me by clicking X button!
</div>
<!-- /alert -->

<div class="row">

  <div class="col-xs-3">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Admin system</h3>
      </div>
      <div class="panel-body">
        PayWay's BackEnd System
      </div>
    </div>
  </div>

  <div class="col-xs-3">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">Stats</h3>
      </div>
      <div class="panel-body">
        <div>Visitors: 100</div>
        <div>Pageload: 10000</div>
      </div>
    </div>
  </div>
  <div class="col-xs-3">
    <div class="panel panel-warning">
      <div class="panel-heading">
        <h3 class="panel-title">Quick links</h3>
      </div>
      <div class="panel-body">

      </div>
    </div>
  </div>
  <div class="col-xs-3">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h3 class="panel-title">Sitemap</h3>
      </div>
      <div class="panel-body">
        <ul class="sitemap">
          <li>Dashboard</li>
          <li>Blog
            <ul>
              <li>Category</li>
              <li>Article</li>
            </ul>
          </li>
          <li>Website
            <ul>
              <li>Contact</li>
            </ul>
          </li>
          <li>Module
            <ul>
              <li>Partner</li>
            </ul>
          </li>
          <li>User
            <ul>
              <li>Account</li>
              <li>Group</li>
              <li>Permission</li>
            </ul>
          </li>
          <li>Profile</li>
        </ul>
      </div>
    </div>
  </div>

</div>
