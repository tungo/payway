<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_method extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('TN_Message');
		$this->load->helper(array('form','cms'));
		$this->load->model('database/system_method_model');

		$this->data['title'] = 'System : Method';
	}

	public function index($page = 1)
	{
		$this->tn_message->flash();

		$this->load->library('TN_Pagination');
		$this->load->helper('cms');
		// filter
		$this->data['params']['filter'] = array();
		if (! $this->session->userdata($this->data['namespace']))
		{
			$this->data['params']['filter'] = array(
				'keyword'  => '',
				'sort'     => 'id',
				'order'    => 'DESC',
				'per_page' => $this->tn_pagination->per_page
			);
			$this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
		}
		else
		{
			$this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
		}

		// pagination
		$this->tn_pagination->cur_page   = (int) $page;
		$this->tn_pagination->base_url   = base_url($this->data['main_method']);
		$this->tn_pagination->per_page   = $this->data['params']['filter']['per_page'];
		$this->tn_pagination->total_rows = $this->system_method_model->count_entries_admin($this->data['params']);
		$this->tn_pagination->create_links();

		$this->data['params']['pagination'] = $this->tn_pagination;

		// get list
		$this->data['data']['items'] = $this->system_method_model->get_entries_admin($this->data['params']);

		$this->data['title'] = $this->data['title'].' : Manage';
		$this->load_view('system_method/manage', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$unique = array(
				'field' => 'name',
				'value' => $this->data['params']['name']
			);
			if ($this->system_method_model->check_unique($unique))
			{
				$this->data['name_error'] = 'The Name field must contain a unique value.<br>';
			}
			else
			{
								if ($this->system_method_model->add_entry($this->data['params']))
				{
					$this->tn_message->set_success('Added successfully.');
					$this->tn_message->save();
				}
				else
				{
					$this->tn_message->set_error('Failed to add.');
					$this->tn_message->save();
				}
				redirect($this->data['current_controller']);
				return;
			}
		}

		$this->data['title'] = $this->data['title'].' : Add';
		$this->load->library('parser');
		$this->load_view('system_method/add', $this->data);
	}

	public function edit($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->system_method_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->tn_message->set_warning('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$unique = array(
				'field' => 'name',
				'value' => $this->data['params']['name'],
				'old'   => $this->data['data']['item']->name
			);
			if ($this->system_method_model->check_unique($unique))
			{
				$this->data['name_error'] = 'The Name field must contain a unique value.<br>';
			}
			else
			{
								if ($this->system_method_model->update_entry($this->data['params']))
				{
					$this->tn_message->set_success('Edited successfully.');
					$this->tn_message->save();
				}
				else
				{
					$this->tn_message->set_error('Failed to edit.');
					$this->tn_message->save();
				}
				redirect($this->data['current_controller']);
				return;
			}
		}

		$this->load->library('parser');
		$this->data['title'] = $this->data['title'].' : Edit';
		$this->load_view('system_method/edit', $this->data);
	}

	public function multi_copy()
	{
		if ($_POST)
		{
			if ($this->system_method_model->copy_multi_entries($this->data['params']))
			{
				$this->tn_message->set_success('Coped successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to copy.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		if ( ! $this->system_method_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->system_method_model->delete_entry($this->data['params']))
		{
			$this->tn_message->set_success('Deleted successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
		}
		else
		{
			$this->tn_message->set_error('Failed to delete.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function multi_delete()
	{
		if ($_POST)
		{
			if ($this->system_method_model->delete_multi_entries($this->data['params']))
			{
				$this->tn_message->set_success('Deleted successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to delete.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function save()
	{
		if ($_POST)
		{
			if ($this->system_method_model->save_entries($this->data['params']))
			{
				$this->tn_message->set_success('Updated successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to update.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function status($id = 0, $status = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$status = (int) $status;
		$this->data['params']['status'] = $status;

		if ( ! $this->system_method_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->system_method_model->change_status_entry($this->data['params']))
		{
			$this->tn_message->set_success('Changed status successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_error('Failed to change status.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function active()
	{
		if ($_POST)
		{
			if ($this->system_method_model->active_entries($this->data['params']))
			{
				$this->tn_message->set_success('Active successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to active.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function disable()
	{
		if ($_POST)
		{
			if ($this->system_method_model->disable_entries($this->data['params']))
			{
				$this->tn_message->set_success('Disable successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to disable.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function search()
	{
		if ($_POST)
		{
			$keyword = isset($this->data['params']['keyword']) ? $this->data['params']['keyword'] : '';

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['keyword'] = $keyword;
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function show()
	{
		if ($_POST)
		{
			$per_page = isset($this->data['params']['list']) ? $this->data['params']['list'] : 10;
			if ($per_page < 1) $per_page = 10;

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['per_page'] = $per_page;
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function sort()
	{
		if ($_POST)
		{
			$sort = isset($this->data['params']['sort']) ? $this->data['params']['sort'] : $this->system_method_model->table.'.id';

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['sort'] = $sort;
			$filter['order'] = ($filter['order'] == 'DESC') ? 'ASC' : 'DESC';
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}
}