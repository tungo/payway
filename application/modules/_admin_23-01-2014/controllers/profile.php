<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('TN_Message');
		$this->load->helper(array('form','cms'));
		$this->load->model('database/user_account_model');

		$this->data['title'] = 'Profile';
	}

	public function index()
	{
		$this->data['params']['id'] = $this->data['member']['id'];

		$this->data['data']['item'] = $this->user_account_model->get_entry($this->data['params']);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', 'Password', 'matches[confirm]');
		$this->form_validation->set_rules('confirm', 'Confirm', 'matches[password]');
		$this->form_validation->set_rules('firstname', 'Firstname', 'required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			if ($this->user_account_model->update_entry($this->data['params']))
			{
				$this->data['member']['firstname'] = $data['firstname'];
				$this->data['member']['lastname'] = $data['lastname'];
				$this->session->set_userdata('member', $this->data['member']);

				$this->tn_message->set_success('Edited successfully.');
				$this->tn_message->save();
			}
			else
			{
				$this->tn_message->set_error('Failed to edit.');
				$this->tn_message->save();
			}
			redirect($this->data['path']);
		}

		$this->load->library('parser');
		$this->load_view('profile', $this->data);
	}
}