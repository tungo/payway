<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_support extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form','cms'));
		$this->data['theme'] = 'admin';
		$this->load->model('database/web_support_model');
		$this->data['messages'] = array();
	}

	public function index($page = 1)
	{
		// load paginator
		$this->load->library('TN_Pagination');
		$pagination = $this->config->item('admin');

		if ( ! $this->session->userdata('per_page'))
		{
			$this->session->set_userdata('per_page', $pagination['per_page']);
		}

		// config paginator
		$pagination['cur_page'] = (int) $page;
		$pagination['base_url'] = base_url($this->data['main_method']);
		$pagination['per_page'] = $this->session->userdata('per_page');

		$pagination['total_rows'] = $this->web_support_model->count_admin_entries($this->data['params']);

		$this->tn_pagination->initialize('admin', $pagination);
		$this->tn_pagination->create_links();

		$this->data['params']['pagination'] = $this->tn_pagination;

		$this->data['data']['items'] = $this->web_support_model->get_admin_entries($this->data['params']);

		$this->data['title'] = 'Website : Support : Manage';
		$this->load_view('support/index', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('yahoo', 'Yahoo', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

		if ($this->form_validation->run() == TRUE)
		{
						if ($this->web_support_model->add_entry($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Added successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to add.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			redirect($this->data['current_controller']);
		}
		else
		{
			$this->data['title'] = 'Website : Support : Add';
			$this->load->library('parser');
			$this->load_view('support/add', $this->data);
		}
	}

	public function info($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->web_support_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}
		else
		{
			$this->data['title'] = 'Website : Support : Information';
			$this->load->library('parser');
			$this->load_view('support/info', $this->data);
		}
	}

	public function edit($id = 0)
	{
		$this->load->library('form_validation');

		$id = (int) $id;
		$this->data['params']['id'] = $id;
		// sua lai
		$this->data['data']['item'] = $this->web_support_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}
		else
		{
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('yahoo', 'Yahoo', 'required');
			$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

			if ($this->form_validation->run() == TRUE)
			{
								if ($this->web_support_model->update_entry($this->data['params']))
				{
					$this->data['messages'][] = array('type' => 'success', 'content' => 'Edited successfully.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				else
				{
					$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to edit.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				redirect($this->data['current_controller']);
			}
			else
			{
				$this->load->library('parser');
				$this->data['title'] = 'Website : Support : Edit';
				$this->load_view('support/edit', $this->data);
			}
		}
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->web_support_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}
		else
		{
			if ($this->web_support_model->delete_entry($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
		}
		redirect($this->data['current_controller']);
	}

	public function multi_delete()
	{
		if ($_POST){
			if ($this->web_support_model->delete_multi_entries($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
		}
		else
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Access restricted');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}

	public function per_page($number = 5)
	{
		$number = (int) $number;
		if ($number > 0)
		{
			$this->session->set_userdata('per_page', $number);
		}
		redirect($this->data['current_controller']);
	}
}