<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_content extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form','cms'));
		$this->data['theme'] = 'admin';
		$this->load->model('database/web_content_model');
		$this->data['messages'] = array();
	}

	public function index()
	{
		//$this->data['data']['items'] = $this->web_content_model->get_entries_admin($this->data['params']);

		$this->data['title'] = 'Website : Content : Manage';
		$this->load_view('content/manage', $this->data);
	}

	public function entries()
	{
		$this->data['data']['items'] = $this->web_content_model->get_entries_admin($this->data['params']);

		$data = array();
		foreach($this->data['data']['items'] as $key => $value)
		{
			$status = 'Disable';
			$status_link = site_url($this->data['current_controller'].'/status/'.$value->id.'/1');
			$status_img = base_url('public/templates/admin/img/icons/basic/block-16.png');
			if ($value->status)
			{
				$status = 'Active';
				$status_link = site_url($this->data['current_controller'].'/status/'.$value->id.'/0');
				$status_img = base_url('public/templates/admin/img/icons/basic/tick-16.png');
			}

			$data[] = array(
				$value->id,
				$value->name,
				$value->note,
				array($value->status, $status, $status_link, $status_img),
				$value->position,
				$value->id,
				array(
					$value->id,
					site_url($this->data['current_controller'].'/edit/'.$value->id),
					base_url('public/templates/admin/img/icons/basic/pencil-16.png'),
					site_url($this->data['current_controller'].'/delete/'.$value->id),
					base_url('public/templates/admin/img/icons/basic/delete-16.png')
				)
			);
		}

		$data = array(
			'aaData' => $data
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
		return;
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() == TRUE)
		{
			$this->data['params']['method'] = $this->data['method'];
			$this->data['params']['picture'] = $this->web_content_model->upload_file($this->data['params']);
			if ($this->data['params']['picture'] === FALSE)
			{
				$this->data['picture_error'] = $this->upload->display_errors('', '<br>');
			}
			else
			{
								if ($this->web_content_model->add_entry($this->data['params']))
				{
					$this->data['messages'][] = array('type' => 'success', 'content' => 'Added successfully.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				else
				{
					$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to add.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				redirect($this->data['current_controller']);
			}
		}

		$this->data['title'] = 'Website : Content : Add';
		$this->load->library('parser');
		$this->load_view('content/add', $this->data);
	}

	public function edit($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['params']['method'] = $this->data['method'];
		$this->data['data']['item'] = $this->web_content_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() == TRUE)
		{
			$this->data['params']['picture'] = $this->web_content_model->upload_file($this->data['params']);
			if ($this->data['params']['picture'] === FALSE)
			{
				$this->data['picture_error'] = $this->upload->display_errors('', '<br>');
			}
			else
			{
								if ($this->web_content_model->update_entry($this->data['params']))
				{
					$this->data['messages'][] = array('type' => 'success', 'content' => 'Edited successfully.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				else
				{
					$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to edit.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				redirect($this->data['current_controller']);
			}
		}

		$this->load->library('parser');
		$this->data['title'] = 'Website : Content : Edit';
		$this->load_view('content/edit', $this->data);
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		if ( ! $this->web_content_model->check_entry($this->data['params']))
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Information is not founded.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}

		if ($this->web_content_model->delete_entry($this->data['params']))
		{
			$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Deleted successfully.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
		else
		{
			$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to delete.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}
/*
	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		if ( ! $this->web_content_model->check_entry($this->data['params']))
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		if ($this->web_content_model->delete_entry($this->data['params']))
		{
			$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		else
		{
			$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}
*/
	public function multi_delete()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if ($this->web_content_model->delete_multi_entries($this->data['params']))
			{
				$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Deleted successfully.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
			else
			{
				$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to delete.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
		}
		else
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Access restricted.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}
/*
	public function multi_delete()
	{
		if ($_POST){
			if ($this->web_content_model->delete_multi_entries($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
		}
		else
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Access restricted');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}
*/
	public function save()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			if ($this->web_content_model->save_entries($this->data['params']))
			{
				$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Updated successfully.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
			else
			{
				$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to update.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
		}
		else
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Access restricted.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}

	public function status($id = 0, $status = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$status = (int) $status;
		$this->data['params']['status'] = $status;

		if ( ! $this->web_content_model->check_entry($this->data['params']))
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Information is not founded.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}

		if ($this->web_content_model->change_status_entry($this->data['params']))
		{
			$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Changed status successfully.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
		else
		{
			$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to change status.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}

	public function active()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			if ($this->web_content_model->active_entries($this->data['params']))
			{
				$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Active successfully.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
			else
			{
				$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to active.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
		}
		else
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Access restricted.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}

	public function disable()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			if ($this->web_content_model->disable_entries($this->data['params']))
			{
				$this->data['messages'] = array('type' => 'success', 'title' => 'Success', 'content' => 'Disable successfully.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
			else
			{
				$this->data['messages'] = array('type' => 'error', 'title' => 'Failed', 'content' => 'Failed to disable.');
				$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
				return;
			}
		}
		else
		{
			$this->data['messages'] = array('type' => 'warn', 'title' => 'Failed', 'content' => 'Access restricted.');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->data['messages']));
			return;
		}
	}
}