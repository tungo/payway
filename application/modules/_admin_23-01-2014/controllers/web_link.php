<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_link extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form','cms'));
		$this->data['theme'] = 'admin';
		$this->load->model('database/web_link_model');
		$this->data['messages'] = array();
	}

	public function index($page = 1)
	{
		// load paginator
		$this->load->library('TN_Pagination');
		$pagination = $this->config->item('admin');

		if ( ! $this->session->userdata('per_page'))
		{
			$this->session->set_userdata('per_page', $pagination['per_page']);
		}

		// config paginator
		$pagination['cur_page'] = (int) $page;
		$pagination['base_url'] = base_url($this->data['main_method']);
		$pagination['per_page'] = $this->session->userdata('per_page');

		$pagination['total_rows'] = $this->web_link_model->count_admin_entries($this->data['params']);

		$this->tn_pagination->initialize('admin', $pagination);
		$this->tn_pagination->create_links();

		$this->data['params']['pagination'] = $this->tn_pagination;

		$this->data['data']['items'] = $this->web_link_model->get_admin_entries($this->data['params']);

		$this->data['title'] = 'Website : Link : Manage';
		$this->load_view('link/index', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

		if ($this->form_validation->run() == TRUE)
		{
			$config['upload_path'] = FILESPATH.'link/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '4096';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);

			$image = $_FILES['image']['name'];
			if ($image)
			{
				if ($this->upload->do_upload('image'))
				{
					$image = $this->upload->data();
					$this->data['params']['image'] = $image['file_name'];
										if ($this->web_link_model->add_entry($this->data['params']))
					{
						$this->data['messages'][] = array('type' => 'success', 'content' => 'Added successfully.');
						$this->session->set_flashdata('messages', $this->data['messages']);
					}
					else
					{
						$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to add.');
						$this->session->set_flashdata('messages', $this->data['messages']);
					}
					redirect($this->data['current_controller']);
				}
				else
				{
					$image = '';
					$this->data['image_error'] = $this->upload->display_errors('<span class="input-notification error png_bg">', '</span>');
				}

			}
			else
			{
				$this->data['params']['image'] = '';
								if ($this->web_link_model->add_entry($this->data['params']))
				{
					$this->data['messages'][] = array('type' => 'success', 'content' => 'Added successfully.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				else
				{
					$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to add.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				redirect($this->data['current_controller']);
			}
		}

		$this->data['title'] = 'Website : Link : Add';
		$this->load->library('parser');
		$this->load_view('link/add', $this->data);
	}

	public function info($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->web_link_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->data['title'] = 'Website : Link : Information';
		$this->load->library('parser');
		$this->load_view('link/info', $this->data);
	}

	public function edit($id = 0)
	{
		$this->load->library('form_validation');

		$id = (int) $id;
		$this->data['params']['id'] = $id;
		// sua lai
		$this->data['data']['item'] = $this->web_link_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

		if ($this->form_validation->run() == TRUE)
		{
			$config['upload_path'] = FILESPATH.'link/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = '4096';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);

			$image = $_FILES['image']['name'];
			if ($image)
			{
				if ($this->upload->do_upload('image'))
				{
					$this->web_link_model->delete_image($this->data['params']);
					$image = $this->upload->data();
					$this->data['params']['image'] = $image['file_name'];
										if ($this->web_link_model->update_entry($this->data['params']))
					{
						$this->data['messages'][] = array('type' => 'success', 'content' => 'Edited successfully.');
						$this->session->set_flashdata('messages', $this->data['messages']);
					}
					else
					{
						$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to edit.');
						$this->session->set_flashdata('messages', $this->data['messages']);
					}
					redirect($this->data['current_controller']);
				}
				else
				{
					$image = '';
					$this->data['image_error'] = $this->upload->display_errors('<span class="input-notification error png_bg">', '</span>');
				}

			}
			else
			{
				$this->data['params']['image'] = '';
								if ($this->web_link_model->update_entry($this->data['params']))
				{
					$this->data['messages'][] = array('type' => 'success', 'content' => 'Edited successfully.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				else
				{
					$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to edit.');
					$this->session->set_flashdata('messages', $this->data['messages']);
				}
				redirect($this->data['current_controller']);
			}
		}

		$this->load->library('parser');
		$this->data['title'] = 'Website : Link : Edit';
		$this->load_view('link/edit', $this->data);
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->web_link_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->web_link_model->delete_image($this->data['params']);
		if ($this->web_link_model->delete_entry($this->data['params']))
		{
			$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			$this->web_link_model->delete_image($this->data['params']);
		}
		else
		{
			$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}

		redirect($this->data['current_controller']);
	}

	public function multi_delete()
	{
		if ($_POST){
			$this->web_link_model->delete_multi_images($this->data['params']);
			if ($this->web_link_model->delete_multi_entries($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			$this->web_link_model->delete_multi_entries($this->data['params']);
		}
		else
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Access restricted');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}

	public function per_page($number = 5)
	{
		$number = (int) $number;
		if ($number > 0)
		{
			$this->session->set_userdata('per_page', $number);
		}
		redirect($this->data['current_controller']);
	}
}