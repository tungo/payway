<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('TN_Message');
		$this->load->helper(array('form','cms'));
		$this->data['theme']['layout'] = 'login';
	}

	// Alert access restrict at auth/index
	public function index()
	{
		// if user can access admin system
		if ($this->tn_auth->is_admin())
		{
			redirect($this->data['path']);
		}

		$this->tn_message->set_warning('Your account can not access this page.');

		$this->data['title'] = 'Admin';
		$this->load_view('auth', $this->data);
	}

	// Log in page
	public function login()
	{
		// if logged in
		if ($this->tn_auth->is_logged_in())
		{
			redirect($this->data['path']);
		}

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('', '<br>');

		$this->data['params']['remember'] = (bool) $this->input->post('remember');

		$this->tn_message->flash();
		if ($this->form_validation->run() == TRUE)
		{
			// logged in successfully
			if ($this->tn_auth->login($this->data))
			{
				redirect($this->data['path']);
			}
			else
			{
				$this->tn_message->set_error('Username or Password is incorrect.');
			}
		}
		else
		{
			$this->tn_message->set_multi_error(array(form_error('username'), form_error('password')));
		}
		$this->data['title'] = 'Login';
		$this->load_view('login', $this->data);
	}

	// Log out page
	public function logout()
	{
		$this->tn_auth->logout();

		$this->tn_message->set_success('Signed out successfully.');
		$this->tn_message->save();

		redirect($this->data['path'].'auth/login');
	}
}