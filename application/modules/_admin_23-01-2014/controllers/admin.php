<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form','cms'));

		$this->data['request'] = $this->uri->uri_to_assoc(3);
	}

	public function index()
	{
		$this->data['title'] = 'Dashboard';
		$this->load_view('dashboard', $this->data);
	}

	public function language()
	{
		$this->session->set_userdata($this->data['language_session'], $this->data['request']['id']);
		redirect($this->input->server('HTTP_REFERER'));
	}
}