<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_account extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('TN_Message');
		$this->load->helper(array('form','cms'));
		$this->load->model('database/user_account_model');

		$this->data['title'] = 'User : Account';
	}

	public function index($page = 1)
	{
		$this->tn_message->flash();

		$this->load->library('TN_Pagination');
		$this->load->helper('cms');
		// filter
		$this->data['params']['filter'] = array();
		if (! $this->session->userdata($this->data['namespace']))
		{
			$this->data['params']['filter'] = array(
				'keyword'  => '',
				'sort'     => 'cp.id',
				'order'    => 'DESC',
				'per_page' => $this->tn_pagination->per_page,
				'category' => ''
			);
			$this->session->set_userdata($this->data['namespace'], $this->data['params']['filter']);
		}
		else
		{
			$this->data['params']['filter'] = $this->session->userdata($this->data['namespace']);
		}

		// pagination
		$this->tn_pagination->cur_page   = (int) $page;
		$this->tn_pagination->base_url   = base_url($this->data['main_method']);
		$this->tn_pagination->per_page   = $this->data['params']['filter']['per_page'];
		$this->tn_pagination->total_rows = $this->user_account_model->count_entries_admin($this->data['params']);
		$this->tn_pagination->create_links();

		$this->data['params']['pagination'] = $this->tn_pagination;

		// get list
		$this->data['data']['items'] = $this->user_account_model->get_entries_admin($this->data['params']);

		$this->load->model('database/catalog_category_model');
		$this->data['select_category'] = $this->catalog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'product-filter'));

		$this->data['title'] = $this->data['title'].' : Manage';
		$this->load_view('user_account/manage', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE)
		{
						if ($this->user_account_model->add_entry($this->data['params']))
			{
				$this->tn_message->set_success('Added successfully.');
				$this->tn_message->save();
			}
			else
			{
				$this->tn_message->set_error('Failed to add.');
				$this->tn_message->save();
			}
			redirect($this->data['current_controller']);
			return;
		}

		$this->load->model('database/catalog_category_model');
		$this->data['select_category'] = $this->catalog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'product-admin'));

		$this->data['title'] = $this->data['title'].' : Add';
		$this->load->library('parser');
		$this->load_view('user_account/add', $this->data);
	}

	public function edit($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->user_account_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->tn_message->set_warning('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE)
		{
						if ($this->user_account_model->update_entry($this->data['params']))
			{
				$this->tn_message->set_success('Edited successfully.');
				$this->tn_message->save();
			}
			else
			{
				$this->tn_message->set_error('Failed to edit.');
				$this->tn_message->save();
			}
			redirect($this->data['current_controller']);
			return;
		}

		$this->load->model('database/catalog_category_model');
		$this->data['select_category'] = $this->catalog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'product-admin'));

		$this->load->library('parser');
		$this->data['title'] = $this->data['title'].' : Edit';
		$this->load_view('user_account/edit', $this->data);
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		if ( ! $this->user_account_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->user_account_model->delete_entry($this->data['params']))
		{
			$this->tn_message->set_success('Deleted successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_error('Failed to delete.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function multi_delete()
	{
		if ($_POST)
		{
			if ($this->user_account_model->delete_multi_entries($this->data['params']))
			{
				$this->tn_message->set_success('Deleted successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to delete.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function save()
	{
		if ($_POST)
		{
			if ($this->user_account_model->save_entries($this->data['params']))
			{
				$this->tn_message->set_success('Updated successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to update.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function status($id = 0, $status = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$status = (int) $status;
		$this->data['params']['status'] = $status;

		if ( ! $this->user_account_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->user_account_model->change_status_entry($this->data['params']))
		{
			$this->tn_message->set_success('Changed status successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_error('Failed to change status.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function active()
	{
		if ($_POST)
		{
			if ($this->user_account_model->active_entries($this->data['params']))
			{
				$this->tn_message->set_success('Active successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to active.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function disable()
	{
		if ($_POST)
		{
			if ($this->user_account_model->disable_entries($this->data['params']))
			{
				$this->tn_message->set_success('Disable successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to disable.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function search()
	{
		if ($_POST)
		{
			$keyword = isset($this->data['params']['keyword']) ? $this->data['params']['keyword'] : '';

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['keyword'] = $keyword;
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function show()
	{
		if ($_POST)
		{
			$per_page = isset($this->data['params']['list']) ? $this->data['params']['list'] : 10;
			if ($per_page < 1) $per_page = 10;

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['per_page'] = $per_page;
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function sort()
	{
		if ($_POST)
		{
			$sort = isset($this->data['params']['sort']) ? $this->data['params']['sort'] : $this->user_account_model->table.'.id';

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['sort'] = $sort;
			$filter['order'] = ($filter['order'] == 'DESC') ? 'ASC' : 'DESC';
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function filter()
	{
		if ($_POST)
		{
			$category_filter = '';
			if (isset($this->data['params']['category_filter']))
			{
				$category_filter = ($this->data['params']['category_filter'] !== '')
					? (int) $this->data['params']['category_filter']
					: '';
			}

			$filter = $this->session->userdata($this->data['namespace']);
			$filter['category'] = $category_filter;
			$this->session->set_userdata($this->data['namespace'], $filter);

			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}
}