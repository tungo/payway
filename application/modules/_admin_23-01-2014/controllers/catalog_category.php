<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_category extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('TN_Message');
		$this->load->helper(array('form','cms'));
		$this->load->model('database/catalog_category_model');

		$this->data['image_url'] = $this->catalog_category_model->image_url;

		$this->data['title'] = 'Catalog : Category';
	}

	public function index($page = 1)
	{
		$this->tn_message->flash();

		$this->load->library('TN_Pagination');
		$this->load->helper('cms');

		$this->data['data']['total_rows'] = $this->catalog_category_model->count_entries_admin($this->data['params']);
		// get list
		$this->data['data']['items'] = $this->catalog_category_model->get_entries_admin($this->data['params']);

		$this->data['title'] = $this->data['title'].' : Manage';
		$this->load_view('catalog_category/manage', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('parent_id', 'Parent Category', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$this->data['params']['method'] = $this->data['method'];
			$this->data['params']['image'] = $this->catalog_category_model->upload_file($this->data['params']);
			if ($this->data['params']['image'] === FALSE)
			{
				$this->data['image_error'] = $this->upload->display_errors('', '<br>');
			}
			else
			{
								if ($this->catalog_category_model->add_entry($this->data['params']))
				{
					$this->tn_message->set_success('Added successfully.');
					$this->tn_message->save();
				}
				else
				{
					$this->tn_message->set_error('Failed to add.');
					$this->tn_message->save();
				}
				redirect($this->data['current_controller']);
				return;
			}
		}

		$this->data['select_category'] = $this->catalog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'category-add'));

		$this->data['title'] = $this->data['title'].' : Add';
		$this->load->library('parser');
		$this->load_view('catalog_category/add', $this->data);
	}

	public function edit($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->catalog_category_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->tn_message->set_warning('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('parent_id', 'Parent Category', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$this->data['params']['method'] = $this->data['method'];
			$this->data['params']['image'] = $this->catalog_category_model->upload_file($this->data['params']);
			if ($this->data['params']['image'] === FALSE)
			{
				$this->data['image_error'] = $this->upload->display_errors('', '<br>');
			}
			else
			{
								if ($this->catalog_category_model->update_entry($this->data['params']))
				{
					$this->tn_message->set_success('Edited successfully.');
					$this->tn_message->save();
				}
				else
				{
					$this->tn_message->set_error('Failed to edit.');
					$this->tn_message->save();
				}
				redirect($this->data['current_controller']);
				return;
			}
		}

		$this->data['select_category'] = $this->catalog_category_model->get_entries_select_admin($this->data['params'], array('method' => 'category-edit'));

		$this->load->library('parser');
		$this->data['title'] = $this->data['title'].' : Edit';
		$this->load_view('catalog_category/edit', $this->data);
	}

	public function multi_copy()
	{
		if ($_POST)
		{
			if ($this->catalog_category_model->copy_multi_entries($this->data['params']))
			{
				$this->tn_message->set_success('Coped successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to copy.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		if ( ! $this->catalog_category_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->catalog_category_model->delete_entry($this->data['params']))
		{
			$this->tn_message->set_success('Deleted successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
		}
		else
		{
			$this->tn_message->set_error('Failed to delete.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function multi_delete()
	{
		if ($_POST)
		{
			if ($this->catalog_category_model->delete_multi_entries($this->data['params']))
			{
				$this->tn_message->set_success('Deleted successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to delete.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function save()
	{
		if ($_POST)
		{
			if ($this->catalog_category_model->save_entries($this->data['params']))
			{
				$this->tn_message->set_success('Updated successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to update.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function status($id = 0, $status = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$status = (int) $status;
		$this->data['params']['status'] = $status;

		if ( ! $this->catalog_category_model->check_entry($this->data['params']))
		{
			$this->tn_message->set_error('Information is not founded.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}

		if ($this->catalog_category_model->change_status_entry($this->data['params']))
		{
			$this->tn_message->set_success('Changed status successfully.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
		else
		{
			$this->tn_message->set_error('Failed to change status.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function active()
	{
		if ($_POST)
		{
			if ($this->catalog_category_model->active_entries($this->data['params']))
			{
				$this->tn_message->set_success('Active successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to active.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}

	public function disable()
	{
		if ($_POST)
		{
			if ($this->catalog_category_model->disable_entries($this->data['params']))
			{
				$this->tn_message->set_success('Disable successfully.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
			else
			{
				$this->tn_message->set_error('Failed to disable.');
				$this->tn_message->save();
				redirect($this->input->server('HTTP_REFERER'));
				return;
			}
		}
		else
		{
			$this->tn_message->set_warning('Access restricted.');
			$this->tn_message->save();
			redirect($this->input->server('HTTP_REFERER'));
			return;
		}
	}
}