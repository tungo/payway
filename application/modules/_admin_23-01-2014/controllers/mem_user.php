<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mem_user extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form','cms'));
		$this->data['theme'] = 'admin';
		$this->load->model('database/mem_user_model');
		$this->data['messages'] = array();
	}

	public function index($page = 1)
	{
		// load paginator
		$this->load->library('TN_Pagination');
		$pagination = $this->config->item('admin');

		if ( ! $this->session->userdata('per_page'))
		{
			$this->session->set_userdata('per_page', $pagination['per_page']);
		}

		// config paginator
		$pagination['cur_page'] = (int) $page;
		$pagination['base_url'] = base_url($this->data['main_method']);
		$pagination['per_page'] = $this->session->userdata('per_page');

		$pagination['total_rows'] = $this->mem_user_model->count_admin_entries($this->data['params']);

		$this->tn_pagination->initialize('admin', $pagination);
		$this->tn_pagination->create_links();

		$this->data['params']['pagination'] = $this->tn_pagination;

		$this->data['data']['items'] = $this->mem_user_model->get_admin_entries($this->data['params']);

		$this->data['title'] = 'Member : User : Manage';
		$this->load_view('user/index', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

		if ($this->form_validation->run() == TRUE)
		{
						if ($this->mem_user_model->add_entry($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Added successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to add.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			redirect($this->data['current_controller']);
		}

		$this->data['title'] = 'Member : User : Add';
		$this->load->library('parser');
		$this->load_view('user/add', $this->data);
	}

	public function info($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->mem_user_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->data['title'] = 'Member : User : Information';
		$this->load->library('parser');
		$this->load_view('user/info', $this->data);
	}

	public function edit($id = 0)
	{
		$this->load->library('form_validation');

		$id = (int) $id;
		$this->data['params']['id'] = $id;
		// sua lai
		$this->data['data']['item'] = $this->mem_user_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');

		if ($this->form_validation->run() == TRUE)
		{
						if ($this->mem_user_model->update_entry($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Edited successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to edit.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			redirect($this->data['current_controller']);
		}

		$this->load->library('parser');
		$this->data['title'] = 'Member : User : Edit';
		$this->load_view('user/edit', $this->data);
	}

	public function delete($id = 0)
	{
		$id = (int) $id;
		$this->data['params']['id'] = $id;

		$this->data['data']['item'] = $this->mem_user_model->get_entry($this->data['params']);
		if ( ! $this->data['data']['item']->id)
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Information is not founded.');
			$this->session->set_flashdata('messages', $this->data['messages']);
			redirect($this->data['current_controller']);
		}

		if ($this->mem_user_model->delete_entry($this->data['params']))
		{
			$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		else
		{
			$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}

	public function multi_delete()
	{
		if ($_POST){
			if ($this->mem_user_model->delete_multi_entries($this->data['params']))
			{
				$this->data['messages'][] = array('type' => 'success', 'content' => 'Deleted successfully.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
			else
			{
				$this->data['messages'][] = array('type' => 'error', 'content' => 'Failed to delete.');
				$this->session->set_flashdata('messages', $this->data['messages']);
			}
		}
		else
		{
			$this->data['messages'][] = array('type' => 'warn', 'content' => 'Access restricted');
			$this->session->set_flashdata('messages', $this->data['messages']);
		}
		redirect($this->data['current_controller']);
	}

	public function per_page($number = 5)
	{
		$number = (int) $number;
		if ($number > 0)
		{
			$this->session->set_userdata('per_page', $number);
		}
		redirect($this->data['current_controller']);
	}
}