<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acl_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->model('database/user_group_model', 'group_m');
	}

	public function get_acl($group_id = '')
	{
		$privileges = $this->group_m->get_privilege_acl(array('group_id' => $group_id));
		$acl = array();
		foreach ($privileges as $privilege)
			$acl[$privilege['privilege_id']] = $privilege['module'].'-'.$privilege['controller'].'-'.$privilege['method'];
		return $acl;
	}
}