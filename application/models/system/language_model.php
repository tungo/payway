<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

    $this->table = 'language';
	}

  public function check($id = 0)
  {
    return $this->db->where(array('id' => $id))->count_all_results($this->table);
  }

  public function get_default()
  {
    $this->db->select('id');
    $this->db->from($this->table);
    $this->db->where('status', 1);
    $this->db->limit(1);
    $this->db->order_by('position', 'DESC');
    $row = $this->db->get()->row_array();
    return $row['id'];
  }

  public function get()
  {
    $this->db->select('id, name, code, locale, image, directory');
    $this->db->from($this->table);
    $this->db->where('status', 1);
    $this->db->order_by('position', 'DESC');
    $result = $this->db->get()->result_array();

    foreach ($result as $language)
    {
      $languages[$language['id']] = $language;
    }
    return $languages;
  }
}