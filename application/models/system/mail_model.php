<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail_model extends MY_Model {

  function __construct()
  {
    parent::__construct();

    $this->data['email'] = $this->config->item('email_smtp');
    $this->load->library('email', $this->data['email']);
  }

  public function send($data = array(), $options = array())
  {
    $this->email->from('contact@payway.vn');
    $this->email->to($data['mail_to']);

    if(!empty($data['cc']))
      $this->email->cc(implode(',', $data['cc']));

    if(!empty($data['bcc']))
      $this->email->bcc(implode(',', $data['bcc']));

    $this->email->subject($data['mail_msg']['subject']);
    $this->email->message($data['mail_msg']['message']);

    return $this->email->send();
  }
}