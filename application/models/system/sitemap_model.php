<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

  public function get_entries_site($params = array(), $options = array())
  {
    $return = array();

    // blog category
    $this->db->select('bc.seo_name, bc.link');
    $this->db->from('blog_category AS bc');
    $this->db->where('bc.status', '1');
    $this->db->order_by('bc.position, bc.id');

    $blog_category = $this->db->get()->result_array();
    foreach ($blog_category as $item)
    {
      if ($item['link'] == '')
        $return[] = $item['seo_name'].'.html';
    }

    // blog article
    $this->db->select('ba.seo_name, bc.seo_name AS category_seo_name');
    $this->db->from('blog_article AS ba');
    $this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
    $this->db->where(array('ba.status' => 1, 'bc.status' => 1));
    $this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');

    $blog_article = $this->db->get()->result_array();
    foreach ($blog_article as $item)
    {
      $return[] = $item['category_seo_name'].'/'.$item['seo_name'].'.html';
    }

    return $return;
  }

}