<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->model('database/user_account_model', 'account_m');
		$this->load->model('database/user_group_model', 'group_m');
	}

	// Process log in
	public function login($params = array(), $options = array())
	{
		// get user's data
		$where = array(
			'email' => $this->db->escape_str($params['email']),
		);
		$user = $this->account_m->get_entry_auth($where);

		// email is correct
		if (!empty($user))
		{
			$where = array(
				'email' => $user['email'],
				'password' => $this->account_m->hash_password($params['password'], $user['salt']),
				'status' => 1,
			);
			$user = $this->account_m->get_entry_auth($where);

			// password is correct
			if (!empty($user))
			{
				// update last login time
				$this->account_m->update_last_login(array('id' => $user['id'], 'ip_address' => $this->input->ip_address()));

				// get user's group
				$group = $this->group_m->get_entry_auth(array('id' => $user['group_id'], 'status' => 1));

				// return false if can not get group
				if (!empty($group))
				{
					$member = array(
						'id' => $user['id'],
						'email' => $user['email'],
						'salt' => $user['salt'],
						'firstname' => $user['firstname'],
						'lastname' => $user['lastname'],
						'group_id' => $user['group_id'],
						'group_name' => $group['name'],
						'group_type' => $group['type'],
						'group_admin' => $group['admin'],
						'group_level' => $group['level'],
						'group_permission' => $group['permission'],
					);

					// if user chose remember login
					if ($params['remember'])
						$this->remember_user($member);

					return $member;
				}
			}
		}
		return false;
	}

	// auto log in if user chose remember me
	public function login_remembered_user()
	{
		//check for valid data
		$identity = $this->input->cookie('identity');
		$remember_code = $this->input->cookie('remember_code');
		if ( ! $identity || ! $remember_code)
			return false;

		// get user's data
		$where = array(
			'id' => $identity,
			'remember_code' => $remember_code,
			'status' => 1,
		);
		$user = $this->account_m->get_entry_auth($where);

		//if the user was found, sign them in
		if (!empty($user))
		{
			// update last login time
			$this->account_m->update_last_login(array('id' => $user['id'], 'ip_address' => $this->input->ip_address()));

			// get user's group
			$group = $this->group_m->get_entry_auth(array('id' => $user['group_id'], 'status' => 1));

			// return false if can not get group
			if (!empty($group))
			{
				$member = array(
					'id' => $user['id'],
					'email' => $user['email'],
					'salt' => $user['salt'],
					'firstname' => $user['firstname'],
					'lastname' => $user['lastname'],
					'group_id' => $user['group_id'],
					'group_name' => $group['name'],
					'group_type' => $group['type'],
					'group_admin' => $group['admin'],
					'group_level' => $group['level'],
					'group_permission' => $group['permission'],
				);

				return $member;
			}
		}
		return false;
	}

	// check log in
	public function check_login($params = array(), $options = array())
	{
		// get user's data
		$where = array(
			'email' => $this->db->escape_str($params['email']),
			'salt' => $this->db->escape_str($params['salt']),
		);
		$user = $this->account_m->get_entry_auth($where);

		if (!empty($user))
		{
			// get user's group
			$group = $this->group_m->get_entry_auth(array('id' => $user['group_id'], 'status' => 1));

			// return false if can not get group
			if (!empty($group))
			{
				$member = array(
					'id' => $user['id'],
					'email' => $user['email'],
					'salt' => $user['salt'],
					'firstname' => $user['firstname'],
					'lastname' => $user['lastname'],
					'group_id' => $user['group_id'],
					'group_name' => $group['name'],
					'group_type' => $group['type'],
					'group_admin' => $group['admin'],
					'group_level' => $group['level'],
					'group_permission' => $group['permission'],
				);

				return $member;
			}
		}
		return false;
	}

	// init cookie for remember me
	public function remember_user($member = array())
	{
		if (empty($member))
			return false;

		// remember_code: user's salt.email.ip.random salt
		$remember_code = sha1($this->input->ip_address().$member['email'].$member['password'].$this->account_m->salt());

		// set cookie if updated remember_code successfully
		if ($this->account_m->remember_user(array('id' => $member['id'], 'remember_code' => $remember_code)))
		{
			// if no expiration -> 2 years
			$expire = (int) $this->config->item('remember_expire');
			$expire = ($expire) ? $expire : (60*60*24*365*2);

			$this->input->set_cookie(array(
			    'name'   => 'identity',
			    'value'  => $member['id'],
			    'expire' => $expire,
			));

			$this->input->set_cookie(array(
			    'name'   => 'remember_code',
			    'value'  => $remember_code,
			    'expire' => $expire,
			));

			return true;
		}
		return false;
	}
}