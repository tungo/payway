<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bnvn_paygate_model extends MY_Model {

  public $client;
  public $webservice;
  public $url;
  public $trans_id;
  public $status;
  public $error;

  public function __construct()
  {
    parent::__construct();

    $this->config->load('webservice');
    $this->load->helper('crypt');
    $this->load->library('Nusoap');

    $this->webservice = $this->config->item('ws_bnvn_gate');

    if (!$this->client)
    {
      $this->client = new nusoap_client($this->webservice['url'], true);
      $this->client->response_timeout = 120;
    }
    $this->table = 'log_bnvn_paygate';
  }

  public function send_order($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'Merchant_trans_id' => $data['transaction']['id'],
      'Merchant_code' => $this->webservice['merchant_code'],
      'Country_code' => $this->webservice['country_code'],
      'Good_code' => $data['transaction']['good_code'],
      'Net_cost' => $data['transaction']['net_cost'],
      'Xml_description' => '',
      'Ship_fee' => '0',
      'Tax' => '0',
      'Url_success' => $data['transaction']['url_success'],
      'Url_fail' => $data['transaction']['url_fail'],
      'Trans_key' => '',
      'selected_bank' => $data['transaction']['selected_bank'],
      'service_code' => '720',
    );
    $input['Trans_key'] = md5(
      $input['Merchant_trans_id'].
      $input['Merchant_code'].
      $input['Good_code'].
      $input['Net_cost'].
      $input['Ship_fee'].
      $input['Tax'].
      $this->webservice['trans_key']
    );
    $result = $this->client->call('Send_GoodInfo_Ext2', $input);

    // log
    $dt = array(
      'type' => 'Send_GoodInfo_Ext2',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['return']);

    $url = substr($return[2], 0, $return[1]);
    $trans_key = substr($return[2], $return[1]);
    if ($trans_key == md5($return[0].$return[1].$url.$this->webservice['trans_key']))
    {
      if ($return[0] == '010')
      {
        $this->url = $url;
        $this->trans_id = explode('Trans_id=', $this->url);
        $this->trans_id = end($this->trans_id);
        $this->error['code'] = '0';
        return true;
      }
      elseif ($return[0] == '011')
      {
        $this->error['code'] = $return[1];
        return false;
      }
    }
    $this->error['code'] = '-1';
    return false;
  }

  public function check_status($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'Merchant_trans_id' => $data['transaction']['id'],
      'Trans_id' => $data['transaction']['payment']['trans_id'],
      'Merchant_code' => $this->webservice['merchant_code'],
    );
    $input['Trans_key'] = md5(implode('', $input).$this->webservice['trans_key']);

    $result = $this->client->call('QuerryBillStatus', $input);

    // log
    $dt = array(
      'type' => 'QuerryBillStatus',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['return']);

    if ($return[2] == md5($return[0].$return[1].$this->webservice['trans_key']))
    {
      if ($return[0] == '00')
      {
        $this->error['code'] = $return[1];
        return true;
      }
      elseif ($return[0] == '011')
      {
        $this->error['code'] = $return[1];
        return false;
      }
    }
    $this->error['code'] = '-1';
    return false;
  }

  public function confirm_transaction($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'Merchant_trans_id' => $data['transaction']['id'],
      'Trans_id' => $data['transaction']['payment']['trans_id'],
      'Merchant_code' => $this->webservice['merchant_code'],
      'Trans_result' => $data['transaction']['result'],
    );
    $input['Trans_key'] = md5(implode('', $input).$this->webservice['trans_key']);

    $result = $this->client->call('ConfirmTransactionResult', $input);

    // log
    $dt = array(
      'type' => 'ConfirmTransactionResult',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['return']);

    if ($return[2] == md5($return[0].$return[1].$this->webservice['trans_key']))
    {
      if ($return[0] == '00')
      {
        $this->error['code'] = $return[1];
        return true;
      }
      elseif ($return[0] == '011')
      {
        $this->error['code'] = $return[1];
        return false;
      }
    }
    $this->error['code'] = '-1';
    return false;
  }
}