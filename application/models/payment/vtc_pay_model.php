<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vtc_pay_model extends MY_Model {

  public $client;
  public $webservice;
  public $result;

  private $_rsa_pv_path;
  private $_rsa_vtc_pl_path;

  public function __construct()
  {
    parent::__construct();

    $this->config->load('webservice');
    $this->load->helper('crypt');
    $this->load->library('Nusoap');

    $this->webservice = $this->config->item('ws_vtc_pay');

    if (!$this->client)
    {
      $this->client = new nusoap_client($this->webservice['url'], true);
      $this->client->response_timeout = 120;
    }
    $this->table = 'log_vtc_pay';
    $this->_rsa_pv_path = $this->webservice['rsa_private_path'];
    $this->_rsa_vtc_pl_path = $this->webservice['rsa_vtc_public_path'];
  }

  public function topup_telco($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'TopupTelco',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['transaction']['vtc_pay']['service_code'],
      'account'      => $data['transaction']['vtc_pay']['account'],
      'amount'       => $data['transaction']['vtc_pay']['amount'],
      'partner_code' => $this->webservice['partner_code'],
      'date'         => date('YmdHis'),
      'trans_id'     => $data['transaction']['id'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account>'.$params['account'].'</Account>
  <Amount>'.$params['amount'].'</Amount>
  <Quantity>1</Quantity>
  <TransDate>'.$params['date'].'</TransDate>
  <OrgTransID>'.$params['trans_id'].'</OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'TopupTelco',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);
    $data_sign = array_pop($return);

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('-', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);

      if ($return[0] == '1')
      {
        $this->result['code'] = $return[0];
        return true;
      }
      else
      {
        $this->result['code'] = $return[0];
        return false;
      }
    }
    openssl_free_key($public_key);
    $this->result['code'] = '69';
    return false;
  }

  public function buy_card($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'BuyCard',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['transaction']['vtc_pay']['service_code'],
      'amount'       => $data['transaction']['vtc_pay']['amount'],
      'quantity'     => $data['transaction']['vtc_pay']['quantity'],
      'partner_code' => $this->webservice['partner_code'],
      'date'         => date('YmdHis'),
      'trans_id'     => $data['transaction']['id'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account></Account>
  <Amount>'.$params['amount'].'</Amount>
  <Quantity>'.$params['quantity'].'</Quantity>
  <TransDate>'.$params['date'].'</TransDate>
  <OrgTransID>'.$params['trans_id'].'</OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'BuyCard',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);
    $data_sign = array_pop($return);

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('-', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);

      if ($return[0] == '1')
      {
        $this->result['code'] = $return[0];
        $this->result['vtc_trans_id'] = $return[2];
        return true;
      }
      else
      {
        $this->result['code'] = $return[0];
        return false;
      }
    }
    openssl_free_key($public_key);
    $this->result['code'] = '69';
    return false;
  }

  public function get_card($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'GetCard',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['transaction']['vtc_pay']['service_code'],
      'amount'       => $data['transaction']['vtc_pay']['amount'],
      'partner_code' => $this->webservice['partner_code'],
      'trans_id'     => $data['transaction']['vtc_pay']['trans_id'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account></Account>
  <Amount>'.$params['amount'].'</Amount>
  <TransDate></TransDate>
  <OrgTransID>'.$params['trans_id'].'</OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'GetCard',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = decrypt_triple_des($result['RequestTransactionResult'], $this->webservice['triple_des_key']);
    $return = explode('|', $return);
    $size = count($return);
    if ($size > 2)
    {
      if ($return[0] == '1')
      {
        $this->result['code'] = $return[0];

        $j = 0;
        for ($i = 2; $i < $size; $i++)
        {
          $card = explode(':', $return[$i]);
          $this->result['cards'][$j]['code'] = $card[0];
          $this->result['cards'][$j]['serial'] = $card[1];
          $this->result['cards'][$j]['date'] = $card[2];
          $j++;
        }
        $this->result['code'] = $return[0];
        return true;
      }
      else
      {
        $this->result['code'] = $return[0];
        return false;
      }
    }
    $this->result['code'] = '69';
    return false;
  }

  public function get_quantiy_card($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'GetQuantityCard',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['vtc_card']['service_code'],
      'trans_id'     => date('YmdHis'),
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account></Account>
  <Amount>0</Amount>
  <TransDate></TransDate>
  <OrgTransID>'.$params['trans_id'].'</OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // kq của vtc bị lỗi ký tự
    $result['RequestTransactionResult'] = remove_non_utf8_char($result['RequestTransactionResult']);

    // log
    $dt = array(
      'type' => 'GetQuantityCard',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);
    $data_sign = array_pop($return);

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('-', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);
      $quantities = json_decode($return[0], true);
      $this->result['quantities'] = $quantities;
      $this->result['quantity'] = 0;

      foreach ((array)$quantities as $quantity)
      {
        if (floatval($quantity['Value']) == floatval($data['vtc_card']['value']))
        {
          if (intval($quantity['SLTonCuoiKi']) > intval($data['vtc_card']['quantity']) + 3) // còn 10 cho chắc ăn
          {
            $this->result['quantity'] = intval($quantity['SLTonCuoiKi']);
          }
        }
      }

      if ($this->result['quantity'] > 0)
      {
        $this->result['code'] = '1';
        return true;
      }
      else
      {
        $this->result['code'] = '0';
        return false;
      }

    }
    openssl_free_key($public_key);
    $this->result['code'] = '69';
    return false;
  }

  public function topup_partner($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'TopupPartner',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['transaction']['vtc_pay']['service_code'],
      'account'      => $data['transaction']['vtc_pay']['account'],
      'amount'       => $data['transaction']['vtc_pay']['amount'],
      'partner_code' => $this->webservice['partner_code'],
      'date'         => date('YmdHis'),
      'trans_id'     => $data['transaction']['id'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account>'.$params['account'].'</Account>
  <Amount>'.$params['amount'].'</Amount>
  <Quantity>1</Quantity>
  <Description></Description>
  <TransDate>'.$params['date'].'</TransDate>
  <OrgTransID>'.$params['trans_id'].'</OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'TopupPartner',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);
    $data_sign = array_pop($return);

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('-', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);

      if ($return[0] == '1')
      {
        $this->result['code'] = $return[0];
        return true;
      }
      else
      {
        $this->result['code'] = $return[0];
        return false;
      }
    }
    openssl_free_key($public_key);
    $this->result['code'] = '69';
    return false;
  }

  public function check_account_exists($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'CheckAccount',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      'service_code' => $data['vtc_acc']['service_code'],
      'account'      => $data['vtc_acc']['account'],
      'partner_code' => $this->webservice['partner_code'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode>'.$params['service_code'].'</ServiceCode>
  <Account>'.$params['account'].'</Account>
  <Amount></Amount>
  <TransDate></TransDate>
  <OrgTransID></OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'CheckAccountExits',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);

    // truong hop cho truyen hinh ky thuat so vtc
    if ($data['vtc_acc']['service_code'] == 'VTC0193')
    {
      $data_sign = $return[1];
      unset($return[1]);
    }
    else
    {
      $data_sign = array_pop($return);
    }

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);

      if ($return[0] == '1')
      {
        $this->result['code'] = $return[0];
        return true;
      }
      else
      {
        $this->result['code'] = $return[0];
        return false;
      }
    }
    openssl_free_key($public_key);
    $this->result['code'] = '69';
    return false;
  }

  public function get_balance($data = array(), $options = array())
  {
    $input = array(
      'partnerCode' => $this->webservice['partner_code'],
      'commandType' => 'GetBalance',
      'version'     => '1.0',
      'requesData'  => '',
    );

    $params = array(
      // 'service_code' => '',
      // 'account'      => '',
      // 'amount'       => '',
      // 'date'         => '',
      // 'trans_id'     => '',
      'partner_code' => $this->webservice['partner_code'],
    );

    $private_key = openssl_pkey_get_private(file_get_contents($this->_rsa_pv_path));
    $params['data_sign'] = sign_data(implode('-', $params), $private_key);
    openssl_free_key($private_key);

    $input['requesData'] = '<?xml version="1.0" encoding="utf-8" ?>
<RequestData>
  <ServiceCode></ServiceCode>
  <Account></Account>
  <Amount></Amount>
  <Quantity>1</Quantity>
  <TransDate></TransDate>
  <OrgTransID></OrgTransID>
  <DataSign>'.$params['data_sign'].'</DataSign>
</RequestData>';

    $result = $this->client->call('RequestTransaction', $input);

    // log
    $dt = array(
      'type' => 'GetBalance',
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['RequestTransactionResult']);
    $data_sign = array_pop($return);

    $public_key = openssl_pkey_get_public(file_get_contents($this->_rsa_vtc_pl_path));

    if (verify_data(implode('-', $return), $data_sign, $public_key))
    {
      openssl_free_key($public_key);

      $this->result['code'] = $return[0];
      return true;
    }
    openssl_free_key($public_key);
    $this->result['code'] = -69;
    return false;
  }


}