<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vnpay_paygate_model extends MY_Model {

  public $client;
  public $webservice;
  public $url;
  public $trans_id;
  public $status;
  public $error;

  public function __construct()
  {
    parent::__construct();

    $this->config->load('webservice');
    $this->load->helper('crypt');
    $this->load->library('Nusoap');

    $this->webservice = $this->config->item('ws_vnpay_gate');

    if (!$this->client)
    {
      $this->client = new nusoap_client($this->webservice['url'], true);
      $this->client->response_timeout = 120;
    }
    $this->table = 'log_vnpay_paygate';
  }

  public function init_trans($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'terminalCode'  => $this->webservice['terminal_code'],
      'orderId'       => $data['transaction']['id'],
      'amount'        => $data['transaction']['amount'],
      'currCode'      => 'VND',
      'paymentMethod' => $data['transaction']['bank'],
      'localDate'     => date('YmdHis'),
      'orderDesc'     => $data['transaction']['order'],
      'clientIp'      => $this->input->ip_address(),
    );
    $input['signature'] = strtoupper(md5(implode('|', $input).'|'.$this->webservice['secret_key']));

    $params = array(
      'cmdCode'  => 'INITTRANS',
      'strData' => implode('|', $input).'|',
    );
    $result = $this->client->call('Execute', $params);

    // log
    $dt = array(
      'type' => 'INITTRANS',
      'time' => date('YmdHis'),
      'data' => serialize(array('input' => $input, 'params' => $params)),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['ExecuteResult']);

    if (count($return) < 4)
    {
      $this->error['code'] = '-1';
      $this->error['desc'] = 'Error Server';
      return false;
    }

    // rspCode|message|url|signature
    $url = $return[2];
    // $url = https://www.vnpayment.vn/paymentsite/transaction/FromMerchant/?OrderID=895d5231c7014c45a2b74c3a9f5ead76&paymethod=

    $signature = $return[3];
    $sign = strtoupper(md5($return[0].'|'.$return[1].'|'.$return[2].'|'.$this->webservice['secret_key']));
    if ($sign == $signature)
    {
      if ($return[0] == '00')
      {
        $this->url = $url;
        if (preg_match('/OrderID=([a-z 0-9]*)&?/', $url, $trans_id))
          $this->trans_id = $trans_id[1];
        $this->error['code'] = $return[0];
        $this->error['desc'] = $return[1];
        return true;
      }
      else
      {
        $this->error['code'] = $return[0];
        $this->error['desc'] = $return[1];
        return false;
      }
    }
    $this->error['code'] = '-1';
    $this->error['desc'] = 'Error Sign';
    return false;
  }

  public function check_trans($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'terminalCode'  => $this->webservice['terminal_code'],
      'orderId'       => $data['transaction']['id'],
      'vnpTranId'     => '',
      'dateRequest'   => date('YmdHis'),
    );
    $input['signature'] = strtoupper(md5(implode('|', $input).'|'.$this->webservice['secret_key']));

    $params = array(
      'cmdCode'  => 'CHECKTRANS',
      'strData' => implode('|', $input).'|',
    );
    $result = $this->client->call('Execute', $params);

    // log
    $dt = array(
      'type' => 'CHECKTRANS',
      'time' => date('YmdHis'),
      'data' => serialize(array('input' => $input, 'params' => $params)),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['ExecuteResult']);

    if (count($return) < 4)
    {
      $this->error['code'] = '-1';
      $this->error['desc'] = 'Error Server';
      return false;
    }

    // rspCode|message|paymentMethod|signature
    $signature = $return[3];
    $sign = strtoupper(md5($return[0].'|'.$return[1].'|'.$return[2].'|'.$this->webservice['secret_key']));
    if ($signature == $sign)
    {
      $this->error['code'] = $return[0];
      $this->error['desc'] = $return[1];
      if ($return[0] == '00')
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    $this->error['code'] = '-1';
    $this->error['desc'] = 'Error Sign';
    return false;
  }

  public function delivered($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $input = array(
      'terminalCode' => $this->webservice['terminal_code'],
      'orderId'      => $data['transaction']['id'],
      'amount'       => $data['transaction']['amount'],
      'localDate'    => date('YmdHis'),
      'orderDesc'    => '',
    );
    $input['signature'] = strtoupper(md5(implode('|', $input).'|'.$this->webservice['secret_key']));

    $params = array(
      'cmdCode'  => 'DELIVERED',
      'strData' => implode('|', $input).'|',
    );
    $result = $this->client->call('Execute', $params);

    // log
    $dt = array(
      'type' => 'DELIVERED',
      'time' => date('YmdHis'),
      'data' => serialize(array('input' => $input, 'params' => $params)),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);

    $return = explode('|', $result['ExecuteResult']);

    if (count($return) < 2)
    {
      $this->error['code'] = '-1';
      $this->error['desc'] = 'Error Server';
      return false;
    }

    // rspCode|message|
    $this->error['code'] = $return[0];
    $this->error['desc'] = $return[1];
    if ($return[0] == '00')
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  // public function confirm_trans($data = array(), $options = array())
  // {
  //   if (empty($data['transaction']))
  //     return false;

  //   $input = array(
  //     'rspCode'         => $data['transaction']['result'],
  //     'terminalCode'    => $this->webservice['terminal_code'],
  //     'orderId'         => $data['transaction']['id'],
  //     'amount'          => $data['transaction']['amount'],
  //     'currCode'        => 'VND',
  //     'vnpTranid'       => '',
  //     'paymentMethod'   => $data['transaction']['bank'],
  //     'payDate'         => date('YmdHis'),
  //     'additional_info' => $data['transaction']['order'],
  //   );
  //   $input['signature'] = strtoupper(md5(implode('|', $input).'|'.$this->webservice['secret_key']));

  //   $params = array(
  //     'cmdCode'  => 'CONFIRMTRANS',
  //     'strData' => implode('|', $input).'|',
  //   );
  //   $result = $this->client->call('Execute', $params);

  //   // log
  //   $dt = array(
  //     'type' => 'CONFIRMTRANS',
  //     'time' => date('YmdHis'),
  //     'data' => serialize($input),
  //     'result' => serialize($result),
  //   );
  //   $this->db->insert($this->table, $dt);

  //   $return = explode('|', $result['return']);

  //   // rspCode|message|terminalCode|orderId|localdate|signature

  //   $signature = array_pop($return);
  //   $sign = strtoupper(md5(implode('|', $return).'|'.$this->webservice['secret_key']));
  //   if ($signature == $sign)
  //   {
  //     $this->error['code'] = $return[0];
  //     $this->error['desc'] = $return[1];
  //     if ($return[0] == '00')
  //     {
  //       return true;
  //     }
  //     else
  //     {
  //       return false;
  //     }
  //   }
  //   $this->error['code'] = '-1';
  //   $this->error['desc'] = 'Error Sign';
  //   return false;
  // }

  public function check_sign($data = array(), $signature = '')
  {
    if (!is_array($data) || empty($data) || !$signature)
    {
      return false;
    }

    $signature = strtoupper($signature);
    $sign = strtoupper(md5(implode('|', $data).'|'.$this->webservice['secret_key']));
    if ($signature == $sign)
      return true;
    return false;
  }

  public function init_sign($data = array())
  {
    if (!is_array($data) || empty($data))
    {
      return false;
    }

    return strtoupper(md5(implode('|', $data).'|'.$this->webservice['secret_key']));
  }

  public function log_ws($type = '', $input = array(), $result = array())
  {
    $dt = array(
      'type' => $type,
      'time' => date('YmdHis'),
      'data' => serialize($input),
      'result' => serialize($result),
    );
    $this->db->insert($this->table, $dt);
  }
}