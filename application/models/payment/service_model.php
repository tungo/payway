<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

    $this->data['service_code'] = $this->config->item('vtc_pay_service_code');
    $this->data['bank_code'] = $this->config->item('vnpay_paygate_bank_code');
	}

  public function didongtratruoc()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['dauso'] = array(''=>'');
    $data['menhgia'] = array(''=>'');
    $data['thanhtien'] = array(''=>'');
    foreach ($this->data['service_code'][DI_DONG_TRA_TRUOC] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      foreach ($dichvu['dauso'] as $dauso)
        $data['dauso'][$kdv][$dauso] = $dauso;
      foreach ($dichvu['menhgia'] as $menhgia)
      {
        $data['menhgia'][$kdv][$menhgia] = tn_money_format($menhgia);
        $thanhtien = $menhgia * $dichvu['giamgia'];
        $data['thanhtien'][$kdv][$menhgia] = tn_money_format($thanhtien);
      }
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }

  public function didongtrasau()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['dauso'] = array(''=>'');
    $data['giamgia'] = array();
    foreach ($this->data['service_code'][DI_DONG_TRA_SAU] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      $data['giamgia'][$kdv] = $dichvu['giamgia'];
      foreach ($dichvu['dauso'] as $dauso)
        $data['dauso'][$kdv][$dauso] = $dauso;
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }

  public function muathedienthoai()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['menhgia'] = array(''=>'');
    $data['thanhtien'] = array(''=>'');
    foreach ($this->data['service_code'][MUA_THE_DIEN_THOAI] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      foreach ($dichvu['menhgia'] as $menhgia)
      {
        $data['menhgia'][$kdv][$menhgia] = tn_money_format($menhgia);
        $thanhtien = $menhgia * $dichvu['giamgia'];
        $data['thanhtien'][$kdv][$menhgia] = $thanhtien;
      }
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }

  public function muathegame()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['menhgia'] = array(''=>'');
    $data['thanhtien'] = array(''=>'');
    foreach ($this->data['service_code'][MUA_THE_GAME] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      foreach ($dichvu['menhgia'] as $menhgia)
      {
        $data['menhgia'][$kdv][$menhgia] = tn_money_format($menhgia);
        $thanhtien = $menhgia * $dichvu['giamgia'];
        $data['thanhtien'][$kdv][$menhgia] = $thanhtien;
      }
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }

  public function naptiengame()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['menhgia'] = array(''=>'');
    $data['thanhtien'] = array(''=>'');
    foreach ($this->data['service_code'][NAP_TIEN_GAME] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      foreach ($dichvu['menhgia'] as $menhgia)
      {
        $data['menhgia'][$kdv][$menhgia] = tn_money_format($menhgia);
        $thanhtien = $menhgia * $dichvu['giamgia'];
        $data['thanhtien'][$kdv][$menhgia] = tn_money_format($thanhtien);
      }
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }

  public function naptientaikhoan()
  {
    $data['madichvu'] = array();
    $data['nhacungcap'] = array();
    $data['menhgia'] = array(''=>'');
    $data['thanhtien'] = array(''=>'');
    foreach ($this->data['service_code'][NAP_TIEN_TAI_KHOAN] as $kdv => $dichvu)
    {
      $data['madichvu'][] = $kdv;
      $data['nhacungcap'][$kdv] = $dichvu['nhacungcap'];
      foreach ($dichvu['menhgia'] as $menhgia)
      {
        $data['menhgia'][$kdv][$menhgia] = tn_money_format($menhgia);
        $thanhtien = $menhgia * $dichvu['giamgia'];
        $data['thanhtien'][$kdv][$menhgia] = tn_money_format($thanhtien);
      }
      $data['disabled'][$kdv] = (!empty($dichvu['disabled'])) ? true : false;
    }
    return $data;
  }
}