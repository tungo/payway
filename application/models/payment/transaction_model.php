<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'transaction';
	}

  public function get_entries_admin($params = array(), $options = array())
  {
    $this->db->select('t.id, t.type, t.amount, t.bank, t.status, t.created_time, t.completed_time, t.data, t.note, ua.email');
    $this->db->from($this->table.' AS t');
    $this->db->join('user_account AS ua', 't.member_id = ua.id', 'left');

    if (! empty($params['filter']['keyword']))
      $this->db->where("(t.id LIKE '%{$params['filter']['keyword']}%' OR t.amount LIKE '%{$params['filter']['keyword']}%' OR ua.email LIKE '%{$params['filter']['keyword']}%')");
    if (! empty($params['filter']['type']))
      $this->db->where('t.type', $params['filter']['type']);
    if (! empty($params['filter']['status']))
      $this->db->where('t.status', $params['filter']['status']);
    if (! empty($params['filter']['bank']))
      $this->db->where('t.bank', $params['filter']['bank']);
    if (! empty($params['filter']['from_date']))
    {
      $date = date_create_from_format('Y/m/d', $params['filter']['from_date']);
      if ($date)
        $this->db->where('created_time >=', $date->format('Y-m-d').' 00:00:00');
    }
    if (! empty($params['filter']['to_date']))
    {
      $date = date_create_from_format('Y/m/d', $params['filter']['to_date']);
      if ($date)
        $this->db->where('created_time <=', $date->format('Y-m-d').' 23:59:59');
    }

    $this->db->order_by($params['filter']['sort'], $params['filter']['order']);
    $this->db->order_by('t.id', 'DESC');

    if (isset($params['pagination'])) {
      $this->db->limit($params['pagination']->per_page, $params['pagination']->offset);
    }

    return $this->db->get()->result_array();
  }

  public function count_entries_admin($params = array(), $options = array())
  {
    $this->db->select('t.id');
    $this->db->from($this->table.' AS t');
    $this->db->join('user_account AS ua', 't.member_id = ua.id', 'left');

    if (! empty($params['filter']['keyword']))
      $this->db->where("(t.id LIKE '%{$params['filter']['keyword']}%' OR t.amount LIKE '%{$params['filter']['keyword']}%' OR ua.email LIKE '%{$params['filter']['keyword']}%')");
    if (! empty($params['filter']['type']))
      $this->db->where('t.type', $params['filter']['type']);
    if (! empty($params['filter']['status']))
      $this->db->where('t.status', $params['filter']['status']);
    if (! empty($params['filter']['bank']))
      $this->db->where('t.bank', $params['filter']['bank']);
    if (! empty($params['filter']['from_date']))
    {
      $date = date_create_from_format('Y/m/d', $params['filter']['from_date']);
      if ($date)
        $this->db->where('created_time >=', $date->format('Y-m-d').' 00:00:00');
    }
    if (! empty($params['filter']['to_date']))
    {
      $date = date_create_from_format('Y/m/d', $params['filter']['to_date']);
      if ($date)
        $this->db->where('created_time <=', $date->format('Y-m-d').' 23:59:59');
    }

    return $this->db->get()->num_rows();
  }

  public function get_entry($params = array(), $options = array())
  {
    if (! isset($params['id']))
    {
      return FALSE;
    }

    $this->db->select('t.*, ua.email');
    $this->db->from($this->table.' AS t');
    $this->db->join('user_account AS ua', 't.member_id = ua.id', 'left');
    $this->db->where('t.id', $params['id']);
    return $this->db->get()->row_array();
  }

  public function add_entry($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $data['transaction']['amount'] = (int) $data['transaction']['amount'];
    $data['transaction']['status'] = GD_KHOI_TAO;
    $data['transaction']['created_time'] = date('Y-m-d H:i:s');

    if ($this->db->insert($this->table, $data['transaction']))
      return $this->db->insert_id();
    else
      return false;
  }

  public function update_entry($params = array(), $options = array())
  {
    $member = $this->session->userdata('member');

    $data = array(
      'note' => $params['note'],
    );
    $this->db->where('id', $params['id']);
    return $this->db->update($this->table, $data);
  }

  public function update_entry_payment($data = array(), $options = array())
  {
    if (empty($data['transaction']['payment']))
      return false;
    $dt = array('payment' => $data['transaction']['payment']);
    $this->db->where('id', $data['transaction']['id']);
    return $this->db->update($this->table, $dt);
  }

  public function update_entry_data($data = array(), $options = array())
  {
    if (empty($data['transaction']['data']))
      return false;
    $dt = array('data' => $data['transaction']['data']);
    $this->db->where('id', $data['transaction']['id']);
    return $this->db->update($this->table, $dt);
  }

  public function get_entry_payment($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $this->db->from($this->table);
    $this->db->where('id', $data['transaction']['id']);
    // $this->db->where('type', $data['transaction']['type']);

    return $this->db->get()->row_array();
  }

  public function update_status($data = array(), $options = array())
  {
    if (empty($data['transaction']))
      return false;

    $dt = array('status' => $data['transaction']['status']);
    if ($data['transaction']['status'] == GD_XAC_NHAN_TC || $data['transaction']['status'] == GD_HOAN_TIEN_TC)
    {
      $dt['completed_time'] = date('Y-m-d H:i:s');
    }

    $this->db->where('id', $data['transaction']['id']);
    if (!$this->db->update($this->table, $dt))
    {
      return false;
    }

    // log
    $dt = array(
      'transaction_id' => $data['transaction']['id'],
      'type' => 'update_status',
      'time' => date('Y-m-d H:i:s'),
      'data' => serialize($data['transaction']),
      'result' => $data['transaction']['status'],
    );
    $this->db->insert('transaction_log', $dt);
    return true;
  }

  public function get_entries_member($data = array(), $options = array())
  {
    $this->db->select('id, type, amount, bank, status, created_time');
    $this->db->from($this->table);
    $this->db->where('member_id', $data['member']['id']);
    $this->db->order_by('id', 'DESC');
    $this->db->limit(5);
    return $this->db->get()->result_array();
  }

  public function get_entries_history($data = array(), $options = array())
  {
    $this->db->select('id, type, amount, bank, status, created_time');
    $this->db->from($this->table.' AS t');
    $this->db->where('member_id', $data['member']['id']);

    if (! empty($data['filter']['keyword']))
      $this->db->like('id', $data['filter']['keyword']);
    if (! empty($data['filter']['type']))
      $this->db->where('type', $data['filter']['type']);
    if (! empty($data['filter']['status']))
    {
      if ($data['filter']['status'] == GD_MB_KHOI_TAO)
        $this->db->where('status', GD_KHOI_TAO);
      elseif ($data['filter']['status'] == GD_MB_HOAN_TAT)
        $this->db->where_in('status', array(GD_XU_LY_TC, GD_XAC_NHAN_TC, GD_XAC_NHAN_TB));
      elseif ($data['filter']['status'] == GD_MB_THAT_BAI)
        $this->db->where_in('status', array(GD_THANH_TOAN_TC, GD_THANH_TOAN_TB, GD_XU_LY_TB));
      elseif ($data['filter']['status'] == GD_MB_DA_HOAN_TIEN)
        $this->db->where('status', GD_HOAN_TIEN_TC);
      elseif ($data['filter']['status'] == GD_MB_HOAN_TIEN_TB)
        $this->db->where('status', GD_HOAN_TIEN_TB);
      elseif ($data['filter']['status'] == GD_MB_LOI)
        $this->db->where_in('status', array(GD_KO_XAC_DINH, GD_LOI));
    }
    if (! empty($data['filter']['from_date']))
    {
      $date = date_create_from_format('Y/m/d', $data['filter']['from_date']);
      if ($date)
        $this->db->where('created_time >=', $date->format('Y-m-d').' 00:00:00');
    }
    if (! empty($data['filter']['to_date']))
    {
      $date = date_create_from_format('Y/m/d', $data['filter']['to_date']);
      if ($date)
        $this->db->where('created_time <=', $date->format('Y-m-d').' 23:59:59');
    }

    $this->db->order_by($data['filter']['sort'], $data['filter']['order']);
    $this->db->order_by('id', 'DESC');
    $this->db->limit($data['data']['pagination']->per_page, $data['data']['pagination']->offset);

    return $this->db->get()->result_array();
  }

  public function count_entries_history($data = array(), $options = array())
  {
    $this->db->select('id');
    $this->db->from($this->table.' AS t');
    $this->db->where('member_id', $data['member']['id']);

    if (! empty($data['filter']['keyword']))
      $this->db->like('id', $data['filter']['keyword']);
    if (! empty($data['filter']['type']))
      $this->db->where('type', $data['filter']['type']);
    if (! empty($data['filter']['status']))
    {
      if ($data['filter']['status'] == GD_MB_KHOI_TAO)
        $this->db->where('status', GD_KHOI_TAO);
      elseif ($data['filter']['status'] == GD_MB_HOAN_TAT)
        $this->db->where_in('status', array(GD_XU_LY_TC, GD_XAC_NHAN_TC, GD_XAC_NHAN_TB));
      elseif ($data['filter']['status'] == GD_MB_THAT_BAI)
        $this->db->where_in('status', array(GD_THANH_TOAN_TC, GD_THANH_TOAN_TB, GD_XU_LY_TB));
      elseif ($data['filter']['status'] == GD_MB_DA_HOAN_TIEN)
        $this->db->where('status', GD_HOAN_TIEN_TC);
      elseif ($data['filter']['status'] == GD_MB_HOAN_TIEN_TB)
        $this->db->where('status', GD_HOAN_TIEN_TB);
      elseif ($data['filter']['status'] == GD_MB_LOI)
        $this->db->where_in('status', array(GD_KO_XAC_DINH, GD_LOI));
    }
    if (! empty($data['filter']['from_date']))
    {
      $date = date_create_from_format('Y/m/d', $data['filter']['from_date']);
      if ($date)
        $this->db->where('created_time >=', $date->format('Y-m-d').' 00:00:00');
    }
    if (! empty($data['filter']['to_date']))
    {
      $date = date_create_from_format('Y/m/d', $data['filter']['to_date']);
      if ($date)
        $this->db->where('created_time <=', $date->format('Y-m-d').' 23:59:59');
    }

    return $this->db->get()->num_rows();
  }

  public function get_entry_history_detail($data = array(), $options = array())
  {
    if (empty($data['id']))
      return false;

    $this->db->from($this->table.' AS t');
    $this->db->where('id', $data['id']);
    $this->db->where('member_id', $data['member']['id']);
    return $this->db->get()->row_array();
  }

  public function get_log_admin($params = array(), $options = array())
  {
    if (empty($params['id']))
      return false;

    $this->db->from('transaction_log AS tl');
    $this->db->where('transaction_id', $params['id']);
    $this->db->order_by('tl.id', 'DESC');
    return $this->db->get()->result_array();
  }

  public function export_excel($params = array(), $option = array())
  {
    require_once(FCPATH.'application/libraries/PHPExcel.php');
    require_once(FCPATH.'application/libraries/PHPExcel/Writer/Excel5.php');

    $php_excel = new PHPExcel();
    // set properties
    $props = $php_excel->getProperties();
    $props->setCreator($params['member']['email']);
    $props->setLastModifiedBy($params['member']['firstname'].$params['member']['lastname']);

    // set active sheet
    $php_excel->setActiveSheetIndex(0);

    // sheet properties
    $ws = $php_excel->getActiveSheet();
    $ws->setTitle('Sheet 1');
    $ws->getPageMargins()->setTop(1)->setRight(1)->setLeft(1)->setBottom(1);

    $default_styles = $ws->getDefaultStyle();
    $default_styles->getFont()->setName('Arial')->setSize(10);
    $default_styles->getAlignment()->setWrapText(true);

    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
      ),
    );

    for ($i = 0; $i <= 8; $i++ ) {
        $ws->getColumnDimensionByColumn($i)->setAutoSize(true);
    }

    $row = 1;
    $col = 0;

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Id');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Email');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Type');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Amount');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Bank');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Status');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Created');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Data');

    $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
    $ws->setCellValueByColumnAndRow($col++, $row, 'Note');


    $row++;

    foreach($params['data']['items'] as $item)
    {
      $col = 0;

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $item['id']);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $item['email']);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $params['select_type'][$item['type']]);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $item['amount']);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $params['bank_code'][$item['bank']]);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $params['select_status'][$item['status']]);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $item['created_time']);

$temp = '';
if(!empty($item['data']))
{
  $item['data'] = unserialize($item['data']);
  if ($item['type'] == DI_DONG_TRA_TRUOC)
  {
    $temp .= lang('nha-cung-cap').': '.$params['didongtratruoc']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$params['didongtratruoc']['menhgia'][$item['data']['nhacungcap']][$item['data']['menhgia']].";\n";
    $temp .= lang('so-di-dong').': '.$item['data']['dauso'].$item['data']['didong'].";";
  }
  elseif ($item['type'] == DI_DONG_TRA_SAU)
  {
    $temp .= lang('nha-cung-cap').': '.$params['didongtrasau']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$item['data']['menhgia'].";\n";
    $temp .= lang('so-di-dong').': '.$item['data']['dauso'].$item['data']['didong'].";";
  }
  elseif ($item['type'] == MUA_THE_DIEN_THOAI)
  {
    $temp .= lang('nha-cung-cap').': '.$params['muathedienthoai']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$params['muathedienthoai']['menhgia'][$item['data']['nhacungcap']][$item['data']['menhgia']].";\n";
    $temp .= lang('so-luong').': '.$item['data']['soluong'].";";
  }
  elseif ($item['type'] == MUA_THE_GAME)
  {
    $temp .= lang('nha-cung-cap').': '.$params['muathegame']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$params['muathegame']['menhgia'][$item['data']['nhacungcap']][$item['data']['menhgia']].";\n";
    $temp .= lang('so-luong').': '.$item['data']['soluong'].";";
  }
  elseif ($item['type'] == NAP_TIEN_GAME)
  {
    $temp .= lang('nha-cung-cap').': '.$params['naptiengame']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$params['naptiengame']['menhgia'][$item['data']['nhacungcap']][$item['data']['menhgia']].";\n";
    $temp .= lang('tai-khoan').': '.$item['data']['taikhoan'].";";
  }
  elseif ($item['type'] == NAP_TIEN_TAI_KHOAN)
  {
    $temp .= lang('nha-cung-cap').': '.$params['naptientaikhoan']['nhacungcap'][$item['data']['nhacungcap']].";\n";
    $temp .= lang('menh-gia').': '.$params['naptientaikhoan']['menhgia'][$item['data']['nhacungcap']][$item['data']['menhgia']].";\n";
    $temp .= lang('tai-khoan').': '.$item['data']['taikhoan'].";";
  }
}

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $temp);

      $ws->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
      $ws->setCellValueByColumnAndRow($col++, $row, $item['note']);

      $row++;
    }

    $php_excel->getActiveSheet()->getStyle('A1:I'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

    $filepath = FCPATH.'public/files/download/doisoat/';
    $filename = 'Payway-vn-Transaction-'.date('YmdHis').'.xls';
    $writer = new PHPExcel_Writer_Excel5($php_excel);
    $writer->save($filepath.$filename);

    return base_url().'public/files/download/doisoat/'.$filename;
  }

}