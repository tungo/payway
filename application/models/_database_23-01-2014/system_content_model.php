<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_content_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'system_content';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = FILESURL.$image[$this->table];
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('note', $params['filter']['keyword'])
				->or_like('description', $params['filter']['keyword']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		$this->db->select('id, name, note, status, position');

		return $this->db->get($this->table)->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('note', $params['filter']['keyword'])
				->or_like('description', $params['filter']['keyword']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'note'        => $params['note'],
			'image'       => $params['image'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'note'        => $params['note'],
			'image'       => $params['image'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		$this->db->select('id, description');
		$result = $this->db->get($this->table)->result();
		$return = array();
		foreach ($result as $row)
		{
			$return[$row->id] = $row->description;
		}
		return $return;
	}

	public function upload_file($params = array(), $options = array())
	{
		// ten input file
		$this->file = 'image';

		/// lay file dc upload
		$this->filename = $_FILES[$this->file]['name'];

		// neu co chon file de upload
		if ( ! empty($this->filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$this->filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload($this->file))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$this->filename = $file_data['file_name'];

			// tao thumbs
			$thumbs_config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'thumbs/',
				'width' => 150,
				'height' => 100
			);

			$this->load->library('image_lib', $thumbs_config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit' && isset($params['old_image']))
			{
				@unlink($this->image_path.$params['old_image']);
				@unlink($this->image_path.'thumbs/'.$params['old_image']);
			}
		}
		else
		{
			// dang edit thong tin
			if ($params['method'] == 'edit')
			{
				// chon xoa hinh cu
				$delete_image = (isset($params['delete_image'])) ? (int) $params['delete_image'] : '0';

				if ($delete_image && isset($params['old_image']))
				{
					@unlink($this->image_path.$params['old_image']);
					@unlink($this->image_path.'thumbs/'.$params['old_image']);
				}
				else
				{
					// lay ten hinh cu de luu database
					$this->filename = $params['old_image'];
				}
			}
		}

		return $this->filename;
	}
}