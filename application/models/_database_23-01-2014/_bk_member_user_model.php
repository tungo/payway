<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'member_user';
	}

	public function get_admin_entries($params = array(), $options = array())
	{
		$this->db->select('id, username, firstname, lastname');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);
		$member = $this->session->userdata('member');
		if ($member['group_id'] > 1)
		{
			$this->db->where(array('id <>' => $member['id'], 'group_id >' => $member['group_id']));
		}
		else
		{
			$this->db->where('id <>', $member['id']);
		}
		return $this->db->get($this->table)->result();
	}

	public function count_admin_entries($params = array(), $options = array())
	{
		$this->db->select('id');
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$salt = $this->salt();
		$this->data = array(
			'username'  => $params['username'],
			'salt'      => $salt,
			'password'  => $this->hash_password($params['password'], $salt),
			'firstname' => $params['firstname'],
			'lastname'  => $params['lastname'],
			'status'    => 1,
			'group_id'  => 3
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		if (isset($params['password']) && !empty($params['password']))
		{
			$salt = $this->salt();
			$this->data = array(
				'username'  => $params['username'],
				'salt'      => $salt,
				'password'  => $this->hash_password($params['password'], $salt),
				'firstname' => $params['firstname'],
				'lastname'  => $params['lastname']
			);
		}
		else
		{
			$this->data = array(
				'username'  => $params['username'],
				'firstname' => $params['firstname'],
				'lastname'  => $params['lastname']
			);
		}
		return $this->data;
	}

	public function update_last_login($params = array(), $options = array())
	{
		if ( ! empty($params))
		{
			return $this->db->where($params)->update($this->table, array('visited_time' => date('Y-m-d H:i:s')));
		}
		return FALSE;
	}

	public function change_password($params = array(), $options = array())
	{
		$salt = $this->salt();
		$data = array(
			'salt'      => $salt,
			'password'  => $this->hash_password($params['newpass'], $salt),
		);
		return $this->db->where('id', $params['id'])->update($this->table, $data);
	}

	// Hash password for sercurity
	public function hash_password($password = '', $salt = '')
	{
		if ($password == '')
		{
			return FALSE;
		}
		return sha1(md5($password.$salt));
	}

	// Init random salt
	public function salt($length = 10)
	{
		return substr(md5(uniqid(rand(), true)), 0, $length);
	}
}