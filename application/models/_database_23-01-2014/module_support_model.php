<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_support_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'module_support';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('account', $params['filter']['keyword']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		$this->db->select('id, name, account, type, status, position');

		return $this->db->get($this->table)->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('account', $params['filter']['keyword']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['type'] = (isset($params['type'])) ? (int) $params['type'] : 1;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = isset($params['position']) ? $params['position'] : 0;
		$this->data = array(
			'name'     => $params['name'],
			'account'  => $params['account'],
			'type'     => $params['type'],
			'status'   => $params['status'],
			'position' => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['type'] = (isset($params['type'])) ? (int) $params['type'] : 1;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = isset($params['position']) ? $params['position'] : 0;
		$this->data = array(
			'name'     => $params['name'],
			'account'  => $params['account'],
			'type'     => $params['type'],
			'status'   => $params['status'],
			'position' => $params['position']
		);
		return $this->data;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		$this->db->select('id, name, account, type');
		$this->db->where('status', 1);
		$this->db->order_by('type', 'ASC')->order_by('position', 'ASC')->order_by('id', 'DESC');
		return $this->db->get($this->table)->result();
	}
}