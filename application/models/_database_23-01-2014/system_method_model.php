<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_method_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'system_method';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		$this->db->select('id, name, status, position');

		return $this->db->get($this->table)->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function get_entries_site($params = array(), $options = array())
	{

	}

	public function delete_privilege_by_id($params = array(), $options = array())
	{

	}

	public function delete_entry($params = array(), $options = array())
	{
		$this->load->model('database/user_privilege_model');
		$this->user_privilege_model->delete_entries_by_where(array('method_id' => $params['id']));
		return $this->db->where('id', $params['id'])->delete($this->table);
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->load->model('database/user_privilege_model');
			$this->user_privilege_model->delete_entries_by_multi_method($params);

			$this->db->where_in('id', $cid);
			return $this->db->delete($this->table);
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}
}