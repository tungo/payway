<?php if ( ! defined('BASEPATH')) exit('No direct script abcess allowed');

class Blog_category_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'blog_category';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = base_url(FILESURL.$image[$this->table]).'/';

		$this->load->library('TN_Recursive');
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('bc.id, bcd.name, bc.status, bc.position, bc.parent_id, bcd.language_id, COUNT(ba.id) AS article_amount');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->join('blog_article AS ba', 'ba.category_id = bc.id', 'left');
		$this->db->where('bcd.language_id', $params['language_id']);
		$this->db->group_by('bc.id, bcd.language_id');
		$this->db->order_by('bc.position, bc.id');

		$result = $this->db->get()->result_array();
		$this->tn_recursive->initialize($result);
		return $this->tn_recursive->get(0);
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		return $this->db->get($this->table)->num_rows();
	}

	public function get_entries_select_admin($params = array(), $options = array())
	{
		$this->db->select('bc.id, bcd.name, bc.parent_id');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->where('bcd.language_id', $params['language_id']);
		$this->db->group_by('bc.id, bcd.language_id');
		$this->db->order_by('bc.position, bc.id');

		if ($options['method'] == 'category-edit')
			$this->db->where('bc.id != ', $params['id']);

		$result = $this->db->get()->result_array();
		$this->tn_recursive->initialize($result);
		$result = $this->tn_recursive->get(0);

		$root = array();
		// dung cho trang them sua san pham co chon danh muc
		if ($options['method'] == 'article-admin')
		{
			$root[] = array(
				'id'              => 0,
				'name'            => 'No category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}
		// dung cho trang quan ly san pham co che do loc theo danh muc
		elseif ($options['method'] == 'article-filter')
		{
			$root[] = array(
				'id'              => '',
				'name'            => 'All category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
			$root[] = array(
				'id'              => 0,
				'name'            => 'No category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}
		// dung cho trang them sua danh muc
		// method = category-add/category-edit
		else
		{
			$root[] = array(
				'id'              => 0,
				'name'            => 'Root category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}

		$result = array_merge($root, $result);

		return $result;
	}

	public function get_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->select('bc.*, bcd.*');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->where('bc.id', $params['id']);
		$result = $this->db->get()->result_array();

		$result1 = $result[0];
		$result1['description'] = array();
		foreach ($result as $row)
		{
			$result1['description'][$row['language_id']]['name'] = $row['name'];
			$result1['description'][$row['language_id']]['link'] = $row['link'];
			$result1['description'][$row['language_id']]['description'] = $row['description'];
		}
		return $result1;
	}

	public function add_entry($params = array(), $options = array())
	{
		$params['parent_id'] = (isset($params['parent_id'])) ? (int) $params['parent_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'parent_id'   => $params['parent_id'],
			'seo_name'    => $params['seo_name'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		if ($this->db->insert($this->table, $data))
		{
			$category_id = $this->db->insert_id();
			$languages = $this->session->userdata('languages');
			foreach ($languages as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$link = ($params['description'][$language['id']]['link'])
					? $params['description'][$language['id']]['link']
					: $params['description'][$params['language_id']]['link'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'category_id' => $category_id,
					'language_id' => $language['id'],
					'name'        => $name,
					'link'        => $link,
					'description' => $description,
				);
				$this->db->insert('blog_category_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function update_entry($params = array(), $options = array())
	{
		$params['parent_id'] = (isset($params['parent_id'])) ? (int) $params['parent_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'parent_id'   => $params['parent_id'],
			'seo_name'    => $params['seo_name'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		$this->db->where('id', $params['id']);
		if ($this->db->update($this->table, $data))
		{
			$languages = $this->session->userdata('languages');
			foreach ($languages as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$link = ($params['description'][$language['id']]['link'])
					? $params['description'][$language['id']]['link']
					: $params['description'][$params['language_id']]['link'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'name'        => $name,
					'link'        => $link,
					'description' => $description,
				);
				$this->db->where('category_id', $params['id']);
				$this->db->where('language_id', $language['id']);
				$this->db->update('blog_category_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function delete_entry($params = array(), $options = array())
	{
		$ids = $this->get_children_by_id($params, $options);
		$this->db->where_in('id', $ids);
		if ($this->db->delete($this->table))
		{
			$this->db->where_in('category_id', $ids);
			$this->db->delete('blog_category_description');
			return TRUE;
		}
		return FALSE;
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$ids = array();
			foreach ($cid as $id)
			{
				$params['id'] = $id;
				$array = $this->get_children_by_id($params, $options);
				$ids = array_merge($ids, $array);
			}
			$this->db->where_in('id', $ids);
			if ($this->db->delete($this->table))
			{
				$this->db->where_in('category_id', $ids);
				$this->db->delete('blog_category_description');
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	public function get_children_by_id($params = array(), $options = array())
	{
		$this->db->select('id, parent_id');
		$result = $this->db->get($this->table)->result_array();

		$this->tn_recursive->initialize($result);
		$result = $this->tn_recursive->get($params['id']);

		$ids = array($params['id']);
		if (count($result))
		{
			foreach ($result as $val)
			{
				$ids[] = $val['id'];
			}
		}
		return $ids;
	}

	public function copy_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$data = array();
			foreach ($cid as $id)
			{
				if ($this->check_entry(array('id' => $id)))
				{
					$row = (array) $this->get_entry(array('id' => $id));
					unset($row['id']);
					$this->add_entry($row);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		$this->db->select('bc.*, bcd.name, bcd.link, bcd.description');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->where('bcd.language_id', $params['language_id']);
		$this->db->where('bc.status', '1');
		$this->db->group_by('bc.id, bcd.language_id');
		$this->db->order_by('bc.position, bc.id');

		$result = $this->db->get()->result_array();

		$this->tn_recursive->initialize($result);
		$id = isset($params['category_id']) ? (int) $params['category_id'] : 0;
		$return = isset($this->tn_recursive->tree[$id]) ? $this->tn_recursive->tree[$id] : array();
		return $return;
	}

	public function get_entries_site_menu($params = array(), $options = array())
	{
		$this->db->select('bc.id, bc.seo_name, bcd.name, bcd.link, bc.parent_id');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->where('bcd.language_id', $params['language_id']);
		$this->db->where('bc.status', '1');
		$this->db->group_by('bc.id, bcd.language_id');
		$this->db->order_by('bc.position, bc.id');

		$result = $this->db->get()->result_array();

		$this->tn_recursive->initialize($result);
		return $this->tn_recursive->tree[0];

		// $tree = $this->tn_recursive->get(0);
		// debuge($tree);
		// $return = array();
		// $i = -1;
		// foreach ($tree as $row)
		// {
		// 	if (! $row['parent_id'])
		// 	{
		// 		$return[($i+1)] = $row;
		// 		$return[($i+1)]['children'] = array();
		// 		$i ++;
		// 	}
		// 	elseif ($row['parent_id'] == $return[$i]['id'])
		// 	{
		// 		$return[$i]['children'][] = $row;
		// 	}
		// }
		// return $return;
	}

	public function get_entries_site_list($params = array(), $options = array())
	{
		$this->db->select('bc.id, bc.seo_name, bcd.name, bcd.link, bcd.description, bc.parent_id');
		$this->db->from($this->table.' AS bc');
		$this->db->join('blog_category_description AS bcd', 'bcd.category_id = bc.id', 'left');
		$this->db->where('bcd.language_id', $params['language_id']);
		$this->db->where('bc.status', '1');
		$this->db->group_by('bc.id, bcd.language_id');
		$this->db->order_by('bc.position, bc.id');

		$result = $this->db->get()->result_array();

		$return = array();
		foreach ($result as $row)
		{
			$return[$row['id']]['name'] = $row['name'];
			$return[$row['id']]['seo_name'] = $row['seo_name'];
			$return[$row['id']]['link'] = $row['link'];
			$return[$row['id']]['description'] = $row['description'];
		}
		return $return;
	}

	public function get_id_from_seo($params = array(), $options = array())
	{
		$this->db->select('bc.id');
		$this->db->from($this->table.' AS bc');
		$this->db->where('bc.seo_name', $params['seo_name']);
		$result = $this->db->get()->row_array();
		return $result['id'];
	}

	public function upload_file($params = array(), $options = array())
	{
		/// lay file dc upload
		$filename = $_FILES['image']['name'];

		// neu co chon file de upload
		if ( ! empty($filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload('image'))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$filename = $file_data['file_name'];

			// tao thumbnail
			$config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'/thumbs',
				'width' => 150,
				'height' => 100
			);

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit')
			{
				@unlink($this->image_path.$params['old_image']);
			}
		}
		else
		{
			// truong hop ko upload hinh nhung dang edit
			if ($params['method'] == 'edit')
			{
				// lay ten hinh cu lam hinh hien tai
				$filename = $params['old_image'];
			}
		}
		return $filename;
	}
}