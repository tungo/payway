<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_category_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'catalog_category';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = base_url(FILESURL.$image[$this->table]).'/';

		$this->load->library('TN_Recursive');
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('cc.id, cc.name, cc.image, cc.status, cc.position, cc.parent_id, COUNT(cp.id) AS product_amount');
		$this->db->from($this->table.' AS cc');
		$this->db->join('catalog_product AS cp', 'cp.category_id = cc.id', 'left');
		$this->db->group_by('cc.id');
		$this->db->order_by('cc.position, cc.id');

		$result = $this->db->get()->result();

		$this->tn_recursive->initialize($result);
		return $this->tn_recursive->get(0);
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		return $this->db->get($this->table)->num_rows();
	}

	public function get_entries_select_admin($params = array(), $options = array())
	{
		$this->db->select('id, name, parent_id');
		$this->db->from($this->table);
		$this->db->order_by('position, id');

		if ($options['method'] == 'category-edit')
			$this->db->where('id !=', $params['id']);

		$result = $this->db->get()->result();
		$this->tn_recursive->initialize($result);
		$result = $this->tn_recursive->get(0);

		$root = array();
		// dung cho trang them sua san pham co chon danh muc
		if ($options['method'] == 'product-admin')
		{
			$root[] = (object) array(
				'id'              => 0,
				'name'            => 'No category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}
		// dung cho trang quan ly san pham co che do loc theo danh muc
		elseif ($options['method'] == 'product-filter')
		{
			$root[] = (object) array(
				'id'              => '',
				'name'            => 'All category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
			$root[] = (object) array(
				'id'              => 0,
				'name'            => 'No category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}
		// dung cho trang them sua danh muc
		// method = category-add/category-edit
		else
		{
			$root[] = (object) array(
				'id'              => 0,
				'name'            => 'Root category',
				'depth'           => 1,
				'parent_id'       => 0,
				'children_amount' => 0
			);
		}

		$result = array_merge($root, $result);

		return $result;
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['parent_id'] = (isset($params['parent_id'])) ? (int) $params['parent_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'      => $params['name'],
			'image'     => $params['image'],
			'parent_id' => $params['parent_id'],
			'status'    => $params['status'],
			'position'  => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['parent_id'] = (isset($params['parent_id'])) ? (int) $params['parent_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'      => $params['name'],
			'image'     => $params['image'],
			'parent_id' => $params['parent_id'],
			'status'    => $params['status'],
			'position'  => $params['position']
		);
		return $this->data;
	}

	public function delete_entry($params = array(), $options = array())
	{
		$ids = $this->get_children_by_id($params, $options);
		$this->db->where_in('id', $ids);
		return $this->db->delete($this->table);
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$ids = array();
			foreach ($cid as $id)
			{
				$params['id'] = $id;
				$array = $this->get_children_by_id($params, $options);
				$ids = array_merge($ids, $array);
			}
			$this->db->where_in('id', $ids);
			return $this->db->delete($this->table);
		}
		return FALSE;
	}

	public function get_children_by_id($params = array(), $options = array())
	{
		$this->db->select('id, name, parent_id');
		$result = $this->db->get($this->table)->result();

		$this->tn_recursive->initialize($result);
		$result = $this->tn_recursive->get($params['id']);

		$ids = array($params['id']);
		if (count($result))
		{
			foreach ($result as $val)
			{
				$ids[] = $val->id;
			}
		}
		return $ids;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		$this->db->select('cc.id, cc.name, cc.image, cc.status, cc.position, cc.parent_id, COUNT(cp.id) AS product_amount');
		$this->db->from($this->table.' AS cc');
		$this->db->where('cc.status', '1');
		$this->db->join('catalog_product AS cp', 'cp.category_id = cc.id', 'left');
		$this->db->group_by('cc.id');
		$this->db->order_by('cc.position, cc.id');

		$result = $this->db->get()->result();

		$this->tn_recursive->initialize($result);
		return $this->tn_recursive->tree[0];
	}

	public function get_entries_site_menu($params = array(), $options = array())
	{
		$this->db->select('id, name, parent_id');
		$this->db->where('status', '1');

		$result = $this->db->get($this->table)->result();

		$this->tn_recursive->initialize($result);
		$tree = $this->tn_recursive->get(0);

		$return = array();
		$i = -1;
		foreach ($tree as $row)
		{
			if (! $row->parent_id)
			{
				$return[($i+1)] = $row;
				$return[($i+1)]->children = array();
				$i ++;
			}
			else
			{
				$return[$i]->children[] = $row;
			}
		}
		return $return;
	}

	public function get_entries_site_list($params = array(), $options = array())
	{
		$this->db->select('id, name');
		$this->db->where('status', '1');

		$result = $this->db->get($this->table)->result();

		$return = array();
		foreach ($result as $row)
		{
			$return[$row->id] = $row->name;
		}
		return $return;
	}

	public function upload_file($params = array(), $options = array())
	{
		/// lay file dc upload
		$filename = $_FILES['image']['name'];

		// neu co chon file de upload
		if ( ! empty($filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload('image'))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$filename = $file_data['file_name'];

			// tao thumbnail
			$config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'/thumbs',
				'width' => 150,
				'height' => 100
			);

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit')
			{
				@unlink($this->image_path.$params['old_image']);
			}
		}
		else
		{
			// truong hop ko upload hinh nhung dang edit
			if ($params['method'] == 'edit')
			{
				// lay ten hinh cu lam hinh hien tai
				$filename = $params['old_image'];
			}
		}
		return $filename;
	}
}