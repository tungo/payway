<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website_contact_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'website_contact';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('wc.*');
		$this->db->from($this->table.' AS wc');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('wc.name', $params['filter']['keyword'])
				->or_like('wc.description', $params['filter']['keyword']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('wc.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		$this->db->select('wc.id');
		$this->db->from($this->table.' AS wc');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('wc.name', $params['filter']['keyword'])
				->or_like('wc.description', $params['filter']['keyword']);
		}
		return $this->db->get()->num_rows();
	}

	public function add_entry($params = array(), $options = array())
	{
		$data = array(
			'name'        => $params['name'],
			'email'       => $params['email'],
			'phone'       => $params['phone'],
			'message'     => $params['message'],
			'created_time' => date('Y-m-d H:i:s'),
		);
		return $this->db->insert($this->table, $data);
	}

	public function delete_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->where('id', $params['id']);
		if ($this->db->delete($this->table))
		{
			return TRUE;
		}
		return FALSE;
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('id', $cid);
			if ($this->db->delete($this->table))
			{
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{

	}
}