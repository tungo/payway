<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_privilege_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'user_privilege';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('up.id, up.name, up.status, up.position, up.controller_id, up.method_id');
		$this->db->from($this->table.' AS up');
		$this->db->join('system_controller AS sc', 'up.controller_id = sc.id', 'left');
		$this->db->join('system_method AS sm', 'up.method_id = sm.id', 'left');
		$this->db->group_by('up.id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('up.name', $params['filter']['keyword']);
		}
		if (! empty($params['filter']['controller']))
		{
			$this->db->where('up.controller_id', $params['filter']['controller']);
		}
		if (! empty($params['filter']['method']))
		{
			$this->db->where('up.method_id', $params['filter']['method']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('up.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword']);
		}
		if (! empty($params['filter']['controller']))
		{
			$this->db->where('controller_id', $params['filter']['controller']);
		}
		if (! empty($params['filter']['method']))
		{
			$this->db->where('method_id', $params['filter']['method']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['controller_id'] = (isset($params['controller_id'])) ? (int) $params['controller_id'] : 0;
		$params['method_id'] = (isset($params['method_id'])) ? (int) $params['method_id'] : 0;
		if (! $params['controller_id'] || ! $params['method_id'])
		{
			return FALSE;
		}
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'controller_id' => $params['controller_id'],
			'method_id'     => $params['method_id'],
			'name'          => $params['name'],
			'status'        => $params['status'],
			'position'      => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['controller_id'] = (isset($params['controller_id'])) ? (int) $params['controller_id'] : 0;
		$params['method_id'] = (isset($params['method_id'])) ? (int) $params['method_id'] : 0;
		if (! $params['controller_id'] || ! $params['method_id'])
		{
			return FALSE;
		}
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'controller_id' => $params['controller_id'],
			'method_id'     => $params['method_id'],
			'name'          => $params['name'],
			'status'        => $params['status'],
			'position'      => $params['position']
		);
		return $this->data;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function delete_entries_by_multi_controller($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('controller_id', $cid);
			return $this->db->delete($this->table);
		}
		return FALSE;
	}

	public function delete_entries_by_multi_method($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('method_id', $cid);
			return $this->db->delete($this->table);
		}
		return FALSE;
	}

	public function get_entries_acl($params = array(), $options = array())
	{
		$this->db->select('up.*, sc.name AS controller, sm.name AS method');
		$this->db->from($this->table.' AS up');
		$this->db->where('up.status', 1);

		$this->db->join('system_controller AS sc', 'sc.id = up.controller_id');
		$this->db->join('system_model AS sm', 'sm.id = up.method_id');

		return $this->db->get();
	}
}