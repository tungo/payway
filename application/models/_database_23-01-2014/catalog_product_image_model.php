<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_product_image_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'catalog_product_image';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}
		$this->db->select('id, name, image, product_id, status, position');
		$this->db->from($this->table);
		$this->db->where('product_id', $params['id']);
		$this->db->order_by('position', 'ASC');
		$this->db->order_by('id', 'ASC');

		return $this->db->get()->result();
	}

	public function set_data_add_product($params = array(), $options = array())
	{
		$this->data = array();
		foreach ($params['product_image_image'] as $key => $image)
		{
			$this->data[] = array(
				'name'       => $params['name'].' '.($key + 1),
				'image'      => $image,
				'product_id' => $params['product_id'],
				'status'     => 1,
				'position'   => $key
			);
		}
		return $this->data;
	}

	public function add_product_entry($params = array(), $options = array())
	{
		if (empty($this->data))
		{
			return FALSE;
		}
		return $this->db->insert_batch($this->table, $this->data);
	}

	public function get_entries_site_product($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->db->where(array('status' => 1, 'product_id' => $params['id']));
		$this->db->order_by('position', 'ASC');
		$this->db->order_by('id', 'ASC');

		return $this->db->get($this->table)->result();
	}
}