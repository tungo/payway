<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_product_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'catalog_product';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = FILESURL.$image[$this->table];
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('cp.id, cp.name, cp.price, cp.status, cp.position, cp.category_id');
		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'left');
		$this->db->group_by('cp.id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('cp.name', $params['filter']['keyword'])
				->or_like('cp.description', $params['filter']['keyword']);
		}
		if (! empty($params['filter']['category']) || $params['filter']['category'] == '0')
		{
			$this->db->where('cp.category_id', $params['filter']['category']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('cp.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('description', $params['filter']['keyword']);
		}
		if (! empty($params['filter']['category']) || $params['filter']['category'] == '0')
		{
			$this->db->where('category_id', $params['filter']['category']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;
		$params['price'] = (isset($params['price'])) ? (int) $params['price'] : 0;
		$params['hot'] = (isset($params['hot'])) ? (int) $params['hot'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'category_id' => $params['category_id'],
			'price'       => $params['price'],
			'hot'         => $params['hot'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function add_entry($params = array(), $options = array())
	{
		if (empty($this->data))
		{
			return FALSE;
		}
		if ($this->db->insert($this->table, $this->data))
		{
			// filename
			$params['product_image_image'] = (isset($params['product_image_image'])) ? $params['product_image_image'] : array();

			if (count($params['product_image_image']))
			{
				// neu co filename thi them hinh
				$this->load->model('database/catalog_product_image_model');
				$params['product_id'] = $this->db->insert_id();
				$this->catalog_product_image_model->set_data_add_product($params);
				$this->catalog_product_image_model->add_product_entry($params);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;
		$params['price'] = (isset($params['price'])) ? (int) $params['price'] : 0;
		$params['hot'] = (isset($params['hot'])) ? (int) $params['hot'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'category_id' => $params['category_id'],
			'price'       => $params['price'],
			'hot'         => $params['hot'],
			'image'       => $params['image'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function update_entry($params = array(), $options = array())
	{
		if (! isset($params['id']) || empty($this->data))
		{
			return FALSE;
		}
		if ($this->db->where('id', $params['id'])->update($this->table, $this->data))
		{
			// filename
			$params['product_image_image'] = (isset($params['product_image_image'])) ? $params['product_image_image'] : array();

			// xoa het hinh cu
			$this->load->model('database/catalog_product_image_model');
			$this->catalog_product_image_model->delete_entries_by_where(array('product_id' => $params['id']));

			if (count($params['product_image_image']))
			{
				// neu co filename thi them hinh
				$params['product_id'] = $params['id'];
				$this->catalog_product_image_model->set_data_add_product($params);
				$this->catalog_product_image_model->add_product_entry($params);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'price' => 'int',
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{

	}

	public function get_entries_site_product($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->db->select('cp.*, cc.name AS category_name');
		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->where('cp.id', $params['id']);

		return $this->db->get()->row_array();
	}

	public function get_entries_site_category($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->load->model('database/catalog_category_model');
		$ids = $this->catalog_category_model->get_children_by_id($params);

		$this->db->select('cp.*, cc.name AS category_name');
		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->where_in('cp.category_id', $ids);

		$this->db->order_by('cp.position', 'ASC')->order_by('cp.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result();
	}

	public function count_entries_site_category($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->load->model('database/catalog_category_model');
		$ids = $this->catalog_category_model->get_children_by_id($params);

		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->where_in('cp.category_id', $ids);

		return $this->db->get()->num_rows();
	}

	public function get_entries_site_search($params = array(), $options = array())
	{
		$params['keyword'] = (isset($params['keyword'])) ? (int) $params['keyword'] : 0;

		if (! $params['keyword'])
		{
			return FALSE;
		}

		$this->db->select('cp.*, cc.name AS category_name');
		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->like('cp.name', $params['keyword'])
			->or_like('cp.description', $params['keyword']);

		$this->db->order_by('cp.position', 'ASC')->order_by('cp.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result();
	}

	public function count_entries_site_search($params = array(), $options = array())
	{
		$params['keyword'] = (isset($params['keyword'])) ? (int) $params['keyword'] : 0;

		if (! $params['keyword'])
		{
			return FALSE;
		}

		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->like('cp.name', $params['keyword'])
			->or_like('cp.description', $params['keyword']);

		return $this->db->get()->num_rows();
	}

	public function get_entries_site_home($params = array(), $options = array())
	{
		$this->db->start_cache();
		// cache cai db nay chung cho gon
		$this->db->select('cp.*, cc.name AS category_name');
		$this->db->from($this->table.' AS cp');
		$this->db->join('catalog_category AS cc', 'cp.category_id = cc.id', 'right');
		$this->db->where(array('cp.status' => 1, 'cc.status' => 1));
		$this->db->group_by('cp.id');

		$this->db->stop_cache();

		// cac loai sp
		$this->db->where('cp.hot', 1);
		$this->db->order_by('cp.position', 'ASC')->order_by('cp.id', 'DESC');
		$result['hot'] = $this->db->get()->result();

		$this->db->order_by('cp.id', 'DESC')->order_by('cp.position', 'ASC');
		$this->db->limit(12);
		$result['new'] = $this->db->get()->result();

		$this->db->order_by('cp.view', 'DESC')->order_by('cp.position', 'ASC')->order_by('cp.id', 'DESC');
		$this->db->limit(12);
		$result['view'] = $this->db->get()->result();

		$this->db->flush_cache();

		return $result;
	}

	public function upload_file($params = array(), $options = array())
	{
		// ten input file
		$this->file = 'image';

		/// lay file dc upload
		$this->filename = $_FILES[$this->file]['name'];

		// neu co chon file de upload
		if ( ! empty($this->filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$this->filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload($this->file))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$this->filename = $file_data['file_name'];

			// tao thumbs
			$thumbs_config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'thumbs/',
				'width' => 150,
				'height' => 100
			);

			$this->load->library('image_lib', $thumbs_config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit' && isset($params['old_image']))
			{
				@unlink($this->image_path.$params['old_image']);
				@unlink($this->image_path.'thumbs/'.$params['old_image']);
			}
		}
		else
		{
			// dang edit thong tin
			if ($params['method'] == 'edit')
			{
				// chon xoa hinh cu
				$delete_image = (isset($params['delete_image'])) ? (int) $params['delete_image'] : '0';

				if ($delete_image && isset($params['old_image']))
				{
					@unlink($this->image_path.$params['old_image']);
					@unlink($this->image_path.'thumbs/'.$params['old_image']);
				}
				else
				{
					// lay ten hinh cu de luu database
					$this->filename = $params['old_image'];
				}
			}
		}

		return $this->filename;
	}

	public function multi_upload_file($params = array(), $options = array())
	{
		$this->files = 'product_image_image';
		$this->filename = array(
			'filename' => array(),
			'error'    => array()
		);

		// tong so file duoc chon upload
		$this->total_files = (isset($_FILES[$this->files]))	? count(array_filter($_FILES[$this->files]['name'])) : 0;

		// neu co upload hinh
		if ($this->total_files)
		{
			$this->load->library('upload');
			$this->load->library('image_lib');
			$config = $this->config->item('image_upload');

			// tong so input file duoc tao.
			$count = count($_FILES[$this->files]['name']);

			// loop multi upload
			for ($i = 0; $i < $count; $i++)
			{
				// neu input do ko co file de upload thi bo qua va tiep tuc loop
				if (! $_FILES[$this->files]['name'][$i])
				{
					continue;
				}

				$_FILES['temp_file']['name']     = $_FILES[$this->files]['name'][$i];
				$_FILES['temp_file']['type']     = $_FILES[$this->files]['type'][$i];
				$_FILES['temp_file']['tmp_name'] = $_FILES[$this->files]['tmp_name'][$i];
				$_FILES['temp_file']['error']    = $_FILES[$this->files]['error'][$i];
				$_FILES['temp_file']['size']     = $_FILES[$this->files]['size'][$i];

				$config['upload_path'] = $this->image_path.'image/';
				$config['file_name'] = date('Ymd-His-'.($i + 1).'_').$_FILES['temp_file']['name'];

				$this->upload->initialize($config);

				// upload thanh cong ko co loi
				if ($this->upload->do_upload('temp_file'))
				{
					$file_data = $this->upload->data();
					$this->filename['filename'][$i] = $file_data['file_name'];

					// tao thumbs
					$thumbs_config = array(
						'source_image' => $file_data['full_path'],
						'new_image' => $this->image_path.'image/thumbs/',
						'width' => 150,
						'height' => 100
					);

					$this->image_lib->initialize($thumbs_config);
					$this->image_lib->resize();
				}
				else
				{
					$this->filename['error'][$i] = $this->upload->display_errors('', '');
					break;
				}
			}

			// neu xay ra loi
			if (count($this->filename['error']))
			{
				// xoa het file da up
				foreach($this->filename['filename'] as $image)
				{
					$this->delete_product_image($image);
				}
			}
			else
			{
				// neu dang edit thong tin
				if ($params['method'] == 'edit')
				{
					// co hinh cu thi moi xu ly
					if (isset($params['old_product_image']))
					{
						// neu co tham so xu ly hinh
						if (isset($params['old_product_image_image']))
						{
							// kiem tra xem hinh nao da bi xoa
							foreach ($params['old_product_image'] as $id => $image)
							{
								// neu hinh chua bi xoa
								$key = array_search($id, $params['old_product_image_image']);
								if ($key !== FALSE)
								{
									// neu bi upload de len thi xoa, khong thi them vao filename
									if (array_key_exists($key, $this->filename['filename']))
									{
										$this->delete_product_image($image);
									}
									else
									{
										$this->filename['filename'][$key] = $image;
									}
								}
								else
								{
									// neu bi xoa thi xoa hinh
									$this->delete_product_image($image);
								}
							}
						}
						else
						{
							// co hinh up thi se co tham so nay
							// neu truong hop nay xay ra la da bi hack xoa mat tham so nay
							// xu ly la xoa het hinh cu di
							foreach ($params['old_product_image'] as $image)
							{
								$this->delete_product_image($image);
							}
						}
					}
				}
			}
		}
		else
		{
			// neu dang edit thong tin
			if ($params['method'] == 'edit')
			{
				// co hinh cu thi moi xu ly
				if (isset($params['old_product_image']))
				{
					// neu co tham so xu ly hinh
					if (isset($params['old_product_image_image']))
					{
						// kiem tra xem hinh nao da bi xoa
						foreach ($params['old_product_image'] as $id => $image)
						{
							// hinh chua xoa thi tra ve, con lai thi xoa het
							if (in_array($id, $params['old_product_image_image']))
							{
								$this->filename['filename'][$id] = $image;
							}
							else
							{
								$this->delete_product_image($image);
							}
						}
					}
					else
					{
						// truong hop nay toan bo hinh cu da bi xoa
						foreach ($params['old_product_image'] as $image)
						{
							$this->delete_product_image($image);
						}
					}
				}
			}
		}
		ksort($this->filename['filename']);
		return $this->filename;
	}

	public function delete_product_image($image = '')
	{
		if (! $image)
		{
			return FALSE;
		}
		@unlink($this->image_path.'image/'.$image);
		@unlink($this->image_path.'image/thumbs/'.$image);
		return TRUE;
	}
}