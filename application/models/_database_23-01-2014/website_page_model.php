<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website_page_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'website_page';
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('note', $params['filter']['keyword'])
				->or_like('description', $params['filter']['keyword']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		$this->db->select('id, name, note, status, position');

		return $this->db->get($this->table)->result();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		if (! empty($params['filter']['keyword']))
		{
			$this->db->like('name', $params['filter']['keyword'])
				->or_like('note', $params['filter']['keyword'])
				->or_like('description', $params['filter']['keyword']);
		}
		return $this->db->get($this->table)->num_rows();
	}

	public function set_data_add($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'note'        => $params['note'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function set_data_update($params = array(), $options = array())
	{
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$this->data = array(
			'name'        => $params['name'],
			'note'        => $params['note'],
			'description' => $params['description'],
			'status'      => $params['status'],
			'position'    => $params['position']
		);
		return $this->data;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->db->where('id', $params['id']);

		return $this->db->get($this->table)->row_array();
	}
}