<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_article_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'blog_article';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = FILESURL.$image[$this->table];
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('ba.id, ba.image, bad.name, ba.status, ba.position, ba.category_id');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'left');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->group_by('ba.id, bad.language_id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->where("(bad.name LIKE '%{$params['filter']['keyword']}%' OR bad.synopsis LIKE '%{$params['filter']['keyword']}%' OR bad.description LIKE '%{$params['filter']['keyword']}%')");
		}
		if (! empty($params['filter']['category']) || $params['filter']['category'] == '0')
		{
			$this->db->where('ba.category_id', $params['filter']['category']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('ba.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		$this->db->select('ba.id');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'left');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->group_by('ba.id, bad.language_id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->where("(bad.name LIKE '%{$params['filter']['keyword']}%' OR bad.synopsis LIKE '%{$params['filter']['keyword']}%' OR bad.description LIKE '%{$params['filter']['keyword']}%')");
		}
		if (! empty($params['filter']['category']) || $params['filter']['category'] == '0')
		{
			$this->db->where('ba.category_id', $params['filter']['category']);
		}
		return $this->db->get()->num_rows();
	}

	public function get_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->select('ba.*, bad.*');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->where('ba.id', $params['id']);
		$result = $this->db->get()->result_array();

		$result1 = $result[0];
		$result1['description'] = array();
		foreach ($result as $row)
		{
			$result1['description'][$row['language_id']]['name'] = $row['name'];
			$result1['description'][$row['language_id']]['synopsis'] = $row['synopsis'];
			$result1['description'][$row['language_id']]['description'] = $row['description'];
		}
		return $result1;
	}

	public function add_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;
		$params['display'] = (isset($params['display'])) ? (int) $params['display'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'category_id'  => $params['category_id'],
			'image'        => $params['image'],
			'seo_name'     => $params['seo_name'],
			'display'      => $params['display'],
			'status'       => $params['status'],
			'position'     => $params['position'],
			'created_time' => date('Y-m-d H:i:s'),
			'created_by'   => $member['id'],
		);
		if ($this->db->insert($this->table, $data))
		{
			$article_id = $this->db->insert_id();
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'article_id'  => $article_id,
					'language_id' => $language['id'],
					'name'        => $name,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->insert('blog_article_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function update_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;
		$params['display'] = (isset($params['display'])) ? (int) $params['display'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'category_id'   => $params['category_id'],
			'image'         => $params['image'],
			'seo_name'      => $params['seo_name'],
			'display'       => $params['display'],
			'status'        => $params['status'],
			'position'      => $params['position'],
			'modified_time' => date('Y-m-d H:i:s'),
			'modified_by'   => $member['id'],
		);
		$this->db->where('id', $params['id']);
		if ($this->db->update($this->table, $data))
		{
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'name'        => $name,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->where('article_id', $params['id']);
				$this->db->where('language_id', $language['id']);
				$this->db->update('blog_article_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function delete_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->where('id', $params['id']);
		if ($this->db->delete($this->table))
		{
			$this->db->where('article_id', $params['id']);
			$this->db->delete('blog_article_description');
			return TRUE;
		}
		return FALSE;
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('id', $cid);
			if ($this->db->delete($this->table))
			{
				$this->db->where_in('article_id', $cid);
				$this->db->delete('blog_article_description');
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	public function copy_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$data = array();
			foreach ($cid as $id)
			{
				if ($this->check_entry(array('id' => $id)))
				{
					$row = (array) $this->get_entry(array('id' => $id));
					unset($row['id']);
					$this->add_entry($row);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{

	}

	public function get_entries_site_article($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->db->select('ba.*, bad.*, bc.seo_name AS category_seo_name');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');
		$this->db->where('ba.id', $params['id']);

		return $this->db->get()->row_array();
	}

	public function get_entries_site_article_more($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;
		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;

		if (! $params['category_id'] || ! $params['id'])
		{
			return FALSE;
		}

		$this->load->model('database/blog_category_model');

		$this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');

		$this->db->where('ba.category_id', $params['category_id']);
		$this->db->where('ba.id <>', $params['id']);
		$this->db->where('ba.created_time < ', $params['created_time']);

		$this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		$this->db->limit(3);

		return $this->db->get()->result_array();
	}

	public function get_entries_site_category($params = array(), $options = array())
	{
		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;

		if (! $params['category_id'])
		{
			return FALSE;
		}

		$this->load->model('database/blog_category_model');
		$ids = $this->blog_category_model->get_children_by_id(array('id' => $params['category_id']));

		$this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');

		$this->db->where_in('ba.category_id', $ids);

		$this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_site_category($params = array(), $options = array())
	{
		$params['category_id'] = (isset($params['category_id'])) ? (int) $params['category_id'] : 0;

		if (! $params['category_id'])
		{
			return FALSE;
		}

		$this->load->model('database/blog_category_model');
		$ids = $this->blog_category_model->get_children_by_id(array('id' => $params['category_id']));

		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id');

		$this->db->where_in('ba.category_id', $ids);

		return $this->db->get()->num_rows();
	}

	public function get_entries_site_search($params = array(), $options = array())
	{
		if (! $params['keyword'])
		{
			return FALSE;
		}

		$this->load->model('database/blog_category_model');
		$ids = $this->blog_category_model->get_children_by_id(array('id' => 1004));

		$this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');

		$this->db->where_in('ba.category_id', $ids);

		$like = $this->db->escape_like_str($params['keyword']);
		$this->db->where("(bad.name LIKE '%".$like."%' OR bad.synopsis LIKE '%".$like."%' OR bad.description LIKE '%".$like."%')");

		$this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_site_search($params = array(), $options = array())
	{
		if (! $params['keyword'])
		{
			return FALSE;
		}

		$this->load->model('database/blog_category_model');
		$ids = $this->blog_category_model->get_children_by_id(array('id' => 1004));

		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');

		$this->db->where_in('ba.category_id', $ids);

		$like = $this->db->escape_like_str($params['keyword']);
		$this->db->where("(bad.name LIKE '%".$like."%' OR bad.synopsis LIKE '%".$like."%' OR bad.description LIKE '%".$like."%')");

		return $this->db->get()->num_rows();
	}

	public function get_entries_site_home($params = array(), $options = array())
	{
		$this->load->model('database/blog_category_model');
		// $ids[1019] = $this->blog_category_model->get_children_by_id(array('id' => 1019));
		// $ids[1020] = $this->blog_category_model->get_children_by_id(array('id' => 1020));
		$ids[1021] = $this->blog_category_model->get_children_by_id(array('id' => 1021));

		// $this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		// $this->db->from($this->table.' AS ba');
		// $this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		// $this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		// $this->db->where('bad.language_id', $params['language_id']);
		// $this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		// $this->db->group_by('ba.id, bad.language_id');
		// $this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		// $this->db->where_in('ba.category_id', $ids[1019]);
		// $this->db->limit(3);
		// $result[1019] = $this->db->get()->result_array();

		// $this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		// $this->db->from($this->table.' AS ba');
		// $this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		// $this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		// $this->db->where('bad.language_id', $params['language_id']);
		// $this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		// $this->db->group_by('ba.id, bad.language_id');
		// $this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		// $this->db->where_in('ba.category_id', $ids[1020]);
		// $this->db->limit(3);
		// $result[1020] = $this->db->get()->result_array();

		$this->db->select('ba.*, bad.name, bad.synopsis, bad.description, bc.seo_name AS category_seo_name');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_article_description AS bad', 'bad.article_id = ba.id', 'left');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('bad.language_id', $params['language_id']);
		$this->db->where(array('ba.status' => 1, 'bc.status' => 1));
		$this->db->group_by('ba.id, bad.language_id');
		$this->db->order_by('ba.position', 'ASC')->order_by('ba.id', 'DESC');
		$this->db->where_in('ba.category_id', $ids[1021]);

		if (!empty($options['limit']))
			$this->db->limit($options['limit']);
		else
			$this->db->limit(3);
		$result[1021] = $this->db->get()->result_array();

		return $result;
	}

	public function get_id_from_seo($params = array(), $options = array())
	{
		$this->db->select('ba.id');
		$this->db->from($this->table.' AS ba');
		$this->db->join('blog_category AS bc', 'ba.category_id = bc.id', 'right');
		$this->db->where('ba.seo_name', $params['seo_name']);
		$this->db->where('bc.seo_name', $params['category_seo_name']);
		$this->db->order_by('ba.id', 'DESC');
		$result = $this->db->get()->row_array();
		$return = isset($result['id']) ? $result['id'] : '';
		return $return;
	}

	public function get_category_id_from_seo($params = array(), $options = array())
	{
		$this->db->select('ba.category_id');
		$this->db->from($this->table.' AS ba');
		$this->db->where('ba.seo_name', $params['seo_name']);
		$result = $this->db->get()->row_array();
		return $result['category_id'];
	}

	public function upload_file($params = array(), $options = array())
	{
		// ten input file
		$this->file = 'image';

		/// lay file dc upload
		$this->filename = $_FILES[$this->file]['name'];

		// neu co chon file de upload
		if ( ! empty($this->filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$this->filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload($this->file))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$this->filename = $file_data['file_name'];

			// tao thumbs
			$thumbs_config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'thumbs/',
				'width' => 300,
				'height' => 200
			);

			$this->load->library('image_lib', $thumbs_config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit' && isset($params['old_image']))
			{
				@unlink($this->image_path.$params['old_image']);
				@unlink($this->image_path.'thumbs/'.$params['old_image']);
			}
		}
		else
		{
			// dang edit thong tin
			if ($params['method'] == 'edit')
			{
				// chon xoa hinh cu
				$delete_image = (isset($params['delete_image'])) ? (int) $params['delete_image'] : '0';

				if ($delete_image && isset($params['old_image']))
				{
					@unlink($this->image_path.$params['old_image']);
					@unlink($this->image_path.'thumbs/'.$params['old_image']);
				}
				else
				{
					// lay ten hinh cu de luu database
					$this->filename = $params['old_image'];
				}
			}
		}

		return $this->filename;
	}
}