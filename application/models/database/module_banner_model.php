<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_banner_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'module_banner';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = FILESURL.$image[$this->table];
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('mb.id, mb.link, mb.type_id, mbd.image, mbd.name, mb.status, mb.position, mbt.name AS type_name');
		$this->db->from($this->table.' AS mb');
		$this->db->join('module_banner_description AS mbd', 'mbd.banner_id = mb.id', 'left');
		$this->db->join('module_banner_type AS mbt', 'mb.type_id = mbt.id', 'left');
		$this->db->where('mbd.language_id', $params['language_id']);
		$this->db->group_by('mb.id, mbd.language_id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->where("(mbd.name LIKE '%{$params['filter']['keyword']}%' OR mbd.synopsis LIKE '%{$params['filter']['keyword']}%' OR mbd.description LIKE '%{$params['filter']['keyword']}%')");
		}
		if (! empty($params['filter']['type']) || $params['filter']['type'] == '0')
		{
			$this->db->where('mb.type_id', $params['filter']['type']);
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('mb.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		$this->db->select('mb.id');
		$this->db->from($this->table.' AS mb');
		$this->db->join('module_banner_description AS mbd', 'mbd.banner_id = mb.id', 'left');
		$this->db->join('module_banner_type AS mbt', 'mb.type_id = mbt.id', 'left');
		$this->db->where('mbd.language_id', $params['language_id']);
		$this->db->group_by('mb.id, mbd.language_id');

		if (! empty($params['filter']['keyword']))
		{
			$this->db->where("(mbd.name LIKE '%{$params['filter']['keyword']}%' OR mbd.synopsis LIKE '%{$params['filter']['keyword']}%' OR mbd.description LIKE '%{$params['filter']['keyword']}%')");
		}
		if (! empty($params['filter']['type']) || $params['filter']['type'] == '0')
		{
			$this->db->where('mb.type_id', $params['filter']['type']);
		}
		return $this->db->get()->num_rows();
	}

	public function get_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->select('mb.*, mbd.*');
		$this->db->from($this->table.' AS mb');
		$this->db->join('module_banner_description AS mbd', 'mbd.banner_id = mb.id', 'left');
		$this->db->where('mb.id', $params['id']);
		$result = $this->db->get()->result_array();

		$result1 = $result[0];
		$result1['description'] = array();
		foreach ($result as $row)
		{
			$result1['description'][$row['language_id']]['name'] = $row['name'];
			$result1['description'][$row['language_id']]['image'] = $row['image'];
			$result1['description'][$row['language_id']]['synopsis'] = $row['synopsis'];
			$result1['description'][$row['language_id']]['description'] = $row['description'];
		}
		return $result1;
	}

	public function get_type_select_admin($params = array(), $options = array())
	{
		$this->db->select('mbt.*');
		$this->db->from('module_banner_type AS mbt');
		$this->db->order_by('mbt.position, mbt.id');

		$result = $this->db->get()->result_array();

		$root = array();
		// dung cho trang them sua san pham co chon danh muc
		if ($options['method'] == 'banner-admin')
		{
			$root[] = array(
				'id'              => 0,
				'name'            => 'No Type',
			);
		}

		// dung cho trang quan ly san pham co che do loc theo danh muc
		elseif ($options['method'] == 'type-filter')
		{
			$root[] = array(
				'id'              => '',
				'name'            => 'All type',
			);
			$root[] = array(
				'id'              => 0,
				'name'            => 'No type',
				'depth'           => 1,
			);
		}
		// dung cho trang them sua danh muc
		// method = category-add/category-edit
		// else
		// {
		// 	$root[] = array(
		// 		'id'              => 0,
		// 		'name'            => 'Root Type',
		// 		'depth'           => 1,
		// 		'parent_id'       => 0,
		// 		'children_amount' => 0
		// 	);
		// }

		$result = array_merge($root, $result);

		return $result;
	}

	public function add_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['type_id'] = (isset($params['type_id'])) ? (int) $params['type_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'type_id'      => $params['type_id'],
			'link'         => $params['link'],
			'status'       => $params['status'],
			'position'     => $params['position'],
			'created_time' => date('Y-m-d H:i:s'),
			'created_by'   => $member['id'],
		);
		if ($this->db->insert($this->table, $data))
		{
			$banner_id = $this->db->insert_id();
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$image = ($params['image'][$language['id']])
					? $params['image'][$language['id']]
					: $params['image'][$params['language_id']];

				$data = array(
					'banner_id'  => $banner_id,
					'language_id' => $language['id'],
					'name'        => $name,
					'image'       => $image,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->insert('module_banner_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function update_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['type_id'] = (isset($params['type_id'])) ? (int) $params['type_id'] : 0;
		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'type_id'       => $params['type_id'],
			'link'          => $params['link'],
			'status'        => $params['status'],
			'position'      => $params['position'],
			'modified_time' => date('Y-m-d H:i:s'),
			'modified_by'   => $member['id'],
		);
		$this->db->where('id', $params['id']);
		if ($this->db->update($this->table, $data))
		{
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$image = ($params['image'][$language['id']])
					? $params['image'][$language['id']]
					: $params['image'][$params['language_id']];

				$data = array(
					'name'        => $name,
					'image'       => $image,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->where('banner_id', $params['id']);
				$this->db->where('language_id', $language['id']);
				$this->db->update('module_banner_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function delete_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->where('id', $params['id']);
		if ($this->db->delete($this->table))
		{
			$this->db->where('banner_id', $params['id']);
			$this->db->delete('module_banner_description');
			return TRUE;
		}
		return FALSE;
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('id', $cid);
			if ($this->db->delete($this->table))
			{
				$this->db->where_in('banner_id', $cid);
				$this->db->delete('module_banner_description');
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	public function copy_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$data = array();
			foreach ($cid as $id)
			{
				if ($this->check_entry(array('id' => $id)))
				{
					$row = (array) $this->get_entry(array('id' => $id));
					unset($row['id']);
					$this->add_entry($row);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{
		// type
		$this->db->select('mbt.*');
		$this->db->from('module_banner_type AS mbt');
		$this->db->where(array('mbt.status' => 1));

		$result = $this->db->get()->result_array();

		$return = array();
		foreach ($result as $type)
		{
			$return[$type['name']] = array();
		}

		// banner
		$this->db->select('mb.*, mbd.name, mbd.image, mbd.synopsis, mbd.description, mbt.name AS type_name');
		$this->db->from($this->table.' AS mb');
		$this->db->join('module_banner_description AS mbd', 'mbd.banner_id = mb.id', 'left');
		$this->db->join('module_banner_type AS mbt', 'mbt.id = mb.type_id', 'left');
		$this->db->where('mbd.language_id', $params['language_id']);
		$this->db->where(array('mb.status' => 1));
		$this->db->group_by('mb.id, mbd.language_id');

		$this->db->order_by('mb.position', 'ASC')->order_by('mb.id', 'DESC');

		$result = $this->db->get()->result_array();

		foreach ($result as $banner)
		{
			$return[$banner['type_name']][$banner['id']] = $banner;
		}
		return $return;
	}

	public function get_entries_site_type($params = array(), $options = array())
	{
		$params['type_id'] = (isset($params['type_id'])) ? (int) $params['type_id'] : 0;

		if (! $params['type_id'])
		{
			return FALSE;
		}

		$this->db->select('mb.*, mbd.name, mbd.image, mbd.synopsis, mbd.description');
		$this->db->from($this->table.' AS mb');
		$this->db->join('module_banner_description AS mbd', 'mbd.banner_id = mb.id', 'left');
		$this->db->where('mbd.language_id', $params['language_id']);
		$this->db->where(array('mb.status' => 1));
		$this->db->group_by('mb.id, mbd.language_id');

		$this->db->where_in('mb.type_id', $params['type_id']);

		$this->db->order_by('mb.position', 'ASC')->order_by('mb.id', 'DESC');

		return $this->db->get()->result_array();
	}

	public function upload_multi_file($params = array(), $options = array())
	{
		$this->file = array();
		$this->filename = array();
		$flag = true;

		$this->load->library('upload');
		// cau hinh cho upload
		$config = $this->config->item('image_upload');

		$this->load->library('image_lib');

		foreach ($params['languages'] as $language)
		{
			// ten input file
			$this->file[$language['id']] = 'image'.$language['id'];

			/// lay file dc upload
			$this->filename[$language['id']] = $_FILES[$this->file[$language['id']]]['name'];

			// neu co chon file de upload
			if ( ! empty($this->filename[$language['id']]))
			{
				$config['upload_path'] = $this->image_path;
				$config['file_name'] = date('Ymd-His_').$this->filename[$language['id']];

				$this->upload->initialize($config);

				// goi upload neu that bai thi ngung va bao loi o controller
				if ( ! $this->upload->do_upload($this->file[$language['id']]))
				{
					$flag = FALSE;
					break;
				}

				// lay du lieu file vua dc upload de tra ve cho controller luu database
				$file_data = $this->upload->data();
				$this->filename[$language['id']] = $file_data['file_name'];

				// tao thumbs
				$thumbs_config = array(
					'source_image' => $file_data['full_path'],
					'new_image' => $this->image_path.'thumbs/',
					'width' => 300,
					'height' => 200
				);

				$this->image_lib->initialize($thumbs_config);
				$this->image_lib->resize();
			}
		}

		if (!$flag)
		{
			foreach ($this->filename as $k => $filename)
			{
				@unlink($this->image_path.$filename);
				@unlink($this->image_path.'thumbs/'.$filename);
			}
			return false;
		}
		else
		{
			foreach ($params['languages'] as $language)
			{
				if ( ! empty($this->filename[$language['id']]))
				{
					// neu trong truong hop edit thi xoa file cu
					if ($params['method'] == 'edit' && isset($params['old_image'.$language['id']]))
					{
						@unlink($this->image_path.$params['old_image'.$language['id']]);
						@unlink($this->image_path.'thumbs/'.$params['old_image'.$language['id']]);
					}
				}
				else
				{
					// dang edit thong tin
					if ($params['method'] == 'edit')
					{
						// chon xoa hinh cu
						$delete_image = (isset($params['delete_image'.$language['id']])) ? (int) $params['delete_image'.$language['id']] : '0';

						if ($delete_image && isset($params['old_image'.$language['id']]))
						{
							@unlink($this->image_path.$params['old_image'.$language['id']]);
							@unlink($this->image_path.'thumbs/'.$params['old_image'.$language['id']]);
						}
						else
						{
							// lay ten hinh cu de luu database
							$this->filename[$language['id']] = $params['old_image'.$language['id']];
						}
					}
				}
			}
		}

		return $this->filename;
	}

}