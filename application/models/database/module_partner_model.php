<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_partner_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'module_partner';

		$image = $this->config->item('image_path');
		$this->image_path = FILESPATH.$image[$this->table];
		$this->image_url = FILESURL.$image[$this->table];
	}

	public function get_entries_admin($params = array(), $options = array())
	{
		$this->db->select('mp.id, mp.image, mpd.name, mp.status, mp.position');
		$this->db->from($this->table.' AS mp');
		$this->db->join('module_partner_description AS mpd', 'mpd.partner_id = mp.id', 'left');
		$this->db->where('mpd.language_id', $params['language_id']);
		$this->db->group_by('mp.id, mpd.language_id');

		if (! empty($params['filter']['keyword']))
		{
      $this->db->where("(mpd.name LIKE '%{$params['filter']['keyword']}%' OR mpd.description LIKE '%{$params['filter']['keyword']}%')");
		}

		$this->db->order_by($params['filter']['sort'], $params['filter']['order']);
		$this->db->order_by('mp.id', 'DESC');
		$this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

		return $this->db->get()->result_array();
	}

	public function count_entries_admin($params = array(), $options = array())
	{
		$this->db->select('mp.id');
		$this->db->from($this->table.' AS mp');
		$this->db->join('module_partner_description AS mpd', 'mpd.partner_id = mp.id', 'left');
		$this->db->where('mpd.language_id', $params['language_id']);
		$this->db->group_by('mp.id, mpd.language_id');

		if (! empty($params['filter']['keyword']))
		{
      $this->db->where("(mpd.name LIKE '%{$params['filter']['keyword']}%' OR mpd.description LIKE '%{$params['filter']['keyword']}%')");
		}
		return $this->db->get()->num_rows();
	}

	public function get_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->select('mp.*, mpd.*');
		$this->db->from($this->table.' AS mp');
		$this->db->join('module_partner_description AS mpd', 'mpd.partner_id = mp.id', 'left');
		$this->db->where('mp.id', $params['id']);
		$result = $this->db->get()->result_array();

		$result1 = $result[0];
		$result1['description'] = array();
		foreach ($result as $row)
		{
			$result1['description'][$row['language_id']]['name'] = $row['name'];
			$result1['description'][$row['language_id']]['synopsis'] = $row['synopsis'];
			$result1['description'][$row['language_id']]['description'] = $row['description'];
		}
		return $result1;
	}

	public function add_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'image'       => $params['image'],
			'link'        => $params['link'],
			'status'      => $params['status'],
			'position'    => $params['position'],
			'created_time' => date('Y-m-d H:i:s'),
			'created_by' => $member['id'],
		);
		if ($this->db->insert($this->table, $data))
		{
			$partner_id = $this->db->insert_id();
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'partner_id'  => $partner_id,
					'language_id' => $language['id'],
					'name'        => $name,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->insert('module_partner_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function update_entry($params = array(), $options = array())
	{
		$member = $this->session->userdata('member');

		$params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
		$params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

		$data = array(
			'image'         => $params['image'],
			'link'          => $params['link'],
			'status'        => $params['status'],
			'position'      => $params['position'],
			'modified_time' => date('Y-m-d H:i:s'),
			'modified_by'   => $member['id'],
		);
		$this->db->where('id', $params['id']);
		if ($this->db->update($this->table, $data))
		{
			foreach ($params['languages'] as $language)
			{
				$name = ($params['description'][$language['id']]['name'])
					? $params['description'][$language['id']]['name']
					: $params['description'][$params['language_id']]['name'];
				$synopsis = ($params['description'][$language['id']]['synopsis'])
					? $params['description'][$language['id']]['synopsis']
					: $params['description'][$params['language_id']]['synopsis'];
				$description = ($params['description'][$language['id']]['description'])
					? $params['description'][$language['id']]['description']
					: $params['description'][$params['language_id']]['description'];

				$data = array(
					'name'        => $name,
					'synopsis'    => $synopsis,
					'description' => $description,
				);
				$this->db->where('partner_id', $params['id']);
				$this->db->where('language_id', $language['id']);
				$this->db->update('module_partner_description', $data);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function delete_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}

		$this->db->where('id', $params['id']);
		if ($this->db->delete($this->table))
		{
			$this->db->where('partner_id', $params['id']);
			$this->db->delete('module_partner_description');
			return TRUE;
		}
		return FALSE;
	}

	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('id', $cid);
			if ($this->db->delete($this->table))
			{
				$this->db->where_in('partner_id', $cid);
				$this->db->delete('module_partner_description');
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	public function copy_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$data = array();
			foreach ($cid as $id)
			{
				if ($this->check_entry(array('id' => $id)))
				{
					$row = (array) $this->get_entry(array('id' => $id));
					unset($row['id']);
					$this->add_entry($row);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$options['field'] = array(
			'position' => 'int'
		);
		return parent::save_entries($params, $options);
	}

	public function get_entries_site($params = array(), $options = array())
	{

	}

	public function get_entries_site_home($params = array(), $options = array())
	{
		$this->db->select('mp.*, mpd.*');
		$this->db->from($this->table.' AS mp');
		$this->db->join('module_partner_description AS mpd', 'mpd.partner_id = mp.id', 'left');
		$this->db->where('mpd.language_id', $params['language_id']);
		$this->db->where('mp.status', 1);
		$this->db->group_by('mp.id, mpd.language_id');

		return $this->db->get()->result_array();
	}

	public function get_entries_site_partner($params = array(), $options = array())
	{
		$params['id'] = (isset($params['id'])) ? (int) $params['id'] : 0;

		if (! $params['id'])
		{
			return FALSE;
		}

		$this->db->select('mp.*, mpd.*');
		$this->db->from($this->table.' AS mp');
		$this->db->join('module_partner_description AS mpd', 'mpd.partner_id = mp.id', 'left');
		$this->db->join('blog_category AS bc', 'mp.category_id = bc.id', 'right');
		$this->db->where('mpd.language_id', $params['language_id']);
		$this->db->where(array('mp.status' => 1, 'bc.status' => 1));
		$this->db->group_by('mp.id, mpd.language_id');
		$this->db->where('mp.id', $params['id']);
		$this->db->where('bc.id', $params['category_id']);

		return $this->db->get()->row_array();
	}

	public function upload_file($params = array(), $options = array())
	{
		// ten input file
		$this->file = 'image';

		/// lay file dc upload
		$this->filename = $_FILES[$this->file]['name'];

		// neu co chon file de upload
		if ( ! empty($this->filename))
		{
			$this->load->library('upload');

			// cau hinh cho upload
			$config = $this->config->item('image_upload');
			$config['upload_path'] = $this->image_path;
			$config['file_name'] = date('Ymd-His_').$this->filename;

			$this->upload->initialize($config);

			// goi upload neu that bai thi ngung va bao loi o controller
			if ( ! $this->upload->do_upload($this->file))
			{
				return FALSE;
			}

			// lay du lieu file vua dc upload de tra ve cho controller luu database
			$file_data = $this->upload->data();
			$this->filename = $file_data['file_name'];

			// tao thumbs
			$thumbs_config = array(
				'source_image' => $file_data['full_path'],
				'new_image' => $this->image_path.'thumbs/',
				'width' => 300,
				'height' => 200
			);

			$this->load->library('image_lib', $thumbs_config);
			$this->image_lib->resize();

			// neu trong truong hop edit thi xoa file cu
			if ($params['method'] == 'edit' && isset($params['old_image']))
			{
				@unlink($this->image_path.$params['old_image']);
				@unlink($this->image_path.'thumbs/'.$params['old_image']);
			}
		}
		else
		{
			// dang edit thong tin
			if ($params['method'] == 'edit')
			{
				// chon xoa hinh cu
				$delete_image = (isset($params['delete_image'])) ? (int) $params['delete_image'] : '0';

				if ($delete_image && isset($params['old_image']))
				{
					@unlink($this->image_path.$params['old_image']);
					@unlink($this->image_path.'thumbs/'.$params['old_image']);
				}
				else
				{
					// lay ten hinh cu de luu database
					$this->filename = $params['old_image'];
				}
			}
		}

		return $this->filename;
	}
}