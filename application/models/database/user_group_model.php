<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group_model extends MY_Model {

  public function __construct()
  {
    parent::__construct();
    $this->table = 'user_group';
  }

  public function get_entries_admin($params = array(), $options = array())
  {
    $this->db->select('ug.id, ug.name, ug.status, ug.position, COUNT(ua.id) AS account_amount');
    $this->db->from($this->table.' AS ug');
    $this->db->join('user_account AS ua', 'ug.id = ua.group_id', 'left');
    $this->db->group_by('ug.id');

    if (! empty($params['filter']['keyword']))
    {
      $this->db->like('ug.name', $params['filter']['keyword']);
    }

    $this->db->order_by($params['filter']['sort'], $params['filter']['order']);
    $this->db->order_by('ug.id', 'DESC');
    $this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

    $result = $this->db->get()->result_array();
    return $result;
  }

  public function count_entries_admin($params = array(), $options = array())
  {
    $this->db->select('ug.id');
    $this->db->from($this->table.' AS ug');
    $this->db->join('user_account AS ua', 'ug.id = ua.group_id', 'left');
    $this->db->order_by('ug.position, ug.id');
    $this->db->group_by('ug.id');

    if (! empty($params['filter']['keyword']))
    {
      $this->db->like('ug.name', $params['filter']['keyword']);
    }

    return $this->db->get()->num_rows();
  }

  public function get_entries_select_admin($params = array(), $options = array())
  {
    $this->db->select('ug.id, ug.name');
    $this->db->from($this->table.' AS ug');
    $this->db->order_by('ug.position, ug.id');

    $result = $this->db->get()->result_array();

    $root = array();
    if ($options['method'] == 'account-filter')
    {
      $root[] = array(
        'id'              => '',
        'name'            => 'All group',
        'depth'           => 1,
        'parent_id'       => 0,
        'children_amount' => 0
      );
    }
    $result = array_merge($root, $result);

    return $result;
  }

  public function get_entry($params = array(), $options = array())
  {
    if (! isset($params['id']))
    {
      return FALSE;
    }

    $this->db->select('ug.*');
    $this->db->from($this->table.' AS ug');
    $this->db->where('ug.id', $params['id']);
    $row = $this->db->get()->row_array();
    $row['privilege'] = array();

    $this->db->select('up.name');
    $this->db->from('user_group_privilege AS ugp');
    $this->db->join('user_privilege AS up', 'ugp.privilege_id = up.id', 'left');
    $this->db->where('ugp.group_id', $params['id']);
    $this->db->where('ugp.status', 1);
    $this->db->group_by('up.name');
    $result = $this->db->get()->result_array();

    foreach ($result as $privilege)
      $row['privilege'][] = $privilege['name'];

    return $row;
  }

  public function add_entry($params = array(), $options = array())
  {
    $member = $this->session->userdata('member');

    $params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
    $params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

    $data = array(
      'name'         => $params['name'],
      'type'         => 1,
      'admin'        => 1,
      'level'        => 3,
      'status'       => $params['status'],
      'position'     => $params['position'],
      'created_time' => date('Y-m-d H:i:s'),
      'created_by'   => $member['id'],
    );

    if ($this->db->insert($this->table, $data))
    {
      $params['id'] = $this->db->insert_id();

      $privileges = $this->get_privilege_id();
      $data = array();
      foreach ($privileges as $privilege)
      {
        $data[] = array(
          'group_id' => (int) $params['id'],
          'privilege_id' => (int) $privilege['id'],
          'status' => 0,
        );
      }
      if ($this->db->insert_batch('user_group_privilege', $data))
        if ($this->update_group_privilege($params))
          return true;
    }
    $this->delete_entry($params['id']);
    return false;
  }

  public function update_entry($params = array(), $options = array())
  {
    $member = $this->session->userdata('member');

    $params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
    $params['position'] = (isset($params['position'])) ? (int) $params['position'] : 0;

    $data = array(
      'name'          => $params['name'],
      'status'        => $params['status'],
      'position'      => $params['position'],
      'modified_time' => date('Y-m-d H:i:s'),
      'modified_by'   => $member['id'],
    );
    $this->db->where('id', $params['id']);
    if ($this->db->update($this->table, $data))
    {
      $privileges = $this->get_privilege_id();
      $data = array('status' => 0);
      $this->db->where('group_id', $params['id']);

      if ($this->db->update('user_group_privilege', $data))
        if ($this->update_group_privilege($params))
          return true;
    }
    return false;
  }

  public function update_group_privilege($params = array(), $options = array())
  {
    if (!$params['id'])
      return false;
    if (empty($params['privilege']))
      return true;

    $this->db->select('up.id, up.name');
    $this->db->from('user_privilege AS up');
    $result = $this->db->get()->result_array();

    $privileges = array();
    foreach ($result as $privilege)
      $privileges[$privilege['name']][] = $privilege['id'];

    $data = array('status' => 1);
    $pid = array();

    foreach ($params['privilege'] as $privilege_name)
      if (isset($privileges[$privilege_name]))
        foreach ($privileges[$privilege_name] as $privilege_id)
          $pid[] = $privilege_id;

    $this->db->where_in('privilege_id', $pid);
    $this->db->where('group_id', $params['id']);

    if ($this->db->update('user_group_privilege', $data))
      return true;
    return false;
  }

  public function delete_entry($params = array(), $options = array())
  {
    if (! isset($params['id']))
    {
      return false;
    }

    $this->db->where('id', $params['id']);
    if ($this->db->delete($this->table))
    {
      $this->db->where('group_id', $params['id']);
      $this->db->delete('user_group_privilege');
      return true;
    }
    return false;
  }

  public function check_default($params = array(), $options = array())
  {
    if (! isset($params['id']))
    {
      return FALSE;
    }
    return $this->db->get_where($this->table, array('id' => $params['id'], 'default' => 1))->num_rows();
  }

  public function check_multi_default($params = array(), $options = array())
  {

    $cid = (isset($params['cid'])) ? $params['cid'] : array();

    if (count($cid) > 0)
    {
      $this->db->where_in('id', $cid);
      $this->db->where('default', 1);
      return $this->db->get($this->table)->num_rows();
    }
    return FALSE;
  }

  public function save_entries($params = array(), $options = array())
  {
    $options['field'] = array(
      'position' => 'int'
    );
    return parent::save_entries($params, $options);
  }

  public function get_entry_auth($params = array(), $options = array())
  {
    if (empty($params))
      return false;

    return $this->db->get_where($this->table, $params)->row_array();
  }

  public function get_privilege_id($params = array(), $options = array())
  {
    $this->db->select('up.id');
    $this->db->from('user_privilege AS up');
    return $this->db->get()->result_array();
  }

  public function get_privilege_acl($params = array(), $options = array())
  {
    if (! isset($params['group_id']))
      return false;

    $this->db->select('ugp.*, up.*');
    $this->db->from('user_group_privilege AS ugp');
    $this->db->join('user_privilege AS up', 'ugp.privilege_id = up.id', 'left');
    $this->db->where('ugp.group_id', $params['group_id']);
    $this->db->where(array('ugp.status' => 1, 'up.status' => 1));
    $this->db->group_by('ugp.privilege_id');

    return $this->db->get()->result_array();
  }

  public function get_privilege_admin($params = array(), $options = array())
  {
    $this->db->select('up.*');
    $this->db->from('user_privilege AS up');
    $this->db->group_by('up.name');
    $this->db->where('up.status', 1);

    $result = $this->db->get()->result_array();
    $privileges = array();
    foreach ($result as $privilege)
    {
      $privileges[$privilege['controller']][] = $privilege['name'];
    }

    $return = array();
    foreach ($privileges as $prvl_ctrl => $prvl_prvl)
    {
      switch ($prvl_ctrl) {
        case 'blog_article':
        case 'blog_category':
          $ctrl = 'blog';
          break;
        case 'module_partner':
        case 'module_banner':
        case 'website_contact':
          $ctrl = 'administration';
          break;
        case 'user_account':
        case 'user_group':
          $ctrl = 'user';
          break;
        case 'payment_transaction':
          $ctrl = 'payment';
          break;
        default:
          $ctrl = '';
          break;
      }
      if (!isset($return[$ctrl]))
        $return[$ctrl] = array();
      $return[$ctrl] = array_merge($return[$ctrl], $prvl_prvl);
    }
    return $return;
  }

  public function synchronize_group_privilege($params = array(), $options = array())
  {
    $privileges = $this->get_privilege_by_group_id($params);
    $privilege_ids = $this->get_privilege_id($params);

    foreach ($privilege_ids as $priv)
    {
      if (!array_key_exists($priv['id'], $privileges))
      {
        $data = array(
          'group_id'     => $params['id'],
          'privilege_id' => $priv['id'],
          'status'       => 0,
        );
        if (!$this->db->insert('user_group_privilege', $data))
        {
          return false;
        }
      }
    }

    return true;
  }

  public function get_privilege_by_group_id($params = array(), $options = array())
  {
    $this->db->select('ugp.privilege_id, ugp.status');
    $this->db->from('user_group_privilege AS ugp');
    $this->db->where('ugp.group_id', $params['id']);
    $result = $this->db->get()->result_array();

    $return = array();
    foreach ($result as $res)
    {
      $return[$res['privilege_id']] = $res['status'];
    }
    return $return;
  }
}