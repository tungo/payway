<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_account_model extends MY_Model {

  public function __construct()
  {
    parent::__construct();
    $this->table = 'user_account';
  }

  public function get_entries_admin($params = array(), $options = array())
  {
    $this->db->select('ua.id, ua.email, ua.firstname, ua.lastname, ua.status, ua.group_id, ug.name AS group_name');
    $this->db->from($this->table.' AS ua');
    $this->db->join('user_group AS ug', 'ua.group_id = ug.id', 'left');

    if (! empty($params['filter']['keyword']))
    {
      $this->db->where("(ua.email LIKE '%{$params['filter']['keyword']}%' OR ua.firstname LIKE '%{$params['filter']['keyword']}%' OR ua.lastname LIKE '%{$params['filter']['keyword']}%')");
    }
    if (! empty($params['filter']['group']) || $params['filter']['group'] == '0')
    {
      $this->db->where('ua.group_id', $params['filter']['group']);
    }

    $this->db->order_by($params['filter']['sort'], $params['filter']['order']);
    $this->db->order_by('ua.id', 'DESC');
    $this->db->limit($params['pagination']->per_page, $params['pagination']->offset);

    return $this->db->get()->result_array();
  }

  public function count_entries_admin($params = array(), $options = array())
  {
    $this->db->select('ua.id');
    $this->db->from($this->table.' AS ua');
    $this->db->join('user_group AS ug', 'ua.group_id = ug.id', 'left');

    if (! empty($params['filter']['keyword']))
    {
      $this->db->where("(ua.email LIKE '%{$params['filter']['keyword']}%' OR ua.firstname LIKE '%{$params['filter']['keyword']}%' OR ua.lastname LIKE '%{$params['filter']['keyword']}%')");
    }
    if (! empty($params['filter']['group']) || $params['filter']['group'] == '0')
    {
      $this->db->where('ua.group_id', $params['filter']['group']);
    }
    return $this->db->get()->num_rows();
  }

  public function get_entry($params = array(), $options = array())
  {
    if (! isset($params['id']))
    {
      return FALSE;
    }

    $this->db->select('ua.*');
    $this->db->from($this->table.' AS ua');
    $this->db->where('ua.id', $params['id']);
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function add_entry($params = array(), $options = array())
  {
    $params['group_id'] = (isset($params['group_id'])) ? (int) $params['group_id'] : 0;
    $params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;
    $salt = $this->salt();

    $data = array(
      'group_id'  => $params['group_id'],
      'email'     => $params['email'],
      'salt'      => $salt,
      'password'  => $this->hash_password($params['password'], $salt),
      'firstname' => $params['firstname'],
      'lastname'  => $params['lastname'],
      'status'    => $params['status'],
      'registered_time' => date('Y-m-d H:i:s'),
      'registered_ip' => $this->input->ip_address(),
    );
    return $this->db->insert($this->table, $data);
  }

  public function update_entry($params = array(), $options = array())
  {
    $params['group_id'] = (isset($params['group_id'])) ? (int) $params['group_id'] : 0;
    $params['status'] = (isset($params['status'])) ? (int) $params['status'] : 0;

    $data = array(
      'group_id'  => $params['group_id'],
      'email'     => $params['email'],
      'firstname' => $params['firstname'],
      'lastname'  => $params['lastname'],
      'status'    => $params['status'],
      'remember_code' => '',
    );

    if (isset($params['password']) && !empty($params['password']))
    {
      $data['salt'] = $this->salt();
      $data['password'] = $this->hash_password($params['password'], $data['salt']);
    }

    $this->db->where('id', $params['id']);
    return $this->db->update($this->table, $data);
  }

  public function delete_entry($params = array(), $options = array())
  {
    return parent::delete_entry($params, $options);
  }

  public function delete_multi_entries($params = array(), $options = array())
  {
    return parent::delete_multi_entries($params, $options);
  }

  public function check_email($params = array(), $options = array())
  {
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }
    $this->db->select('ua.id');
    $this->db->from($this->table.' AS ua');
    $this->db->where(array('email' => $params['email']));

    if (isset($params['id']) && $params['id'])
      $this->db->where(array('id <>' => $params['id']));

    return $this->db->get()->num_rows();
  }

  public function register($params = array(), $options = array())
  {
    $salt = $this->salt();
    $date = new DateTime($params['namsinh'].'-'.$params['thangsinh'].'-'.$params['ngaysinh']);

    $data = array(
      'group_id'        => 4,
      'email'           => $params['email'],
      'salt'            => $salt,
      'password'        => $this->hash_password($params['password'], $salt),
      'firstname'       => $params['ten'],
      'lastname'        => $params['ho'],
      'gender'          => (int) $params['gioitinh'],
      'birthday'        => $date->format('Y-m-d'),
      'nationality'     => $params['quocgia'],
      'address'         => $params['diachi'],
      'additional_info' => $params['thongtinthem'],
      'city'            => $params['thanhpho'],
      'postcode'        => $params['postcode'],
      'phone'           => $params['dienthoai'],
      'identification'  => $params['chungminhthu'],
      'question_id'     => (int) $params['cauhoi'],
      'answer'          => $params['traloi'],
      'status'          => 1,
      'registered_time' => date('Y-m-d H:i:s'),
      'registered_ip'   => $this->input->ip_address(),
    );
    return $this->db->insert($this->table, $data);
  }

  public function check_email_register($params = array(), $options = array())
  {
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }
    $this->db->select('ua.id');
    $this->db->from($this->table.' AS ua');
    $this->db->where(array('email' => $params['email']));

    return $this->db->get()->num_rows();
  }

  public function change_password($data = array(), $options = array())
  {
    $params = $data['params'];
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }

    $dt['salt'] = $this->salt();
    $dt['password'] = $this->hash_password($params['newpass'], $dt['salt']);

    $this->db->where('id', $data['member']['id']);
    return $this->db->update($this->table, $dt);
  }

  public function check_password($data = array(), $options = array())
  {
    $params = $data['params'];
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }
    $this->db->select('ua.password, ua.salt');
    $this->db->from($this->table.' AS ua');
    $this->db->where(array('ua.id' => $data['member']['id']));

    $row = $this->db->get()->row_array();

    if ($row['password'] == $this->hash_password($params['oldpass'], $row['salt']))
      return true;
    return false;
  }

  public function get_entry_member($params = array(), $options = array())
  {
    if (empty($params))
    {
      return FALSE;
    }
    return $this->db->get_where($this->table, $params)->row_array();
  }

  public function update_entry_member($data = array(), $options = array())
  {
    $params = $data['params'];
    $date = new DateTime($params['namsinh'].'-'.$params['thangsinh'].'-'.$params['ngaysinh']);

    $dt = array(
      'firstname'       => $params['ten'],
      'lastname'        => $params['ho'],
      'gender'          => (int) $params['gioitinh'],
      'birthday'        => $date->format('Y-m-d'),
      'nationality'     => $params['quocgia'],
      'address'         => $params['diachi'],
      'additional_info' => $params['thongtinthem'],
      'city'            => $params['thanhpho'],
      'postcode'        => $params['postcode'],
    );

    if ($params['dienthoai'])
      $dt['phone'] = $params['dienthoai'];
    if ($params['chungminhthu'])
      $dt['identification'] = $params['chungminhthu'];
    if ($params['cauhoi'])
      $dt['question_id'] = $params['cauhoi'];
    if ($params['traloi'])
      $dt['answer'] = $params['traloi'];

    $this->db->where('id', $data['member']['id']);
    return $this->db->update($this->table, $dt);
  }

  public function get_entry_auth($params = array(), $options = array())
  {
    if (empty($params))
    {
      return FALSE;
    }
    return $this->db->get_where($this->table, $params)->row_array();
  }

  public function renew_password($data = array(), $options = array())
  {
    $params = $data['params'];
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }

    $dt['salt'] = $this->salt();
    $dt['password'] = $this->hash_password($params['newpass'], $dt['salt']);

    $this->db->where('email', $data['params']['email']);
    return $this->db->update($this->table, $dt);
  }

  public function update_last_login($params = array(), $options = array())
  {
    if (! is_array($params) || empty($params))
    {
      return FALSE;
    }
    return $this->db->where(array('id' => $params['id']))->update($this->table, array(
      'visited_time' => date('Y-m-d H:i:s'),
      'visited_ip'   => $params['ip_address'],
    ));
  }

  public function remember_user($params = array())
  {
    if (! isset($params['id']) || ! isset($params['remember_code']))
    {
      return FALSE;
    }

    $this->db->where(array('id' => $params['id']))->update($this->table, array('remember_code' => $params['remember_code']));

    return $this->db->affected_rows();
  }

  // Hash password for sercurity
  public function hash_password($password = '', $salt = '')
  {
    if ($password == '')
    {
      return FALSE;
    }
    return sha1(md5($password.'Death'.$salt));
  }

  // Init random salt
  public function salt($length = 32)
  {
    return substr(sha1(uniqid(rand(), true)), 0, $length);
  }

  public function get_nation($data = array(), $options = array())
  {
    $this->db->select('na.code, na.name');
    $this->db->from('nation AS na');

    $result = $this->db->get()->result_array();
    $return = array();
    foreach ($result as $nation)
      $return[$nation['code']] = $nation['name'];
    return $return;
  }

  public function get_security_question($data = array(), $options = array())
  {
    $this->db->select('usq.id, usqd.name');
    $this->db->from('user_security_question AS usq');
    $this->db->join('user_security_question_description AS usqd', 'usq.id = usqd.security_question_id', 'left');
    $this->db->where('usqd.language_id', $data['language_id']);
    $this->db->group_by('usq.id, usqd.language_id');

    $result = $this->db->get()->result_array();
    $return = array();
    foreach ($result as $question)
      $return[$question['id']] = $question['name'];
    return $return;
  }

}