<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><!--<![endif]-->
<head>
  <title><?php echo $template['title']; ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <!-- Seo Meta -->
    <meta name="description" content="<?php echo $template['title']; ?> | Payway cung cấp dịch vụ topup, thanh toán hóa đơn tiền điện, nước, ADSL, mua thẻ game, thẻ cào, chuyển tiền kiều hối an toàn và nhanh chóng. Hotline: 0919 891 909.">
    <meta name="keywords" content="thanh toan truc tuyen, thanh toan hoa don, tien dien, tien nuoc, chuyen tien, topup, nap tien dien thoai, vi dien tu, cong thanh toan dien tu, tai khoan ngan hang, thu ho, chi ho, chuyen tien ve viet nam, chuyen tien kieu hoi">
    <base href="<?php echo base_url(); ?>">
  <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['path'].'bootstrap/css/bootstrap.min.css'); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['css'].'style.css'); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['css'].'login.css'); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['css'].'icons.css'); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['css'].'animate.css'); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['js'].'layerslider/css/custom.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url($theme['css'].'nivo-slider/default/default.css'); ?>" media="screen" />
  <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url($theme['image'].'favicon.ico'); ?>">
    <link rel="apple-touch-icon" href="<?php echo base_url($theme['image'].'apple-touch-icon.png'); ?>">

  <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=EmulateIE8; IE=EDGE" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <script type="text/javascript" src="<?php echo base_url($theme['js'].'jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'jquery.simplyscroll.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'silder-home.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'jquery.nivo.slider.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'theme20.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'custom.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['js'].'tn_script.js'); ?>"></script>
  <script type="text/javascript">
    (function($) {
      $(function() {
        $("#scroller").simplyScroll();
      });
      $(function() {
        $("#scroller-top").simplyScroll();
      })
      $(document).ready(function(){
            $(".show-khuyen-mai").hide();
            $(".hide-khuyen-mai").show();
            $('.hide-khuyen-mai').click(function(){
                $(".show-khuyen-mai").slideToggle();
                return false;
            });
      });
    })(jQuery);
  </script>
</head>
<body>
  <div id="frame_">
    <div id="layout" class="full">
      <header id="header">
        <div class="head_up">
          <div class="row clearfix">
            <div class="languages">
              <a><span><img class="flag" src="<?php echo base_url('/public/images/flags/'.$languages[$language_id]['image']); ?>"></span></a>
              <div class="other_languages">
                <?php foreach ($languages as $language):
                    if ($language['id'] == $language_id) continue;
                ?>
                <a href="<?php echo site_url('main/language/id/'.$language['id']); ?>">
                  <span><img class="flag" src="<?php echo base_url('/public/images/flags/'.$language['image']); ?>"></span>
                </a>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="l_ht" style="width:60%;line-height:12px;margin-top:12px;">
              <div class="simply-scroll simply-scroll-container">
                <div class="simply-scroll-clip">
                  <div id="scroller-top" class="top-noi-dung simply-scroll-list" style="width: 2520px;">
                    <li>PAY WAY: The Smart & Multiple Ways To Send Money Home By 25 banks, #50,000,000 Deposit Accounts, 30,000,000 ATM & Cards – Pickup @ >3,600 POS – Whole Country Home Delivery – And Mobile Platform Services.</li>
                  </div>
                </div>
              </div>
            </div>
            <div class="r_ht">
              <div class="login">
                <?php if (empty($member)): ?>
                <a href="<?php echo site_url('member/login'); ?>"><?php elang('dang-nhap'); ?></a>
                <a href="<?php echo site_url('member/register'); ?>"><?php elang('dang-ky'); ?></a>
                <?php else: ?>
                <a href="<?php echo site_url('member'); ?>" class="bottomtip" title="<?php elang('thong-tin-tai-khoan'); ?>"><i class="icon_profile"></i> <?php echo $member['email']; ?></a> || 
                <a href="<?php echo site_url('member/logout');?>" class="bottomtip" title="<?php elang('thoat'); ?>"><i class="fa-power-off"></i></a>
                <?php endif; ?>

              </div>
            </div>
          </div><!-- row -->
        </div><!-- head -->

        <div class="headdown">
          <div class="row clearfix">
            <div class="logo">
              <a href="<?php echo site_url(); ?>" title=""><img src="<?php echo base_url($theme['image'].'logo.png'); ?>" alt=""></a>
            </div>

            <div class="search">
              <div class="search_icon"><i class="icon_search icon_close"></i></div>
              <div class="s_form">
                <form action="<?php echo site_url('tim-kiem'); ?>" id="search" method="get">
                  <input id="inputhead" name="keyword" type="text" placeholder="Enter keywords">
                  <button type="submit"><i class="icon_search"></i></button>
                </form><!-- end form -->
              </div>
            </div>

            <nav>
              <ul class="sf-menu">
                <?php echo recursive_category_menu($category_menu); ?>
              </ul><!-- end menu -->
            </nav><!-- end nav -->
          </div><!-- row -->
        </div><!-- headdown -->
      </header><!-- end header -->

      <?php echo $template['body']; ?>

      <footer id="footer">
        <div class="row pad_foot clearfix">
          <div class="grid_4">
            <h3 class="col-title"><?php elang('lien-he'); ?></h3>
            <!--<img src="<?php echo base_url($theme['image'].'logo-footer.png'); ?>" alt="logo title" class="mb">-->
            <div class="widget-content">
              <p style="font-size:12px;">
                <?php elang('footer-contact'); ?>
              </p>
            </div> 
          </div><!-- grid 4 -->

          <div class="grid_2">
            <h3 class="col-title"><?php elang('ho-tro'); ?></h3>
            <div class="widget-content">
              <ul>
                  <li><a href="thong-tin/chinh-sach-quy-dinh.html"><?php elang('chinh-sach-quy-dinh'); ?></a></li>
                  <li><a href="thong-tin/dieu-khoan-bao-mat.html"><?php elang('dieu-khoan-bao-mat'); ?></a></li>
                  <li><a href="thong-tin/quy-trinh-giao-dich.html"><?php elang('quy-trinh-giao-dich'); ?></a></li>
                  <li><a href="ho-tro/cau-hoi-thuong-gap.html"><?php elang('cau-hoi-thuong-gap'); ?></a></li>
                  <li><a href="ho-tro/huong-dan-nghiep-vu.html"><?php elang('huong-dan-nghiep-vu'); ?></a></li>
                  <li><a href="ty-gia-ngoai-te.html"><?php elang('ty-gia-ngoai-te'); ?></a></li>
              </ul>
            </div><!-- widget content -->
          </div><!-- grid 2 -->
          <div class="grid_2">
            <h3 class="col-title"><?php elang('dich-vu'); ?></h3>
            <div class="widget-content">
              <ul>
                  <li><a href="dich-vu/dich-vu-kieu-hoi.html"><?php elang('dich-vu-kieu-hoi'); ?></a></li>
                  <li><a href="dich-vu/thanh-toan-online.html"><?php elang('thanh-toan-online'); ?></a></li>
                  <li><a href="dich-vu/thanh-toan-offline.html"><?php elang('thanh-toan-offline'); ?></a></li>
                  <li><a href="dich-vu/di-dong-tra-truoc.html"><?php elang('di-dong-tra-truoc'); ?></a></li>
                  <li><a href="dich-vu/di-dong-tra-sau.html"><?php elang('di-dong-tra-sau'); ?></a></li>
                  <li><a href="dich-vu/mua-the-game.html"><?php elang('mua-the-game'); ?></a></li>
                  <li><a href="dich-vu/mua-the-dien-thoai.html"><?php elang('mua-the-dien-thoai'); ?></a></li>
            </div><!-- widget content -->
          </div><!-- grid 2 -->
          <div class="grid_4">
            <h3 class="col-title"><?php elang('doi-tac-dau-tu'); ?></h3>
            <div class="widget-content">
              <p><?php elang('noi-dung-doi-tac-dau-tu'); ?> ( <a href="doi-tac-dau-tu.html"<span><?php elang('xem-chi-tiet'); ?></span></a> )</p>
            </div>
            <h3 class="col-title"><?php elang('mail-tu-payway'); ?></h3>
            <div class="widget-content">
              <form id="contactForm" action="<?php echo site_url('main/newsletter'); ?>" method="post">
                <p><?php elang('nhan-email'); ?><br><br></p>
                <input type="text" class="requiredField email newsletter" placeholder="<?php elang('mail-newsletter'); ?>" title="<?php elang('mail-newsletter'); ?>" name="nl_email">
                <input type="submit" id="sendMessage" class="tbutton" value="<?php elang('theo-doi'); ?>">
              </form>
            </div>
              <div class="social" style="padding-top: 10px;float:right">
                <a href="#" class="toptip" title="Twitter"><i class="fa-twitter"></i></a>
                <a href="#" class="toptip" title="Facebook"><i class="fa-facebook"></i></a>
                <!--<a href="#" class="toptip" title="Dribbble"><i class="fa-dribbble"></i></a>
                <a href="#" class="toptip" title="Pinterest"><i class="fa-pinterest"></i></a>
                <a href="#" class="toptip" title="RSS"><i class="fa-rss"></i></a>-->
                <a href="https://www.youtube.com/paywayvn" class="toptip" title="Youtube" target="_blank"><i class="fa-youtube"></i></a>
                <!--<a href="#" class="toptip" title="SoundCloud"><i class="fa-cloud"></i></a>-->
                <a href="https://google.com/+payway-vn-the-brand-of-choice" class="toptip" title="Google Plus" target="_blank"><i class="fa-google-plus"></i></a>
              </div>
            <!-- widget content -->
          </div><!-- grid 3 -->
        </div><!-- row -->

        <div class="footer-last">
          <div class="row clearfix">
            <span class="copyright"><?php elang('footer-copyright'); ?></span>

            <div id="toTop" class="toptip" title="Back to Top"><i class="fa-angle-up"></i></div><!-- Back to top -->

            <div class="foot-menu">
              <ul>
                <li><a href="/"><?php elang('trang-chu'); ?></a></li>
                <li><a href="gioi-thieu.html"><?php elang('gioi-thieu'); ?></a></li>
                <li><a href="dich-vu.html"><?php elang('dich-vu'); ?></a></li>
                <li><a href="tin-tuc.html"><?php elang('tin-tuc'); ?></a></li>
                <li><a href="ho-tro.html"><?php elang('ho-tro'); ?></a></li>
                <li><a href="lien-he.html"><?php elang('lien-he'); ?></a></li>
              </ul><!-- end links -->
            </div><!-- end foot menu -->
          </div><!-- end row -->
        </div><!-- end last footer -->

      </footer><!-- end footer -->
    </div><!-- end layout -->
  </div><!-- end frame -->

  <div class="khuyen-mai">
    <h3><a href="#" class="hide-khuyen-mai"><?php elang('chuong-trinh-khuyen-mai'); ?></a></h3>
    <div class="show-khuyen-mai">
      <div class="menu-right-news">
          <ul>
             <?php foreach ($articles_km[1021] as $article) {
                $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
                echo ' <li><a href="'.$link.'">'.$article['name'].'</a></li>';
              } ?>
           </ul>
        </div>
    </div>
  </div>

</div>
  <!-- Scripts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42599909-11', 'payway.vn');
  ga('send', 'pageview');
</script>
</body>
</html>

