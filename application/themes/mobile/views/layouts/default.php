<!DOCTYPE HTML>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
  <meta name="apple-mobile-web-app-capable" content="yes"/>
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/splash/splash-icon.png">
  <link rel="apple-touch-startup-image" href="images/splash/splash-screen.png"      media="screen and (max-device-width: 320px)" />  
  <link rel="apple-touch-startup-image" href="images/splash/splash-screen_402x.png"     media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
  <link rel="apple-touch-startup-image" sizes="640x1096" href="images/splash/splash-screen_403x.png" />
  <link rel="apple-touch-startup-image" sizes="1024x748" href="images/splash/splash-screen-ipad-landscape" media="screen and (min-device-width : 481px) and (max-device-width : 1024px) and (orientation : landscape)" />
  <link rel="apple-touch-startup-image" sizes="768x1004" href="images/splash/splash-screen-ipad-portrait.png" media="screen and (min-device-width : 481px) and (max-device-width : 1024px) and (orientation : portrait)" />
  <link rel="apple-touch-startup-image" sizes="1536x2008" href="images/splash/splash-screen-ipad-portrait-retina.png"   media="(device-width: 768px)  and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)"/>
  <link rel="apple-touch-startup-image" sizes="1496x2048" href="images/splash/splash-screen-ipad-landscape-retina.png"   media="(device-width: 768px) and (orientation: landscape)  and (-webkit-device-pixel-ratio: 2)"/>
  <title><?php echo $template['title']; ?></title>
  <base href="<?php echo base_url(); ?>">
  <link href="<?php echo base_url($theme['path'].'styles/style.css'); ?>"         rel="stylesheet" type="text/css">
  <link href="<?php echo base_url($theme['path'].'styles/framework.css'); ?>"     rel="stylesheet" type="text/css">
  <link href="<?php echo base_url($theme['path'].'styles/owl.carousel.css'); ?>"   rel="stylesheet" type="text/css">
  <link href="<?php echo base_url($theme['path'].'styles/owl.theme.css'); ?>"     rel="stylesheet" type="text/css">
  <link href="<?php echo base_url($theme['path'].'styles/swipebox.css'); ?>"     rel="stylesheet" type="text/css">
  <link href="<?php echo base_url($theme['path'].'styles/colorbox.css'); ?>"     rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="public/templates/payway2/styles/icons.css" media="screen" />
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/jquery.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/jqueryui.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/owl.carousel.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/jquery.swipebox.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/colorbox.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/snap.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/contact.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/custom.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/framework.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url($theme['path'].'scripts/framework.launcher.js'); ?>"></script>
    <script type="text/javascript" src="public/templates/payway2/js/tn_script.js"></script>
  
</head>
<body>
<div class="all-elements">
  <div id="sidebar" class="page-sidebar">
    <div class="page-sidebar-scroll">
      <div class="sidebar-shortcuts">
        <a href="#" class="shortcut-call"></a>
        <a href="#" class="shortcut-facebook"></a>
        <a href="#" class="shortcut-twitter"></a>
        <a href="#" class="shortcut-search"></a>
        <a href="#" class="shortcut-close"></a>
      </div>
      <div class="sidebar-search">
        <div class="search-box">
          <input type="text" class="search-field" name="search" onfocus="if(this.value == 'Looking for something?') { this.value = ''; }" value="Looking for something?">
          <a href="#" class="search-close"></a>
        </div>
      </div>
      <div class="sidebar-logo">
        <img src="<?php echo base_url($theme['path'].'images/misc/logo.png'); ?>" height="52" alt="img">
      </div>
      <div class="sidebar-breadcrumb"><?php elang('danh-muc'); ?></div>
        <?php echo recursive_category_menu_mobile($category_menu); ?>
      <div class="sidebar-breadcrumb"><?php elang('thong-tin'); ?></div>
        <div class="navigation-item"><a href="/" class="nav-item"><?php elang('dang-nhap'); ?><em class="unselected-item"></em></a></div>
        <div class="sidebar-decoration"></div>
        <div class="navigation-item"><a href="/" class="nav-item"><?php elang('dang-ky'); ?><em class="unselected-item"></em></a></div>
        <div class="sidebar-decoration"></div>
        <div class="navigation-item"><a href="/" class="nav-item submenu-deploy"><?php elang('ngon-ngu'); ?><em class="dropdown-item"></em></a>
          <div class="nav-submenu">
              <a href="main/language/id/1">Việt Nam</a>
              <a href="main/language/id/2">English</a>
          </div>
        </div>
        <div class="sidebar-decoration"></div>
      <div class="sidebar-breadcrumb"><?php elang('thanh-toan-online'); ?></div>  
        <div class="dich-vu-home">
        <div class="content thanh-toan-online">
          <div class="grid_6 set_two">
            <a href="dich-vu/di-dong-tra-truoc.html">
              <div class="s_icon-2"><span class="tra-truoc"></span></div>
              <div class="s_info">
                <h3><?php elang('di-dong-tra-truoc'); ?></h3>
                <p><?php elang('noi-dung-di-dong-tra-truoc'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/di-dong-tra-sau.html">
              <div class="s_icon-2"><span class="tra-sau"></span></div>
              <div class="s_info">
                <h3><?php elang('di-dong-tra-sau'); ?></h3>
                <p><?php elang('noi-dung-di-dong-tra-sau'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/mua-the-dien-thoai.html">
              <div class="s_icon-2"><span class="the-dien-thoai"></span></div>
              <div class="s_info">
                <h3><?php elang('mua-the-dien-thoai'); ?></h3>
                <p><?php elang('noi-dung-mua-the-dien-thoai'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/mua-the-game.html">
              <div class="s_icon-2"><span class="the-game"></span></div>
              <div class="s_info">
                <h3><?php elang('mua-the-game'); ?></h3>
                <p><?php elang('noi-dung-mua-the-game'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/nap-tien-game.html">
              <div class="s_icon-2"><span class="nap-tien-game"></span></div>
              <div class="s_info">
                <h3><?php elang('nap-tien-game'); ?></h3>
                <p><?php elang('noi-dung-nap-tien-game'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/nap-tien-tai-khoan.html">
              <div class="s_icon-2"><span class="nap-tien-tai-khoan"></span></div>
              <div class="s_info">
                <h3><?php elang('nap-tien-tai-khoan'); ?></h3>
                <p><?php elang('noi-dung-nap-tien-tai-khoan'); ?></p>
              </div>
            </a>
          </div>
        </div>
      </div>
        <div class="sidebar-decoration"></div>
      <div class="sidebar-breadcrumb">COPYRIGHT 2014. ALL RIGHTS RESERVED!</div>
    </div>         
  </div>
  <div id="content" class="page-content"> 
  <div class="page-header">
    <a href="#" class="deploy-sidebar"></a>
    <img class="header-logo" src="<?php echo base_url($theme['path'].'images/misc/logo-header.png'); ?>" width="71" alt="img">
    <a href="#" class="deploy-contact"></a>
  </div>
  <div class="page-header-clear"></div>
  <?php echo $template['body']; ?>
  <div class="decoration"></div>
  <div class="footer">
    <div class="footer-socials">
      <a href="#" class="facebook-footer"></a>
      <a href="#" class="goup-footer"></a>
      <a href="#" class="twitter-footer"></a>
    </div>
    <p class="copyright uppercase center-text no-bottom">Copyright 2013<br> All rights reserved</p>        
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42599909-11', 'payway.vn');
  ga('send', 'pageview');
</script>
</body>
</html>