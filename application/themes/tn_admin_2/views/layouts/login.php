<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Login">
    <meta name="author" content="TuNgo">

    <title><?php echo $template['title']; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url($theme['image'].'icon/admin.gif'); ?>">

    <link href="<?php echo base_url($theme['css'].'reset.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($theme['css'].'bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($theme['css'].'bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($theme['css'].'login.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url($theme['js'].'html5shiv.js'); ?>"></script>
      <script src="<?php echo base_url($theme['js'].'respond.min.js'); ?>"></script>
    <![endif]-->

    <!-- javascript -->
    <script src="<?php echo base_url($theme['js'].'jquery-1.10.2.min.js'); ?>"></script>

    <?php echo $template['metadata']; ?>

  </head>

  <body>

    <div class="container">

      <?php echo $template['body']; ?>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url($theme['js'].'bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url($theme['js'].'jquery.validate.min.js'); ?>"></script>

  </body>
</html>