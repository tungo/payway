<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin">
    <meta name="author" content="TuNgo">

    <title><?php echo $template['title']; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url($theme['image'].'icon/admin.gif'); ?>">

    <!-- <link href="<?php echo base_url($theme['css'].'reset.css'); ?>" rel="stylesheet"> -->
    <link href="<?php echo base_url($theme['css'].'bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($theme['css'].'bootstrap-non-responsive.css'); ?>" rel="stylesheet">
    <!-- <link href="<?php echo base_url($theme['css'].'bootstrap-theme.min.css'); ?>" rel="stylesheet"> -->
    <link href="<?php echo base_url($theme['css'].'admin.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url($theme['js'].'html5shiv.js'); ?>"></script>
      <script src="<?php echo base_url($theme['js'].'respond.min.js'); ?>"></script>
    <![endif]-->

    <!-- javascript -->
    <script src="<?php echo base_url($theme['js'].'jquery-1.10.2.min.js'); ?>"></script>
    <script src="<?php echo base_url($theme['js'].'headscript.js'); ?>"></script>

    <?php echo $template['metadata']; ?>

  </head>

  <body>

    <!-- module -->
    <div id="backtop"><a href="#top"></a></div>
    <!-- /module -->

    <!-- header -->
    <header>
      <div class="container">

        <div class="pull-left">
          <img src="<?php echo base_url($theme['image'].'tn-admin.png'); ?>" id="logo-img">
        </div>
        <div class="pull-right">
          <div class="user-panel">
            <div class="btn-group language">
              <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                <img class="flag" src="<?php echo base_url('/public/images/flags/'.$languages[$language_id]['image']); ?>">&nbsp;
                <?php echo $languages[$language_id]['name']?>
              </a>
              <ul class="dropdown-menu" role="menu">
              <?php foreach ($languages as $language): ?>
                <li>
                  <a href="<?php echo site_url($path.'language/id/'.$language['id']); ?>">
                    <img class="flag" src="<?php echo base_url('/public/images/flags/'.$language['image']); ?>">
                    <?php echo $language['name']?>
                  </a>
                </li>
              <?php endforeach; ?>
              </ul>
            </div>
            <a class="btn btn-default btn-sm" target="_blank" href="<?php echo site_url(); ?>" title="Frontend"><span class="glyphicon glyphicon-home"></span> Frontend</a>
            <a class="btn btn-primary btn-sm" href="<?php echo site_url($path.'user_account/edit/id/'.$member['id']); ?>" title="Profile"><span class="glyphicon glyphicon-user"></span> Welcome, <?php echo $member['firstname'].' '.$member['lastname']; ?></a>
            <a class="btn btn-danger btn-sm" href="<?php echo site_url($path.'auth/logout'); ?>" title="Log out"><span class="glyphicon glyphicon-off"></span> Log out</a>
          </div>
        </div>

        <div class="clearfix"></div>

        <nav class="navbar navbar-default" role="navigation" id="menu">

          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menu-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <div class="collapse navbar-collapse navbar-menu-collapse">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo site_url($path); ?>" title="Dashboard">Dashboard</a></li>
              <li class="dropdown">
                <a href="#" title="Blog" class="dropdown-toggle" data-toggle="dropdown">
                  Blog <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'blog_article'); ?>" title="Article">Article</a></li>
                  <li><a href="<?php echo site_url($path.'blog_category'); ?>" title="Category">Category</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" title="Website" class="dropdown-toggle" data-toggle="dropdown">
                  Website <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'website_contact'); ?>" title="Contact">Contact</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" title="Module" class="dropdown-toggle" data-toggle="dropdown">
                  Module <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'module_banner'); ?>" title="Banner">Banner</a></li>
                  <li><a href="<?php echo site_url($path.'module_partner'); ?>" title="Partner">Partner</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" title="User" class="dropdown-toggle" data-toggle="dropdown">
                  User <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'user_account'); ?>" title="Account">Account</a></li>
                  <li><a href="<?php echo site_url($path.'user_group'); ?>" title="Group">Group</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" title="Payment" class="dropdown-toggle" data-toggle="dropdown">
                  Payment <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'payment_transaction'); ?>" title="Transaction">Transaction</a></li>
                </ul>
              </li>
              <!-- <li class="dropdown">
                <a href="#" title="System" class="dropdown-toggle" data-toggle="dropdown">
                  System <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url($path.'system_content'); ?>" title="Content">Content</a></li>
                  <li><a href="<?php echo site_url($path.'system_setting'); ?>" title="Setting">Setting</a></li>
                </ul>
              </li> -->
            </ul>
          </div>

        </nav>

        <script type="text/javascript">
        $(document).ready(function($) {
          var nav_link = '<?php echo current(explode('_', $controller)); ?>';
          if (nav_link == 'admin')
          {
            $('.nav').find('a[title="Dashboard"]').parent('li').addClass('active');
          }
          else
          {
            $('.nav').find('a').each(function(){
              if ($(this).prop('title').toLowerCase() == nav_link)
              {
                $(this).parent('li').addClass('active');
              }
            });
          }
        });
        </script>

      </div>
    </header><!-- /header -->

    <div class="clearfix"></div>

    <!-- content -->
    <section id="content-wrapper">
      <div class="container">

        <?php echo $template['body']; ?>

      </div>
    </section>
    <!-- /content -->

    <div class="clearfix"></div>

    <!-- footer -->
    <footer>
      <div class="container">
        <hr>
        <div class="row">
          <div class="col-xs-9">
            <p>&copy; Admin 2013</p>
          </div>
          <div class="col-xs-3">
            <p class="text-muted pull-right"><small>Developed by <abbr title="Ngô Anh Tú">Tu Ngo</abbr>.</small></p>
          </div>
        </div>
      </div>
    </footer>
    <!-- /footer -->

    <!-- javascript -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url($theme['js'].'bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url($theme['js'].'jquery.validate.min.js'); ?>"></script>
    <script src="<?php echo base_url($theme['js'].'footscript.js'); ?>"></script>

  </body>
</html>