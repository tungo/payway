<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public $data;
	public $database;
	public $table;

	public $image_path;
	public $image_url;

	public function __construct()
	{
		parent::__construct();

		$this->database = $this->config->item('database');
	}

	// Count all entries in table
	public function count_all_entries($params = array(), $options = array())
	{
		return $this->db->count_all($this->table);
	}

	// Count entries by where
	public function count_entries_by_where($params = array(), $options = array())
	{
		if (empty($params))
		{
			return FALSE;
		}
		return $this->db->where($params)->count_all_results($this->table);
	}

	// Check an entry by id - just count by id
	public function check_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}
		return $this->db->get_where($this->table, array('id' => $params['id']))->num_rows();
	}

	// Check an entry by where - just count by id
	public function check_entry_by_where($params = array(), $options = array())
	{
		if (empty($params))
		{
			return FALSE;
		}
		return $this->db->get_where($this->table, $params)->num_rows();
	}

	// Get all entries in table
	public function get_all_entries($params = array(), $options = array())
	{
		return $this->db->get($this->table)->result_array();
	}

	// Get an entry by id
	public function get_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}
		return $this->db->get_where($this->table, array('id' => $params['id']))->row_array();
	}

	// Get entries by where
	public function get_entries_by_where($params = array(), $options = array())
	{
		if (empty($params))
		{
			return FALSE;
		}
		return $this->db->get_where($this->table, $params)->result_array();
	}

	// Add an entry
	public function add_entry($params = array(), $options = array())
	{
		if (empty($this->data))
		{
			return FALSE;
		}
		return $this->db->insert($this->table, $this->data);
	}

	// Update an entry by id
	public function update_entry($params = array(), $options = array())
	{
		if (! isset($params['id']) || empty($this->data))
		{
			return FALSE;
		}
		return $this->db->where('id', $params['id'])->update($this->table, $this->data);
	}

	// Update entries by where
	public function update_entries_by_where($params = array(), $options = array())
	{
		if (empty($params) || empty($this->data))
		{
			return FALSE;
		}
		return $this->db->where($params)->update($this->table, $this->data);
	}

	// Delete an entry by id
	public function delete_entry($params = array(), $options = array())
	{
		if (! isset($params['id']))
		{
			return FALSE;
		}
		return $this->db->where('id', $params['id'])->delete($this->table);
	}

	// Delete entries by where
	public function delete_entries_by_where($params = array(), $options = array())
	{
		if (empty($params))
		{
			return FALSE;
		}
		return $this->db->where($params)->delete($this->table);
	}

	// Delete selected entries by array of id
	public function delete_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$this->db->where_in('id', $cid);
			return $this->db->delete($this->table);
		}
		return FALSE;
	}

	// copy multi entries
	public function copy_multi_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		if (count($cid) > 0)
		{
			$data = array();
			foreach ($cid as $id)
			{
				if ($this->check_entry(array('id' => $id)))
				{
					$row = (array) $this->get_entry(array('id' => $id));
					unset($row['id']);
					$data[] = $row;
				}
			}
			return $this->db->insert_batch($this->table, $data);
		}
		return FALSE;
	}

	public function save_entries($params = array(), $options = array())
	{
		$hid = (isset($params['hid'])) ? $params['hid'] : array();

		// $options['field'] are fields in manage table
		if (! isset($options['field']) || ! is_array($options['field']))
		{
			return FALSE;
		}
		foreach ($options['field'] as $field => $parse)
		{
			if (! isset($params[$field]))
			{
				return FALSE;
			}
			switch ($parse)
			{
				case 'int':
					foreach ($params[$field] as $key => $val)
					{
						$params[$field][$key] = (int) $val;
					}
					break;
			}
		}

		if (count($hid) > 0)
		{
			$data = array();
			foreach ($hid as $id)
			{
				$fields = array();
				foreach ($options['field'] as $field => $parse)
				{
					$fields[$field] = $params[$field][$id];
				}
				$fields = array_merge(array('id' => $id), $fields);
				$data[] = $fields;
			}
			return $this->db->update_batch($this->table, $data, 'id');
		}
		return FALSE;
	}

	// // Save change of selected entries by array of id from manage table
	// // if more field changed use 'save_entries' from controller's model
	// public function save_entries($params = array(), $options = array())
	// {
	// 	if (! isset($params['cid']))
	// 	{
	// 		return FALSE;
	// 	}

	// 	$position = (isset($params['position'])) ? $params['position'] : array();

	// 	if (count($cid) > 0)
	// 	{
	// 		$data = array();
	// 		foreach ($cid as $id)
	// 		{
	// 			$data[] = array(
	// 				'id' => $id,
	// 				'position' => $position[$id]
	// 			);
	// 		}
	// 		return $this->db->update_batch($this->table, $data, 'id');
	// 	}
	// 	return FALSE;
	// }

	// Change status of a entry
	public function change_status_entry($params = array(), $options = array())
	{
		$status = isset($params['status']) ? (bool) $params['status'] : 0;
		$this->data = array('status' => $status);

		if ($params['id'] > 0)
		{
			return $this->db->where('id', $params['id'])->update($this->table, $this->data);
		}
		return FALSE;
	}

	// Change status of seleted entries to active
	public function active_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		$this->data = array('status' => 1);

		if (count($cid) > 0)
		{
			return $this->db->where_in('id', $cid)->update($this->table, $this->data);
		}
		return FALSE;
	}

	// Change status of seleted entries to disable
	public function disable_entries($params = array(), $options = array())
	{
		$cid = (isset($params['cid'])) ? $params['cid'] : array();

		$this->data = array('status' => 0);

		if (count($cid) > 0)
		{
			return $this->db->where_in('id', $cid)->update($this->table, $this->data);
		}
		return FALSE;
	}

	// check unique field
	public function check_unique($params = array(), $options = array())
	{
		if (! isset($params['field']) || ! isset($params['value']))
		{
			return FALSE;
		}
		if (isset($params['old']))
		{
			$this->db->where(array($params['field'].' !=' => $params['old']));
		}
		$this->db->where(array($params['field'] => $params['value']));
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	// not today :)
	public function check_multi_unique($params = array(), $options = array())
	{
		if (empty($params))
		{
			return FALSE;
		}
		$query = $this->db->get_where($this->table, $params);
		return $query->num_rows();
	}

	// Call & cache result of a function in model
	public function cache_function($function = '', $params = array(), $options = array())
	{
		// cache $function($param, $options)
		// return result of function
	}
}