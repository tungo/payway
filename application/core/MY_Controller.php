<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

  public $data = array();

  public function __construct()
  {
    parent::__construct();
    $this->data['module']     = $this->router->fetch_module();
    $this->data['controller'] = $this->router->fetch_class();
    $this->data['method']     = $this->router->fetch_method();

    // app
    if ( ! $this->data['module'])
    {
      $this->data['current_controller'] = $this->data['controller'].'/';
      $this->data['main_method']        = $this->data['controller'].'/index/';
      $this->data['namespace']          = '-'.$this->data['controller'];
      $this->data['request']            = $this->uri->uri_to_assoc(3);
    }
    // hmvc
    else
    {
      $this->data['current_controller'] = $this->data['module'].'/'.$this->data['controller'].'/';
      $this->data['main_method']        = $this->data['module'].'/'.$this->data['controller'].'/index/';
      $this->data['namespace']          = $this->data['module'].'-'.$this->data['controller'];
      $this->data['request']            = $this->uri->uri_to_assoc(4);
    }

    // params: get & post
    /*$this->data['params'] = array();
    $post = (is_array($this->input->post())) ? $this->input->post() : array();
    $get  = (is_array($this->input->get()))  ? $this->input->get()  : array();
    foreach($get as $key => $value)
    {
      $this->data['params'][$key] = $value;
    }
    foreach($post as $key => $value)
    {
      $this->data['params'][$key] = $value;
    }*/
    // params: only post
    $post = (is_array($this->input->post())) ? $this->input->post() : array();
    $this->data['params'] = $post;

    $this->load->library('Template');
  }

  // Load view - Template library
  protected function load_view($view = '', $config = array(), $return = FALSE)
  {
    if (isset($config['theme']['name']))
    {
      $this->template->set_theme((string) $config['theme']['name']);
    }
    if (isset($config['title']))
    {
      $this->template->title((string)$config['title']);
    }
    if (isset($config['theme']['layout']))
    {
      $this->template->set_layout((string) $config['theme']['layout']);
    }
    $this->template->build($config['theme']['name'].'/'.$view, $this->data, $return);
  }

  // Check & set language is being used for home site (default)
  protected function init_language($session = 'tn_language', $file = 'tn')
  {
    if ($session != '' && $file != '')
    {
      $this->load->library('TN_Language');
      $this->data['languages'] = $this->tn_language->initialize($session);
      $this->data['language_id'] = $this->session->userdata($session);

      // setlocale(LC_MONETARY, $this->data['languages'][$this->data['language_id']]['locale']);
      $this->config->set_item('language', $this->data['languages'][$this->data['language_id']]['directory']);
      $this->load->language($file);
      return true;
    }
    return false;
  }
}