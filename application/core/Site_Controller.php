<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

    $this->load->helper(array('tn','cms'));

    $this->data['version'] = 'site';
    if(tn_is_mobile())
        $this->data['version'] = 'mobile';

    if($this->session->userdata('version') == 'mobile')
        $this->data['version'] = 'mobile';
    elseif($this->session->userdata('version') == 'desktop')
        $this->data['version'] = 'site';

    init_module_config($this->config->item($this->data['version']), $this->data);

    $this->load->library('TN_Message');
    $this->tn_message->layout = 'site';

    $this->init_language($this->data['language_session'], $this->data['language_file']);

    $this->data['params']['language_id'] = $this->data['language_id'];

    $this->load->model('database/blog_category_model');
    $this->data['category_menu'] = $this->blog_category_model->get_entries_site_menu($this->data['params']);
    $this->data['category_list'] = $this->blog_category_model->get_entries_site_list($this->data['params']);

    $this->load->model('database/blog_article_model');

    $this->load->model('database/module_banner_model');
    $this->data['banner'] = $this->module_banner_model->get_entries_site($this->data['params']);
    $this->data['banner_image_url'] = $this->module_banner_model->image_url;

    $this->data['title'] = 'PayWay';

	}
}