<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->helper(array('tn','cms'));
    init_module_config($this->config->item('admin'), $this->data);

    $this->load->library('TN_Message');

    $this->load->library(array('TN_Auth', 'TN_Acl'));
    $this->authorize();
    $this->init_language($this->data['language_session'], $this->data['language_file']);
  }

  public function authorize()
  {
    $this->check_login();
    $this->check_access();
    $this->check_privilege();
  }

  public function check_login()
  {
    // auto login
    if (!$this->tn_auth->is_logged_in() && $this->tn_auth->is_remembered())
      $this->tn_auth->login_remembered_user();

    // redirect if not login
    if (!$this->tn_auth->is_logged_in())
    {
      // exclude login page
      if ($this->data['controller'] != 'auth' && $this->data['method'] != 'login')
        redirect($this->data['path'].'auth/login');
    }
    else
    {
      if ($this->tn_auth->check_login())
        $this->data['member'] = $this->session->userdata('member');
      else
        redirect($this->data['path'].'auth/login');
    }
  }

  // check access system
  protected function check_access()
  {
    // can not access panel
    if ($this->tn_auth->is_logged_in() && !$this->tn_auth->check_access())
      // exclude auth page & logout page
      if ($this->data['controller'] != 'auth' || ($this->data['method'] != 'index' && $this->data['method'] != 'logout'))
        redirect($this->data['path'].'auth');
  }

  // Check access control list for user
  protected function check_privilege()
  {
    // check logged in member
    if ($this->tn_auth->is_logged_in())
      // group permission -> full privilege
      if (!$this->data['member']['group_permission'])
        // exclude admin page & auth page
        if ($this->data['controller'] != 'admin' && $this->data['controller'] != 'auth')
        {
          // init acl, dispatch like "module-controller-method"
          $this->data['acl'] = $this->tn_acl->init_acl($this->data['member']['group_id']);
          $this->data['dispatch'] = $this->data['module'].'-'.$this->data['controller'].'-'.$this->data['method'];

          // redirect if user has no privilge
          if (!$this->tn_acl->check_privilege($this->data['dispatch'], $this->data['acl']))
          {
            $this->tn_message->set_error('Your account can not access this page.');
            $this->tn_message->save();
            redirect($this->data['path']);
          }
        }
  }
}