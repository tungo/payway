<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * reCaptcha File Config
 *
 * File     : recaptcha.php
 * Created  : May 14, 2013 | 4:22:40 PM
 * 
 * Author   : Andi Irwandi Langgara <irwandi@ourbluecode.com>
 */

$config['public_key']   = '6LeRne4SAAAAALvWHfMj0L8yBLUGsovebVFY_bo2';
$config['private_key']  = '6LeRne4SAAAAAJOv9GpCQNdmOdcJhIhQ27GLlm8-';
// Set Recaptcha theme, default red (red/white/blackglass/clean)
$config['recaptcha_theme']  = 'clean';

?>
