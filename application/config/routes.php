<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'main';
$route['404_override'] = 'main/error_404';

$route['sitemap\.xml'] = "main/sitemap";

$route['admin/(:any)'] = 'admin/$1';
$route['admin']        = 'admin';

$route['main/(:any)'] = 'main/$1';
$route['main']        = 'main';

$route['member/(:any)'] = 'member/$1';
$route['member']        = 'member';

// dich vu
$route['dich-vu/(:any)'] = 'dich-vu/$1';
$route['dich-vu']        = 'dich-vu';

// khac
$route['gioi-thieu']      = 'main/gioi-thieu';
$route['ho-tro']          = 'main/ho-tro';
$route['tim-kiem']        = 'main/tim-kiem';
$route['lien-he']         = 'main/contact';
$route['ty-gia-ngoai-te'] = 'main/exchange-rate';
$route['doi-tac-dau-tu']  = 'main/partner';

// bai viet
$route['([a-z0-9-]+)/trang/([0-9]+)'] = "main/category/$1";
$route['([a-z0-9-]+)/([a-z0-9-]+)']   = "main/article/$1";
$route['([a-z0-9-]+)']                = "main/category/$1";

// mac dinh
$route['(:any)'] = 'main/error_404';


/* End of file routes.php */
/* Location: ./application/config/routes.php */