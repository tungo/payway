<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// pagination library

// using layout
// defined before layouts
// 'default': default layout in library
$config['tn_pagination']['layout'] = 'admin';


//----- other config
// co the cau hinh thuoc tinh mac dinh dung chung cho toan bo layout
// vi du: ca giao dien admin, site, ... deu su dung chung ki hieu first prev next last, do dai so trang se hien thi, ...

$config['tn_pagination']['show_disable'] = FALSE;


//----- defind your layout
// $config['tn_pagination']['layouts']['your_layout'] (your_layout != default)

// admin
$config['tn_pagination']['layouts']['admin']['per_page']  = 10;
$config['tn_pagination']['layouts']['admin']['num_links'] = 4;

// site
$config['tn_pagination']['layouts']['site']['per_page']  = 5;
$config['tn_pagination']['layouts']['site']['num_links'] = 4;
$config['tn_pagination']['layouts']['site']['page_string']  = 'trang';

$config['tn_pagination']['layouts']['site']['full_tag_open']           = '<ul>';
$config['tn_pagination']['layouts']['site']['full_tag_close']          = '</ul>';

$config['tn_pagination']['layouts']['site']['first_link']              = '<i class="arrow_carrot-2left"></i>';
$config['tn_pagination']['layouts']['site']['first_link_attr']         = '';
$config['tn_pagination']['layouts']['site']['first_tag_open']          = '<li>';
$config['tn_pagination']['layouts']['site']['first_tag_close']         = '</li> ';
$config['tn_pagination']['layouts']['site']['first_disable_tag_open']  = '';
$config['tn_pagination']['layouts']['site']['first_disable_tag_close'] = '';

$config['tn_pagination']['layouts']['site']['prev_link']               = '<i class="arrow_carrot-left"></i>';
$config['tn_pagination']['layouts']['site']['prev_link_attr']          = '';
$config['tn_pagination']['layouts']['site']['prev_tag_open']           = '<li>';
$config['tn_pagination']['layouts']['site']['prev_tag_close']          = '</li> ';
$config['tn_pagination']['layouts']['site']['prev_disable_tag_open']   = '';
$config['tn_pagination']['layouts']['site']['prev_disable_tag_close']  = '';

$config['tn_pagination']['layouts']['site']['next_link']               = '<i class="arrow_carrot-right"></i>';
$config['tn_pagination']['layouts']['site']['next_link_attr']          = '';
$config['tn_pagination']['layouts']['site']['next_tag_open']           = '<li>';
$config['tn_pagination']['layouts']['site']['next_tag_close']          = '</li> ';
$config['tn_pagination']['layouts']['site']['next_disable_tag_open']   = '';
$config['tn_pagination']['layouts']['site']['next_disable_tag_close']  = '';

$config['tn_pagination']['layouts']['site']['last_link']               = '<i class="arrow_carrot-2right"></i>';
$config['tn_pagination']['layouts']['site']['last_link_attr']          = '';
$config['tn_pagination']['layouts']['site']['last_tag_open']           = '<li>';
$config['tn_pagination']['layouts']['site']['last_tag_close']          = '</li> ';
$config['tn_pagination']['layouts']['site']['last_disable_tag_open']   = '';
$config['tn_pagination']['layouts']['site']['last_disable_tag_close']  = '';

$config['tn_pagination']['layouts']['site']['cur_link']                = ''; // after current link
$config['tn_pagination']['layouts']['site']['cur_link_attr']           = '';
$config['tn_pagination']['layouts']['site']['cur_tag_open']            = '<li class="current"><span>';
$config['tn_pagination']['layouts']['site']['cur_tag_close']           = '</span></li> ';

$config['tn_pagination']['layouts']['site']['num_link']                = ''; // after number link
$config['tn_pagination']['layouts']['site']['num_link_attr']           = '';
$config['tn_pagination']['layouts']['site']['num_tag_open']            = '<li>';
$config['tn_pagination']['layouts']['site']['num_tag_close']           = '</li> ';

// tim kiem o trang chu
$config['tn_pagination']['layouts']['site_search'] = $config['tn_pagination']['layouts']['site'];
$config['tn_pagination']['layouts']['site_search']['per_page']   = 5;
$config['tn_pagination']['layouts']['site_search']['num_links']  = 4;
$config['tn_pagination']['layouts']['site_search']['page_query'] = TRUE;

// transaction_history
$config['tn_pagination']['layouts']['trans']['per_page']  = 10;
$config['tn_pagination']['layouts']['trans']['num_links'] = 4;
$config['tn_pagination']['layouts']['trans']['page_string']  = 'trang';
