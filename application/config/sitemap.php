<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// khai báo link tĩnh
$config['sitemap'] = array(

  // main controller
  'gioi-thieu.html',
  'ho-tro.html',
  'lien-he.html',
  'ty-gia-ngoai-te.html',
  'doi-tac-dau-tu.html',

  // member controller
  'member/login.html',
  'member/register.html',
  'member/reset_password.html',

  // dich_vu controller
  'dich-vu.html',
  'dich-vu/dich-vu-kieu-hoi.html',
  'dich-vu/thanh-toan-online.html',
  'dich-vu/thanh-toan-offline.html',
  'dich-vu/dich-vu-khac.html',

  'dich-vu/di-dong-tra-truoc.html',
  'dich-vu/di-dong-tra-sau.html',
  'dich-vu/mua-the-dien-thoai.html',
  'dich-vu/mua-the-game.html',
  'dich-vu/nap-tien-game.html',
  'dich-vu/nap-tien-tai-khoan.html',
  'dich-vu/ve-may-bay.html',
  'dich-vu/hoa-don-adsl.html',
  'dich-vu/tien-dien.html',

);
