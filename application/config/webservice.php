<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// banknetvn paygate
$config['ws_bnvn_gate'] = array(
  'url'           => 'https://online.banknetvn.com.vn/pg/wsdl/PaymentGateway.wsdl',
  'merchant_code' => '010030',
  'trans_key'     => '11d39bdb54d04dc20056dbd8313ed053',
  'country_code'  => '+84'
);

// vnpayment - vnpay paygate
$config['ws_vnpay_gate'] = array(
  'url'           => 'https://www.vnpayment.vn/merchantsvc/merchantsvc.asmx?WSDL',
  'terminal_code' => '10003004',
  'secret_key'    => '70832E918DCF4F9C6D64D07C537D2E23'
);

// vtc payment
$config['ws_vtc_pay'] = array(
  'url'            => 'https://pay.vtc.vn/ws/GoodsPaygate.asmx?WSDL',
  'partner_code'   => 'eWay',
  'triple_des_key' => '329797490',
  'rsa_private_path' => APPPATH.'libraries/rsa/rsa_privateKey.pem',
  'rsa_vtc_public_path' => APPPATH.'libraries/rsa/VTCAPI_eWay_publicKey_1.pem',
);

// ----- TEST -----
// // banknetvn paygate
// $config['ws_bnvn_gate'] = array(
//   'url'           => 'https://online.banknetvn.com.vn/pg/wsdl/PaymentGateway.wsdl',
//   'merchant_code' => '010030',
//   'trans_key'     => '11d39bdb54d04dc20056dbd8313ed053',
//   'country_code'  => '+84'
// );

// // vnpayment - vnpay paygate
// $config['ws_vnpay_gate'] = array(
// 'url'           => 'http://demo.vnpayment.vn/MerchantSVC/MerchantSVC.asmx?WSDL',
// 'terminal_code' => '10006001',
// 'secret_key'    => 'D9256F32E6E7DDBA1E86A36426E80642'
// );

// // vtc payment
// $config['ws_vtc_pay'] = array(
//   'url'            => 'http://117.103.192.46/PayAPI/WS/GoodsPaygate.asmx?WSDL',
//   'partner_code'   => 'eway',
//   'triple_des_key' => '123456789',
//   'rsa_private_path' => APPPATH.'libraries/rsa/rsa_privateKey.pem',
//   'rsa_vtc_public_path' => APPPATH.'libraries/rsa/VTCAPI_eway_publicKey_demo.pem',
// );