<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How long to remember the user (seconds). Set to zero for no expiration
$config['remember_expire'] = (60*60*24*90); //90 days

// images upload path, url
$config['image_path'] = array(
  'blog_category' => 'blog_category/',
  'blog_article' => 'blog_article/',
  'module_partner' => 'module_partner/',
  'module_banner' => 'module_banner/',
);

// images upload config
$config['image_upload'] = array(
  'allowed_types' => 'gif|jpg|jpeg|png',
  'max_size' => '4096',
  'max_width' => '2048',
  'max_height' => '1024'
);

//----- Admin module config
$config['admin'] = array(
  // admin page name
  'path' => '/admin/',
  // language session
  'language_session' => 'tn_language',
  'language_file' => 'tn',
  // website themes
  'theme' => array(
    'name'   => 'tn_admin_2',
    'layout' => 'default',
    'path'   => '/public/templates/tn_admin_2/',
    'css'    => '/public/templates/tn_admin_2/css/',
    'js'     => '/public/templates/tn_admin_2/js/',
    'image'  => '/public/templates/tn_admin_2/images/'
  )
);

$config['site'] = array(
  'path' => '',

  'language_session' => 'tn_language',
  'language_file' => 'tn',

  'theme' => array(
    'name'   => 'payway',
    'layout' => 'default',
    'path'   => '/public/templates/payway/',
    'css'    => '/public/templates/payway/css/',
    'js'     => '/public/templates/payway/js/',
    'image'  => '/public/templates/payway/images/'
  ),
);

$config['mobile'] = array(
  'path' => '',

  'language_session' => 'tn_language',
  'language_file' => 'tn',

  'theme' => array(
    'name'   => 'mobile',
    'layout' => 'default',
    'path'   => '/public/templates/mobile/',
    'css'    => '/public/templates/mobile/styles/',
    'js'     => '/public/templates/mobile/js/',
    'image'  => '/public/templates/mobile/images/'
  ),
);

$config['email_smtp'] = array(
  'protocol' => 'mail',
  'mailtype' => 'text',
  'charset'  => 'utf-8',
  'wordwrap' => false,
  'newline' => '\r\n',
);

$config['vtc_pay_service_code'] = array(
  DI_DONG_TRA_TRUOC => array(
    'VTC0058' => array(
      'nhacungcap' => 'Mobifone',
      'dauso' => array('090', '093', '0120', '0121', '0122', '0126', '0128'),
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
      // 'disabled' => true,
    ),
    'VTC0056' => array(
      'nhacungcap' => 'Viettel',
      'dauso' => array('096', '097', '098', '0162', '0163', '0164', '0165', '0166', '0167', '0168', '0169'),
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.98,
    ),
    'VTC0057' => array(
      'nhacungcap' => 'Vinaphone',
      'dauso' => array('091', '094', '0123', '0124', '0125', '0127', '0129'),
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0176' => array(
      'nhacungcap' => 'Vietnamobile',
      'dauso' => array('092', '0186', '0188'),
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.96,
    ),
    'VTC0177' => array(
      'nhacungcap' => 'Gmobile',
      'dauso' => array('099', '0199'),
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
    ),
  ),
  DI_DONG_TRA_SAU => array(
    'VTC0130' => array(
      'nhacungcap' => 'Mobifone',
      'dauso' => array('090', '093', '0120', '0121', '0122', '0126', '0128'),
      'giamgia' => 0.97,
      // 'disabled' => true,
    ),
    'VTC0329' => array(
      'nhacungcap' => 'Viettel',
      'dauso' => array('096', '097', '098', '0162', '0163', '0164', '0165', '0166', '0167', '0168', '0169'),
      'giamgia' => 0.98,
    ),
    'VTC0201' => array(
      'nhacungcap' => 'Vinaphone',
      'dauso' => array('091', '094', '0123', '0124', '0125', '0127', '0129'),
      'giamgia' => 0.97,
    ),
  ),
  MUA_THE_DIEN_THOAI => array(
    'VTC0029' => array(
      'nhacungcap' => 'Mobifone',
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0027' => array(
      'nhacungcap' => 'Viettel',
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.98,
    ),
    'VTC0028' => array(
      'nhacungcap' => 'Vinaphone',
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0030' => array(
      'nhacungcap' => 'Sfone',
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.93,
    ),
    'VTC0154' => array(
      'nhacungcap' => 'Vietnamobile',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.96,
    ),
    'VTC0173' => array(
      'nhacungcap' => 'Gmobile',
      'menhgia' => array('10000', '20000', '30000', '50000', '100000', '200000', '300000', '500000'),
      'giamgia' => 0.97,
    ),
  ),
  MUA_THE_GAME => array(
    'VTC0114' => array(
      'nhacungcap' => 'VCoin',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '300000', '500000', '1000000'),
      'giamgia' => 0.96,
    ),
    'VTC0067' => array(
      'nhacungcap' => 'Zing Xu',
      'menhgia' => array('20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.96,
    ),
    'VTC0068' => array(
      'nhacungcap' => 'Gate',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0319' => array(
      'nhacungcap' => 'Garena',
      'menhgia' => array('20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.94,
    ),
    'VTC0166' => array(
      'nhacungcap' => 'OnCash',
      'menhgia' => array('20000', '60000', '100000', '200000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0099' => array(
      'nhacungcap' => 'Softnyx',
      'menhgia' => array('63000', '105000', '208110'),
      'giamgia' => 0.88,
    ),
    'VTC0185' => array(
      'nhacungcap' => 'MyCard',
      'menhgia' => array('49000', '146500', '330000', '422000', '930000'),
      'giamgia' => 0.93,
    ),
    'VTC0321' => array(
      'nhacungcap' => 'Like',
      'menhgia' => array('100000', '500000'),
      'giamgia' => 0.94,
    ),
    'VTC0171' => array(
      'nhacungcap' => 'CD KEY 7554',
      'menhgia' => array('125000'),
      'giamgia' => 0.8,
    ),
    'VTC0144' => array(
      'nhacungcap' => 'Key Bitdefender',
      'menhgia' => array('230000'),
      'giamgia' => 0.9,
    ),
  ),
  NAP_TIEN_GAME => array(
    'VTC0115' => array(
      'nhacungcap' => 'Vcoin',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '300000', '500000', '1000000'),
      'giamgia' => 0.95,
      'disabled' => true,
    ),
    'VTC0307' => array(
      'nhacungcap' => 'Xu (Zing)',
      'menhgia' => array('10000', '20000', '50000', '60000', '100000', '120000', '200000', '500000'),
      'giamgia' => 0.95,
    ),
    'VTC0187' => array(
      'nhacungcap' => 'Bạc (Gate)',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.97,
    ),
    'VTC0048' => array(
      'nhacungcap' => '@Point - Độc Bá Giang Hồ (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0049' => array(
      'nhacungcap' => '@Point - HuangYi (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0050' => array(
      'nhacungcap' => '@Point - Ragnarok (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0051' => array(
      'nhacungcap' => '@Point - Tam Quốc Chí (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0052' => array(
      'nhacungcap' => '@Point - Cabal Online (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0053' => array(
      'nhacungcap' => '@Point - PlayPark (AsiaSoft)',
      'menhgia' => array('20000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0209' => array(
      'nhacungcap' => 'mCash',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.92,
    ),
    'VTC0308' => array(
      'nhacungcap' => 'OnCash',
      'menhgia' => array('10000', '20000', '50000', '100000', '200000', '500000'),
      'giamgia' => 0.97,
    ),
  ),
  NAP_TIEN_TAI_KHOAN => array(
    'VTC0040' => array(
      'nhacungcap' => 'Hocmai.vn',
      'menhgia' => array('30000', '50000', '100000'),
      'giamgia' => 0.92,
    ),
    'VTC0193' => array(
      'nhacungcap' => 'Gia hạn truyền hình số VTC',
      'menhgia' => array('400000', '650000', '720000', '1200000'),
      'giamgia' => 0.97,
    ),
  ),
);

$config['bnvn_paygate_bank_code'] = array(
  '970488' => 'BIDV',
  '161087' => 'Saigon Bank',
  '970428' => 'Nam A Bank',
  '970489' => 'Vietinbank',
  '970499' => 'Agribank',
  '970459' => 'Anbinh Bank',
  '970403' => 'Sacombank',
  '970430' => 'PG Bank',
  '970408' => 'GP.Bank',
  '970414' => 'OceanBank',
);

$config['vnpay_paygate_bank_code'] = array(
  'VNMART'      => 'Ví điện tử VnMart',
  'VIETINBANK'  => 'Ngân hàng VietinBank',
  'DONGABANK'   => 'Ngân hàng Đông Á',
  'BIDV'        => 'Ngân hàng BIDV',
  'SACOMBANK'   => 'Ngân hàng SacomBank',
  'MSBANK'      => 'Ngân hàng MaritimeBank',
  'VIETCOMBANK' => 'Ngân hàng Vietcombank',
  'OJB'         => 'ngân hàng OceanBank',
  'TECHCOMBANK' => 'Ngân hàng TechcomBank',
  'HDBANK'      => 'Ngân hàng HD Bank',
);

$config['trans_status'] = array(
  GD_KHOI_TAO,
  GD_THANH_TOAN_TC,
  GD_THANH_TOAN_TB,
  GD_XU_LY_TC,
  GD_XU_LY_TB,
  GD_XAC_NHAN_TC,
  GD_XAC_NHAN_TB,
  GD_HOAN_TIEN_TC,
  GD_HOAN_TIEN_TB,
  GD_KO_XAC_DINH,
  GD_LOI,
);

$config['trans_member_status'] = array(
  GD_MB_KHOI_TAO,
  GD_MB_HOAN_TAT,
  GD_MB_THAT_BAI,
  GD_MB_DA_HOAN_TIEN,
  GD_MB_HOAN_TIEN_TB,
  GD_MB_LOI,
);

$config['trans_error'] = array(
  '1011' => 'khong-tim-thay-giao-dich',
  '1012' => 'khong-the-kiem-tra-trang-thai-giao-dich',
  '1013' => 'khong-dung-trang-giao-dich',
);