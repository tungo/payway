<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// messages library

// type
// 0 (default): warning, 1: error, 2: success, 3: info

//----- using layout
// defined before layouts
// 'default': default layout in library
// $config['tn_message']['layout'] = 'bootstrap';


//----- other config
// get flash session?
// $config['tn_message']['flash'] = FALSE;


// ----- defind your layout
// $config['tn_message']['layouts']['your_layout'] (your_layout != default)

// bootstrap
// $config['tn_message']['layouts']['bootstrap'] = array(
//   '<div class="alert alert-dismissable alert-warning">
//     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//     [message]
//   </div>',
//   '<div class="alert alert-dismissable alert-danger">
//     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//     [message]
//   </div>',
//   '<div class="alert alert-dismissable alert-success">
//     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//     [message]
//   </div>',
//   '<div class="alert alert-dismissable alert-info">
//     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//     [message]
//   </div>',
// );

$config['tn_message']['layouts']['site'] = array(
  '<div class="notification-box notification-box-warning">[message]</div>',
  '<div class="notification-box notification-box-error">[message]</div>',
  '<div class="notification-box notification-box-success">[message]</div>',
  '<div class="notification-box notification-box-info">[message]</div>',
);
