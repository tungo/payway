<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('ho-tro'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('ho-tro'); ?></li>
      </ul>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="clearfix mbs ho-tro">
      <div class="grid_6">
        <div class="bg">
          <h3><?php elang('cau-hoi-thuong-gap'); ?></h3>
          <p>Pay Way là gì?
            <br>Làm thế nào để tìm được địa điểm chuyển tiền của Pay Way?
            <br>Đối tượng sử dụng dịch vụ của Pay Way?Vì sao chọn Pay Way?</p>
          <div class="right"><a href="ho-tro/cau-hoi-thuong-gap.html" class="view"><?php elang('xem-chi-tiet'); ?></a></div>
        </div>
      </div>
      <div class="grid_6">
        <div class="bg">
          <h3><?php elang('huong-dan-nghiep-vu'); ?></h3>
          <p>1. SMS Banking<br>
           2. Ứng dụng Mobile Banking<br>
           3. Thanh toán hóa đơn qua ngân hàng<br></p>
          <div class="right"><a href="ho-tro/huong-dan-nghiep-vu.html" class="view"><?php elang('xem-chi-tiet'); ?></a></div>
        </div>
      </div>
      <div class="grid_6">
        <div class="bg">
          <h3><?php elang('huong-dan-su-dung'); ?></h3>
          <ul>
            <li><a href="ho-tro/dia-diem-chuyen-tien.html">Địa Điểm Chuyển Tiền</a></li>
            <li><a href="ho-tro/dia-diem-nhan-tien.html">Địa Điểm Nhận Tiền</a></li>
            <li><a href="ho-tro/bieu-phi-dich-vu.html">Biểu Phí Dịch Vụ</a></li>
            <li><a href="ho-tro/cach-nhan-tien.html">Cách Nhận Tiền</a></li>
            <li><a href="ho-tro/cach-chuyen-tien.html">Cách Chuyển Tiền</a></li>
          </ul>
        </div>
      </div>
      <div class="grid_6">
        <div class="bg">
          <h3><?php elang('ty-gia-ngoai-te'); ?></h3>
          <p>Tỷ giá ngoại tệ USD, AUD, HKD, EUR, JPY, RUB, THB.... Được cập nhật theo tỷ giá của ngân hàng Vietcombank</p>
          <div class="right"><a href="ho-tro/ty-gia-ngoai-te.html" class="view"><?php elang('xem-chi-tiet'); ?></a></div>
        </div>
      </div>
    </div>
  </div>
</div>