
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('lien-he'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('lien-he'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->
<div style="position: absolute;width:100%;left:0px;">
  <script src="https://maps.googleapis.com/maps/api/js?sensor=false&v=3.exp"></script>
  <script src="https://payway.vn/public/map/js/jquery.gmap.min.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#responsive_map").gMap({
       maptype: google.maps.MapTypeId.ROADMAP, 
       zoom: 17, 
       markers: [{
         latitude: 10.791700535523804, 
         longitude: 106.67273462698358, 
         html: "<img src='https://payway.vn/public/templates/payway2/images/logo.png' width='147' height='42'><br/><font size='2'>401 Huỳnh Văn Bánh - P11 - Q. Phú Nhuận - TP.HCM</font>", 
         popup: true, 
         flat: true, 
         icon: { 
           image: "https://payway.vn/public/map/icons/ancienttemple.png", 
           iconsize: [35, 37], 
           iconanchor: [15, 30], 
           shadow: "https://payway.vn/public/map/icons/icon-shadow.png", 
           shadowsize: [35, 37], 
           shadowanchor: null}
          } 
        ], 
       panControl: false, 
       zoomControl: false, 
       mapTypeControl: false, 
       scaleControl: false, 
       streetViewControl: false, 
       scrollwheel: true, 
       styles: [ { "stylers": [ { "hue": "#00a2ff" }, { "gamma": 1.58 } ] } ], 
       onComplete: function() {
         // Resize and re-center the map on window resize event
         var gmap = $("#responsive_map").data('gmap').gmap;
         window.onresize = function(){
           google.maps.event.trigger(gmap, 'resize');
           $("#responsive_map").gMap('fixAfterResize');
         };
      }
    });
  });
  </script>
  <div id="responsive_map"></div>
  <style type="text/css">
  #responsive_map {height: 360px; width: 100%;}
  #responsive_map div {-webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;}
  .gm-style-iw {max-width: none !important; min-width: none !important; max-height: none !important; min-height: none !important; overflow-y: hidden !important; overflow-x: hidden !important; line-height: normal !important; padding: 5px !important; }
  </style>
</div>
<div class="page-content">
  <div class="row clearfix lienhe">
    <div class="grid_8 contact_c">
      <h3 class="col-title"><?php elang('lien-he'); ?></h3>
      <p><?php elang('contact-ghi-chu'); ?></p>

      <form method="post" id="contactForm" action="">

        <input type="text" name="senderName" id="senderName" placeholder="Name *" class="requiredField">

        <div class="clearfix">
          <div class="grid_6 alpha fll">
            <input type="text" name="senderPhone" id="senderPhone" placeholder="Phone *" class="requiredField">
          </div>
          <div class="grid_6 omega flr">
            <input type="text" name="senderEmail" id="senderEmail" placeholder="Email Address *" class="requiredField email">
          </div>
        </div>
        <div>
          <textarea name="message" id="message" placeholder="Message *" class="requiredField" rows="8"></textarea>
        </div>
        <input type="submit" id="sendMessage" name="sendMessage" value="Send Email">
        <span></span>
      </form>

    </div><!-- grid8 -->

    <div class="grid_4 sidebar">
      <div class="widget">
        <p><?php elang('contact-info'); ?></p>
      </div><!-- widget -->
    </div><!-- sidebar -->

  </div><!-- row -->
</div><!-- end page content -->
