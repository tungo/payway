<div class="grid_3">
<?php if (count($category_news)):?>
  <div class="widget">
    <div class="menu-right-news"><h3><?php elang('danh-muc'); ?></h3></div>
    <div class="menu-right">
      <ul>
        <?php foreach ($category_news as $cat_child)
        {
          $link = (! empty($cat_child['link'])) ? $cat_child['link'] : site_url($cat_child['seo_name']);
          echo '<li><a href="'.$link.'"><i class="icon-caret-right"></i> '.$cat_child['name'].'</a></li>';
        } ?>
      </ul>
    </div>
  </div>
<?php endif; ?>

  <div class="widget ads">
    <h3 class="col-title">Ads</h3><span class="liner"></span>
    <?php foreach ($banner[1005] as $bn)
    {
      $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
      if ($bn['link'] != '')
      {
        echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
      }
      else
      {
        echo $img;
      }
    } ?>
  </div><!-- widget ads -->

</div><!-- sidebar -->