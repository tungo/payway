<form method="post" id="muathedienthoai" name="muathedienthoai" action="<?php echo base_url('dich-vu/mua-the-dien-thoai.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($muathedienthoai['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($muathedienthoai['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <select name="menhgia" id="menhgia">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('so-luong'); ?>:</label>
    <span>
      <input type="text" value="" name="soluong" maxlength="5" autocomplete="off">
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($muathedienthoai['menhgia']); ?>;
  var thanhtien = <?php echo json_encode($muathedienthoai['thanhtien']); ?>;

  $('#muathedienthoai #nhacungcap').linkSelect('#muathedienthoai #menhgia', menhgia);

  function tinhtien()
  {
    var ncc = $('#muathedienthoai select[name="nhacungcap"]').val();
    var mg = $('#muathedienthoai select[name="menhgia"]').val();
    var sl = $('#muathedienthoai input[name="soluong"]').val();
    var tien = (!ncc || !mg || !sl) ? 0 : thanhtien[ncc][mg] * sl;
    $('#muathedienthoai span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('#muathedienthoai input[name="soluong"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('#muathedienthoai select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('#muathedienthoai select[name="nhacungcap"]').each(function() {
    $('#muathedienthoai select[name="menhgia"]').change();
  });
  $('#muathedienthoai select[name="nhacungcap"]').change(function() {
    $('#muathedienthoai select[name="menhgia"]').change();
  });
  $('#muathedienthoai').submit(function(){
    $('#muathedienthoai .loading.hide').removeClass('hide');
    $('#muathedienthoai input:submit').prop('disabled', true);
  });
});
</script>
