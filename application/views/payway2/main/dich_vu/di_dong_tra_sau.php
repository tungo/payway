<form method="post" id="didongtrasau" name="didongtrasau" action="<?php echo base_url('dich-vu/di-dong-tra-sau.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($didongtrasau['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($didongtrasau['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('so-di-dong'); ?>:</label>
    <span>
      <select name="dauso" class="prephone" id="dauso">
        <option value=""></option>
      </select>
      <input type="text" value="" name="didong" class="postphone" maxlength="7" autocomplete="off">
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <input type="text" value="" name="menhgia" maxlength="7" autocomplete="off">
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($didongtrasau['dauso']); ?>;
  var giamgia = <?php echo json_encode($didongtrasau['giamgia']); ?>;

  $('#didongtrasau #nhacungcap').linkSelect('#didongtrasau #dauso', dauso);

  function tinhtien()
  {
    var ncc = $('#didongtrasau select[name="nhacungcap"]').val();
    var mg = $('#didongtrasau input[name="menhgia"]').val();
    var tien = (!ncc || !mg) ? 0 : mg * giamgia[ncc];
    $('#didongtrasau span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('#didongtrasau input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });
  $('#didongtrasau input[name="menhgia"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('#didongtrasau select[name="nhacungcap"]').each(function() {
    $('#didongtrasau input[name="menhgia"]').keyup();
  });
  $('#didongtrasau select[name="nhacungcap"]').change(function() {
    $('#didongtrasau input[name="menhgia"]').keyup();
  });
  $('#didongtrasau').submit(function(){
    $('#didongtrasau .loading.hide').removeClass('hide');
    $('#didongtrasau input:submit').prop('disabled', true);
  });
});
</script>