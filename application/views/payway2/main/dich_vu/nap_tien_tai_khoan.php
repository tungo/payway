<form method="post" id="naptientaikhoan" name="naptientaikhoan" action="<?php echo base_url('dich-vu/nap-tien-tai-khoan.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($naptientaikhoan['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($naptientaikhoan['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('tai-khoan'); ?>:</label>
    <span>
      <input type="text" value="" name="taikhoan">
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <select name="menhgia" id="menhgia">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($naptientaikhoan['menhgia']); ?>;
  var thanhtien = <?php echo json_encode($naptientaikhoan['thanhtien']); ?>;

  $('#naptientaikhoan #nhacungcap').linkSelect('#naptientaikhoan #menhgia', menhgia);

  function tinhtien()
  {
    var ncc = $('#naptientaikhoan select[name="nhacungcap"]').val();
    var mg = $('#naptientaikhoan select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('#naptientaikhoan span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('#naptientaikhoan select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('#naptientaikhoan select[name="nhacungcap"]').each(function() {
    $('#naptientaikhoan select[name="menhgia"]').change();
  });
  $('#naptientaikhoan select[name="nhacungcap"]').change(function() {
    $('#naptientaikhoan select[name="menhgia"]').change();
  });
  $('#naptientaikhoan ').submit(function(){
    $('#naptientaikhoan .loading.hide').removeClass('hide');
    $('#naptientaikhoan input:submit').prop('disabled', true);
  });
});
</script>
