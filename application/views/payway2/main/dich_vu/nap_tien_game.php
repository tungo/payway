<form method="post" id="naptiengame" name="naptiengame" action="<?php echo base_url('dich-vu/nap-tien-game.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($naptiengame['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($naptiengame['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('tai-khoan'); ?>:</label>
    <span>
      <input type="text" value="" name="taikhoan">
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <select name="menhgia" id="menhgia">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($naptiengame['menhgia']); ?>;
  var thanhtien = <?php echo json_encode($naptiengame['thanhtien']); ?>;

  $('#naptiengame #nhacungcap').linkSelect('#naptiengame #menhgia', menhgia);

  function tinhtien()
  {
    var ncc = $('#naptiengame select[name="nhacungcap"]').val();
    var mg = $('#naptiengame select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('#naptiengame span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('#naptiengame select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('#naptiengame select[name="nhacungcap"]').each(function() {
    $('#naptiengame select[name="menhgia"]').change();
  });
  $('#naptiengame select[name="nhacungcap"]').change(function() {
    $('#naptiengame select[name="menhgia"]').change();
  });
  $('#naptiengame ').submit(function(){
    $('#naptiengame .loading.hide').removeClass('hide');
    $('#naptiengame input:submit').prop('disabled', true);
  });
});
</script>
