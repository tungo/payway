
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('doi-tac-dau-tu'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('doi-tac-dau-tu'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_12 alpha">
                <div class="clearfix mbs tro-thanh-doi-tac">
                    <ul class="tabs">
                        <li><a href="#t-1" class="active"><span class="dai-ly"></span></a><h3><?php elang('dai-ly'); ?></h3></li>
                        <li><a href="#t-2" class=""><span class="doi-tac"></span></a><h3><?php elang('doi-tac'); ?></h3></li>
                        <li><a href="#t-3" class=""><span class="nha-dau-tu"></span></a><h3><?php elang('nha-dau-tu'); ?></h3></li>
                    </ul>
                    <ul class="tabs-content">
                        <li id="t-1" class="active">
                            <h3 class="col-eleven"><span> <?php elang('dai-ly'); ?> </span></h3>
                            <br>
                            <?php elang('noi-dung-dai-ly'); ?>
                            <form>
                                    <div class="form">
                                        <label><?php elang('ho-ten'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('so-dien-thoai'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('email'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('thuoc-don-vi'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('dia-chi'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('nganh-nghe-kinh-doanh'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('chuc-vu-hien-tai'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('noi-dung-can-trao-doi'); ?> <font color="red">*</font></label>
                                        <textarea></textarea>
                                    </div>
                                    <div class="form center">
                                        <input type="submit" value="<?php elang('gui'); ?>" name="">
                                        <input type="reset" value="<?php elang('nhap-lai'); ?>" name="">
                                    </div>
                                </form>
                        </li>
                        <li id="t-2" class="" style="display: none;">
                            <h3 class="col-eleven"><span> <?php elang('doi-tac'); ?> </span></h3>
                            <br>
                            <?php elang('noi-dung-doi-tac'); ?>
                        </li>
                        <li id="t-3" class="" style="display: none;">
                            <h3 class="col-eleven"><span> <?php elang('nha-dau-tu'); ?> </span></h3>
                            <br>
                            <?php elang('noi-dung-nha-dau-tu'); ?>
                        </li>
                    </ul>
                </div>
            </div>
  </div>
</div>