<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('gioi-thieu'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('gioi-thieu'); ?></li>
      </ul>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="clearfix mbs">
      <div class="clearfix mbs">
        <span class="liner double"></span>
        <div class="grid_4">
          <img src="public/images/payway.png" alt="#">
        </div><!-- grid4 img -->

        <div class="grid_8">
          <div class="text_block"><p><?php elang('gioi-thieu-noi-dung-ngan'); ?></p> 
            <a href="gioi-thieu/ve-chung-toi.html" class="tbutton medium"><span><i class="icon-thumbs-up"></i> <?php elang('xem-chi-tiet'); ?> </span></a>
          </div>
        </div>
      </div>
      <div class="services mbs">
         <h3 class="two-line"><span><?php elang('dich-vu'); ?></span></h3>
          <div class="row clearfix dich-vu">
            <div class="grid_3">
              <img src="public/images/dich-vu/kieu-hoi.png">
              <div class="s_info">
                <h3><?php elang('dich-vu-kieu-hoi'); ?></h3>
                <p><?php elang('gioi-thieu-dich-vu-kieu-hoi'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/dich-vu-kieu-hoi.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/thanh-toan-online.png">
              <div class="s_info">
                <h3><?php elang('thanh-toan-online'); ?></h3>
                <p><?php elang('gioi-thieu-dich-vu-thanh-toan-online'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/thanh-toan-online.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/thanh-toan-offline.png">
              <div class="s_info">
                <h3><?php elang('thanh-toan-offline'); ?></h3>
                <p><?php elang('gioi-thieu-dich-vu-thanh-toan-offline'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/thanh-toan-offline.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/dich-vu-khac.png">
              <div class="s_info">
                <h3><?php elang('dich-vu-khac'); ?></h3>
                <p style="text-align:center;height:110px;"><?php elang('gioi-thieu-dich-vu-khac'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/dich-vu-khac.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
          </div>
      </div>
    </div><!-- clearfix -->
  </div>
</div>