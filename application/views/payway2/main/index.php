
<script type="text/javascript" src="<?php echo base_url($theme['js'].'jquery-selectboxes-pack.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url($theme['js'].'tn_payment/jquery-select-service1-vn.js'); ?>"></script>

    <!-- one bt one slider -->

    <div class="wrape">
      <div class="row clearfix">
        <div class="grid_5">
          <div class="dich-vu-nhanh">
            <div class="grid_4">
              <ul class="tabs">
                <li><a href="#t-1" class="active"><?php elang('di-dong-tra-truoc'); ?></a></li>
                <li><a href="#t-2" class=""><?php elang('di-dong-tra-sau'); ?></a></li>
                <li><a href="#t-3" class=""><?php elang('mua-the-dien-thoai'); ?></a></li>
                <li><a href="#t-4" class=""><?php elang('mua-the-game'); ?></a></li>
                <li><a href="#t-5" class=""><?php elang('nap-tien-game'); ?></a></li>
                <li><a href="#t-6" class=""><?php elang('nap-tien-tai-khoan'); ?></a></li>
                <li><a href="#t-7" class=""><?php elang('dich-vu-khac'); ?></a></li>
              </ul>
            </div>
            <div class="grid_8">
              <ul class="tabs-content">
                <li id="t-1" class="active" style="display: block;">
                  <?php echo $view_didongtratruoc; ?>
                </li>
                <li id="t-2" style="display: none;" class="">
                  <?php echo $view_didongtrasau; ?>
                </li>
                <li id="t-3" style="display: none;" class="">
                  <?php echo $view_muathedienthoai; ?>
                </li>
                <li id="t-4" style="display: none;" class="">
                  <?php echo $view_muathegame; ?>
                </li>
                <li id="t-5" style="display: none;" class="">
                  <?php echo $view_naptiengame; ?>
                </li>
                <li id="t-6" style="display: none;" class="">
                  <?php echo $view_naptientaikhoan; ?>
                </li>
                <li id="t-7" style="display: none;" class="">
                  <div class="index-thanh-toan-online">

                    <div class="grid_12 set_two">
                      <a href="dich-vu/ve-may-bay.html">
                        <div class="s_icon-2"><span class="ve-may-bay"></span></div>
                        <div class="s_info">
                          <h3><?php elang('ve-may-bay'); ?></h3>
                          <p><?php elang('noi-dung-ve-may-bay'); ?></p>
                        </div>
                      </a>
                    </div>
                    <div class="grid_12 set_two">
                      <a href="dich-vu/tien-dien.html">
                        <div class="s_icon-2"><span class="tien-dien"></span></div>
                        <div class="s_info">
                          <h3><?php elang('tien-dien'); ?></h3>
                          <p><?php elang('noi-dung-tien-dien'); ?></p>
                        </div>
                      </a>
                    </div>
                    <div class="grid_12 set_two">
                      <a href="dich-vu/hoa-don-adsl.html">
                        <div class="s_icon-2"><span class="adsl"></span></div>
                        <div class="s_info">
                          <h3><?php elang('hoa-don-adsl'); ?></h3>
                          <p><?php elang('noi-dung-hoa-don-adsl'); ?></p>
                        </div>
                      </a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="theme-default grid_7">
          <div id="nivo-slider" class="nivoSlider">
            <?php foreach ($banner['Top'] as $bn)
              {
                $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
                if ($bn['link'] != '')
                {
                  echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'" target="_blank">'.$img.'</a>';
                }
                else
                {
                  echo $img;
                }
              } ?>
          </div>
        </div>
      </div>
    </div>
      <div class="page-content">
         <div class="services mbs">
          <div class="row clearfix dich-vu">
            <div class="grid_3">
              <img src="public/images/dich-vu/kieu-hoi.png">
              <div class="s_info">
                <h3><?php elang('dich-vu-kieu-hoi'); ?></h3>
                <p><?php elang('home-info-1'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/dich-vu-kieu-hoi.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/thanh-toan-online.png">
              <div class="s_info">
                <h3><?php elang('thanh-toan-online'); ?></h3>
                <p><?php elang('home-info-2'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/thanh-toan-online.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/thanh-toan-offline.png">
              <div class="s_info">
                <h3><?php elang('thanh-toan-offline'); ?></h3>
                <p><?php elang('home-info-3'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/thanh-toan-offline.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
            <div class="grid_3">
              <img src="public/images/dich-vu/dich-vu-khac.png">
              <div class="s_info">
                <h3><?php elang('dich-vu-khac'); ?></h3>
                <p><?php elang('home-info-4'); ?></p>
                <span class="line"></span>
                <a href="gioi-thieu/dich-vu-khac.html"><span><?php elang('xem-chi-tiet'); ?></span></a>
                <span class="line"></span>
              </div>
            </div>
          </div>
      </div>
        <div class="row clearfix">
          <div class="grid_6 tin-tuc">
            <h3 class="two-line"><span> <?php elang('tin-tuc'); ?></span></h3>
              <div id="banner-km" class="fader">
                <?php foreach ($banner['Middle'] as $bn)
                  {
                    $img = '<img class="slide" src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
                    if ($bn['link'] != '')
                    {
                      echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
                    }
                    else
                    {
                      echo $img;
                    }
                  } ?>
                  <div class="fader_controls">
                    <ul class="pager_list"></ul>
                  </div>
              </div>
          </div>
          <div class="grid_6 tin-tuc">
            <h3 class="two-line"><span><?php elang('huong-dan-su-dung-dich-vu-nhanh'); ?></span></h3>
            <ul class="tt-toggle">
              <li class="sub-toggle">
                <a href="ho-tro/cach-chuyen-tien.html">
                  <div class="toggle-head">
                    <div class="toggle-head-sign"></div>
                    <p><?php elang('cach-chuyen-tien'); ?></p>
                  </div>
                </a>
              </li>
              <li class="sub-toggle">
                <a href="ho-tro/cach-nhan-tien.html">
                  <div class="toggle-head">
                    <div class="toggle-head-sign"></div>
                    <p><?php elang('cach-nhan-tien'); ?></p>
                  </div>
                </a>
              </li>
              <li class="sub-toggle">
                <a href="dich-vu/thanh-toan-online.html">
                  <div class="toggle-head">
                    <div class="toggle-head-sign"></div>
                    <p><?php elang('thanh-toan-hoa-don-online'); ?></p>
                  </div>
                </a>
              </li>
              <li class="sub-toggle">
                <a href="dich-vu/thanh-toan-offline.html">
                  <div class="toggle-head">
                    <div class="toggle-head-sign"></div>
                    <p><?php elang('thanh-toan-hoa-don-offline'); ?></p>
                  </div>
                </a>
              </li>
              <li class="sub-toggle">
                <a href="ho-tro/dia-diem-nhan-tien.html">
                  <div class="toggle-head">
                    <div class="toggle-head-sign"></div>
                    <p><?php elang('dia-diem-nhan-tien'); ?></p>
                  </div>
                </a>
              </li>
          </ul>
          </div>
        </div>
        <div class="row clearfix mbs" data-gen="bigEntrance">
          <div class="tac">
            <h3 class="two-line"><span> <?php elang('nha-cung-cap-dich-vu'); ?> & <?php elang('chap-nhan-thanh-toan'); ?> </span></h3>
            <div class="simply-scroll simply-scroll-container">
              <div class="simply-scroll-clip">
                <ul id="scroller" class="chap-nhan-thanh-toan simply-scroll-list" style="width: 2520px;">
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/viettel.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Mobifone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vinaphone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vietnamobile.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/sfone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Beeline.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/EVN.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vnpt.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/capnuocgiadinh.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Vietnam-Airlines.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vietjetair.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Jetstar-Pacific.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/agribank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/bidv.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/dongabank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/hdbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/mastercard.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/oceanbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/sacombank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/tcb.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/vcb.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/viettinbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/visa.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/vnmart.jpg'); ?>"></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>