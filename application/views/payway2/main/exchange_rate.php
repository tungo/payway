<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('ty-gia-ngoai-te'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href=""><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('ty-gia-ngoai-te'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->
<div class="page-content">
  <div class="row clearfix mbs">
    <table class="" cellpadding="0" cellspacing="0" width="100%">
      <caption>Tỷ giá ngoại tệ Vietcombank ngày: <?php echo $source['DateTime']?></caption>
      <thead>
        <tr>
          <th>Mã NT</th>
          <th>Tên ngoại tệ</th>
          <th>Mua tiền mặt</th>
          <th>Chuyển khoản</th>
          <th>Bán</th>
        </tr>
      </thead>
      <tbody>
<?php
foreach ($source['Exrate'] as $exrate):
  $code     = $exrate['@attributes']['CurrencyCode'];
  $name     = $exrate['@attributes']['CurrencyName'];
  $buy      = ($exrate['@attributes']['Buy'] != 0) ? $exrate['@attributes']['Buy'] : '-';
  $transfer = ($exrate['@attributes']['Transfer'] != 0) ? $exrate['@attributes']['Transfer'] : '-';
  $sell     = ($exrate['@attributes']['Sell'] != 0) ? $exrate['@attributes']['Sell'] : '-';
?>
        <tr>
          <td><?php echo $code ?></td>
          <td><?php echo $name ?></td>
          <td><?php echo $buy ?></td>
          <td><?php echo $transfer ?></td>
          <td><?php echo $sell ?></td>
        </tr>
<?php endforeach; ?>
      </tbody>
    </table>
  </div><!-- row -->
</div><!-- end page content -->
