<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('thanh-toan-online'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="dich-vu.html"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('thanh-toan-online'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons dich-vu-chinh"><img src="public/images/dich-vu/thanh-toan-online.png"></div>
        <div class="title"><h3><?php elang('thanh-toan-online'); ?></h3></div>
        <div class="content thanh-toan-online">
          <div class="grid_6 set_two">
            <a href="dich-vu/di-dong-tra-truoc.html">
              <div class="s_icon-2"><span class="tra-truoc"></span></div>
              <div class="s_info">
                <h3><?php elang('di-dong-tra-truoc'); ?></h3>
                <p><?php elang('noi-dung-di-dong-tra-truoc'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/di-dong-tra-sau.html">
              <div class="s_icon-2"><span class="tra-sau"></span></div>
              <div class="s_info">
                <h3><?php elang('di-dong-tra-sau'); ?></h3>
                <p><?php elang('noi-dung-di-dong-tra-sau'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/mua-the-dien-thoai.html">
              <div class="s_icon-2"><span class="the-dien-thoai"></span></div>
              <div class="s_info">
                <h3><?php elang('mua-the-dien-thoai'); ?></h3>
                <p><?php elang('noi-dung-mua-the-dien-thoai'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/mua-the-game.html">
              <div class="s_icon-2"><span class="the-game"></span></div>
              <div class="s_info">
                <h3><?php elang('mua-the-game'); ?></h3>
                <p><?php elang('noi-dung-mua-the-game'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/nap-tien-game.html">
              <div class="s_icon-2"><span class="nap-tien-game"></span></div>
              <div class="s_info">
                <h3><?php elang('nap-tien-game'); ?></h3>
                <p><?php elang('noi-dung-nap-tien-game'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/nap-tien-tai-khoan.html">
              <div class="s_icon-2"><span class="nap-tien-tai-khoan"></span></div>
              <div class="s_info">
                <h3><?php elang('nap-tien-tai-khoan'); ?></h3>
                <p><?php elang('noi-dung-nap-tien-tai-khoan'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/tien-dien.html">
              <div class="s_icon-2"><span class="tien-dien"></span></div>
              <div class="s_info">
                <h3><?php elang('tien-dien'); ?></h3>
                <p><?php elang('noi-dung-tien-dien'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/hoa-don-adsl.html">
              <div class="s_icon-2"><span class="adsl"></span></div>
              <div class="s_info">
                <h3><?php elang('hoa-don-adsl'); ?></h3>
                <p><?php elang('noi-dung-hoa-don-adsl'); ?></p>
              </div>
            </a>
          </div>
          <div class="grid_6 set_two">
            <a href="dich-vu/ve-may-bay.html">
              <div class="s_icon-2"><span class="ve-may-bay"></span></div>
              <div class="s_info">
                <h3><?php elang('ve-may-bay'); ?></h3>
                <p><?php elang('noi-dung-ve-may-bay'); ?></p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix">
<?php foreach ($banner['Bottom'] as $bn)
{
  $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
  if ($bn['link'] != '')
  {
    echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
  }
  else
  {
    echo $img;
  }
} ?>
</div>
</div>