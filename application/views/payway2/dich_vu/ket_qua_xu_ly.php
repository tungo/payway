<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('ket-qua-xu-ly'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('ket-qua-xu-ly'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu <?php echo ($rs_code == '00') ? 'thanh-cong' : 'that-bai'; ?>">
        <div class="icons"><?php echo ($rs_code == '00') ? '<i class="icon_check"></i>' : '<i class="icon_error-triangle_alt"></i>'; ?></div>
        <div class="title"><h3><?php echo $trans_service; ?> <?php echo ($rs_code == '00') ? lang('thanh-cong') : lang('that-bai'); ?></h3></div>
        <div class="content">
          <div class="thong-bao">
            <p><?php echo $trans_noti; ?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix">
<?php foreach ($banner['Bottom'] as $bn)
{
  $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
  if ($bn['link'] != '')
  {
    echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
  }
  else
  {
    echo $img;
  }
} ?>
</div>
</div>