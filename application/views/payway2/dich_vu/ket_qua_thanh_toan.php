<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('ket-qua-thanh-toan'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('ket-qua-thanh-toan'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu <?php echo ($trans_result == 's') ? 'xu-ly-giao-dich' : 'xu-ly-that-bai'; ?>">
        <div class="icons"><?php echo ($trans_result == 's') ? '<i class="icon_check"></i>' : '<i class="icon_error-triangle_alt"></i>'; ?></div>
        <div class="title"><h3><?php echo ($trans_result == 's') ? lang('xu-ly-giao-dich') : lang('xu-ly-that-bai'); ?></h3></div>
        <div class="content">
        <?php if ($trans_result == 's'): ?>
          <div id="tn_count"></div>
          <p class="xu-ly"><?php elang('noi-dung-xu-ly-giao-dich'); ?></p>
          <script>
            var reload_time = 11;
            var counter = setInterval(tn_count, 1000);
            function tn_count() {
              reload_time -= 1;
              if (reload_time < 0)
              {
                clearInterval(counter);
                window.location.href='<?php echo $redirect_url; ?>';
                return;
              }
              var div = document.getElementById("tn_count");
              div.innerHTML = reload_time;
            }
          </script>
        <?php else: ?>
          <p class="thu-lai"><?php elang('noi-dung-xu-ly-that-bai'); ?></p>
        <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix">
<?php foreach ($banner['Bottom'] as $bn)
{
  $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
  if ($bn['link'] != '')
  {
    echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
  }
  else
  {
    echo $img;
  }
} ?>
</div>
</div>