<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('di-dong-tra-truoc'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('di-dong-tra-truoc'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons"><span class="tra-truoc"></span></div>
        <div class="title"><h3><?php elang('di-dong-tra-truoc'); ?></h3></div>
        <div class="content">
          <div class="loading hide">
            <label><img src="/public/templates/payway2/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('so-di-dong'); ?>:</label>
              <select name="dauso" class="prephone" id="dauso">
                <option value=""></option>
              </select>
              <input type="text" value="<?php echo set_value('didong'); ?>" name="didong" class="postphone" maxlength="7" autocomplete="off">
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <select name="menhgia" id="menhgia">
                <option value=""></option>
              </select>
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
          <div class="form">
            <fieldset data-id="VTC0058" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử MobiFone</legend>
<p>
  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
</p>
            </fieldset>
            <fieldset data-id="VTC0056" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Viettel</legend>
<p>
  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
</p>
            </fieldset>

            <fieldset data-id="VTC0057" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Vinaphone</legend>
<p>
  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
</p>
            </fieldset>

            <fieldset data-id="VTC0176" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Vietnamobile</legend>
<p>
  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
</p>
            </fieldset>

            <fieldset data-id="VTC0177" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Gmobile</legend>
<p>
  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
</p>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix">
<?php foreach ($banner['Bottom'] as $bn)
{
  $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
  if ($bn['link'] != '')
  {
    echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
  }
  else
  {
    echo $img;
  }
} ?>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url($theme['js'].'jquery-selectboxes-pack.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url($theme['js'].'tn_payment/jquery-select-service1-vn.js'); ?>"></script>

<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($dauso); ?>;
  var dauso_df = '<?php echo set_value('dauso'); ?>';
  var menhgia = <?php echo json_encode($menhgia); ?>;
  var menhgia_df = '<?php echo set_value('menhgia'); ?>';
  var thanhtien = <?php echo json_encode($thanhtien); ?>;

  $('#nhacungcap').linkSelect('#dauso', dauso, dauso_df);
  $('#nhacungcap').linkSelect('#menhgia', menhgia, menhgia_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });

  $('select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>
