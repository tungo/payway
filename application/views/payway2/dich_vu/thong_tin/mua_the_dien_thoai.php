<div class="thong-bao">
  <p><?php echo $trans_noti; ?></p>
</div>
<?php if (isset($transaction['data']['cards']) && count($transaction['data']['cards'])) : ?>
<div class="ma-the">
  <?php
    foreach ($transaction['data']['cards'] as $card)
    {
      $code = str_split($card['code'], 4);
      $code = implode(' ', $code);
      echo '<div class="grid_6 bg">';
      echo '<strong>'.lang('ma-the').':</strong> <span>'.$code.'</span>';
      echo '<hr>';
      echo '<strong>'.lang('so-seri').':</strong> '.$card['serial'].'<br>';
      echo '<strong>'.lang('han-su-dung').':</strong> '.$card['date'].'<br>';
      echo '</div>';
    }
  ?>
</div>
<?php endif; ?>
<div class="clearfix"></div>
<div class="chi-tiet">
  <h3><span><?php elang('thong-tin-dich-vu'); ?></span></h3>
  <ul>
      <li><label><?php elang('dich-vu'); ?>:</label> <?php elang('mua-the-dien-thoai'); ?></li>
      <li><label><?php elang('nha-cung-cap'); ?>:</label> <?php echo $nhacungcap[$transaction['data']['nhacungcap']]; ?></li>
      <li><label><?php elang('menh-gia'); ?>:</label> <?php echo $menhgia[$transaction['data']['nhacungcap']][$transaction['data']['menhgia']]; ?></li>
  </ul>
  <h3><span><?php elang('thong-tin-giao-dich'); ?></span></h3>
  <ul>
      <li><label><?php elang('ma-giao-dich'); ?>:</label> <?php echo $transaction['id']; ?></li>
      <li><label><?php elang('ngan-hang'); ?>:</label> <?php echo $bank_code[$transaction['bank']]; ?></li>
      <li><label><?php elang('so-luong'); ?>:</label> <?php echo $transaction['data']['soluong']; ?></li>
      <li><label><?php elang('thanh-tien'); ?>:</label> <?php echo tn_money_format($transaction['amount']); ?></li>
  </ul>
</div>