
  <div class="widget">
    <div class="menu-right">
      <ul>
        <li<?php if($method=='di_dong_tra_truoc') echo ' class="current"'; ?>><a href="dich-vu/di-dong-tra-truoc.html"><?php elang('di-dong-tra-truoc'); ?></a></li>
        <li<?php if($method=='di_dong_tra_sau') echo ' class="current"'; ?>><a href="dich-vu/di-dong-tra-sau.html"><?php elang('di-dong-tra-sau'); ?></a></li>
        <li<?php if($method=='mua_the_dien_thoai') echo ' class="current"'; ?>><a href="dich-vu/mua-the-dien-thoai.html"><?php elang('mua-the-dien-thoai'); ?></a></li>
        <li<?php if($method=='mua_the_game') echo ' class="current"'; ?>><a href="dich-vu/mua-the-game.html"><?php elang('mua-the-game'); ?></a></li>
        <li<?php if($method=='nap_tien_game') echo ' class="current"'; ?>><a href="dich-vu/nap-tien-game.html"><?php elang('nap-tien-game'); ?></a></li>
        <li<?php if($method=='nap_tien_tai_khoan') echo ' class="current"'; ?>><a href="dich-vu/nap-tien-tai-khoan.html"><?php elang('nap-tien-tai-khoan'); ?></a></li>
        <li<?php if($method=='ve_may_bay') echo ' class="current"'; ?>><a href="dich-vu/ve-may-bay.html"><?php elang('ve-may-bay'); ?></a></li>
        <li<?php if($method=='hoa_don_adsl') echo ' class="current"'; ?>><a href="dich-vu/hoa-don-adsl.html"><?php elang('hoa-don-adsl'); ?></a></li>
        <li<?php if($method=='tien_dien') echo ' class="current"'; ?>><a href="dich-vu/tien-dien.html"><?php elang('tien-dien'); ?></a></li>
      </ul>
    </div>
  </div>
  <div class="widget">
    <div class="menu-right-news">
      <h3><?php elang('tin-tuc'); ?></h3>
      <ul>
        <?php foreach ($articles[1021] as $article) {
          $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
          echo ' <li><a href="'.$link.'">'.$article['name'].'</a></li>';
        } ?>
      </ul>
    </div>
  </div>
  <div class="widget ads">
    <?php foreach ($banner['Right'] as $bn)
    {
      $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
      if ($bn['link'] != '')
      {
        echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
      }
      else
      {
        echo $img;
      }
    } ?>
  </div><!-- widget ads -->