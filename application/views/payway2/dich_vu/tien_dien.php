<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('tien-dien'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('tien-dien'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons"><span class="tra-sau"></span></div>
        <div class="title"><h3><?php elang('tien-dien'); ?></h3></div>
        <div class="content" id="content">
          <iframe style="border: none;" src="https://www.vban.vn/merchantvban/BillElectricity.aspx?tmcode=10000075&svcode=BILLELEC-001" width="100%" height="800" scrolling="No" frameborder="No" ></iframe>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="row clearfix">
<?php foreach ($banner['Bottom'] as $bn)
{
  $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
  if ($bn['link'] != '')
  {
    echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
  }
  else
  {
    echo $img;
  }
} ?>
</div>
</div>