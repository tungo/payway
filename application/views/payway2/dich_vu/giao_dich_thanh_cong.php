<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('thong-tin-giao-dich'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('thong-tin-giao-dich'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu thanh-cong">

        <div class="icons"><i class="icon_check"></i></div>
        <div class="title"><h3><?php elang('giao-dich-thanh-cong'); ?></h3></div>

        <div class="content">
          <?php echo $detail; ?>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>