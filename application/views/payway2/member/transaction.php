<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('chi-tiet-giao-dich'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="member/"><?php elang('thong-tin-tai-khoan'); ?></a></li>
        <li><?php elang('chi-tiet-giao-dich'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_3">
      <div class="widget">
        <ul class="list right-arrow">
          <li><a href="member/"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
          <li class="current"><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
          <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
        </ul>
      </div>
    </div>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_table"></i> <?php elang('chi-tiet-giao-dich'); ?></span></h3>
        <?php echo $detail; ?>
      </div>
  </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#tool_form input:reset').click(function(){
    $('#tool_form input:text').val('');
    $('#tool_form select option[value=""]').prop('selected', true);
    return false;
  });
});
</script>