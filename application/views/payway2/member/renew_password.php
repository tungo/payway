
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('doi-mat-khau'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="member/"><?php elang('thong-tin-tai-khoan'); ?></a></li>
        <li><?php elang('doi-mat-khau'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_3">
      <div class="widget">
        <ul class="list right-arrow">
          <li class="current"><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
        </ul>
      </div>
    </div>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></span></h3>
        <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <div class="register">
            <label><?php elang('email'); ?></label>
            <?php echo $params['email']; ?>
          </div>
          <div class="register">
            <label><?php elang('mat-khau-moi'); ?></label>
            <input type="password" name="newpass" class="requiredField">
          </div>
          <div class="register">
            <label><?php elang('xac-nhan-mat-khau'); ?></label>
            <input type="password" name="confirm" class="requiredField">
          </div>
          <div class="register">
            <input type="submit" value="<?php elang('luu'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
          </div>
        </form>
      </div>
  </div>
</div>