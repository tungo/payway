<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('lich-su-giao-dich'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="member/"><?php elang('thong-tin-tai-khoan'); ?></a></li>
        <li><?php elang('lich-su-giao-dich'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_3">
      <div class="widget">
        <ul class="list right-arrow">
          <li><a href="member/"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
          <li class="current"><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
          <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
        </ul>
      </div>
    </div>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></span></h3>
        <div class="tim-lich-su-giao-dich">
          <form id="tool_form" method="post">
            <div class="tim">
              <label><?php elang('ma-giao-dich'); ?>:</label>
              <input type="text" name="keyword" value="<?php echo $filter['keyword']; ?>">
            </div>
            <div class="tim">
              <label><?php elang('dich-vu'); ?>:</label>
              <select name="type">
                <option value=""><?php elang('chon'); ?></option>
              <?php foreach ($select_type as $key => $val)
              {
                $selected = ($filter['type'] == $key) ? 'selected="selected"' : '';
                echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="tim">
              <label><?php elang('trang-thai'); ?>:</label>
              <select name="status">
                <option value=""><?php elang('chon'); ?></option>
              <?php foreach ($select_status as $key => $val)
              {
                $selected = ($filter['status'] == $key) ? 'selected="selected"' : '';
                echo '<option value="'.$key.'" '.$selected.' class="trans_member_st'.$key.'">'.$val.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="tim">
              <label><?php elang('thoi-gian'); ?>:</label>
              <?php elang('tu-ngay'); ?> <input type="text" style="width:24%" id="datetimepicker1" name="from_date" value="<?php echo $filter['from_date']; ?>" data-date-format="YYYY/MM/DD">
              <?php elang('den-ngay'); ?> <input type="text" style="width:24%" id="datetimepicker2" name="to_date" value="<?php echo $filter['to_date']; ?>" data-date-format="YYYY/MM/DD">
            </div>
            <div class="tim">
              <input type="submit" value="<?php elang('tim-kiem'); ?>">
              <input type="reset" value="<?php elang('nhap-lai'); ?>">
            </div>
          </form>
        </div>
        <div class="mbs padding-top-20">
          <table class="lich-su-giao-dich">
            <thead>
              <tr>
                <th><?php elang('ma-giao-dich'); ?></th>
                <th><?php elang('thoi-gian'); ?></th>
                <th><?php elang('dich-vu'); ?></th>
                <th><?php elang('so-tien'); ?></th>
                <th><?php elang('trang-thai'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
<?php if (! empty($items)):
  foreach ($items as $item): ?>
              <tr>
                <th><?php echo $item['id']?></th>
                <th><?php echo tn_datetime($item['created_time']); ?></th>
                <th><?php echo $select_type[$item['type']]; ?></th>
                <th><?php echo tn_money_format($item['amount']); ?></th>
                <th><?php echo '<span class="trans_st'.$item['status'].'">'.lang('trans_st'.$item['status']).'</span>'; ?></th>
                <th><a href="<?php echo base_url('member/transaction/id/'.$item['id']); ?>"><?php elang('xem-chi-tiet'); ?></a></th>
              </tr>
  <?php endforeach;
else: ?>
              <tr>
                <th colspan="6"><?php elang('chua-co-thong-tin'); ?></th>
              </tr>
<?php endif; ?>
            </tbody>
          </table>

          <div class="text-info entries-info pull-left">
            <?php elang('tong-so'); ?>: <?php echo $data['pagination']->total_rows; ?>
          </div>

          <div class="pull-right">
            <?php echo $data['pagination']->links; ?>
          </div>

        </div>
      </div>
  </div>
</div>

<link href="<?php echo base_url($theme['css'].'bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">

<script src="<?php echo base_url($theme['js'].'moment.min.js'); ?>"></script>
<script src="<?php echo base_url($theme['js'].'bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($theme['js'].'bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">
$(function () {
  $('#datetimepicker1').datetimepicker({
    pickTime: false
  });
  $('#datetimepicker2').datetimepicker({
    pickTime: false
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#tool_form input:reset').click(function(){
    $('#tool_form input:text').val('');
    $('#tool_form select option[value=""]').prop('selected', true);
    return false;
  });
});
</script>