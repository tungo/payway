
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('dang-nhap'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('dang-nhap'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_6" style="border-right:1px solid #d1d1d1">
      <h3><?php elang('dang-nhap'); ?></h3>
      <div class="clearfix">
      <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <div class="register">
            <input type="text" name="email" placeholder="<?php elang('email'); ?> *" class="requiredField email" style="width:98%">
          </div>
          <div class="register">
            <input type="password" name="password" placeholder="<?php elang('password'); ?> *" class="requiredField" style="width:98%">
          </div>
          <div class="register">
            <input type="submit" value="<?php elang('dang-nhap'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
            <a class="quen-mat-khau" href="<?php echo site_url('member/reset_password'); ?>"><?php elang('quen-mat-khau'); ?></a>
          </div>
        </form>
      </div>
    </div>
    <div class="grid_6">
      <h3><?php elang('khong-phai-thanh-vien'); ?></h3>
      <p><?php elang('tao-tai-khoan-moi'); ?>
          <a href="<?php echo site_url('member/register'); ?>"><?php elang('dang-ky-tai-khoan'); ?></a>
      </p>
    </div>
  </div>
</div>
