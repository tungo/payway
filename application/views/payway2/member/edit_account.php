
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('sua-thong-tin-tai-khoan'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="member/"><?php elang('thong-tin-tai-khoan'); ?></a></li>
        <li><?php elang('sua-thong-tin-tai-khoan'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_3">
      <div class="widget">
        <ul class="list right-arrow">
          <li><a href="member/"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
          <li class="current"><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
          <li><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
          <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
        </ul>
      </div>
    </div>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></span></h3>
        <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
        <div class="grid_12 padding-top-20">
          <h3 class="col-title"> <?php elang('thong-tin-tai-khoan'); ?></h3><span class="liner"></span>
          <span class="mbs">
              <div class="register">
                <label><?php elang('email'); ?></label>
                <?php echo $member['email']; ?>
              </div>
              <div class="register">
                <label><?php elang('ten'); ?> *</label>
                <input type="text" name="ten" value="<?php echo set_value('ten', $account['firstname']); ?>" class="requiredField">
              </div>
              <div class="register">
                <label><?php elang('ho'); ?> *</label>
                <input type="text" name="ho" value="<?php echo set_value('ho', $account['lastname']); ?>" class="requiredField">
              </div>
          </span>
        </div>
        <div class="grid_12 padding-top-20">
          <h3 class="col-title"> <?php elang('thong-tin-chung'); ?></h3><span class="liner"></span>
          <span class="mbs">
              <div class="register">
              <label><?php elang('gioi-tinh'); ?> *</label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="1" class="requiredField" <?php if (set_value('gioitinh', $account['gender']) == 1) echo 'checked="checked"'; ?>><?php elang('nam'); ?></label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="2" class="requiredField" <?php if (set_value('gioitinh', $account['gender']) == 2) echo 'checked="checked"'; ?>><?php elang('nu'); ?></label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="0" class="requiredField" <?php if (!set_value('gioitinh', $account['gender'])) echo 'checked="checked"'; ?>><?php elang('khac'); ?></label>
            </div>
            <div class="register">
              <label><?php elang('ngay-sinh'); ?> *</label>
              <select name="ngaysinh" style="width:21%;">
                <option value=""><?php elang('date-ngay'); ?></option>
              <?php
              for ($i = 1; $i <= 31; $i++)
              {
                $selected = (set_value('ngaysinh', date('j', strtotime($account['birthday']))) == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
              <select name="thangsinh" style="width:21%">
                <option value=""><?php elang('date-thang'); ?></option>
              <?php
              for ($i = 1; $i <= 12; $i++)
              {
                $selected = (set_value('thangsinh', date('n', strtotime($account['birthday']))) == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
              <select name="namsinh" style="width:21%">
                <option value=""><?php elang('date-nam'); ?></option>
              <?php
              for ($i = 1996; $i >= 1896; $i--)
              {
                $selected = (set_value('namsinh', date('Y', strtotime($account['birthday']))) == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('quoc-gia'); ?> *</label>
              <select name="quocgia">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nations as $id => $name)
              {
                $selected = (set_value('quocgia', $account['nationality']) == $id) ? 'selected="selected"' : '' ;
                echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('dia-chi'); ?> *</label>
              <input type="text" name="diachi" value="<?php echo set_value('diachi', $account['address']); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('thong-tin-them'); ?></label>
              <input type="text" name="thongtinthem" value="<?php echo set_value('thongtinthem', $account['additional_info']); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('thanh-pho'); ?> *</label>
              <input type="text" name="thanhpho" value="<?php echo set_value('thanhpho', $account['city']); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('post-code'); ?> *</label>
              <input type="text" name="postcode" value="<?php echo set_value('postcode', $account['postcode']); ?>" class="requiredField">
            </div>
          </span>
        </div>
        <div class="grid_12 padding-top-20">
          <h3 class="col-title"> <?php elang('thong-tin-bao-mat'); ?></h3><span class="liner"></span>
          <span class="mbs">
            <div class="register">
              <label><?php elang('cau-hoi-bao-mat'); ?> *</label>
              <select name="cauhoi">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($questions as $id => $name)
              {
                $selected = (set_value('cauhoi') == $id) ? 'selected="selected"' : '' ;
                echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('cau-tra-loi'); ?> *</label>
              <input type="text" name="traloi" value="<?php echo set_value('traloi'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('chung-minh-thu'); ?> *</label>
              <input type="text" name="chungminhthu" value="<?php echo set_value('chungminhthu'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('so-dien-thoai'); ?> *</label>
              <input type="text" name="dienthoai" value="<?php echo set_value('dienthoai'); ?>" class="requiredField">
            </div>
            <div class="register">
              <input type="submit" value="<?php elang('luu'); ?>">
              <input type="reset" value="<?php elang('nhap-lai'); ?>">
            </div>
          </span>
        </div>
      </div>
      </form>
  </div>
</div>