<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('thong-tin-tai-khoan'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('thong-tin-tai-khoan'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->

<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_3">
      <div class="widget">
        <ul class="list right-arrow">
          <li class="current"><a href="member"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
          <li><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
          <li><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
          <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
        </ul>
      </div>
    </div>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_profile"></i> <?php elang('chao'); ?>, <?php echo $account['firstname']; ?></span></h3>
        <p class="mbs"></p>
        <?php echo $this->tn_message->get(); ?>
      </div>
      <div class="grid_6">
        <h3 class="col-title"> <?php elang('thong-tin-tai-khoan'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('email'); ?>:</label> <?php echo $account['email']; ?></p>
          <p><label><?php elang('ten'); ?>:</label> <?php echo $account['firstname']; ?></p>
          <p><label><?php elang('ho'); ?>:</label> <?php echo $account['lastname']; ?></p>
          <p><label><?php elang('password'); ?>:</label> ********</p>
        </span>
      </div>
      <div class="grid_6">
        <h3 class="col-title"><?php elang('thong-tin-bao-mat'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('cau-hoi-bao-mat'); ?>:</label> <?php echo ($account['question_id']) ? $questions[$account['question_id']] : ''; ?></p>
          <p><label><?php elang('cau-tra-loi'); ?>:</label> <?php echo tn_hide($account['answer']); ?></p>
          <p><label><?php elang('chung-minh-thu'); ?>:</label> <?php echo tn_hide($account['identification']); ?></p>
          <p><label><?php elang('so-dien-thoai'); ?>:</label> <?php echo tn_hide($account['phone']); ?></p>
        </span>
      </div>
      <div class="grid_12 padding-top-20">
        <h3 class="col-title"><?php elang('thong-tin-chung'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('gioi-tinh'); ?>:</label> <?php echo $gender[$account['gender']]; ?></p>
          <p><label><?php elang('ngay-sinh'); ?>:</label> <?php echo date('d/m/Y', strtotime($account['birthday'])); ?></p>
          <p><label><?php elang('quoc-gia'); ?>:</label> <?php echo ($account['nationality']) ? $nations[$account['nationality']] : ''; ?></p>
          <p><label><?php elang('dia-chi'); ?>:</label> <?php echo $account['address']; ?></p>
          <p><label><?php elang('thong-tin-them'); ?>:</label> <?php echo $account['additional_info']; ?></p>
          <p><label><?php elang('thanh-pho'); ?>:</label> <?php echo $account['city']; ?></p>
          <p><label><?php elang('post-code'); ?>:</label> <?php echo $account['postcode']; ?></p>
        </span>
      </div>
      <div class="grid_12 padding-top-20">
        <h3 class="col-eleven"><span><?php elang('lich-su-giao-dich'); ?></span></h3>
        <div class="mbs">
          <table class="lich-su-giao-dich">
            <thead>
              <tr>
                <th><?php elang('ma-giao-dich'); ?></th>
                <th><?php elang('thoi-gian'); ?></th>
                <th><?php elang('dich-vu'); ?></th>
                <th><?php elang('so-tien'); ?></th>
                <th><?php elang('trang-thai'); ?></th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
<?php if(!empty($transactions)):
  foreach ($transactions as $transaction): ?>
              <tr>
                <th><?php echo $transaction['id']; ?></th>
                <th><?php echo tn_datetime($transaction['created_time']); ?></th>
                <th><?php elang($transaction['type']); ?></th>
                <th><?php echo tn_money_format($transaction['amount']); ?></th>
                <th><?php echo '<span class="trans_st'.$transaction['status'].'">'.lang('trans_st'.$transaction['status']).'</span>'; ?></th>
                <th><a href="<?php echo base_url('member/transaction/id/'.$transaction['id']); ?>"><?php elang('xem-chi-tiet'); ?></a></th>
              </tr>
  <?php endforeach;
else: ?>
              <tr>
                <th colspan="6"><?php elang('chua-co-thong-tin'); ?></th>
              </tr>
<?php endif; ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="6"><a href="<?php echo base_url('member/history/'); ?>"><?php elang('xem-them'); ?></a></th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
  </div>
</div>