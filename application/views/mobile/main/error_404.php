<div class="error_page">
  <div class="row clearfix">
    <div class="CLE">
      <i class="icon_dislike errori"></i>
      <h2 class="tac mtt"><?php elang('error-404-info'); ?></h2>
      <a href="/" class="tbutton medium"><span><i class="icons-arrow-left mi"></i> <?php elang('ve-trang-chu'); ?></span></a>
    </div>
  </div><!-- row -->
</div><!-- end 404 place -->
