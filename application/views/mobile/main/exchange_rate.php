<section>
  <div class="support">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('ty-gia-ngoai-te'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>

<div class="page-content">
  <div class="row clearfix mbs ngoai-te">
    <table class="" cellpadding="0" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th style="border-bottom:1px solid #f1f1f1;padding:5px;background: #fff;color: #055c98;border-right: 1px solid #055c98;"><?php elang('ma-ngoai-te'); ?></th>
          <th style="border-bottom:1px solid #f1f1f1;padding:5px;background: #fff;color: #055c98;border-right: 1px solid #055c98;"><?php elang('ten-ngoai-te'); ?></th>
          <th style="border-bottom:1px solid #f1f1f1;padding:5px;background: #fff;color: #055c98;border-right: 1px solid #055c98;"><?php elang('mua-ngoai-te'); ?></th>
        </tr>
      </thead>
      <tbody>
<?php
foreach ($source['Exrate'] as $exrate):
  $code     = $exrate['@attributes']['CurrencyCode'];
  $name     = $exrate['@attributes']['CurrencyName'];
  $buy      = ($exrate['@attributes']['Buy'] != 0) ? $exrate['@attributes']['Buy'] : '-';
?>
        <tr>
          <td style="border-bottom:1px dashed #f1f1f1;padding:5px;"><?php echo $code ?></td>
          <td style="border-bottom:1px dashed #f1f1f1;padding:5px;"><?php echo $name ?></td>
          <td style="border-bottom:1px dashed #f1f1f1;padding:5px;font-weight:bold;"><?php echo $buy ?></td>
        </tr>
<?php endforeach; ?>
      </tbody>
    </table>
  </div><!-- row -->
</div><!-- end page content -->
  </div>
</section>
