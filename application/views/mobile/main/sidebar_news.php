<div class="grid_3">
<?php if (count($category_news)):?>
  <div class="widget">
    <div class="menu-right-news"><h3><?php elang('danh-muc'); ?></h3></div>
    <div class="menu-right">
      <ul>
        <?php foreach ($category_news as $cat_child)
        {
          $link = (! empty($cat_child['link'])) ? $cat_child['link'] : site_url($cat_child['seo_name']);
          echo '<li><a href="'.$link.'"><i class="icon-caret-right"></i> '.$cat_child['name'].'</a></li>';
        } ?>
      </ul>
    </div>
  </div>
<?php endif; ?>

  <div class="widget ads">
    <h3 class="col-title">Ads</h3><span class="liner"></span>
    <a href="#" class="#" title="Advertise"><img src="https://payway.vn/public/upload/files/banner-right-payway.jpg" alt=""></a>
  </div><!-- widget ads -->

</div><!-- sidebar -->