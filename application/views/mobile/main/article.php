<section>
  <div class="about">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php echo $article['name']; ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <div class="bai-viet">
  <?php if ($article['display'] == 1):
    echo $article['description'];
  else: ?>
    <div class="grid_9 posts">
      <div class="post">
        <div class="thumb_f">
          <?php if ($article['image']) echo '<img src="'.$article_image_url.$article['image'].'" alt="">';?>
        </div>

        <div class="post_content">
          <?php echo $article['description']; ?>
        </div>
      </div><!-- post image -->
      <hr>
      <div style="margin-top:20px;"></div>
    <?php if (count($article_more)): ?>
      <div class="related_posts clearfix">
        <h3 class="col-title mb"><?php elang('cac-bai-viet-khac'); ?></h3>
        <ul>
        <?php foreach ($article_more as $atc_more)
        {
          $atc_image = ($atc_more['image']) ? $article_image_url.$atc_more['image'] : NO_IMAGE;
          echo '<li>
            <a href="'.site_url($atc_more['category_seo_name'].'/'.$atc_more['seo_name']).'">
              <span>'.$atc_more['name'].'</span>
            </a>
          </li>';
        }
        ?>
        </ul>
      </div>
    <?php endif;?>
    
    </div><!-- posts -->
    
  <?php endif; ?>
    </div>
  </div>
</section>