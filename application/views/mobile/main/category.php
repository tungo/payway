<div class="breadcrumb">
  <h4><?php echo $category['name']; ?></h4>
</div>
<div class="decoration"></div> 
<div class="content danh-muc">
<div class="page-content">
  <div class="row clearfix mbs">
<?php if (count($articles)): ?>

    <div class="grid_9 posts m_thumbnails">
    <?php foreach ($articles as $article) : ?>

      <div class="post clearfix">
        <div class="thumb_f"><a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>"><img src="<?php echo ($article['image']) ? $article_image_url.$article['image'] : NO_IMAGE; ?>" alt=""></a></div>
        <div class="content_half">
          <div class="meta_box">
            <h3> <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>"><?php echo $article['name']; ?></a> </h3>
            <div class="post_meta">
              <span><i class="icon_clock_alt"></i><a> <?php echo tn_date($article['created_time']); ?></a></span>
            </div><!-- meta -->
          </div><!-- meta box -->
          <div class="post_content">
            <p>
              <?php echo $article['synopsis']; ?>
              <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>" class="post_more"><?php elang('xem-them'); ?></a>
            </p>
          </div>
        </div>
      </div><!-- post image -->
      <hr>

    <?php endforeach; ?>

      <div class="pagination-tt clearfix">
        <?php echo $params['pagination']->links; ?>
      </div><!-- pagination -->

    </div><!-- posts -->
<?php else: ?>
    <div class="grid_12 posts m_thumbnails text-center">
      <h3><?php elang('chua-co-thong-tin'); ?></h3>
    </div><!-- posts -->
<?php endif; ?>
  </div><!-- row -->
</div><!-- end page content -->
</div>