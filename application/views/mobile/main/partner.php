
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('doi-tac-dau-tu'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('doi-tac-dau-tu'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_12 alpha">
                <div class="clearfix mbs tro-thanh-doi-tac">
                    <ul class="tabs">
                        <li><a href="#t-1" class="active"><span class="dai-ly"></span></a><h3><?php elang('dai-ly'); ?></h3></li>
                        <li><a href="#t-2" class=""><span class="doi-tac"></span></a><h3><?php elang('doi-tac'); ?></h3></li>
                        <li><a href="#t-3" class=""><span class="nha-dau-tu"></span></a><h3><?php elang('nha-dau-tu'); ?></h3></li>
                    </ul>
                    <ul class="tabs-content">
                        <li id="t-1" class="active">
                            <h3 class="col-eleven"><span> <?php elang('dai-ly'); ?> </span></h3>
                            <br>
                            <h3>Tiêu chí trở thành đại lý của chúng tôi: </h3>
                                Với mục tiêu tiếp cận khách hàng khắp mọi miền đất nước nhằm mang lại những dịch vụ tiện ích. Pay Way chào đón các cá nhân, pháp nhân trở thành đại lý của chúng tôi trên tinh thần xây dựng mối quan hệ hợp tác đôi bên cùng có lợi.
                                <br><br>
                            <h3>Lợi ích hấp dẫn của đại lý</h3>
                                <ul>
                                    <li><strong>CHIẾT KHẤU CAO:</strong> Sử dụng dịch vụ tại Payway.vn Thanh Toán Hóa Đơn đảm bảo sẽ mang lại hiệu quả về doanh thu và lợi nhuận cho đôi bên cùng có lợi.</li>
                                    <li><strong>AN TOÀN VÀ ỔN ĐỊNH:</strong> Dịch vụ ổn định và đảm bảo an toàn. Kết nối không chỉ toàn quốc mà mở rộng trên toàn cầu từ tất cả các nhà cung cấp dịch vụ viễn thông, dịch vụ kiều hối, thanh toán trực tuyến,…</li>
                                    <li><strong>TĂNG KHẢ NĂNG CẠNH TRANH:</strong> Hưởng mức chiết khấu cao hơn mức chiết khấu chung toàn hệ thống, theo chính sách thưởng doanh thu. </li>
                                </ul>
                            <strong>Nếu quý khách quan tâm trở thành đại lý của Pay Way hoặc cần biết thêm chi tiết vui lòng cung cấp thông tin theo form dưới đây:</strong><br>
                                <form>
                                    <div class="form">
                                        <label><?php elang('ho-ten'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('so-dien-thoai'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('email'); ?> <font color="red">*</font></label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('thuoc-don-vi'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('dia-chi'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('nganh-nghe-kinh-doanh'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('chuc-vu-hien-tai'); ?> </label>
                                        <input type="text" value name="">
                                    </div>
                                    <div class="form">
                                        <label><?php elang('noi-dung-can-trao-doi'); ?> <font color="red">*</font></label>
                                        <textarea></textarea>
                                    </div>
                                    <div class="form center">
                                        <input type="submit" value="<?php elang('gui'); ?>" name="">
                                        <input type="reset" value="<?php elang('nhap-lai'); ?>" name="">
                                    </div>
                                </form>
                        </li>
                        <li id="t-2" class="" style="display: none;">
                            <h3 class="col-eleven"><span> <?php elang('doi-tac'); ?> </span></h3>
                            <br>
                            <h3>Hợp tác với Pay Way, Các đơn vị cung ứng dịch vụ thông qua Pay Way cũng được hưởng lợi ích như: </h3>
                            <ul>
                                <li>Các chương trình Co-Branding đồng thương hiệu sẽ do Pay Way thực hiện. Đặc biệt, các chuyên viên marketing chuyên nghiệp sẽ thực hiện E-marketing hay các tương tác từ các mạng xã hội.</li>
                                <li>Giảm chi phí công việc hành chính về xử lý thanh toán, chuyển tiền và nâng cao dữ liệu thông tin hơn phương pháp truyền thống </li>
                                <li>Cải thiện mối quan hệ với khách hàng bằng cách cung cấp thêm lựa chọn dịch vụ cho họ.</li>
                                <li>Hỗ trợ tăng doanh số bán, gia tăng lợi nhuận, thu hút khách hàng và phát triển doanh nghiệp.</li>
                                <li>Triển khai các chương trình thưởng cho đại lý.</li>
                                <li>Triển khai các chương trình khuyến mãi và cùng quảng bá thương hiệu</li>
                            </ul>
                            <h3>Liên hệ để trở thành đối tác Payway.vn</h3><br>
                            <font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
                            Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
                            MST: 0305221500<br>
                            H: 08 6686 5378 – 091 989 1909<br>
                            P: 08 3602 8733 - F: 08 629 09 340<br>
                            E: info@eway.com.vn<br>
                            URL: www.Payway.vn<br>

                        </li>
                        <li id="t-3" class="" style="display: none;">
                            <h3 class="col-eleven"><span> <?php elang('nha-dau-tu'); ?> </span></h3>
                            <br>
                            <h3>Tổng quan về Pay Way</h3>
                            Pay Way là nhà cung cấp dịch vụ thanh toán hóa đơn và chuyển tiền kiều hối chuyên nghiệp, đã hợp tác với nhiều tập đoàn chuyển tiền toàn cầu từ trên 150 quốc gia và vùng lãnh thổ. Chúng tôi cung cấp chuyển tiền nhanh, an toàn và đa dạng đảm bảo cho khách hàng của mình sự chọn lựa hoàn hảo nhất.
                            <h3>Vì sao chọn Pay Way?</h3>
                            <ul>
                                <li>Uy tín – Đối tác của Pay Way là Tập đoàn bưu điện Việt Nam, Công ty Chuyển mạch tài chính quốc gia Việt Nam và các tập đoàn chuyển tiền uy tín toàn cầu.</li>
                                <li>Đa dạng – Quý khách có thể chọn lựa nhiều kênh như nhận tiền mặt tại bưu điện, người thân chuyển tiền vào tài khoản hay thẻ ATM của Quý khách, và dịch vụ giao tiền tận nhà trên toàn quốc.</li>
                                <li>Nhanh chóng – Tiền được chuyển vào tài khỏan trong giây lác, khi người thân vừa gửi là Quý khách sẽ có tiền ngay; nhận tiền tại quầy chỉ trong 5 phút và thời gian giao tiền tại nhà từ 3-48 giờ tùy theo từng khu vực.</li>
                                <li>Thuận tiện – Với mạng lưới trên 100,000 điểm giao dịch trên toàn thế giới thông qua đối tác của Pay Way Dễ dàng – Trung tâm dịch vụ khách hàng 24/7 của chúng tôi sẽ luôn hổ trợ Quý vị</li>
                            </ul>
                            <h3>Hợp tác đầu tư:</h3>
                            <ul>
                                <li>Chiến lược liên minh, hợp tác được mở rộng ngoài ranh giới quốc gia, với tiêu chí www (Bạn Win – Khách hàng Win và Chúng tôi Win). Điều này sẽ góp phần làm nâng cao sức mạnh của chúng tôi nhằm đóng góp tích cực các sản phẩm, dịch vụ mới hoàn hảo cung cấp cho cộng đồng.</li>
                                <li>Với định hướng là công chúng hóa sở hữu nhằm mở rộng quy mô hoạt động và phát triển Pay Way. Chúng tôi luôn trân trọng đón nhận sự hợp tác từ khách hàng, đối tác, quỹ đầu tư, vv.</li>
                            </ul>
                            <h3>Mọi chi tiết liên hệ:</h3><br>
                            <font size="5"><strong>Công ty Cổ Phần Nhất Phương</strong></font><br>
                            Địa chỉ giao dịch: 401 Huỳnh Văn Bánh, P11, Q.PN<br>
                            MST: 0305221500<br>
                            H: 08 6686 5378 – 091 989 1909<br>
                            P: 08 3602 8733 - F: 08 629 09 340<br>
                            E: info@eway.com.vn<br>
                            URL: www.Payway.vn<br>

                        </li>
                    </ul>
                </div>
            </div>
  </div>
</div>