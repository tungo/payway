<section>
  <div class="support">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('ho-tro'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>

<div class="decoration"></div> 
<div class="content">
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="clearfix mbs ho-tro">
      <div class="grid_6">
          <h3><a href="ho-tro/huong-dan-nghiep-vu.html" class="view"><?php elang('huong-dan-nghiep-vu'); ?></a></h3>
      </div>
      <div class="grid_6">
          <h3><?php elang('huong-dan-su-dung'); ?></h3>
          <ul>
            <li><a href="ho-tro/dia-diem-chuyen-tien.html">Địa Điểm Chuyển Tiền</a></li>
            <li><a href="ho-tro/dia-diem-nhan-tien.html">Địa Điểm Nhận Tiền</a></li>
            <li><a href="ho-tro/bieu-phi-dich-vu.html">Biểu Phí Dịch Vụ</a></li>
            <li><a href="ho-tro/cach-nhan-tien.html">Cách Nhận Tiền</a></li>
            <li><a href="ho-tro/cach-chuyen-tien.html">Cách Chuyển Tiền</a></li>
          </ul>
      </div>
      <div class="grid_6">
          <h3><a href="ty-gia-ngoai-te.html" class="view"><?php elang('ty-gia-ngoai-te'); ?></a></h3>
      </div>
    </div>
  </div>
</div>
</div>
  </div>
</section>