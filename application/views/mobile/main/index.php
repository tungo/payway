<section class="banner">
      <div id="banner-km" class="fader">
        <?php foreach ($banner['Mobile'] as $bn)
          {
            $img = '<img class="slide" src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
            if ($bn['link'] != '')
            {
              echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
            }
            else
            {
              echo $img;
            }
          } ?>
          <div class="fader_controls">
            <ul class="pager_list"></ul>
          </div>
      </div>
      </section>
      <section class="service">
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/di-dong-tra-truoc.html"><img src="<?php echo base_url($theme['path'].'images/di-dong-tra-truoc.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('di-dong-tra-truoc'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/di-dong-tra-sau.html"><img src="<?php echo base_url($theme['path'].'images/di-dong-tra-sau.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('di-dong-tra-sau'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/mua-the-dien-thoai.html"><img src="<?php echo base_url($theme['path'].'images/mua-the-dien-thoai.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('mua-the-dien-thoai'); ?>
          </div>
        </div>        
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/mua-the-game.html"><img src="<?php echo base_url($theme['path'].'images/mua-the-game.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('mua-the-game'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/nap-tien-game.html"><img src="<?php echo base_url($theme['path'].'images/nap-tien-game.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('nap-tien-game'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/nap-tien-tai-khoan.html"><img src="<?php echo base_url($theme['path'].'images/nap-tien-tai-khoan.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('nap-tien-tai-khoan'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/tien-dien.html"><img src="<?php echo base_url($theme['path'].'images/tien-dien.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('tien-dien'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/hoa-don-adsl.html"><img src="<?php echo base_url($theme['path'].'images/tien-adsl.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('hoa-don-adsl'); ?>
          </div>
        </div>
        <div class="span-3">
          <div class="icons">
            <a href="dich-vu/ve-may-bay.html"><img src="<?php echo base_url($theme['path'].'images/ve-may-bay.png'); ?>"></a>
          </div>
          <div class="title">
            <?php elang('ve-may-bay'); ?>
          </div>
        </div>
      </section>