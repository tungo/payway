<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('dang-nhap'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <form method="post" id="register_form_increate">
      <div class="register">
        <input type="text" name="email" placeholder="<?php elang('email'); ?> *">
      </div>
      <div class="register">
        <input type="password" name="password" placeholder="<?php elang('password'); ?> *">
      </div>
      <div class="register">
        <input type="submit" value="<?php elang('dang-nhap'); ?>">
        <input type="reset" value="<?php elang('nhap-lai'); ?>">
        <a class="quen-mat-khau" href="<?php echo site_url('member/reset_password'); ?>"><?php elang('quen-mat-khau'); ?></a>
      </div>
    </form>
  </div>
</section>