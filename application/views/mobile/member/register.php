<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('dang-ky'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <h3 class="col-eleven"><span><?php elang('thong-tin-tai-khoan'); ?></span></h3>
          <div class="form-regester">
            <div class="register">
              <label><?php elang('email'); ?> *</label>
              <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="requiredField email">
            </div>
            <div class="register">
              <label><?php elang('password'); ?> *</label>
              <input type="password" name="password" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('nhap-lai-mat-khau'); ?> *</label>
              <input type="password" name="confirm" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('ten'); ?> *</label>
              <input type="text" name="ten" value="<?php echo set_value('ten'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('ho'); ?> *</label>
              <input type="text" name="ho" value="<?php echo set_value('ho'); ?>" class="requiredField">
            </div>
          </div>
          <h3 class="col-eleven"><span><?php elang('thong-tin-chung'); ?></span></h3>
          <div class="form-regester">
            <div class="register">
              <label><?php elang('gioi-tinh'); ?> *</label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="1" class="requiredField" <?php if (set_value('gioitinh') == 1) echo 'checked="checked"'; ?>><?php elang('nam'); ?></label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="2" class="requiredField" <?php if (set_value('gioitinh') == 2) echo 'checked="checked"'; ?>><?php elang('nu'); ?></label>
              <label class="gioi-tinh"><input type="radio" name="gioitinh" value="0" class="requiredField" <?php if (!set_value('gioitinh')) echo 'checked="checked"'; ?>><?php elang('khac'); ?></label>
            </div>
            <div class="register">
              <label><?php elang('ngay-sinh'); ?> *</label>
              <select name="ngaysinh" style="width:21%;">
                <option value=""><?php elang('date-ngay'); ?></option>
              <?php
              for ($i = 1; $i <= 31; $i++)
              {
                $selected = (set_value('ngaysinh') == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
              <select name="thangsinh" style="width:21%">
                <option value=""><?php elang('date-thang'); ?></option>
              <?php
              for ($i = 1; $i <= 12; $i++)
              {
                $selected = (set_value('thangsinh') == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
              <select name="namsinh" style="width:21%">
                <option value=""><?php elang('date-nam'); ?></option>
              <?php
              for ($i = 1996; $i >= 1896; $i--)
              {
                $selected = (set_value('namsinh') == $i) ? 'selected="selected"' : '' ;
                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('quoc-gia'); ?> *</label>
              <select name="quocgia">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nations as $id => $name)
              {
                $selected = (set_value('quocgia') == $id) ? 'selected="selected"' : '' ;
                echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('dia-chi'); ?> *</label>
              <input type="text" name="diachi" value="<?php echo set_value('diachi'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('thong-tin-them'); ?></label>
              <input type="text" name="thongtinthem" value="<?php echo set_value('thongtinthem'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('thanh-pho'); ?> *</label>
              <input type="text" name="thanhpho" value="<?php echo set_value('thanhpho'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('post-code'); ?> *</label>
              <input type="text" name="postcode" value="<?php echo set_value('postcode'); ?>" class="requiredField">
            </div>
          </div>
          <h3 class="col-eleven"><span><?php elang('thong-tin-bao-mat'); ?></span></h3>
          <div class="form-regester">
            <div class="register">
              <label><?php elang('cau-hoi-bao-mat'); ?> *</label>
              <select name="cauhoi">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($questions as $id => $name)
              {
                $selected = (set_value('cauhoi') == $id) ? 'selected="selected"' : '' ;
                echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="register">
              <label><?php elang('cau-tra-loi'); ?> *</label>
              <input type="text" name="traloi" value="<?php echo set_value('traloi'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('chung-minh-thu'); ?> *</label>
              <input type="text" name="chungminhthu" value="<?php echo set_value('chungminhthu'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('so-dien-thoai'); ?> *</label>
              <input type="text" name="dienthoai" value="<?php echo set_value('dienthoai'); ?>" class="requiredField">
            </div>
            <div class="register">
              <label><?php elang('ma-xac-nhan'); ?> *</label>
              <?php echo $recaptcha_html; ?>
            </div>
            <div class="register">
              <?php elang('agreement'); ?>
              <a href="<?php echo site_url('thong-tin/thoa-thuan-nguoi-dung'); ?>"><?php elang('thoa-thuan-nguoi-dung'); ?></a>
            </div>
          </div>
          <div class="register">
            <input type="submit" value="<?php elang('dang-ky'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
          </div>
        </form>
  </div>
</section>
