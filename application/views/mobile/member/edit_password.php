<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('doi-mat-khau'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <div class="grid_9">
      <div class="grid_12">
        <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <div class="register">
            <label><?php elang('mat-khau-cu'); ?></label>
            <input type="password" name="oldpass" class="requiredField email">
          </div>
          <div class="register">
            <label><?php elang('mat-khau-moi'); ?></label>
            <input type="password" name="newpass" class="requiredField">
          </div>
          <div class="register">
            <label><?php elang('xac-nhan-mat-khau'); ?></label>
            <input type="password" name="confirm" class="requiredField">
          </div>
          <div class="register">
            <input type="submit" value="<?php elang('luu'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
          </div>
        </form>
      </div>
  </div>
</section>