<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('lay-lai-mat-khau'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <?php echo $this->tn_message->get(); ?>
    <form method="post" id="register_form_increate">
      <div class="register">
        <input type="text" name="email" placeholder="<?php elang('email'); ?> *">
      </div>
      <div class="register">
        <label><?php elang('ma-xac-nhan'); ?> *</label>
        <?php echo $recaptcha_html; ?>
      </div>
      <div class="register">
        <input type="submit" value="<?php elang('lay-lai-mat-khau'); ?>">
        <input type="reset" value="<?php elang('nhap-lai'); ?>">
      </div>
    </form>
  </div>
</section>

