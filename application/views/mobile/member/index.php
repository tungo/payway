<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('thong-tin-tai-khoan'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <div class="grid_9">
      <div class="grid_12">
        <h3 class="col-eleven"><span><i class="icon_profile"></i> <?php elang('chao'); ?>, <?php echo $account['firstname']; ?></span></h3>
        <p class="mbs"></p>
        <?php echo $this->tn_message->get(); ?>
      </div>
      <div class="grid_6">
        <h3 class="col-title"> <?php elang('thong-tin-tai-khoan'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('email'); ?>:</label> <?php echo $account['email']; ?></p>
          <p><label><?php elang('ten'); ?>:</label> <?php echo $account['firstname']; ?></p>
          <p><label><?php elang('ho'); ?>:</label> <?php echo $account['lastname']; ?></p>
          <p><label><?php elang('password'); ?>:</label> ********</p>
        </span>
      </div>
      <div class="grid_6">
        <h3 class="col-title"><?php elang('thong-tin-bao-mat'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('cau-hoi-bao-mat'); ?>:</label> <?php echo ($account['question_id']) ? $questions[$account['question_id']] : ''; ?></p>
          <p><label><?php elang('cau-tra-loi'); ?>:</label> <?php echo tn_hide($account['answer']); ?></p>
          <p><label><?php elang('chung-minh-thu'); ?>:</label> <?php echo tn_hide($account['identification']); ?></p>
          <p><label><?php elang('so-dien-thoai'); ?>:</label> <?php echo tn_hide($account['phone']); ?></p>
        </span>
      </div>
      <div class="grid_12 padding-top-20">
        <h3 class="col-title"><?php elang('thong-tin-chung'); ?></h3><span class="liner"></span>
        <span class="mbs">
          <p><label><?php elang('gioi-tinh'); ?>:</label> <?php echo $gender[$account['gender']]; ?></p>
          <p><label><?php elang('ngay-sinh'); ?>:</label> <?php echo date('d/m/Y', strtotime($account['birthday'])); ?></p>
          <p><label><?php elang('quoc-gia'); ?>:</label> <?php echo ($account['nationality']) ? $nations[$account['nationality']] : ''; ?></p>
          <p><label><?php elang('dia-chi'); ?>:</label> <?php echo $account['address']; ?></p>
          <p><label><?php elang('thong-tin-them'); ?>:</label> <?php echo $account['additional_info']; ?></p>
          <p><label><?php elang('thanh-pho'); ?>:</label> <?php echo $account['city']; ?></p>
          <p><label><?php elang('post-code'); ?>:</label> <?php echo $account['postcode']; ?></p>
        </span>
      </div>
  </div>
</section>