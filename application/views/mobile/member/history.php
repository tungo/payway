<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('lich-su-giao-dich'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
    <div class="grid_9">
      <div class="grid_12">
        <div class="tim-lich-su-giao-dich">
          <form id="tool_form" method="post">
            <div class="tim">
              <label><?php elang('ma-giao-dich'); ?>:</label>
              <input type="text" name="keyword" value="<?php echo $filter['keyword']; ?>">
            </div>
            <div class="tim">
              <label><?php elang('dich-vu'); ?>:</label>
              <select name="type">
                <option value=""><?php elang('chon'); ?></option>
              <?php foreach ($select_type as $key => $val)
              {
                $selected = ($filter['type'] == $key) ? 'selected="selected"' : '';
                echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="tim">
              <label><?php elang('trang-thai'); ?>:</label>
              <select name="status">
                <option value=""><?php elang('chon'); ?></option>
              <?php foreach ($select_status as $key => $val)
              {
                $selected = ($filter['status'] == $key) ? 'selected="selected"' : '';
                echo '<option value="'.$key.'" '.$selected.' class="trans_member_st'.$key.'">'.$val.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="tim">
              <input type="submit" value="<?php elang('tim-kiem'); ?>">
              <input type="reset" value="<?php elang('nhap-lai'); ?>">
            </div>
          </form>
        </div>
        <div class="mbs padding-top-20">
          <table class="lich-su-giao-dich">
            <thead>
              <tr>
                <th><?php elang('ma-giao-dich'); ?></th>
                <th><?php elang('thoi-gian'); ?></th>
                <th><?php elang('dich-vu'); ?></th>
                <th><?php elang('so-tien'); ?></th>
                <th><?php elang('trang-thai'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
<?php if (! empty($items)):
  foreach ($items as $item): ?>
              <tr>
                <th><?php echo $item['id']?></th>
                <th><?php echo tn_datetime($item['created_time']); ?></th>
                <th><?php echo $select_type[$item['type']]; ?></th>
                <th><?php echo tn_money_format($item['amount']); ?></th>
                <th><?php echo '<span class="trans_st'.$item['status'].'">'.lang('trans_st'.$item['status']).'</span>'; ?></th>
                <th><a href="<?php echo base_url('member/transaction/id/'.$item['id']); ?>"><?php elang('xem-chi-tiet'); ?></a></th>
              </tr>
  <?php endforeach;
else: ?>
              <tr>
                <th colspan="6"><?php elang('chua-co-thong-tin'); ?></th>
              </tr>
<?php endif; ?>
            </tbody>
          </table>

          <div class="text-info entries-info pull-left">
            <?php elang('tong-so'); ?>: <?php echo $data['pagination']->total_rows; ?>
          </div>

          <div class="pull-right">
            <?php echo $data['pagination']->links; ?>
          </div>

        </div>
      </div>
  </div>
</section>

<link href="https://payway.vn/public/templates/payway2/styles/bootstrap-datetimepicker.min.css" rel="stylesheet">

<script src="https://payway.vn/public/templates/payway2/js/moment.min.js"></script>
<script src="https://payway.vn/public/templates/payway2/js/bootstrap.min.js"></script>
<script src="https://payway.vn/public/templates/payway2/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function () {
  $('#datetimepicker1').datetimepicker({
    pickTime: false
  });
  $('#datetimepicker2').datetimepicker({
    pickTime: false
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#tool_form input:reset').click(function(){
    $('#tool_form input:text').val('');
    $('#tool_form select option[value=""]').prop('selected', true);
    return false;
  });
});
</script>