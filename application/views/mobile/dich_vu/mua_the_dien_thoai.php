<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('mua-the-dien-thoai'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="content">
          <div class="loading hide">
            <label><img src="/public/templates/mobile/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <select name="menhgia" id="menhgia">
                <option value=""></option>
              </select>
            </div>
            <div class="form">
              <label><?php elang('so-luong'); ?>:</label>
              <input type="text" value="<?php echo set_value('soluong'); ?>" name="soluong" maxlength="5" autocomplete="off">
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<script type="text/javascript" src="https://payway.vn/public/templates/payway2/js/jquery-selectboxes-pack.js"></script>
<script type="text/javascript" src="https://payway.vn/public/templates/payway2/js/tn_payment/jquery-select-service1-vn.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($menhgia); ?>;
  var menhgia_df = '<?php echo set_value('menhgia'); ?>';
  var thanhtien = <?php echo json_encode($thanhtien); ?>;

  $('#nhacungcap').linkSelect('#menhgia', menhgia, menhgia_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('select[name="menhgia"]').val();
    var sl = $('input[name="soluong"]').val();
    var tien = (!ncc || !mg || !sl) ? 0 : thanhtien[ncc][mg] * sl;
    $('span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('input[name="soluong"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>
