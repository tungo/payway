
  <div class="widget">
    <div class="menu-right">
      <ul>
        <li<?php if($method=='dich_vu_kieu_hoi') echo ' class="current"'; ?>><a href="dich-vu/dich-vu-kieu-hoi.html"><?php elang('dich-vu-kieu-hoi'); ?></a></li>
        <li<?php if($method=='thanh_toan_online') echo ' class="current"'; ?>><a href="dich-vu/thanh-toan-online.html"><?php elang('thanh-toan-online'); ?></a></li>
        <li<?php if($method=='thanh_toan_offline') echo ' class="current"'; ?>><a href="dich-vu/thanh-toan-offline.html"><?php elang('thanh-toan-offline'); ?></a></li>
        <li<?php if($method=='dich_vu_khac') echo ' class="current"'; ?>><a href="dich-vu/dich-vu-khac.html"><?php elang('dich-vu-khac'); ?></a></li>
      </ul>
    </div>
  </div>
  <div class="widget">
    <div class="menu-right-news">
      <h3>Tin Tức</h3>
      <ul>
        <?php foreach ($articles[1021] as $article) {
          $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
          echo ' <li><a href="'.$link.'">'.$article['name'].'</a></li>';
        } ?>
      </ul>
    </div>
  </div>