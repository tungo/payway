<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('ket-qua-thanh-toan'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu <?php echo ($trans_result == 's') ? 'xu-ly-giao-dich' : 'xu-ly-that-bai'; ?>">
        <div class="col-eleven"><h3><?php echo ($trans_result == 's') ? lang('xu-ly-giao-dich') : lang('xu-ly-that-bai'); ?></h3></div>
        <div class="content">
        <?php if ($trans_result == 's'): ?>
          <div id="tn_count"></div>
          <p class="xu-ly"><?php elang('noi-dung-xu-ly-giao-dich'); ?></p>
          <script>
            var reload_time = 6;
            var counter = setInterval(tn_count, 1000);
            function tn_count() {
              reload_time -= 1;
              if (reload_time < 0)
              {
                clearInterval(counter);
                window.location.href='<?php echo $redirect_url; ?>';
                return;
              }
              var div = document.getElementById("tn_count");
              div.innerHTML = reload_time;
            }
          </script>
        <?php else: ?>
          <p class="thu-lai"><?php elang('noi-dung-xu-ly-that-bai'); ?></p>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>