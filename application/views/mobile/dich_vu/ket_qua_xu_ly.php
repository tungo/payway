<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('ket-qua-xu-ly'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu <?php echo ($rs_code == '00') ? 'thanh-cong' : 'that-bai'; ?>">
        <div class="col-eleven"><h3><?php echo $trans_service; ?> <?php echo ($rs_code == '00') ? lang('thanh-cong') : lang('that-bai'); ?></h3></div>
        <div class="content">
          <div class="thong-bao">
            <p><?php echo $trans_noti; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
