<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('dich-vu-kieu-hoi'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="dich-vu.html"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('dich-vu-kieu-hoi'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons dich-vu-chinh"><img src="public/images/dich-vu/kieu-hoi.png"></div>
        <div class="title"><h3><?php elang('dich-vu-kieu-hoi'); ?></h3></div>
        <div class="content">
          <div class="grid_7">
            <strong><?php elang('noi-dung-dich-vu-kieu-hoi'); ?></strong><br><br>
            <a href="gioi-thieu/dich-vu-kieu-hoi.html" class="tbutton medium"><span><i class="icon-thumbs-up"></i> <?php elang('xem-chi-tiet'); ?> </span></a>
          </div>
          <div class="grid_5">
            <object width="280" height="1215"><param name="movie" value="//www.youtube-nocookie.com/v/ssss?hl=vi_VN&amp;version=3"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube-nocookie.com/v/ssss?hl=vi_VN&amp;version=3" type="application/x-shockwave-flash" width="280" height="215" allowscriptaccess="always" allowfullscreen="true"></embed></object>
          </div>
          <div class="grid_12 service-content">
              <ul class="tabs">
                <li><a href="#t-1" class="active"><?php elang('dich-vu-kieu-hoi-nhan-tai-quay'); ?></a></li>
                <li><a href="#t-2" class=""><?php elang('dich-vu-kieu-hoi-nhan-vao-tai-khoan'); ?></a></li>
                <li><a href="#t-3" class=""><?php elang('dich-vu-kieu-hoi-nhan-tai-nha'); ?></a></li>
                <li><a href="#t-4" class=""><?php elang('dich-vu-kieu-hoi-nhan-bang-dien-thoai'); ?></a></li>
              </ul>
              <ul class="tabs-content">
                <li id="t-1" class="active" style="display: block;">
                  <strong><?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-quay'); ?></strong>
                  <div class="clearfix mbs">
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/1-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-1'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/1-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-2'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/1-3.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-3'); ?>
                    </div>
                    <div class="grid_3" style="border:none;">
                      <img src="public/files/upload/chuyen-tien/1-4.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-quay-4'); ?>
                    </div>
                  </div>
                </li>
  
                <li id="t-2" class="" style="display: none;">
                  <strong><?php elang('noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan'); ?></strong>
                  <div class="clearfix mbs">
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-1'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-2'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-3.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-3'); ?>
                    </div>
                    <div class="grid_3" style="border:none;">
                      <img src="public/files/upload/chuyen-tien/2-4.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-vao-tai-khoan-4'); ?>
                    </div>
                  </div>
                </li>
  
                <li id="t-3" class="" style="display: none;">
                  <strong><?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-nha'); ?></strong>
                  <div class="clearfix mbs">
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-1'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-2'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/3-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-3'); ?>
                    </div>
                    <div class="grid_3" style="border:none;">
                      <img src="public/files/upload/chuyen-tien/3-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-tai-nha-4'); ?>
                    </div>
                  </div>
                </li>
  
                <li id="t-4" class="" style="display: none;">
                  <strong><?php elang('noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai'); ?></strong>
                  <div class="clearfix mbs">
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-1'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/2-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-2'); ?>
                    </div>
                    <div class="grid_3">
                      <img src="public/files/upload/chuyen-tien/4-1.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-3'); ?>
                    </div>
                    <div class="grid_3" style="border:none;">
                      <img src="public/files/upload/chuyen-tien/4-2.png">
                      <?php elang('noi-dung-dich-vu-kieu-hoi-nhan-bang-dien-thoai-4'); ?>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>