<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('thong-tin-giao-dich'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu <?php echo ($trans_result == 's') ? 'thanh-cong' : 'that-bai'; ?>">
        <div class="icons"><?php echo ($trans_result == 's') ? '<i class="icon_check"></i>' : '<i class="icon_error-triangle_alt"></i>'; ?></div>
        <div class="title"><h3><?php echo ($trans_result == 's') ? lang('giao-dich-thanh-cong') : lang('giao-dich-that-bai'); ?></h3></div>
        <div class="content">
          <?php echo $detail; ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>