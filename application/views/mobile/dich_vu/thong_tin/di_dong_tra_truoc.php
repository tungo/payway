<div class="thong-bao">
  <p><?php echo $trans_noti; ?></p>
</div>
<div class="chi-tiet">
  <h3><span><?php elang('thong-tin-dich-vu'); ?></span></h3>
  <ul>
      <li><label><?php elang('dich-vu'); ?>:</label> <?php elang('di-dong-tra-truoc'); ?></li>
      <li><label><?php elang('nha-cung-cap'); ?>:</label> <?php echo $nhacungcap[$transaction['data']['nhacungcap']]; ?></li>
      <li><label><?php elang('menh-gia'); ?>:</label> <?php echo $menhgia[$transaction['data']['nhacungcap']][$transaction['data']['menhgia']]; ?></li>
  </ul>
  <h3><span><?php elang('thong-tin-giao-dich'); ?></span></h3>
  <ul>
      <li><label><?php elang('ma-giao-dich'); ?>:</label> <?php echo $transaction['id']; ?></li>
      <li><label><?php elang('ngan-hang'); ?>:</label> <?php echo $bank_code[$transaction['bank']]; ?></li>
      <li><label><?php elang('so-di-dong'); ?>:</label> <?php echo $transaction['data']['dauso'].$transaction['data']['didong']; ?></li>
      <li><label><?php elang('thanh-tien'); ?>:</label> <?php echo tn_money_format($transaction['amount']); ?></li>
  </ul>
</div>