<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('dich-vu'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><?php elang('dich-vu'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <!--<div class="action mbf clearfix">
      <div class="inner">
        <a class="tbutton flr medium" href="main/partner"><span><?php elang('xem-chi-tiet'); ?></span></a>
        <div class="matn">
          <h4>Trở Thành Đại Lý, Đối Tác, Nhà Đầu Tư Của PAY WAY</h4>
          <p>text text text text text text text text text text text text text text text text text text text </p>
        </div>
      </div>
    </div>-->
    <div class="thanh-toan services">
          <div class="row clearfix dich-vu">
            <div class="grid_3">
              <a href="dich-vu/dich-vu-kieu-hoi.html">
                <img src="public/images/dich-vu/kieu-hoi.png">
                <div class="s_info">
                  <h3><?php elang('dich-vu-kieu-hoi'); ?></h3>
                </div>
              </a>
            </div>
            <div class="grid_3">
              <a href="dich-vu/thanh-toan-online.html">
                <img src="public/images/dich-vu/thanh-toan-online.png">
                <div class="s_info">
                  <h3><?php elang('thanh-toan-online'); ?></h3>
                </div>
              </a>
            </div>
            <div class="grid_3">
              <a href="dich-vu/thanh-toan-offline.html">
                <img src="public/images/dich-vu/thanh-toan-offline.png">
                <div class="s_info">
                  <h3><?php elang('thanh-toan-offline'); ?></h3>
                </div>
              </a>
            </div>
            <div class="grid_3">
              <a href="dich-vu/dich-vu-khac.html">
                <img src="public/images/dich-vu/dich-vu-khac.png">
                <div class="s_info">
                  <h3><?php elang('dich-vu-khac'); ?></h3>
                </div>
              </a>
            </div>
          </div>
      </div>
    <div class="thanh-toan-online">
      <div class="grid_8">
        <h3 class="two-line"><span><?php elang('dich-vu-khac'); ?></span></h3>
        <div class="grid_6 set_two">
          <a href="dich-vu/di-dong-tra-truoc.html">
            <div class="s_icon-2"><span class="tra-truoc"></span></div>
            <div class="s_info">
              <h3><?php elang('di-dong-tra-truoc'); ?></h3>
              <p><?php elang('noi-dung-di-dong-tra-truoc'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/di-dong-tra-sau.html">
            <div class="s_icon-2"><span class="tra-sau"></span></div>
            <div class="s_info">
              <h3><?php elang('di-dong-tra-sau'); ?></h3>
              <p><?php elang('noi-dung-di-dong-tra-sau'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/mua-the-dien-thoai.html">
            <div class="s_icon-2"><span class="the-dien-thoai"></span></div>
            <div class="s_info">
              <h3><?php elang('mua-the-dien-thoai'); ?></h3>
              <p><?php elang('noi-dung-mua-the-dien-thoai'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/mua-the-game.html">
            <div class="s_icon-2"><span class="the-game"></span></div>
            <div class="s_info">
              <h3><?php elang('mua-the-game'); ?></h3>
              <p><?php elang('noi-dung-mua-the-game'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/nap-tien-game.html">
            <div class="s_icon-2"><span class="nap-tien-game"></span></div>
            <div class="s_info">
              <h3><?php elang('nap-tien-game'); ?></h3>
              <p><?php elang('noi-dung-nap-tien-game'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/nap-tien-tai-khoan.html">
            <div class="s_icon-2"><span class="nap-tien-tai-khoan"></span></div>
            <div class="s_info">
              <h3><?php elang('nap-tien-tai-khoan'); ?></h3>
              <p><?php elang('noi-dung-nap-tien-tai-khoan'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/tien-dien.html">
            <div class="s_icon-2"><span class="tien-dien"></span></div>
            <div class="s_info">
              <h3><?php elang('tien-dien'); ?></h3>
              <p><?php elang('noi-dung-tien-dien'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/hoa-don-adsl.html">
            <div class="s_icon-2"><span class="adsl"></span></div>
            <div class="s_info">
              <h3><?php elang('hoa-don-adsl'); ?></h3>
              <p><?php elang('noi-dung-hoa-don-adsl'); ?></p>
            </div>
          </a>
        </div>
        <div class="grid_6 set_two">
          <a href="dich-vu/ve-may-bay.html">
            <div class="s_icon-2"><span class="ve-may-bay"></span></div>
            <div class="s_info">
              <h3><?php elang('ve-may-bay'); ?></h3>
              <p><?php elang('noi-dung-ve-may-bay'); ?></p>
            </div>
          </a>
        </div>
      </div>
      <div class="grid_4">
        <h3 class="two-line"><span><?php elang('tin-tuc'); ?></span></h3>
        <div class="menu-right-news">
          <ul>
             <?php foreach ($articles[1021] as $article) {
                $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
                echo ' <li><a href="'.$link.'">'.$article['name'].'</a></li>';
              } ?>
           </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row clearfix mbs" data-gen="bigEntrance">
          <div class="tac">
            <h3 class="col-eleven"><span> <?php elang('nha-cung-cap-dich-vu'); ?> & <?php elang('chap-nhan-thanh-toan'); ?> </span></h3>
            <span class="after_line"></span>
            <div class="simply-scroll simply-scroll-container">
              <div class="simply-scroll-clip">
                <ul id="scroller" class="chap-nhan-thanh-toan simply-scroll-list" style="width: 2520px;">
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/viettel.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Mobifone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vinaphone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vietnamobile.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/sfone.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Beeline.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/EVN.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vnpt.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/capnuocgiadinh.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Vietnam-Airlines.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/vietjetair.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/2/Jetstar-Pacific.gif'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/agribank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/bidv.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/dongabank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/hdbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/mastercard.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/oceanbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/sacombank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/tcb.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/vcb.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/viettinbank.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/visa.jpg'); ?>"></li>
                  <li><img src="<?php echo base_url($theme['path'].'images/pay/1/vnmart.jpg'); ?>"></li>
                </ul>
              </div>
            </div>
          </div>

        </div>
</div>
