<section>
  <div class="login">
    <h3 class="title"><span class="back"><a onclick="history.go(-1);"><img src="<?php echo base_url($theme['path'].'images/back-icon.png'); ?>"></a></span><span><?php elang('di-dong-tra-sau'); ?></span><span class="next"><a onclick="history.go(+1);"><img src="<?php echo base_url($theme['path'].'images/next-icon.png'); ?>"></a></span></h3>
<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="content">
          <div class="loading hide">
            <label><img src="/public/templates/mobile/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('so-di-dong'); ?>:</label>
              <select name="dauso" class="prephone" id="dauso">
                <option value=""></option>
              </select>
              <input type="text" value="<?php echo set_value('didong'); ?>" name="didong" class="postphone" maxlength="7" autocomplete="off" onkeypress='return isNumberKey(event);'>
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <input type="text" value="<?php echo set_value('menhgia'); ?>" name="menhgia" maxlength="7" autocomplete="off">
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript" src="https://payway.vn/public/templates/payway2/js/jquery-selectboxes-pack.js"></script>
<script type="text/javascript" src="https://payway.vn/public/templates/payway2/js/tn_payment/jquery-select-service1-vn.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($dauso); ?>;
  var dauso_df = '<?php echo set_value('dauso'); ?>';
  var giamgia = <?php echo json_encode($giamgia); ?>;

  $('#nhacungcap').linkSelect('#dauso', dauso, dauso_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('input[name="menhgia"]').val();
    var tien = (!ncc || !mg) ? 0 : mg * giamgia[ncc];
    $('span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });
  $('input[name="menhgia"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('input[name="menhgia"]').keyup();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('input[name="menhgia"]').keyup();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>
