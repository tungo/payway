<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('thanh-toan-offline'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="dich-vu.html"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('thanh-toan-offline'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons dich-vu-chinh"><img src="public/images/dich-vu/thanh-toan-offline.png"></div>
        <div class="title"><h3><?php elang('thanh-toan-offline'); ?></h3></div>
        <div class="content">

        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>