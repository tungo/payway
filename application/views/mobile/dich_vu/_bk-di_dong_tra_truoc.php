
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php elang('di-dong-tra-truoc'); ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i><?php elang('trang-chu'); ?></a></li>
        <li><a href="payment/"><?php elang('dich-vu'); ?></a></li>
        <li><?php elang('di-dong-tra-truoc'); ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->


<div class="page-content">
  <div class="row clearfix mbs">
    <div class="grid_9">
      <div class="left-dich-vu">
        <div class="icons"><span class="tra-truoc"></span></div>
        <div class="title"><h3><?php elang('di-dong-tra-truoc'); ?></h3></div>
        <div class="content">
          <div class="loadding hide">
            <label><img src="/public/templates/payway2/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <form>
            <div class="form">
              <label>Dịch Vụ:</label>
              <select name="service" id="service">
                <option>--- Chọn ----</option>
                <option value="tratruoc">Nạp Tiền Trả Sau</option>
                <option value="trasau">Nạp Tiền Trả Trước</option>
              </select>
            </div>
            <div class="form">
              <label>Nhà Cung Cấp:</label>
                <select name="provider" id="provider">
                  <option value="">Please select a country</option>
                </select>
            </div>
            <div class="form">
              <label>Mệnh Giá Tiền:</label>
              <select>
                <option value="">10.000</option>
                <option value="">20.000</option>
                <option value="">30.000</option>
                <option value="">50.000</option>
                <option value="">100.000</option>
                <option value="">200.000</option>
                <option value="">300.000</option>
                <option value="">500.000</option>
              </select>
            </div>
            <div class="form">
              <label>Số Lượng:</label>
              <input type="text" value="" name=""> 
            </div>
            <div class="form">
              <label>Giá Bán:</label>
              <span>0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="Thanh Toán">
              </span>
            </div>
          </form>
          <div class="form">
            <fieldset>
              <legend>Thông Tin Hướng Dẫn</legend>
              <p>
                asda sdasd
              </p>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
    <div class="grid_3">
      <?php echo $sidebar; ?>
    </div>
  </div>
</div>

<script type="text/javascript" src="jquery-selectboxes-pack.js"></script>
<script type="text/javascript" src="jquery-stateselect.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#service').linkToStates('#provider');
  });
</script>
