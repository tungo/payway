
<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title">Liên Hệ</h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="<?php echo site_url(); ?>"><i class="icon_house_alt mi"></i>Trang Chủ</a></li>
        <li>Liên hệ</li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->
<div style="position: absolute;width:100%;left:0px;">
  <iframe src="https://www.google.com/maps/ms?msa=0&amp;msid=206618670661376285872.0004ec989adc1252aef0d&amp;ie=UTF8&amp;t=m&amp;ll=10.794546,106.672826&amp;spn=0.010328,0.014613&amp;z=16&amp;iwloc=0004ec989adcbd8c67281&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="100%" height="300"></iframe>
</div>
<div class="page-content">
  <div class="row clearfix lienhe">
    <div class="grid_8 contact_c">
      <h3 class="col-title">Liên hệ</h3>
      <p>Vui lòng nhập thông tin liên hệ với chúng tôi theo mẫu bên dưới.</p>

      <form method="post" id="contactForm" action="">
        
        <input type="text" name="senderName" id="senderName" placeholder="Name *" class="requiredField">
        
        <div class="clearfix">
          <div class="grid_6 alpha fll">
            <input type="text" name="senderPhone" id="senderPhone" placeholder="Phone *" class="requiredField">
          </div>
          <div class="grid_6 omega flr">
            <input type="text" name="senderEmail" id="senderEmail" placeholder="Email Address *" class="requiredField email">
          </div>
        </div>
        <div>
          <textarea name="message" id="message" placeholder="Message *" class="requiredField" rows="8"></textarea>
        </div>
        <input type="submit" id="sendMessage" name="sendMessage" value="Send Email">
        <span></span>
      </form>

    </div><!-- grid8 -->

    <div class="grid_4 sidebar">
      <div class="widget">
        <p>
          Địa chỉ: <strong>401 Huỳnh Văn Bánh, Phường 11, Quận Phú Nhuận, TpHCM</strong><br>
          HOTLINE: <strong>08 3937 5678 - 0919 89 19 09</strong><br>
          FAX: <strong>08 629 09 340</strong><br>
          Email: <strong>info@payway.vn</strong><br>
          Website: <strong>www.payway.vn</strong><br>
        </p>
      </div><!-- widget -->
    </div><!-- sidebar -->

  </div><!-- row -->
</div><!-- end page content -->
