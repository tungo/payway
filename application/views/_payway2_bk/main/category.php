<ul class="breadcrumb">
	<li><a href="<?php echo site_url(); ?>" title="Trang chủ">Trang chủ</a> <span class="divider">/</span></li>
	<li class="active"><?php echo $category_list[$params['id']];?></li>
</ul>

<div class="clearfix"></div>

<h2 style="text-align:center;"><?php echo $category_list[$params['id']];?></h2>

<hr>

<div class="container" id="main">
	<div class="row">

		<?php echo $sidebar; ?>

		<!-- product -->
		<div class="span9" id="product">

<?php if (isset($product[0])): ?>

			<!-- list -->
			<div class="list">
				<ul class="thumbnails">
<?php
	$i = 1;
	foreach ($product as $val)
	{
		$first = ($i % 3 == 1) ? 'first' : '';
		$image = ($val->image) ? $product_image_url.'thumbs/'.$val->image : NO_IMAGE;
		$html = '<li class="span3 '.$first.'">
			<a href="'.site_url('san-pham/'.$val->id.'-'.furl($val->name)).'" class="thumbnail text-info" title="'.$val->name.'">
				<img src="'.$image.'" alt="'.$val->name.'">
				'.$val->name.'
			</a>
		</li>';
		echo $html;
		$i ++;
	}
?>
				</ul>
			</div>
			<!-- /list -->

			<!-- pagination -->
			<div class="pagination pagination-right">
				<?php echo $params['pagination']->links; ?>
			</div>
			<!-- /pagination -->

<?php else: ?>

	<h4 class="text-error">Thông tin đang được cập nhật.</h4>

<?php endif; ?>

		</div>
		<!-- /product -->

	</div>
</div>
