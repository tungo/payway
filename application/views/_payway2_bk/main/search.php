<ul class="breadcrumb">
	<li><a href="<?php echo site_url(); ?>" title="Trang chủ">Trang chủ</a> <span class="divider">/</span></li>
	<li class="active">Tìm kiếm</li>
</ul>

<div class="clearfix"></div>

<h2 style="text-align:center;"><?php echo 'Từ khóa: '.$params['keyword']; ?></h2>

<hr>

<div class="container" id="main">
	<div class="row">

		<!-- search -->
		<div class="span12">
			<div id="search">

<?php if (isset($product[0])): ?>
				<ul class="thumbnails">
<?php
	$i = 1;
	foreach ($product as $val)
	{
		$first = ($i % 4 == 1) ? 'first' : '';
		$image = ($val->image) ? $product_image_url.'thumbs/'.$val->image : NO_IMAGE;
		$html = '<li class="span3 '.$first.'">
			<a href="'.site_url('san-pham/'.$val->id.'-'.furl($val->name)).'" class="thumbnail text-info" title="'.$val->name.'">
				<img src="'.$image.'" alt="'.$val->name.'">
				'.$val->name.'
			</a>
		</li>';
		echo $html;
		$i ++;
	}
?>
				</ul>

				<!-- pagination -->
				<div class="pagination pagination-right">
					<?php echo $params['pagination']->links; ?>
				</div>
				<!-- /pagination -->

<?php else: ?>

	<h4 class="text-error padding-left">Không tìm thấy kết quả phù hợp với từ khóa.</h4>

<?php endif; ?>

			<div>
		</div>
		<!-- /search -->

	</div>
</div>
