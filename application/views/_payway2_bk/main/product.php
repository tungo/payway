<ul class="breadcrumb">
	<li><a href="<?php echo site_url(); ?>" title="Trang chủ">Trang chủ</a> <span class="divider">/</span></li>
	<li><a href="<?php echo site_url('danh-muc/'.$product['category_id'].'-'.furl($product['category_name'])); ?>" title="<?php echo $product['category_name']; ?>"><?php echo $product['category_name']; ?></a> <span class="divider">/</span></li>
	<li class="active"><?php echo $product['name']; ?></li>
</ul>

<div class="clearfix"></div>

<h2 style="text-align:center;"><?php echo $product['name']; ?></h2>

<hr>

<div class="container" id="main">
	<div class="row">

		<?php echo $sidebar; ?>

		<!-- product -->
		<div class="span9" id="product">

			<!-- detail -->
			<div class="detail">

				<!-- top -->
				<div class="row">

					<!-- avatar -->
					<div class="span4">
						<div class="avatar">
<?php
if ($product['image'])
{
	echo '<a href="'.base_url($product_image_url.'/'.$product['image']).'" rel="lightbox[product]" title="'.$product['name'].'"><img src="'.base_url($product_image_url.'/thumbs/'.$product['image']).'" alt="'.$product['name'].'" class="img-polaroid"></a>';
}
else
{
	echo '<img src="'.NO_IMAGE.'" alt="'.$product['name'].'" class="img-polaroid">';
}
?>
						</div>
						<div class="list-avatar">
							<ul class="inline">
<?php
if (isset($product_image[0]))
{
	$i = 1;
	foreach ($product_image as $item)
	{
		$class = ($i % 3 == 1) ? ' class="clear-left"' : '';
		echo '<li '.$class.'><a href="'.base_url($product_image_url.'/image/'.$item->image).'" rel="lightbox[product]" title="'.$item->name.'"><img src="'.base_url($product_image_url.'/image/thumbs/'.$item->image).'" alt="'.$item->image.'" class="img-polaroid"></a></li>';
		$i ++;
	}
}
?>
							</ul>
						</div>
					</div>
					<!-- /avatar -->

					<!-- info -->
					<div class="span5">
						<div class="info">
							<p>Giá: <span class="price"><?php echo number_format($product['price'], 0, '', '.'); ?></span></p>
							<!-- <p class="muted"><small>not VAT</small></p> -->
							<hr>
						</div>
					</div>
					<!-- /info -->

				</div>
				<!-- /top -->

				<hr>

				<!-- feature -->
				<div class="row">
					<div class="span9">
						<div class="tabbable">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab-description" data-toggle="tab" title="">Description</a>
								</li>
							</ul>
							<div class="tab-content" style="margin-bottom: 20px; border-bottom: 1px solid #ddd;">
								<div class="tab-pane active" id="tab-description">
									<?php echo $product['description']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /feature -->

			</div>
			<!-- /detail -->

		</div>
		<!-- /product -->

	</div>
</div>
