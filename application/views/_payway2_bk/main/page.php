<ul class="breadcrumb">
	<li><a href="<?php echo site_url(); ?>" title="Trang chủ">Trang chủ</a> <span class="divider">/</span></li>
	<li class="active"><?php echo $webpage['name'];?></li>
</ul>

<div class="clearfix"></div>

<h2 style="text-align:center;"><?php echo $webpage['name'];?></h2>

<hr>

<div class="container" id="main">
	<div class="row">

		<!-- sidebar -->
		<div class="span2">
			<ul class="nav nav-list">
				<li><a href="<?php echo site_url(); ?>">Trang chủ</a></li>
				<li><a href="<?php echo site_url('ve-cong-ty'); ?>">Về công ty</a></li>
				<li><a href="<?php echo site_url('lien-he'); ?>">Liên hệ</a></li>
				<li class="divider"></li>
			</ul>
		</div>
		<!-- /sidebar -->

		<!-- page -->
		<div class="span10">
			<?php echo $webpage['description'];?>
		</div>
		<!-- /page -->

	</div>
</div>
