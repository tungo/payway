
    <!-- Start Layer Slider -->
      <div class="sliderr">
        <div id="layerslider-container-fw-second">
          <div id="layerslider" style="width: 100%; height: 700px; margin: 0px auto">
          <div class="ls-layer" style="slidedirection: top; transition2d: all; slidedelay: 5000; durationin: 1000; durationout: 1500; easingin: easeInQuint; easingout: easeInOutQuint; delayin: 0; delayout: 0;">
            <img class="ls-bg" src="<?php echo base_url($theme['image'].'slides/1.jpg'); ?>" alt="">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/ip2.png'); ?>" alt="" style="position: absolute; top: 100px; left: 60px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 1000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_a1.png'); ?>" alt="" style="position: absolute; top: 160px; left: 600px; slidedirection : top; slideoutdirection : top;  durationin : 2000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_a2.png'); ?>" alt="" style="position: absolute; top: 290px; left: 600px; slidedirection : right; slideoutdirection : right;  durationin : 2500; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_a3.png'); ?>" alt="" style="position: absolute; top: 410px; left: 600px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_a3.png'); ?>" alt="" style="position: absolute; top: 410px; left: 750px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 3500; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
          </div>

          <div class="ls-layer" style="slidedirection: bottom; transition2d: all; slidedelay: 5500; durationin: 1000; durationout: 1500; easingin: easeInQuint; easingout: easeInOutQuint; delayin: 0; delayout: 0;">
            <img class="ls-bg" src="<?php echo base_url($theme['image'].'slides/2.jpg'); ?>" alt="">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_b1.png'); ?>" alt="" style="position: absolute; top: 200px; left: 520px; slidedirection : right; slideoutdirection : right;  durationin : 2000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_b2.png'); ?>" alt="" style="position: absolute; top: 270px; left: 520px; slidedirection : right; slideoutdirection : right;  durationin : 2700; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_b5.png'); ?>" alt="" style="position: absolute; top: 336px; left: 481px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 4500; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 100; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_b3.png'); ?>" alt="" style="position: absolute; top: 340px; left: 520px; slidedirection : right; slideoutdirection : right;  durationin : 3400; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_b4.png'); ?>" alt="" style="position: absolute; top: 410px; left: 525px; slidedirection : right; slideoutdirection : right;  durationin : 4100; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/ip1.png'); ?>" alt="" style="position: absolute; top: 82px; left: 60px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 1000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
          </div>

          <div class="ls-layer" style="slidedirection: right; transition2d: all; slidedelay: 4500; durationin: 1000; durationout: 1500; easingin: easeInQuint; easingout: easeInOutQuint; delayin: 0; delayout: 0;">
            <img class="ls-bg" src="<?php echo base_url($theme['image'].'slides/3.jpg'); ?>" alt="">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/ip3.png'); ?>" alt="" style="position: absolute; top: 53px; left: 500px; slidedirection : right; slideoutdirection : right;  durationin : 1000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
            <img class="ls-s2"  src="<?php echo base_url($theme['image'].'slides/cap_c1.png'); ?>" alt="" style="position: absolute; top: 380px; left: 60px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 1700; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500; delayout : 0; showuntil : 0; ">
          </div>
              </div>
        </div>
      </div>

      <script src="<?php echo base_url($theme['js'].'layerslider/js/layerslider.kreaturamedia.jquery.js'); ?>" type="text/javascript"></script>
      <script type="text/javascript">
      /* <![CDATA[ */
        $(document).ready(function(){
          $('#layerslider').layerSlider({
            thumbnailNavigation : 'hover',
            hoverPrevNext : false,
            navButtons : false,
            navStartStop : false,
            responsive : true,
            pauseOnHover : true,
            responsiveUnder : 1140,
            sublayerContainer : 1140
          });
        });
      /* ]]> */
      </script>
    <!-- End Layer Slider -->

      <div class="intro_p tac">
        <div class="row inner clearfix">
          <h2>Chuyển tiền từ trên 150 quốc gia và vùng lãnh thổ vào tài khoản ngân hàng<br> Nhận tiền trên 2000 điểm tại 63 tỉnh và Phương thức Thanh toán hóa đơn đa dạng ....<a href="#">(xem thêm)</a></h2>
          <a href="#" class="tbutton large"><i class="icon_globe-2"></i><span>Địa Điểm Gửi Tiền</span></a>
          <a href="#" class="tbutton large"><i class="icon_pin_alt"></i><span>Địa Điểm Nhận Tiền</span></a>
          <a href="#" class="tbutton large"><i class="icon_creditcard"></i><span>Thanh Toán Hóa Đơn</span></a>
        </div><!-- End row -->
      </div><!-- End intro p -->

      <div class="page-content">
        <div class="services mbs">
          <div class="row clearfix">
            <div class="grid_3">
              <div class="s_icon"><i class="icon_currency"></i><span class="fa-check"></span></div>
              <div class="s_info">
                <h3>Dịch vụ kiều hối</h3>
                <p>Đăng ký trong 10 giây với bước thao tác thân thiện nhất.</p>
                <a href="#" class="tbutton small"><span>Xem Chi Tiết</span></a>
              </div><!-- end info -->
            </div><!-- end grid3 -->
            <div class="grid_3">
              <div class="s_icon"><i class="icon_globe"></i><span class="fa-check"></span></div>
              <div class="s_info">
                <h3>Thanh toán Online</h3>
                <p>Quy trình tích hợp đơn giản với 1 bước, chỉ trong 3 phút.</p>
                <a href="#" class="tbutton small"><span>Xem Chi Tiết</span></a>
              </div><!-- end info -->
            </div><!-- end grid3 -->
            <div class="grid_3">
              <div class="s_icon"><i class="icon_wallet"></i><span class="fa-check"></span></div>
              <div class="s_info">
                <h3>Thanh toán Offline</h3>
                <p>Kết nối trực tuyến với 3 hãng hàng không quốc nội và 27 hãng hàng không quốc tế.</p>
                <a href="#" class="tbutton small"><span>Xem Chi Tiết</span></a>
              </div><!-- end info -->
            </div><!-- end grid3 -->
            <div class="grid_3">
              <div class="s_icon"><i class="icon_creditcard"></i><span class="fa-check"></span></div>
              <div class="s_info">
                <h3>Dịch vụ khác</h3>
                <p>Bảo mật hoàn hảo mọi tài khoản và giao dịch theo tiêu chuẩn bảo mật thông tin.</p>
                <a href="#" class="tbutton small"><span>Xem Chi Tiết</span></a>
              </div><!-- end info -->
            </div><!-- end grid3 -->
          </div><!-- end row -->
        </div><!-- end services -->



        <!--<div class="grey-line mbs">
          <div class="row clearfix">
            <div class="grid_6 custom_block1_img" data-gen="bigEntrance">
              <div class="h_slider"><img class="custom_1" src="<?php echo base_url($theme['image'].'assets/t1.png'); ?>" alt="tablet"></div>
              <div class="h_slider"><img class="custom_2" src="<?php echo base_url($theme['image'].'assets/t2.png'); ?>" alt="tablet"></div>
              <div class="h_slider"><img class="custom_3" src="<?php echo base_url($theme['image'].'assets/t3.png'); ?>" alt="tablet"></div>
              <div class="h_slider"><img class="custom_4" src="<?php echo base_url($theme['image'].'assets/t4.png'); ?>" alt="tablet"></div>
              <div class="h_slider"><img class="custom_5" src="<?php echo base_url($theme['image'].'assets/t5.png'); ?>" alt="tablet"></div>
            </div>

            <div class="grid_6 custom_block1" data-gen="bigEntrance">
              <div class="inner_list">
                <h3>Hướng Dẫn Sử Dụng Dịch Vụ Nhanh</h3>
                <ul>
                  <li><a href="#custom_1"><i class="icon_box-checked"></i>Cách gửi tiền</a></li>
                  <li><a href="#custom_2"><i class="icon_box-checked"></i>Cách nhận tiền</a></li>
                  <li><a href="#custom_3"><i class="icon_box-checked"></i>Thanh toán hóa đơn Online</a></li>
                  <li><a href="#custom_4"><i class="icon_box-checked"></i>Thanh toán hóa đơn Offline</a></li>
                  <li><a href="#custom_5"><i class="icon_box-checked"></i>Những câu hỏi thường gặp</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>-->

        <div class="grey-line mbs">
        <div class="row clearfix">
          <div class="grid_6">
            <h3 class="block_title">Video Galary</h3>
            <div class="video">
              <div class="player">
              <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="461" height="205"
                  poster="http://video-js.zencoder.com/oceans-clip.png"
                  data-setup="{}">
                <source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
              </video>
              </div>
            </div>
            <script src="<?php echo base_url($theme['js'].'video.js'); ?>"></script>
          </div><!-- end grid6 -->

          <div class="grid_6">
            <h3 class="block_title">How Pay Way Works?</h3>
            <img src="<?php echo base_url($theme['path'].'preview/How-Pay-Way-Works.jpg'); ?>" alt="#" class="fll">
          </div><!-- end grid6 -->
        </div><!-- end row -->
        </div>


        <div class="row clearfix mbs">
          <div class="grid_6">
            <h3 class="block_title">Tin Tức</h3>
            <ul class="tabs">
              <li><a href="#A_1" class="active"><?php echo $category_list[107]['name']; ?></a></li>
              <li><a href="#A_2"><?php echo $category_list[108]['name']; ?></a></li>
              <li><a href="#A_3"><?php echo $category_list[109]['name']; ?></a></li>
            </ul><!-- tabs -->

            <ul class="tabs-content">
              <li id="A_1" class="active">
              <?php foreach ($articles[107] as $article) {
                $image = ($article['image']) ? $article_image_url.'thumbs/'.$article['image'] : NO_IMAGE;
                $link = site_url('blog/107-'.furl($category_list[107]['name']).'/'.$article['id'].'-'.furl($article['name']));
                echo '<h5><a href="'.$link.'">'.$article['name'].'</a></h5>
                  <img src="'.$image.'" alt="" class="fll">'.$article['synopsis'].'<hr>';
              } ?>
              </li><!-- tab content -->
              <li id="A_2">
              <?php foreach ($articles[108] as $article) {
                $image = ($article['image']) ? $article_image_url.'thumbs/'.$article['image'] : NO_IMAGE;
                $link = site_url('blog/108-'.furl($category_list[108]['name']).'/'.$article['id'].'-'.furl($article['name']));
                echo '<h5><a href="'.$link.'">'.$article['name'].'</a></h5>
                  <img src="'.$image.'" alt="" class="fll">'.$article['synopsis'].'<hr>';
              } ?>
              </li><!-- tab content -->

              <li id="A_3">
              <?php foreach ($articles[109] as $article) {
                $image = ($article['image']) ? $article_image_url.'thumbs/'.$article['image'] : NO_IMAGE;
                $link = site_url('blog/109-'.furl($category_list[109]['name']).'/'.$article['id'].'-'.furl($article['name']));
                echo '<h5><a href="'.$link.'">'.$article['name'].'</a></h5>
                  <img src="'.$image.'" alt="" class="fll">'.$article['synopsis'].'<hr>';
              } ?>
              </li><!-- tab content -->
            </ul><!-- end tabs -->
          </div><!-- end grid6 -->

          <div class="grid_6">
              <h3 class="block_title">Hướng Dẫn Sử Dụng Dịch Vụ Nhanh</h3>
              <ul class="tt-accordion">
                <li class="sub-accordion">
                  <div class="accordion-head">
                    <div class="accordion-head-sign"></div>
                    <p>CÁCH GỬI TIỀN</p>
                  </div>
                  <div class="accordion-content">
                    Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed consequat, quam id ultricies mollis, quam odio viverra nisi, vulputate fringilla risus leo ac ante. In eleifend sem eu turpis viverra quis interdum dolor tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris.
                  </div>
                </li>
                <li class="sub-accordion">
                  <div class="accordion-head">
                    <div class="accordion-head-sign"></div>
                    <p>CÁCH NHẬN TIỀN</p>
                  </div>
                  <div class="accordion-content">Nulla id ligula arcu. Integer et tincidunt lectus. Duis id ligula diam, quis dapibus erat. Curabitur nec libero et est vulputate sollicitudin. Fusce sit amet turpis sed mauris volutpat posuere. Morbi vulputate, odio eget adipiscing faucibus, lorem ipsum facilisis justo, gravida tempus orci nisi ac eros. Pellentesque metus dolor, imperdiet sed ornare vel, tempus eu ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, massa ut lacinia congue, eros felis aliquam arcu, non euismod ligula sapien in est. Etiam vitae congue orci.
                  </div>
                </li>
                <li class="sub-accordion">
                  <div class="accordion-head">
                    <div class="accordion-head-sign"></div>
                    <p>THANH TOÁN HÓA ĐƠN ONLINE</p>
                  </div>
                  <div class="accordion-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed consequat, quam id ultricies mollis, quam odio viverra nisi, vulputate fringilla risus leo ac ante. In eleifend sem eu turpis viverra quis interdum dolor tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris.
                  </div>
                </li>
                <li class="sub-accordion">
                  <div class="accordion-head">
                    <div class="accordion-head-sign"></div>
                    <p>THANH TOÁN HÓA ĐƠN OFFLINE</p>
                  </div>
                  <div class="accordion-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed consequat, quam id ultricies mollis, quam odio viverra nisi, vulputate fringilla risus leo ac ante. In eleifend sem eu turpis viverra quis interdum dolor tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem dui, molestie vitae ornare tincidunt, semper non metus. Ut id arcu leo, id feugiat mauris.
                  </div>
                </li>
              </ul><!-- end accordion -->
          </div><!-- end grid6 -->
        </div><!-- clearfix -->

        <div class="row clearfix mbs" data-gen="bigEntrance">
          <div class="tac"><h3 class="block_title">Clients We’ve Worked With So Far</h3><span class="after_line"></span></div>
          <ul>
          <?php foreach ($partners as $partner) {
            $image = ($partner['image']) ? $partner_image_url.$partner['image'] : NO_IMAGE;
            echo '<li class="grid_2"><a href="'.$partner['link'].'" target="_blank">
              <img src="'.$image.'" alt="'.$partner['name'].'" class="toptip" title="'.$partner['name'].'">
            </a></li>';
          } ?>
          </ul>
        </div><!-- end row -->
      </div><!-- end page content -->
