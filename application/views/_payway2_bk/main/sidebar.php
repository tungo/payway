<!-- category -->
<div class="span3" id="category">
	<ul class="nav nav-list">
		<?php echo recursive_category($category); ?>
		<li class="divider"></li>
	</ul>

	<script>
		$(document).ready(function($){
			var category = <?php echo $category_id; ?>;
			$('#category ul.subnav').each(function(){
				$(this).parent('li').children('a:nth-child(1)').addClass('mainnav');
			});
			$('#category li[data="'+category+'"]').each(function(){
				$(this).addClass('active');
				$(this).parents('.subnav').siblings('a').addClass('current');
				$(this).parents('.subnav').addClass('current');
			});
			$('#category ul.subnav').parent('li').mouseover(function(){
				$(this).children('a:nth-child(1)').addClass('open');
				$(this).children('ul.subnav').addClass('open');
			}).mouseout(function(){
				// $(this).children('a:nth-child(1)').removeClass('open');
				// $(this).children('ul.subnav').removeClass('open');
			});
		});
	</script>
</div>
<!-- /category -->