<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php echo $category['name']; ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href="#"><i class="icon_house_alt mi"></i>Trang Chủ</a></li>
        <li><?php echo $category['name']; ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->
<div class="page-content">
  <div class="row clearfix mbs">
<?php if (count($articles)): ?>

    <div class="grid_9 posts m_thumbnails">
    <?php foreach ($articles as $article) : ?>

      <div class="post clearfix">
        <div class="thumb_f"><a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>"><img src="<?php echo ($article['image']) ? $article_image_url.'thumbs/'.$article['image'] : NO_IMAGE; ?>" alt=""></a></div>
        <div class="content_half">
          <div class="meta_box">
            <h3> <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>"><?php echo $article['name']; ?></a> </h3>
            <div class="post_meta">
              <span><i class="icon_clock_alt"></i><a> <?php echo tn_date($article['created_time']); ?></a></span>
            </div><!-- meta -->
          </div><!-- meta box -->
          <div class="post_content">
            <p>
              <?php echo $article['synopsis']; ?>
              <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>" class="post_more">Xem thêm &gt;</a>
            </p>
          </div>
        </div>
      </div><!-- post image -->

    <?php endforeach; ?>

      <div class="pagination-tt clearfix">
        <?php echo $params['pagination']->links; ?>
      </div><!-- pagination -->

    </div><!-- posts -->

    <?php echo $sidebar; ?>
<?php else: ?>
    <div class="grid_12 posts m_thumbnails text-center">
      <h3>CHƯA CÓ THÔNG TIN</h3>
    </div><!-- posts -->
<?php endif; ?>
  </div><!-- row -->
</div><!-- end page content -->