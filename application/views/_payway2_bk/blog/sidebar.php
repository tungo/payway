<div class="grid_3 sidebar">
<?php if (count($category_children)):?>
  <div class="widget">
    <h3 class="col-title">Danh Mục</h3>
    <ul class="list">
      <?php foreach ($category_children as $cat_child)
      {
        $link = (! empty($cat_child['link'])) ? $cat_child['link'] : site_url($cat_child['seo_name']);
        echo '<li><a href="'.$link.'"><i class="icon-caret-right"></i> '.$cat_child['name'].'</a></li>';
      } ?>
    </ul><!-- end list -->
  </div><!-- widget list -->
<?php endif; ?>

  <div class="widget ads">
    <h3 class="col-title">Ads</h3><span class="liner"></span>
    <a href="#" class="#" title="Advertise"><img src="<?php echo base_url($theme['image'].'ads1.gif'); ?>" alt=""></a>
  </div><!-- widget ads -->

</div><!-- sidebar -->