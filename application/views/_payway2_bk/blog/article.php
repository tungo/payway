<div class="breadcrumb-place">
  <div class="row clearfix">
    <h3 class="page-title"><?php echo $article['name']; ?></h3>
    <div class="breadcrumbIn">
      <ul>
        <li><a href=""><i class="icon_house_alt mi"></i>Trang chủ</a></li>
        <?php if ($article['category_id'] != 101): ?>
          <li><a href=""><?php echo $category['name']; ?></a></li>
        <?php endif; ?>
        <li><?php echo $article['name']; ?></li>
      </ul>
    </div><!-- breadcrumbIn -->
  </div><!-- row -->
</div><!-- end breadcrumb place -->
<div class="page-content">
  <div class="row clearfix mbs">
  <?php if ($article['display'] == 1):
    echo $article['description'];
  else: ?>
    <div class="grid_9 posts">

      <div class="post">

        <div class="meta_box">
          <div class="post_format"><i class="icon_pencil"></i></div>
          <h3><?php echo $article['name']; ?></h3>
          <div class="post_meta">
            <span><i class="icon_clock_alt"></i> <?php echo tn_date($article['created_time']); ?></span>
          </div><!-- meta more -->
        </div><!-- meta box -->
        <div class="thumb_f"><img src="<?php echo ($article['image']) ? $article_image_url.$article['image'] : NO_IMAGE; ?>" alt="#"></div>

        <div class="post_content">
          <?php echo $article['description']; ?>
        </div>
      </div><!-- post image -->
      
    <?php if (count($article_more)): ?>
      <div class="related_posts clearfix">
        <h3 class="col-title mb">Các Bài Viết Khác</h3>
        <?php foreach ($article_more as $atc_more)
        {
          $atc_image = ($atc_more['image']) ? $article_image_url.'thumbs/'.$atc_more['image'] : NO_IMAGE;
          echo '<div class="grid_4">
            <a href="'.site_url($atc_more['category_seo_name'].'/'.$atc_more['seo_name']).'">
              <img src="'.$atc_image.'" alt=""><span>'.$atc_more['name'].'</span>
            </a>
          </div>';
        }
        ?>
      </div>
    <?php endif;?>
    
    </div><!-- posts -->

    <?php echo $sidebar; ?>
    
  <?php endif; ?>
  </div><!-- row -->
</div><!-- end page content -->
