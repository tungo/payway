<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('doi-tac-dau-tu'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('doi-tac-dau-tu'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
    <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#t-1"  role="tab" data-toggle="tab"><h3><?php elang('dai-ly'); ?></h3></a></li>
                <li><a href="#t-2" role="tab" data-toggle="tab"><h3><?php elang('doi-tac'); ?></h3></a></li>
                <li><a href="#t-3" role="tab" data-toggle="tab"><h3><?php elang('nha-dau-tu'); ?></h3></a></li>
            </ul>
            <ul class="tab-content">
                <li id="t-1" class="tab-pane fade in active">
                    <?php elang('noi-dung-dai-ly'); ?>
                    <br><br>
                    <form>
                        <div class="form">
                            <label><?php elang('ho-ten'); ?> <font color="red">*</font></label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('so-dien-thoai'); ?> <font color="red">*</font></label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('email'); ?> <font color="red">*</font></label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('thuoc-don-vi'); ?> </label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('dia-chi'); ?> </label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('nganh-nghe-kinh-doanh'); ?> </label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('chuc-vu-hien-tai'); ?> </label>
                            <input type="text" value name="" style="width:30%">
                        </div>
                        <div class="form">
                            <label><?php elang('noi-dung-can-trao-doi'); ?> <font color="red">*</font></label>
                            <textarea style="width:30%;height:150px"></textarea>
                        </div>
                        <div class="form center">
                            <input type="submit" value="<?php elang('gui'); ?>" name="">
                            <input type="reset" value="<?php elang('nhap-lai'); ?>" name="">
                        </div>
                    </form>
                </li>
                <li id="t-2" class="tab-pane fade">
                    <?php elang('noi-dung-doi-tac'); ?>
                </li>
                <li id="t-3" class="tab-pane fade">
                    <?php elang('noi-dung-nha-dau-tu'); ?>
                </li>
            </ul>
          </div>
        </div>
    </div>
</section>
