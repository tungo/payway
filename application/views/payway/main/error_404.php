<section class="color error">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="tac mtt"><?php elang('error-404-info'); ?></h2>
      	<a href="/" class="tbutton medium"><span><i class="icons-arrow-left mi"></i> <?php elang('ve-trang-chu'); ?></span></a>
      </div>
    </div>
  </div>
</section>
