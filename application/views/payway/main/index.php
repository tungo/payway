
      <div class="main-banner"  id="showHere">
        <div class="wide-container">
          <div id="slides">
            <ul class="slides-container">
              <?php foreach ($banner['Top'] as $bn)
              {
                $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
                if ($bn['link'] != '')
                {
                  // echo '
                  //   <li>
                  //     <div class="img">
                  //       <a href="'.$bn['link'].'" title="'.$bn['name'].'" target="_blank">'.$img.'</a>
                  //     </div>
                  //     <div class="bg-banner"></div>
                  //     <img src="images/banner1.jpg" alt="">
                  //   </li>
                  // ';
                }
                else
                {
                  // echo '
                  //   <li>
                  //     <div class="img">
                  //       '.$img.'
                  //     </div>
                  //     <div class="bg-banner"></div>
                  //     <img src="images/banner1.jpg" alt="">
                  //   </li>
                  // ';
                }
              } ?>
              <li>
                <div class="img">
                  <a href="#"><img src="<?php echo base_url($theme['image'].'banner1-1.png'); ?>"></a>
                </div>
                <div class="bg-banner"></div>
                <img src="<?php echo base_url($theme['image'].'banner1.jpg'); ?>" alt="">
              </li>
              <li>
                <div class="img">
                  <a href="#"><img src="<?php echo base_url($theme['image'].'banner2-1.png'); ?>"></a>
                </div>
                <div class="plus"></div>
                <img src="<?php echo base_url($theme['image'].'banner2.jpg'); ?>" alt="">
              </li>
              <li>
                <div class="img">
                  <a href="#"><img src="<?php echo base_url($theme['image'].'banner1-1.png'); ?>"></a>
                </div>
                <div class="strip1"></div>
                <img src="<?php echo base_url($theme['image'].'banner3.jpg'); ?>" alt="">
              </li>
              <li>
                <div class="img">
                  <a href="#"><img src="<?php echo base_url($theme['image'].'banner3-1.png'); ?>"></a>
                </div>
                <div class="strip2"></div>
                <img src="<?php echo base_url($theme['image'].'banner4.jpg'); ?>" alt="">
              </li>
            </ul>
            <nav class="slides-navigation">
              <a href="#" class="next"><i class="glyphicons chevron-right"></i></a>
              <a href="#" class="prev"><i class="glyphicons chevron-left"></i></a>
            </nav>
          </div>
        </div>
      </div>
      <section class="color trang">
        <div class="container">
          <div class="row">
            <div class="col-md-6 chuyen-tien">
              <h3 class="title"><span><?php elang('dich-vu-kieu-hoi'); ?></span><div class="bottom"></div><h3>
              <div class="content">
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <label>TÌM KIẾM TRẠNG THÁI GIAO DỊCH</label>
                    </div>
                    <div class="col-md-6">
                      <select name="nhacungcap" id="nhacungcap"  style="width:100%">
                        <option value="">-- Chọn --</option>
                        <option value="VTC0058">Mobifone</option>
                        <option value="VTC0056">Viettel</option>
                        <option value="VTC0057">Vinaphone</option>
                        <option value="VTC0176">Vietnamobile</option>
                        <option value="VTC0177">Gmobile</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <label>Đơn vị chuyển tiền</label>
                      <select name="nhacungcap" id="nhacungcap"  style="width:100%">
                        <option value="">-- Chọn --</option>
                        <option value="VTC0058">Mobifone</option>
                        <option value="VTC0056">Viettel</option>
                        <option value="VTC0057">Vinaphone</option>
                        <option value="VTC0176">Vietnamobile</option>
                        <option value="VTC0177">Gmobile</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label>Mã chuyển tiền</label>
                      <input type="text" style="width:100%">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <label>Tên người gửi</label>
                      <input type="text" style="width:100%">
                    </div>
                    <div class="col-md-6">
                      <label>Tên người nhận</label>
                      <input type="text" style="width:100%">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-4">
                      <label>số tiền</label>
                      <input type="text" style="width:100%">
                    </div>
                    <div class="col-md-2">
                      <label>Loại</label>
                      <select name="nhacungcap" id="nhacungcap"  style="width:100%">
                        <option value=""></option>
                        <option value="VTC0058">Mobifone</option>
                        <option value="VTC0056">Viettel</option>
                        <option value="VTC0057">Vinaphone</option>
                        <option value="VTC0176">Vietnamobile</option>
                        <option value="VTC0177">Gmobile</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label>Điện thoại người nhận</label>
                      <input type="text" style="width:100%">
                    </div>
                  </div>
                  <div class="col-md-12">
                  <div class="col-md-12">
                    <label>Địa chỉ người nhận</label>
                    <input type="text" style="width:100%">
                  </div>
                  </div>
                  <!-- <div class="col-md-12">
                    <div class="col-md-6">
                      <label>Câu hỏi bảo mật</label>
                      <input type="text" style="width:100%">
                    </div>
                    <div class="col-md-6">
                      <label>Câu trả lời</label>
                      <input type="text" style="width:100%">
                    </div>
                  </div> -->
                  <div class="col-md-12">
                    <div class="col-md-12 text-right">
                      <input type="reset" value="Nhập Lại">
                      <input type="submit" value="Xác Nhận">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 thanh-toan-nhanh">
              <h3 class="title"><span><?php elang('thanh-toan-hoa-don'); ?></span><div class="bottom"></div><h3>
              <div class="col-md-5">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="active"><a href="#di-dong-tra-truoc" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('di-dong-tra-truoc'); ?></a></li>
                  <li><a href="#di-dong-tra-sau" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('di-dong-tra-sau'); ?></a></li>
                  <li><a href="#mua-the-dien-thoai" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('mua-the-dien-thoai'); ?></a></li>
                  <li><a href="#mua-the-game" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('mua-the-game'); ?></a></li>
                  <li><a href="#nap-tien-game" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('nap-tien-game'); ?></a></li>
                  <li><a href="#nap-tien-tai-khoan" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('nap-tien-tai-khoan'); ?></a></li>
                  <li><a href="#dien-nuoc" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('tien-dien'); ?></a></li>
                  <li><a href="#dich-vu-khac" role="tab" data-toggle="tab"><i class="glyphicons circle_plus"></i> <?php elang('dich-vu-khac'); ?></a></li>
                </ul>
              </div>
              <div class="col-md-7">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="di-dong-tra-truoc">
                    <?php echo $view_didongtratruoc; ?>
                  </div>
                  <div class="tab-pane fade" id="di-dong-tra-sau">
                    <?php echo $view_didongtrasau; ?>
                  </div>
                  <div class="tab-pane fade" id="mua-the-dien-thoai">
                    <?php echo $view_muathedienthoai; ?>
                  </div>
                  <div class="tab-pane fade" id="mua-the-game">
                    <?php echo $view_muathegame; ?>
                  </div>
                  <div class="tab-pane fade" id="nap-tien-game">
                    <?php echo $view_naptiengame; ?>
                  </div>
                  <div class="tab-pane fade" id="nap-tien-tai-khoan">
                    <?php echo $view_naptientaikhoan; ?>
                  </div>
                  <div class="tab-pane fade" id="dien-nuoc">...</div>
                  <div class="tab-pane fade" id="dich-vu-khac">...</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="color do">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
                <h3 class="title"><span><?php elang('dich-vu'); ?></span><div class="bottom"></div><h3>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/dich-vu-kieu-hoi.html"><img src="<?php echo base_url($theme['image'].'icons/kieu-hoi.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/dich-vu-kieu-hoi.html"><h3><?php elang('dich-vu-kieu-hoi'); ?></h3></a>
                    <p><?php elang('home-info-1'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/di-dong-tra-truoc.html"><img src="<?php echo base_url($theme['image'].'icons/tra-truoc.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/di-dong-tra-truoc.html"><h3><?php elang('di-dong-tra-truoc'); ?></h3></a>
                    <p><?php elang('noi-dung-di-dong-tra-truoc'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/di-dong-tra-sau.html"><img src="<?php echo base_url($theme['image'].'icons/tra-sau.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/di-dong-tra-sau.html"><h3><?php elang('di-dong-tra-sau'); ?></h3></a>
                    <p><?php elang('noi-dung-di-dong-tra-sau'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/mua-the-dien-thoai.html"><img src="<?php echo base_url($theme['image'].'icons/the-dien-thoai.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/mua-the-dien-thoai.html"><h3><?php elang('mua-the-dien-thoai'); ?></h3></a>
                    <p><?php elang('noi-dung-mua-the-dien-thoai'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/mua-the-game.html"><img src="<?php echo base_url($theme['image'].'icons/the-game.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/mua-the-game.html"><h3><?php elang('mua-the-game'); ?></h3></a>
                    <p><?php elang('noi-dung-mua-the-game'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/nap-tien-game.html"><img src="<?php echo base_url($theme['image'].'icons/nap-tien-game.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/nap-tien-game.html"><h3><?php elang('nap-tien-game'); ?></h3></a>
                    <p><?php elang('noi-dung-nap-tien-game'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/nap-tien-tai-khoan.html"><img src="<?php echo base_url($theme['image'].'icons/nap-tien-tai-khoan.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/nap-tien-tai-khoan.html"><h3><?php elang('nap-tien-tai-khoan'); ?></h3></a>
                    <p><?php elang('noi-dung-nap-tien-tai-khoan'); ?>.</p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/tien-dien.html"><img src="<?php echo base_url($theme['image'].'icons/tien-dien.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/tien-dien.html"><h3><?php elang('tien-dien'); ?></h3></a>
                    <p><?php elang('noi-dung-tien-dien'); ?>.</p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/hoa-don-adsl.html"><img src="<?php echo base_url($theme['image'].'icons/adsl.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/hoa-don-adsl.html"><h3><?php elang('hoa-don-adsl'); ?></h3></a>
                    <p><?php elang('noi-dung-hoa-don-adsl'); ?></p>
                  </div>
                </div>
                <div class="col-md-6 tt-dich-vu">
                  <div class="icons"><a href="dich-vu/ve-may-bay.html"><img src="<?php echo base_url($theme['image'].'icons/ve-may-bay.png'); ?>"></a></div>
                  <div class="content">
                    <a href="dich-vu/ve-may-bay.html"><h3><?php elang('ve-may-bay'); ?></h3></a>
                    <p><?php elang('noi-dung-ve-may-bay'); ?></p>
                  </div>
                </div>
            </div>
            <div class="col-md-5">
                <h3 class="title"><span><?php elang('tin-tuc'); ?></span><div class="bottom"></div><h3>
                  <?php foreach ($articles_km[1021] as $article) {
                    $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
                    $img  = $article_image_url.$article['image'];
                    $content = $article['synopsis'];
                    echo '
                      <div class="tin-tuc">
                        <div class="img"><a href="'.$link.'" style="background:url('.$img.');background-size:200% 100%"></a></div>
                        <div class="content">
                          <a href="'.$link.'"><h3>'.$article['name'].'</h3></a>
                          <p>'.$content.'</p>
                        </div>
                      </div>
                    ';
                  } ?>
            </div>
          </div>
        </div>
      </section>
      <section class="color trang">
        <div class="container">
          <div class="row">
            <div class="col-md-6 huong-dan">
              <h3 class="title"><span><?php elang('huong-dan-su-dung-dich-vu-nhanh'); ?></span><div class="bottom"></div><h3>
                <ul>
                  <li><i class="glyphicons circle_plus"></i> <a href="ho-tro/cach-chuyen-tien.html"><?php elang('cach-chuyen-tien'); ?></a></li>
                  <li><i class="glyphicons circle_plus"></i> <a href="ho-tro/cach-nhan-tien.html"><?php elang('cach-nhan-tien'); ?></a></li>
                  <li><i class="glyphicons circle_plus"></i> <a href="ho-tro/dia-diem-nhan-tien.html"><?php elang('dia-diem-nhan-tien'); ?></a></li>
                  <li><i class="glyphicons circle_plus"></i> <a href="dich-vu/thanh-toan-online.html"><?php elang('thanh-toan-hoa-don-online'); ?></a></li>
                  <li><i class="glyphicons circle_plus"></i> <a href="dich-vu/thanh-toan-offline.html"><?php elang('thanh-toan-hoa-don-offline'); ?></a></li>
                </ul>
            </div>
            <div class="col-md-6">
              <div class="banner-km">
                <div id="slides2">
                  <ul class="slides-container">
                    <?php foreach ($banner['Middle'] as $bn)
                    {
                      $img = '<img class="slide" src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
                      if ($bn['link'] != '')
                      {
                        echo '<li><a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a></li>';
                      }
                      else
                      {
                        echo '<li>'.$img.'</li>';
                      }
                    } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>