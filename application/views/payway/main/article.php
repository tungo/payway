<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span class="bai-viet"><?php echo $article['name']; ?></span></h3>
        <ul>
          <li><a href=""><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <?php if ($article['category_id'] != 1001)
          {
            $link = (! empty($category['link'])) ? $category['link'] : site_url($category['seo_name']);
            echo '<li><a href="'.$link.'">'.$category['name'].'</a> <i class="glyphicons chevron-right"></i></li>';
          }
          ?>
          <li><?php echo $article['name']; ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
    <?php if ($article['display'] == 1):
      echo '<div class="noi-dung-bai-viet">';
      echo $article['description'];
      echo '</div>';
    else: ?>
    <div class="col-md-9 noi-dung-bai-viet">
        <h3 class="title-left"><span><?php echo $article['name']; ?></span><div class="bottom"></div></h3>
        <span class="time"><i class="glyphicons clock"></i> <?php echo tn_date($article['created_time']); ?></span>
        <div class="content">
          <?php echo $article['description']; ?>
        </div>
        <h3 class="title-left"><span><?php elang('cac-bai-viet-khac'); ?></span><div class="bottom"></div></h3>
        <ul class="tin-khac">
        <?php foreach ($article_more as $atc_more)
        {
          $atc_image = ($atc_more['image']) ? $article_image_url.$atc_more['image'] : NO_IMAGE;
          echo '<li><a href="'.site_url($atc_more['category_seo_name'].'/'.$atc_more['seo_name']).'"><span>'.$atc_more['name'].'</span>
            </a> - <span class="time">'.tn_date($article['created_time']).'</span></li>';
        }
        ?>
        </ul>
    </div>
    <?php echo $sidebar; ?>
    <?php endif; ?>
    </div>
  </div>
</section>
