<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('ho-tro'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('ho-tro'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container ho-tro">
    <div class="row">

      <div class="col-md-3">
        <h3 class="title-left"><span><?php elang('danh-muc'); ?></span><div class="bottom"></div></h3>
        <div class="right-menu">
          <ul>
            <li><a href="thong-tin/chinh-sach-quy-dinh.html"><?php elang('chinh-sach-quy-dinh'); ?></a></li>
            <li><a href="thong-tin/dieu-khoan-bao-mat.html"><?php elang('dieu-khoan-bao-mat'); ?></a></li>
            <li><a href="thong-tin/quy-trinh-giao-dich.html"><?php elang('quy-trinh-giao-dich'); ?></a></li>
            <li><a href="ho-tro/cau-hoi-thuong-gap.html"><?php elang('cau-hoi-thuong-gap'); ?></a></li>
            <li><a href="ho-tro/huong-dan-nghiep-vu.html"><?php elang('huong-dan-nghiep-vu'); ?></a></li>
            <li><a href="ty-gia-ngoai-te.html"><?php elang('ty-gia-ngoai-te'); ?></a></li>
            <li><a href="ho-tro/dia-diem-chuyen-tien.html"><?php elang('dia-diem-chuyen-tien'); ?></a></li>
            <li><a href="ho-tro/dia-diem-nhan-tien.html"><?php elang('dia-diem-nhan-tien'); ?></a></li>
            <li><a href="ho-tro/bieu-phi-dich-vu.html"><?php elang('bieu-phi-dich-vu'); ?></a></li>
            <li><a href="ho-tro/cach-nhan-tien.html"><?php elang('cach-nhan-tien'); ?></a></li>
            <li><a href="ho-tro/cach-chuyen-tien.html"><?php elang('cach-chuyen-tien'); ?></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        <div class="col-md-12 xam">
          <a href="ho-tro/cau-hoi-thuong-gap.html"><h3><?php elang('cau-hoi-thuong-gap'); ?></h3></a>
          <p><?php elang('ho-tro-cau-hoi-thuong-gap'); ?></p>
        </div>
        <div class="col-md-12 xam">
          <a href="ho-tro/huong-dan-nghiep-vu.html"><h3><?php elang('huong-dan-nghiep-vu'); ?></h3></a>
            <p><?php elang('ho-tro-huong-dan-nghiep-vu'); ?></p>
        </div>
        <div class="col-md-12 xam">
          <h3><?php elang('huong-dan-su-dung'); ?></h3>
            <ul>
              <li><a href="ho-tro/dia-diem-chuyen-tien.html"><?php elang('dia-diem-chuyen-tien'); ?></a></li>
              <li><a href="ho-tro/dia-diem-nhan-tien.html"><?php elang('dia-diem-nhan-tien'); ?></a></li>
              <li><a href="ho-tro/bieu-phi-dich-vu.html"><?php elang('bieu-phi-dich-vu'); ?></a></li>
              <li><a href="ho-tro/cach-nhan-tien.html"><?php elang('cach-nhan-tien'); ?></a></li>
              <li><a href="ho-tro/cach-chuyen-tien.html"><?php elang('cach-chuyen-tien'); ?></a></li>
            </ul>
        </div>
        <div class="col-md-12 xam">
          <a href="ty-gia-ngoai-te.html"><h3><?php elang('ty-gia-ngoai-te'); ?></h3></a>
          <p><?php elang('ho-tro-ty-gia-ngoai-te'); ?></p>
        </div>
      </div>
    </div>
  </div>
</section>