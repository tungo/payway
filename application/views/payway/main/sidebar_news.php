<div class="col-md-3">
  <?php if (count($category_news)):?>
      <h3 class="title-left"><span><?php elang('danh-muc'); ?></span> <div class="bottom"></div></h3>
      <div class="right-menu">
        <ul>
          <?php foreach ($category_news as $cat_child)
          {
            $link = (! empty($cat_child['link'])) ? $cat_child['link'] : site_url($cat_child['seo_name']);
            echo '<li><a href="'.$link.'">'.$cat_child['name'].'</a></li>';
          } ?>
        </ul>
      </div>
  <?php endif; ?>
  <div class="ads-right">
      <?php foreach ($banner['Right'] as $bn)
      {
        $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
        if ($bn['link'] != '')
        {
          echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
        }
        else
        {
          echo $img;
        }
      } ?><!-- widget ads -->
  </div>
</div>