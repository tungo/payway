<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php echo $category['name']; ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php echo $category['name']; ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <?php if (count($articles)): ?>
        <div class="col-md-9">
            <?php foreach ($articles as $article) : ?>
              <div class="cat-tin-tuc">
                <div class="img">
                  <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>" style="background:url(<?php echo ($article['image']) ? $article_image_url.$article['image'] : NO_IMAGE; ?>);background-size:200% 100%"></a></div>
                <div class="content">
                  <a href="<?php echo site_url($article['category_seo_name'].'/'.$article['seo_name']); ?>"><h3><?php echo $article['name']; ?></h3><i class="glyphicons clock"></i> <?php echo tn_date($article['created_time']); ?></a>
                  <p><?php echo $article['synopsis']; ?></p>
                </div>
              </div>
            <?php endforeach; ?>
          <div class="page">
          <?php echo $params['pagination']->links; ?>
          </div>
        </div>
        <?php echo $sidebar; ?>
      <?php else: ?>
        <div class="col-md-12 text-center">
          <h3><?php elang('chua-co-thong-tin'); ?></h3>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>