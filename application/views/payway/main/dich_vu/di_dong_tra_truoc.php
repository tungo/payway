<form method="post" id="didongtratruoc" name="didongtratruoc" action="<?php echo base_url('dich-vu/di-dong-tra-truoc.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap" style="width:100%">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($didongtratruoc['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($didongtratruoc['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('so-di-dong'); ?>:</label>
    <span>
      <select name="dauso" class="prephone" id="dauso" style="width:27%">
        <option value=""></option>
      </select>
      <input type="text" value="" name="didong" class="postphone" maxlength="7" autocomplete="off" style="width:67%">
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <select name="menhgia" id="menhgia" style="width:100%">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang" style="width:100%">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="text-right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($didongtratruoc['dauso']); ?>;
  var menhgia = <?php echo json_encode($didongtratruoc['menhgia']); ?>;
  var thanhtien = <?php echo json_encode($didongtratruoc['thanhtien']); ?>;

  $('#didongtratruoc #nhacungcap').linkSelect('#didongtratruoc #dauso', dauso);
  $('#didongtratruoc #nhacungcap').linkSelect('#didongtratruoc #menhgia', menhgia);

  function tinhtien()
  {
    var ncc = $('#didongtratruoc select[name="nhacungcap"]').val();
    var mg = $('#didongtratruoc select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('#didongtratruoc span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('#didongtratruoc input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });

  $('#didongtratruoc select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('#didongtratruoc select[name="nhacungcap"]').each(function() {
    $('#didongtratruoc select[name="menhgia"]').change();
  });
  $('#didongtratruoc select[name="nhacungcap"]').change(function() {
    $('#didongtratruoc select[name="menhgia"]').change();
  });
  $('#didongtratruoc').submit(function(){
    $('#didongtratruoc .loading.hide').removeClass('hide');
    $('#didongtratruoc input:submit').prop('disabled', true);
  });
});
</script>
