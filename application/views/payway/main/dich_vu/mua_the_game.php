<form method="post" id="muathegame" name="muathegame" action="<?php echo base_url('dich-vu/mua-the-game.html'); ?>">
  <div class="width">
    <label><?php elang('nha-cung-cap'); ?>:</label>
    <span>
      <select name="nhacungcap" id="nhacungcap" style="width:100%">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($muathegame['nhacungcap'] as $ma => $ncc)
      {
        if (!empty($muathegame['disabled'][$ma])) continue 1;
        echo '<option value="'.$ma.'">'.$ncc.'</option>';
      }
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('menh-gia'); ?>:</label>
    <span>
      <select name="menhgia" id="menhgia" style="width:100%">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('so-luong'); ?>:</label>
    <span>
      <input type="text" value="" name="soluong" maxlength="5" autocomplete="off" style="width:100%">
    </span>
  </div>
  <div class="width">
    <label><?php elang('ngan-hang'); ?>:</label>
    <span>
      <select name="nganhang" id="nganhang" style="width:100%">
        <option value="">-- <?php elang('chon'); ?> --</option>
      <?php
      foreach ($bank_code as $code => $bank)
        echo '<option value="'.$code.'">'.$bank.'</option>';
      ?>
      </select>
    </span>
  </div>
  <div class="width">
    <label><?php elang('thanh-tien'); ?>: <strong><span data-id="thanhtien">0 đ</span></strong></label>
    <div class="text-right">
      <input type="submit" value="<?php elang('thanh-toan'); ?>">
    </div>
  </div>
  <div class="loading hide"><img src="/public/templates/payway2/images/loader.gif"></div>
</form>

<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($muathegame['menhgia']); ?>;
  var thanhtien = <?php echo json_encode($muathegame['thanhtien']); ?>;

  $('#muathegame #nhacungcap').linkSelect('#muathegame #menhgia', menhgia);

  function tinhtien()
  {
    var ncc = $('#muathegame select[name="nhacungcap"]').val();
    var mg = $('#muathegame select[name="menhgia"]').val();
    var sl = $('#muathegame input[name="soluong"]').val();
    var tien = (!ncc || !mg || !sl) ? 0 : thanhtien[ncc][mg] * sl;
    $('#muathegame span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('#muathegame input[name="soluong"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('#muathegame select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('#muathegame select[name="nhacungcap"]').each(function() {
    $('#muathegame select[name="menhgia"]').change();
  });
  $('#muathegame select[name="nhacungcap"]').change(function() {
    $('#muathegame select[name="menhgia"]').change();
  });
  $('#muathegame').submit(function(){
    $('#muathegame .loading.hide').removeClass('hide');
    $('#muathegame input:submit').prop('disabled', true);
  });
});
</script>

