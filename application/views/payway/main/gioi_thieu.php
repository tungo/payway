<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('gioi-thieu'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('gioi-thieu'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row gioi-thieu">
      <div class="col-md-12">
        <div class="col-md-3">
          <a href="gioi-thieu/ve-chung-toi.html"><img class="ve-chung-toi" src="https://payway.vn/public/upload/files/images/payway-logo-1.png" alt="#"></a>
        </div>
        <div class="col-md-9">
          <h3 class="title-left"><span><?php elang('ve-chung-toi'); ?></span><div class="bottom"></div></h3>
          <?php elang('gioi-thieu-noi-dung-ngan'); ?> <a href="gioi-thieu/ve-chung-toi.html">(<?php elang('xem-chi-tiet'); ?>)</a>
        </div>
      </div>
      <div class="col-md-12 gt-dich-vu">
          <h3 class="title"><span><?php elang('dich-vu'); ?></span><div class="bottom"></div></h3>
          <div class="col-md-3">
            <div class="icon"><a href="gioi-thieu/dich-vu-kieu-hoi.html"><img src="public/images/dich-vu/kieu-hoi.png"></a></div>
            <div class="content">
              <h3><a href="gioi-thieu/dich-vu-kieu-hoi.html"><?php elang('dich-vu-kieu-hoi'); ?></a></h3>
              <p><?php elang('gioi-thieu-dich-vu-kieu-hoi'); ?></p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="icon"><a href="gioi-thieu/thanh-toan-online.html"><img src="public/images/dich-vu/thanh-toan-online.png"></a></div>
            <div class="content">
              <h3><a href="gioi-thieu/thanh-toan-online.html"><?php elang('thanh-toan-online'); ?></a></h3>
              <p><?php elang('gioi-thieu-dich-vu-thanh-toan-online'); ?></p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="icon"><a href="gioi-thieu/thanh-toan-offline.html"><img src="public/images/dich-vu/thanh-toan-offline.png"></a></div>
            <div class="content">
              <h3><a href="gioi-thieu/thanh-toan-offline.html"><?php elang('thanh-toan-offline'); ?></a></h3>
              <p><?php elang('gioi-thieu-dich-vu-thanh-toan-offline'); ?></p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="icon"><a href="gioi-thieu/dich-vu-khac.html"><img src="public/images/dich-vu/dich-vu-khac.png"></a></div>
            <div class="content">
              <h3><a href="gioi-thieu/dich-vu-khac.html"><?php elang('dich-vu-khac'); ?></a></h3>
              <p><?php elang('gioi-thieu-dich-vu-khac'); ?></p>
            </div>
          </div>
      </div>
      <div class="col-md-12 bottom-banner">
        <?php foreach ($banner['Bottom'] as $bn)
          {
            $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
            if ($bn['link'] != '')
            {
              echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
            }
            else
            {
              echo $img;
            }
          } ?>
      </div>
    </div>
  </div>
</section>