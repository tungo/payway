<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('nap-tien-game'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('nap-tien-game'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('nap-tien-game'); ?></span> <div class="bottom"></div></h3>
        <div class="loading hide">
            <label><img src="/public/templates/payway2/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('tai-khoan'); ?>:</label>
              <input type="text" value="<?php echo set_value('taikhoan'); ?>" name="taikhoan" style="width:30%">
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <select name="menhgia" id="menhgia" style="width:30%">
                <option value=""></option>
              </select>
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
          <div class="form">
            <fieldset data-id="VTC0115" class="thongtinhuongdan hide">
              <legend>Vcoin</legend>

            </fieldset>
            <fieldset data-id="VTC0187" class="thongtinhuongdan hide">
              <legend>Bạc (Gate)</legend>

            </fieldset>

            <fieldset data-id="VTC0051" class="thongtinhuongdan hide">
              <legend>@Point Tam Quốc Chí (AsiaSoft)</legend>

            </fieldset>

            <fieldset data-id="VTC0053" class="thongtinhuongdan hide">
              <legend>@Point PlayPark (AsiaSoft)</legend>

            </fieldset>

            <fieldset data-id="VTC0209" class="thongtinhuongdan hide">
              <legend>mCash</legend>

            </fieldset>
          </div>
          
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {

  var menhgia = <?php echo json_encode($menhgia); ?>;
  var menhgia_df = '<?php echo set_value('menhgia'); ?>';
  var thanhtien = <?php echo json_encode($thanhtien); ?>;

  $('#nhacungcap').linkSelect('#menhgia', menhgia, menhgia_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>