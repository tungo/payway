<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('ket-qua-thanh-toan'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('ket-qua-thanh-toan'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left <?php echo ($trans_result == 's') ? 'thanh-cong' : 'that-bai'; ?>"><span><?php echo ($trans_result == 's') ? lang('xu-ly-giao-dich') : lang('xu-ly-that-bai'); ?></span> <div class="bottom"></div></h3>
          <?php if ($trans_result == 's'): ?>
            <div id="tn_count"></div>
            <p class="xu-ly"><?php elang('noi-dung-xu-ly-giao-dich'); ?></p>
            <script>
              var reload_time = 6;
              var counter = setInterval(tn_count, 1000);
              function tn_count() {
                reload_time -= 1;
                if (reload_time < 0)
                {
                  clearInterval(counter);
                  window.location.href='<?php echo $redirect_url; ?>';
                  return;
                }
                var div = document.getElementById("tn_count");
                div.innerHTML = reload_time;
              }
            </script>
          <?php else: ?>
            <p class="thu-lai"><?php elang('noi-dung-xu-ly-that-bai'); ?></p>
          <?php endif; ?>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>