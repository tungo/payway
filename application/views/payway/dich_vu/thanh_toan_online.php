<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('thanh-toan-online'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('thanh-toan-online'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9 dich-vu">
        <h3 class="title-left"><span><?php elang('thanh-toan-online'); ?></span> <div class="bottom"></div></h3>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/di-dong-tra-truoc.html"><img src="<?php echo base_url($theme['image'].'icons/tra-truoc.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/di-dong-tra-truoc.html"><h3><?php elang('di-dong-tra-truoc'); ?></h3></a>
              <p><?php elang('noi-dung-di-dong-tra-truoc'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/di-dong-tra-sau.html"><img src="<?php echo base_url($theme['image'].'icons/tra-sau.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/di-dong-tra-sau.html"><h3><?php elang('di-dong-tra-sau'); ?></h3></a>
              <p><?php elang('noi-dung-di-dong-tra-sau'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/mua-the-dien-thoai.html"><img src="<?php echo base_url($theme['image'].'icons/the-dien-thoai.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/mua-the-dien-thoai.html"><h3><?php elang('mua-the-dien-thoai'); ?></h3></a>
              <p><?php elang('noi-dung-mua-the-dien-thoai'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/mua-the-game.html"><img src="<?php echo base_url($theme['image'].'icons/the-game.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/mua-the-game.html"><h3><?php elang('mua-the-game'); ?></h3></a>
              <p><?php elang('noi-dung-mua-the-game'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/nap-tien-game.html"><img src="<?php echo base_url($theme['image'].'icons/nap-tien-game.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/nap-tien-game.html"><h3><?php elang('nap-tien-game'); ?></h3></a>
              <p><?php elang('noi-dung-nap-tien-game'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/nap-tien-tai-khoan.html"><img src="<?php echo base_url($theme['image'].'icons/nap-tien-tai-khoan.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/nap-tien-tai-khoan.html"><h3><?php elang('nap-tien-tai-khoan'); ?></h3></a>
              <p><?php elang('noi-dung-nap-tien-tai-khoan'); ?>.</p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/tien-dien.html"><img src="<?php echo base_url($theme['image'].'icons/tien-dien.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/tien-dien.html"><h3><?php elang('tien-dien'); ?></h3></a>
              <p><?php elang('noi-dung-tien-dien'); ?>.</p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/hoa-don-adsl.html"><img src="<?php echo base_url($theme['image'].'icons/adsl.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/hoa-don-adsl.html"><h3><?php elang('hoa-don-adsl'); ?></h3></a>
              <p><?php elang('noi-dung-hoa-don-adsl'); ?></p>
            </div>
          </div>
          <div class="col-md-6 tt-dich-vu">
            <div class="icons"><a href="dich-vu/ve-may-bay.html"><img src="<?php echo base_url($theme['image'].'icons/ve-may-bay.png'); ?>"></a></div>
            <div class="content">
              <a href="dich-vu/ve-may-bay.html"><h3><?php elang('ve-may-bay'); ?></h3></a>
              <p><?php elang('noi-dung-ve-may-bay'); ?></p>
            </div>
          </div>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>