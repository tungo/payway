<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('ket-qua-xu-ly'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('ket-qua-xu-ly'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left <?php echo ($rs_code == '00') ? 'thanh-cong' : 'that-bai'; ?>"><span><?php echo $trans_service; ?> <?php echo ($rs_code == '00') ? lang('thanh-cong') : lang('that-bai'); ?></span> <div class="bottom"></div></h3>
          <div class="thong-bao">
            <p><?php echo $trans_noti; ?></p>
          </div>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>