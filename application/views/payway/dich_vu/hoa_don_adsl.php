<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('hoa-don-adsl'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('hoa-don-adsl'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('hoa-don-adsl'); ?></span> <div class="bottom"></div></h3>
        <iframe style="border: none;" src="https://www.vban.vn/merchantvban/BillADSL.aspx?tmcode=10000076&svcode=BILLADSL-001" width="100%" height="700" scrolling="No" frameborder="No" ></iframe>
        <div class="ads-right">
          <?php foreach ($banner['Bottom'] as $bn)
            {
              $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
              if ($bn['link'] != '')
              {
                echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
              }
              else
              {
                echo $img;
              }
            } ?>
        </div>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>