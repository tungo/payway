<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('thong-tin-giao-dich'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('thong-tin-giao-dich'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left <?php echo ($trans_result == 's') ? 'thanh-cong' : 'that-bai'; ?>"><span><?php echo ($trans_result == 's') ? lang('giao-dich-thanh-cong') : lang('giao-dich-that-bai'); ?></span> <div class="bottom"></div></h3>
        <?php echo $detail; ?>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>