<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('di-dong-tra-truoc'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('di-dong-tra-truoc'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('di-dong-tra-truoc'); ?></span> <div class="bottom"></div></h3>
        <div class="loading hide">
            <label><img src="/public/templates/payway2/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('so-di-dong'); ?>:</label>
              <select name="dauso" class="prephone" id="dauso" style="width:10%">
                <option value=""></option>
              </select>
              <input type="text" value="<?php echo set_value('didong'); ?>" name="didong" class="postphone" maxlength="7" autocomplete="off" style="width:20%">
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <select name="menhgia" id="menhgia" style="width:30%">
                <option value=""></option>
              </select>
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
            <fieldset data-id="VTC0058" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử MobiFone</legend>
              <p>
                <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
                <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
                <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
                <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
                <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
              </p>
            </fieldset>
            <fieldset data-id="VTC0056" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Viettel</legend>
              <p>
                <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
                <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
                <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
                <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
                <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
              </p>
            </fieldset>

            <fieldset data-id="VTC0057" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Vinaphone</legend>
                <p>
                  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
                  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
                  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
                  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
                  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
                </p>
            </fieldset>

            <fieldset data-id="VTC0176" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Vietnamobile</legend>
                <p>
                  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
                  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
                  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
                  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
                  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
                </p>
            </fieldset>

            <fieldset data-id="VTC0177" class="thongtinhuongdan hide">
              <legend>Tiện ích của dịch vụ Nạp tiền điện tử Gmobile</legend>
                <p>
                  <strong>Không sử dụng thẻ cào:</strong> Không cần mua thẻ và thực hiện các thao tác cào thẻ hay gọi số 900 để nạp tiền.<br>
                  <strong>Đơn giản, tiện lợi:</strong> Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
                  <strong>Mệnh giá đa dạng:</strong> Có thể nạp tiền vào tài khoản với bất kỳ mệnh giá nào (từ 10,000 đồng đến 500,000 đồng).<br>
                  <strong>Được hưởng các chương trình khuyến mại:</strong> Tương tự như nạp tiền từ thẻ cào của các công ty Viễn thông.<br>
                  <strong>Kiểm tra tài khoản dễ dàng:</strong> Khách hàng kiểm tra số dư tài khoản bằng cách bấm *101# OK từ bàn phím máy ĐTDĐ.<br>
                </p>
            </fieldset>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($dauso); ?>;
  var dauso_df = '<?php echo set_value('dauso'); ?>';
  var menhgia = <?php echo json_encode($menhgia); ?>;
  var menhgia_df = '<?php echo set_value('menhgia'); ?>';
  var thanhtien = <?php echo json_encode($thanhtien); ?>;

  $('#nhacungcap').linkSelect('#dauso', dauso, dauso_df);
  $('#nhacungcap').linkSelect('#menhgia', menhgia, menhgia_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('select[name="menhgia"]').find("option:selected").val();
    var tien = (!ncc || !mg) ? 0 : thanhtien[ncc][mg];
    $('span[data-id="thanhtien"]').html(tien+' đ');
  }

  $('input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });

  $('select[name="menhgia"]').change(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('select[name="menhgia"]').change();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>