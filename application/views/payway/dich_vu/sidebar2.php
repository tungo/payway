<h3 class="title-left"><span><?php elang('danh-muc'); ?></span> <div class="bottom"></div></h3>
<div class="right-menu">
  <ul>
    <li<?php if($method=='dich_vu_kieu_hoi') echo ' class="active"'; ?>><a href="dich-vu/dich-vu-kieu-hoi.html"><?php elang('dich-vu-kieu-hoi'); ?></a></li>
    <li<?php if($method=='thanh_toan_online') echo ' class="active"'; ?>><a href="dich-vu/thanh-toan-online.html"><?php elang('thanh-toan-online'); ?></a></li>
    <li<?php if($method=='thanh_toan_offline') echo ' class="active"'; ?>><a href="dich-vu/thanh-toan-offline.html"><?php elang('thanh-toan-offline'); ?></a></li>
    <li<?php if($method=='dich_vu_khac') echo ' class="active"'; ?>><a href="dich-vu/dich-vu-khac.html"><?php elang('dich-vu-khac'); ?></a></li>
  </ul>
</div>
<h3 class="title-left"><span><?php elang('tin-tuc'); ?></span> <div class="bottom"></div></h3>
<ul class="right-news">
  <?php foreach ($articles[1021] as $article) {
    $link = site_url($article['category_seo_name'].'/'.$article['seo_name']);
    echo ' <li><a href="'.$link.'">'.$article['name'].'</a></li>';
  } ?>
</ul>
<div class="ads-right">
  <?php foreach ($banner['Right'] as $bn)
  {
    $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
    if ($bn['link'] != '')
    {
      echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
    }
    else
    {
      echo $img;
    }
  } ?>
</div>