<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('di-dong-tra-sau'); ?></span></h3>
        <ul>
          <li><a href="<?php echo site_url(); ?>"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><a href="<?php echo site_url('dich-vu'); ?>"><?php elang('dich-vu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('di-dong-tra-sau'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('di-dong-tra-sau'); ?></span> <div class="bottom"></div></h3>
        <div class="loading hide">
            <label><img src="/public/templates/payway2/images/loader.gif"></label>
            <span>Đang Xử Lý Giao Dịch</span>
          </div>
          <?php echo $this->tn_message->get();?>
          <form method="post" id="xulygiaodich">
            <div class="form">
              <label><?php elang('nha-cung-cap'); ?>:</label>
              <select name="nhacungcap" id="nhacungcap" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($nhacungcap as $ma => $ncc)
              {
                if (!empty($disabled[$ma])) continue 1;
                $selected = (set_value('nhacungcap') == $ma) ? ' selected="selected"' : '';
                echo '<option value="'.$ma.'"'.$selected.'>'.$ncc.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('so-di-dong'); ?>:</label>
              <select name="dauso" class="prephone" id="dauso" style="width:10%">
                <option value=""></option>
              </select>
              <input type="text" value="<?php echo set_value('didong'); ?>" name="didong" class="postphone" maxlength="7" autocomplete="off" onkeypress='return isNumberKey(event);' style="width:20%">
            </div>
            <div class="form">
              <label><?php elang('menh-gia'); ?>:</label>
              <input type="text" value="<?php echo set_value('menhgia'); ?>" name="menhgia" maxlength="7" autocomplete="off" style="width:30%">
            </div>
            <div class="form">
              <label><?php elang('ngan-hang'); ?>:</label>
              <select name="nganhang" id="nganhang" style="width:30%">
                <option value="">-- <?php elang('chon'); ?> --</option>
              <?php
              foreach ($bank_code as $code => $bank)
              {
                $selected = (set_value('nganhang') == $code) ? ' selected="selected"' : '';
                echo '<option value="'.$code.'"'.$selected.'>'.$bank.'</option>';
              }
              ?>
              </select>
            </div>
            <div class="form">
              <label><?php elang('thanh-tien'); ?>:</label>
              <span data-id="thanhtien">0 đ</span>
            </div>
            <div class="form">
              <label></label>
              <span>
                <input type="submit" value="<?php elang('thanh-toan'); ?>">
              </span>
            </div>
          </form>
          <div class="form">
            <fieldset data-id="VTC0130" class="thongtinhuongdan hide">
              <legend>Thông tin cần biết khi thanh toán trả sau Mobifone</legend>
              <strong>Hướng dẫn thanh toán</strong><br>

              Quý khách điền các thông tin theo yêu cầu và bấm thanh toán <br>

              <strong>Số tiền cần nạp:</strong> Nhập số tiền cước thanh toán <br>

              <strong>Số di động được nạp:</strong> Nhập số điện thoại Mobifone trả sau muốn nạp tiền. <br>

              <strong>Tiện ích của dịch vụ thanh toán cước trả sau</strong>  <br>

              Không cần tới các điểm Chăm sóc Khách hàng của Mobifone để đóng cước  <br>

              Đơn giản, tiện lợi: Chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>
            </fieldset>
            <fieldset data-id="VTC0329" class="thongtinhuongdan hide">
              <legend>Thông tin cần biết khi thanh toán trả sau Viettel</legend>
              <strong>Điều kiện thanh toán:</strong> Trong hợp đồng, quý khách chọn hình thức Thanh toán bằng thẻ cào. Số tiền thanh toán phải từ 10.000 VNĐ và là bội số của 1.000 VNĐ<br><br>
              <strong>Cách thay đổi hình thức thanh toán sang thẻ cào:</strong><br>
              <strong>Cách 1:</strong> Gọi *166# và thực hiện theo hướng dẫn<br>
              <strong>Cách 2:</strong> Nhắn tin DK TT gửi 166<br>
            </fieldset>
            <fieldset data-id="VTC0201" class="thongtinhuongdan hide">
              <legend>Thông tin cần biết khi thanh toán trả sau Vinaphone</legend><br>
              <strong>Hướng dẫn thanh toán: </strong><br><br>

              <strong>Quý khách điền các thông tin theo yêu cầu và bấm thanh toán </strong><br>
              <strong>Số tiền cần nạp:</strong> nhập số tiền cước thanh toán <br>
              <strong>Số di động được nạp:</strong> Nhập số điện thoại Vina trả sau muốn nạp tiền. <br><br>

              <strong>Tiện ích của dịch vụ thanh toán cước trả sau </strong><br><br>

              Không cần tới các điểm Chăm sóc Khách hàng của Vinaphone để đóng cước <br>
              Đơn giản, tiện lợi: chỉ cần nhập số ĐTDĐ cần nạp, số tiền sẽ được nạp ngay vào số ĐT đó.<br>

              <strong>Lưu ý:</strong> Khách hàng phải đăng ký mở tài khoản <strong>EZPay</strong> thành công. Nếu khách hàng chưa đăng ký dịch vụ này thì có thể soạn tin nhắn miễn phí <strong>EZPay</strong> gửi <strong>9888</strong>.<br><br>

              Số tiền khách hàng thanh toán phải từ <strong>6,000VNĐ</strong> đến <strong>1,000,000VNĐ</strong>.( số tiền phải chia hết cho 1.000 VNĐ)
            </fieldset>
          </div>
      </div>
      <div class="col-md-3">
        <?php echo $sidebar; ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {

  var dauso = <?php echo json_encode($dauso); ?>;
  var dauso_df = '<?php echo set_value('dauso'); ?>';
  var giamgia = <?php echo json_encode($giamgia); ?>;

  $('#nhacungcap').linkSelect('#dauso', dauso, dauso_df);

  function tinhtien()
  {
    var ncc = $('select[name="nhacungcap"]').val();
    var mg = $('input[name="menhgia"]').val();
    var tien = (!ncc || !mg) ? 0 : mg * giamgia[ncc];
    $('span[data-id="thanhtien"]').html(tien.formatMoney(0, ',', '.')+' đ');
  }

  $('input[name="didong"]').keypress(function(event) {
    return isNumberKey(event);
  });
  $('input[name="menhgia"]').keypress(function(event) {
    return isNumberKey(event);
  }).keyup(function() {
    tinhtien();
  });

  $('select[name="nhacungcap"]').each(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('input[name="menhgia"]').keyup();
  });
  $('select[name="nhacungcap"]').change(function() {
    $('.thongtinhuongdan').addClass('hide');
    $('.thongtinhuongdan[data-id="'+$(this).val()+'"]').removeClass('hide');
    $('input[name="menhgia"]').keyup();
  });
  $('#xulygiaodich').submit(function(){
    $('.loading.hide').removeClass('hide');
    $('input:submit').prop('disabled', true);
  });
});
</script>