<div class="chi-tiet">
  <h3><span><?php elang('thong-tin-dich-vu'); ?></span></h3>
  <ul>
      <li><label><?php elang('dich-vu'); ?>:</label> <?php elang('nap-tien-tai-khoan'); ?></li>
      <li><label><?php elang('nha-cung-cap'); ?>:</label> <?php echo $nhacungcap[$transaction['data']['nhacungcap']]; ?></li>
      <li><label><?php elang('menh-gia'); ?>:</label> <?php echo $menhgia[$transaction['data']['nhacungcap']][$transaction['data']['menhgia']]; ?></li>
  </ul>
  <h3><span><?php elang('thong-tin-giao-dich'); ?></span></h3>
  <ul>
      <li><label><?php elang('ma-giao-dich'); ?>:</label> <?php echo $transaction['id']; ?></li>
      <li><label><?php elang('thoi-gian'); ?>:</label> <?php echo tn_datetime($transaction['created_time']); ?></li>
      <li><label><?php elang('trang-thai'); ?>:</label> <?php echo '<span class="trans_st'.$transaction['status'].'">'.lang('trans_st'.$transaction['status']).'</span>'; ?></li>
      <li><label><?php elang('ngan-hang'); ?>:</label> <?php echo $bank_code[$transaction['bank']]; ?></li>
      <li><label><?php elang('tai-khoan'); ?>:</label> <?php echo $transaction['data']['taikhoan']; ?></li>
      <li><label><?php elang('thanh-tien'); ?>:</label> <?php echo tn_money_format($transaction['amount']); ?></li>
  </ul>
</div>