<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('lay-lai-mat-khau'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('lay-lai-mat-khau'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
          <h3 class="title-left"><span><?php elang('lay-lai-mat-khau'); ?></span><div class="bottom"></div></h3>
          <?php echo $this->tn_message->get(); ?>
          <form method="post" id="register_form_increate">
            <div class="register">
              <label><?php elang('email'); ?> *</label>
              <input type="text" name="email" placeholder="" class="requiredField email" style="width:88%">
            </div>
            <div class="register">
              <label><?php elang('ma-xac-nhan'); ?> *</label>
              <?php echo $recaptcha_html; ?>
            </div>
            <div class="register">
              <input type="submit" value="<?php elang('lay-lai-mat-khau'); ?>">
              <input type="reset" value="<?php elang('nhap-lai'); ?>">
            </div>
          </form>
      </div>
      <div class="col-md-6">
         <h3 class="title-left"><span><?php elang('khong-phai-thanh-vien'); ?></span><div class="bottom"></div></h3>
        <?php elang('tao-tai-khoan-moi'); ?>
          <a href="<?php echo site_url('member/register'); ?>"><?php elang('dang-ky-tai-khoan'); ?></a>
      </div>
    </div>
  </div>
</section>
