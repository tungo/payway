<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('thong-tin-tai-khoan'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('thong-tin-tai-khoan'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container dang-ky">
    <div class="row">
      <div class="col-md-3">
        <h3 class="title-left"><span><?php elang('danh-muc'); ?></span><div class="bottom"></div></h3>
        <div class="right-menu">
          <ul>
            <li class="active"><a href="member"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
            <li><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
            <li><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
            <li><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
            <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
          </ul>
        </div>
        <div class="ads-right">
          <?php foreach ($banner['Left'] as $bn)
          {
            $img = '<img src="'.$banner_image_url.$bn['image'].'" alt="'.$bn['name'].'">';
            if ($bn['link'] != '')
            {
              echo '<a href="'.$bn['link'].'" title="'.$bn['name'].'">'.$img.'</a>';
            }
            else
            {
              echo $img;
            }
          } ?>
        </div>
      </div>
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('chao'); ?>, <?php echo $account['firstname']; ?></span><div class="bottom"></div></h3>
        <?php echo $this->tn_message->get(); ?>
        <div class="col-md-6">
          <h3><?php elang('thong-tin-tai-khoan'); ?></h3>
          <p><label><?php elang('email'); ?>:</label> <?php echo $account['email']; ?></p>
          <p><label><?php elang('ten'); ?>:</label> <?php echo $account['firstname']; ?></p>
          <p><label><?php elang('ho'); ?>:</label> <?php echo $account['lastname']; ?></p>
          <p><label><?php elang('password'); ?>:</label> ********</p>
        </div>
        <div class="col-md-6">
          <h3><?php elang('thong-tin-bao-mat'); ?></h3>
          <p><label><?php elang('cau-hoi-bao-mat'); ?>:</label> <?php echo ($account['question_id']) ? $questions[$account['question_id']] : ''; ?></p>
          <p><label><?php elang('cau-tra-loi'); ?>:</label> <?php echo tn_hide($account['answer']); ?></p>
          <p><label><?php elang('chung-minh-thu'); ?>:</label> <?php echo tn_hide($account['identification']); ?></p>
          <p><label><?php elang('so-dien-thoai'); ?>:</label> <?php echo tn_hide($account['phone']); ?></p>
        </div>
        <div class="col-md-12">
          <h3><?php elang('thong-tin-chung'); ?></h3>
          <p><label><?php elang('gioi-tinh'); ?>:</label> <?php echo $gender[$account['gender']]; ?></p>
          <p><label><?php elang('ngay-sinh'); ?>:</label> <?php echo date('d/m/Y', strtotime($account['birthday'])); ?></p>
          <p><label><?php elang('quoc-gia'); ?>:</label> <?php echo ($account['nationality']) ? $nations[$account['nationality']] : ''; ?></p>
          <p><label><?php elang('dia-chi'); ?>:</label> <?php echo $account['address']; ?></p>
          <p><label><?php elang('thong-tin-them'); ?>:</label> <?php echo $account['additional_info']; ?></p>
          <p><label><?php elang('thanh-pho'); ?>:</label> <?php echo $account['city']; ?></p>
          <p><label><?php elang('post-code'); ?>:</label> <?php echo $account['postcode']; ?></p>
        </div>
      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('lich-su-giao-dich'); ?></span><div class="bottom"></div></h3>
        <table class="lich-su-giao-dich">
            <thead>
              <tr>
                <th><?php elang('ma-giao-dich'); ?></th>
                <th><?php elang('thoi-gian'); ?></th>
                <th><?php elang('dich-vu'); ?></th>
                <th><?php elang('so-tien'); ?></th>
                <th><?php elang('trang-thai'); ?></th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
<?php if(!empty($transactions)):
  foreach ($transactions as $transaction): ?>
              <tr>
                <td><?php echo $transaction['id']; ?></td>
                <td><?php echo tn_datetime($transaction['created_time']); ?></td>
                <td><?php elang($transaction['type']); ?></td>
                <td><?php echo tn_money_format($transaction['amount']); ?></td>
                <td><?php echo '<span class="trans_st'.$transaction['status'].'">'.lang('trans_st'.$transaction['status']).'</span>'; ?></td>
                <td><a href="<?php echo base_url('member/transaction/id/'.$transaction['id']); ?>"><?php elang('xem-chi-tiet'); ?></a></td>
              </tr>
            <tbody>
  <?php endforeach;
else: ?>
              <tr>
                <td colspan="6"><?php elang('chua-co-thong-tin'); ?></td>
              </tr>
<?php endif; ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="6"><a href="<?php echo base_url('member/history/'); ?>"><?php elang('xem-them'); ?></a></th>
              </tr>
            </tfoot>
          </table>
      </div>
    </div>
  </div>
</section>