<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('doi-mat-khau'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('doi-mat-khau'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container dang-ky">
    <div class="row">
      <div class="col-md-3">
        <h3 class="title-left"><span><?php elang('danh-muc'); ?></span><div class="bottom"></div></h3>
        <div class="right-menu">
          <ul>
            <li><a href="member"><i class="icon_profile"></i> <?php elang('thong-tin-tai-khoan'); ?></a></li>
            <li><a href="member/edit/account"><i class="icon_id"></i> <?php elang('sua-thong-tin-tai-khoan'); ?></a></li>
            <li class="active"><a href="member/edit/password"><i class="icon_pencil-edit"></i> <?php elang('doi-mat-khau'); ?></a></li>
            <li><a href="member/history/"><i class="icon_table"></i> <?php elang('lich-su-giao-dich'); ?></a></li>
            <li><a href="<?php echo site_url('member/logout');?>"><i class="fa-power-off"></i> <?php elang('thoat'); ?></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        <h3 class="title-left"><span><?php elang('doi-mat-khau'); ?></span><div class="bottom"></div></h3>
        <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <div class="register">
            <label><?php elang('mat-khau-cu'); ?></label>
            <input type="password" name="oldpass" class="requiredField email" style="width:50%">
          </div>
          <div class="register">
            <label><?php elang('mat-khau-moi'); ?></label>
            <input type="password" name="newpass" class="requiredField" style="width:50%">
          </div>
          <div class="register">
            <label><?php elang('xac-nhan-mat-khau'); ?></label>
            <input type="password" name="confirm" class="requiredField" style="width:50%">
          </div>
          <div class="register">
            <input type="submit" value="<?php elang('luu'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>