<div class="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title"><span><?php elang('dang-nhap'); ?></span></h3>
        <ul>
          <li><a href="#"><?php elang('trang-chu'); ?></a> <i class="glyphicons chevron-right"></i></li>
          <li><?php elang('dang-nhap'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="color trang">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3 class="title-left"><span><?php elang('dang-nhap'); ?></span><div class="bottom"></div></h3>
        <?php echo $this->tn_message->get(); ?>
        <form method="post" id="register_form_increate">
          <label><?php elang('email'); ?> *</label>
            <input type="text" name="email" placeholder="<?php elang('email'); ?> *" class="requiredField email" style="width:100%">
          <label><?php elang('password'); ?> *</label>
            <input type="password" name="password" placeholder="<?php elang('password'); ?> *" class="requiredField" style="width:100%">
            <input type="submit" value="<?php elang('dang-nhap'); ?>">
            <input type="reset" value="<?php elang('nhap-lai'); ?>">
            <a class="quen-mat-khau" href="<?php echo site_url('member/reset_password'); ?>"><?php elang('quen-mat-khau'); ?></a>
        </form>
      </div>
      <div class="col-md-6">
        <h3 class="title-left"><span><?php elang('khong-phai-thanh-vien'); ?></span><div class="bottom"></div></h3>
        <?php elang('tao-tai-khoan-moi'); ?>
          <a href="<?php echo site_url('member/register'); ?>"><?php elang('dang-ky-tai-khoan'); ?></a>
      </div>
    </div>
  </div>
</section>
