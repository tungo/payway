-- phpMyAdmin SQL Dump
-- version 4.1.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2014 at 03:31 PM
-- Server version: 5.0.95
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET NAMES utf8;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `payway1db`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_article`
--

DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE IF NOT EXISTS `blog_article` (
  `id` int(11) NOT NULL auto_increment,
  `image` varchar(255) NOT NULL,
  `seo_name` varchar(255) NOT NULL,
  `publish_time` datetime NOT NULL,
  `view` int(11) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_article_description`
--

DROP TABLE IF EXISTS `blog_article_description`;
CREATE TABLE IF NOT EXISTS `blog_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `synopsis` text NOT NULL,
  `description` text NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyworks` varchar(255) NOT NULL,
  PRIMARY KEY  (`article_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE IF NOT EXISTS `blog_category` (
  `id` int(11) NOT NULL auto_increment,
  `image` varchar(255) NOT NULL,
  `seo_name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_description`
--

DROP TABLE IF EXISTS `blog_category_description`;
CREATE TABLE IF NOT EXISTS `blog_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyworks` varchar(255) NOT NULL,
  PRIMARY KEY  (`category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `directory` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `locale`, `image`, `directory`, `status`, `position`) VALUES
(1, 'Tiếng Việt', 'vi', 'vi_VN', 'vn.png', 'vietnamese', 1, 0),
(2, 'English', 'en', 'en_GB', 'gb.png', 'english', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `module_partner`
--

DROP TABLE IF EXISTS `module_partner`;
CREATE TABLE IF NOT EXISTS `module_partner` (
  `id` int(11) NOT NULL auto_increment,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_partner_description`
--

DROP TABLE IF EXISTS `module_partner_description`;
CREATE TABLE IF NOT EXISTS `module_partner_description` (
  `partner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `synopsis` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`partner_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `additional_info` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `identification` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `registered_time` datetime NOT NULL,
  `registered_ip` varchar(255) NOT NULL,
  `visited_time` datetime NOT NULL,
  `visited_ip` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `active_code` varchar(255) NOT NULL,
  `remember_code` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`id`, `email`, `password`, `firstname`, `lastname`, `status`, `position`, `registered_time`, `registered_ip`, `visited_time`, `visited_ip`, `salt`, `active_code`, `remember_code`, `group_id`) VALUES
(1, 'ngoanhtu.88@gmail.com', '3dde1d7985b7791c402796e88a609da3fff13f86', 'Tú', 'Ngô', 1, 0, '', '', '2014-01-23 14:35:36', '115.78.238.224', '8de97bd5cc', '', '', 1),
(2, 'tu.ngo@thegioitructuyen.vn', '66210971740e5df53d6d76fe0c9cf16ad5568015', 'Tú', 'Ngô', 1, 0, '', '', '2014-01-21 11:46:58', '115.78.238.224', 'a81e3v41d4', '', '', 1),
(11, 'nam.le@thegioitructuyen.vn', 'd242ed702f88022ccc519d95b36fd9e6b4fc7270', 'Nam', 'Lé', 1, 0, '', '', '2014-01-21 11:46:58', '115.78.238.224', 'fa2yd1f54j', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `level` tinyint(1) NOT NULL,
  `permission` tinyint(1) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `type`, `admin`, `default`, `level`, `permission`, `description`, `status`, `position`, `created_time`, `created_by`, `modified_time`, `modified_by`) VALUES
(1, 'Developer', 1, 1, 1, 1, 1, '', 1, 0, '', '', '', ''),
(2, 'Administrator', 1, 1, 1, 2, 1, '', 1, 0, '', '', '', ''),
(3, 'Manager', 1, 1, 1, 3, 0, '', 1, 0, '', '', '', ''),
(4, 'User', 1, 0, 1, 4, 0, '', 1, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_privilege`
--

DROP TABLE IF EXISTS `user_group_privilege`;
CREATE TABLE IF NOT EXISTS `user_group_privilege` (
  `group_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY  (`group_id`,`privilege_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege`
--

DROP TABLE IF EXISTS `user_privilege`;
CREATE TABLE IF NOT EXISTS `user_privilege` (
  `id` int(11) NOT NULL auto_increment,
  `module` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `user_privilege`
--

INSERT INTO `user_privilege` (`status`, `name`, `module`, `controller`, `method`) VALUES

(1, 'blog_article view', 'admin', 'blog_article', 'index'),
(1, 'blog_article manage', 'admin', 'blog_article', 'add'),
(1, 'blog_article manage', 'admin', 'blog_article', 'edit'),
(1, 'blog_article manage', 'admin', 'blog_article', 'multi_copy'),
(1, 'blog_article manage', 'admin', 'blog_article', 'delete'),
(1, 'blog_article manage', 'admin', 'blog_article', 'multi_delete'),
(1, 'blog_article manage', 'admin', 'blog_article', 'save'),
(1, 'blog_article manage', 'admin', 'blog_article', 'status'),
(1, 'blog_article manage', 'admin', 'blog_article', 'active'),
(1, 'blog_article manage', 'admin', 'blog_article', 'disable'),
(1, 'blog_article view', 'admin', 'blog_article', 'search'),
(1, 'blog_article view', 'admin', 'blog_article', 'show'),
(1, 'blog_article view', 'admin', 'blog_article', 'sort'),
(1, 'blog_article view', 'admin', 'blog_article', 'filter'),

(1, 'blog_category view', 'admin', 'blog_category', 'index'),
(1, 'blog_category manage', 'admin', 'blog_category', 'add'),
(1, 'blog_category manage', 'admin', 'blog_category', 'edit'),
(1, 'blog_category manage', 'admin', 'blog_category', 'multi_copy'),
(1, 'blog_category manage', 'admin', 'blog_category', 'delete'),
(1, 'blog_category manage', 'admin', 'blog_category', 'multi_delete'),
(1, 'blog_category manage', 'admin', 'blog_category', 'save'),
(1, 'blog_category manage', 'admin', 'blog_category', 'status'),
(1, 'blog_category manage', 'admin', 'blog_category', 'active'),
(1, 'blog_category manage', 'admin', 'blog_category', 'disable'),
(0, 'blog_category view', 'admin', 'blog_category', 'search'),
(0, 'blog_category view', 'admin', 'blog_category', 'show'),
(0, 'blog_category view', 'admin', 'blog_category', 'sort'),
(0, 'blog_category view', 'admin', 'blog_category', 'filter'),

(1, 'module_partner view', 'admin', 'module_partner', 'index'),
(1, 'module_partner manage', 'admin', 'module_partner', 'add'),
(1, 'module_partner manage', 'admin', 'module_partner', 'edit'),
(1, 'module_partner manage', 'admin', 'module_partner', 'multi_copy'),
(1, 'module_partner manage', 'admin', 'module_partner', 'delete'),
(1, 'module_partner manage', 'admin', 'module_partner', 'multi_delete'),
(1, 'module_partner manage', 'admin', 'module_partner', 'save'),
(1, 'module_partner manage', 'admin', 'module_partner', 'status'),
(1, 'module_partner manage', 'admin', 'module_partner', 'active'),
(1, 'module_partner manage', 'admin', 'module_partner', 'disable'),
(1, 'module_partner view', 'admin', 'module_partner', 'search'),
(1, 'module_partner view', 'admin', 'module_partner', 'show'),
(1, 'module_partner view', 'admin', 'module_partner', 'sort'),
(0, 'module_partner view', 'admin', 'module_partner', 'filter'),

(1, 'user_account view', 'admin', 'user_account', 'index'),
(1, 'user_account manage', 'admin', 'user_account', 'add'),
(1, 'user_account manage', 'admin', 'user_account', 'edit'),
(0, 'user_account manage', 'admin', 'user_account', 'multi_copy'),
(1, 'user_account manage', 'admin', 'user_account', 'delete'),
(1, 'user_account manage', 'admin', 'user_account', 'multi_delete'),
(0, 'user_account manage', 'admin', 'user_account', 'save'),
(1, 'user_account manage', 'admin', 'user_account', 'status'),
(1, 'user_account manage', 'admin', 'user_account', 'active'),
(1, 'user_account manage', 'admin', 'user_account', 'disable'),
(1, 'user_account view', 'admin', 'user_account', 'search'),
(1, 'user_account view', 'admin', 'user_account', 'show'),
(1, 'user_account view', 'admin', 'user_account', 'sort'),
(1, 'user_account view', 'admin', 'user_account', 'filter'),

(1, 'user_group view', 'admin', 'user_group', 'index'),
(1, 'user_group manage', 'admin', 'user_group', 'add'),
(1, 'user_group manage', 'admin', 'user_group', 'edit'),
(0, 'user_group manage', 'admin', 'user_group', 'multi_copy'),
(1, 'user_group manage', 'admin', 'user_group', 'delete'),
(1, 'user_group manage', 'admin', 'user_group', 'multi_delete'),
(1, 'user_group manage', 'admin', 'user_group', 'save'),
(1, 'user_group manage', 'admin', 'user_group', 'status'),
(1, 'user_group manage', 'admin', 'user_group', 'active'),
(1, 'user_group manage', 'admin', 'user_group', 'disable'),
(1, 'user_group view', 'admin', 'user_group', 'search'),
(1, 'user_group view', 'admin', 'user_group', 'show'),
(1, 'user_group view', 'admin', 'user_group', 'sort'),
(0, 'user_group view', 'admin', 'user_group', 'filter'),

(1, 'website_contact view', 'admin', 'website_contact', 'index'),
(0, 'website_contact manage', 'admin', 'website_contact', 'add'),
(0, 'website_contact manage', 'admin', 'website_contact', 'edit'),
(0, 'website_contact manage', 'admin', 'website_contact', 'multi_copy'),
(1, 'website_contact manage', 'admin', 'website_contact', 'delete'),
(1, 'website_contact manage', 'admin', 'website_contact', 'multi_delete'),
(0, 'website_contact manage', 'admin', 'website_contact', 'save'),
(0, 'website_contact manage', 'admin', 'website_contact', 'status'),
(0, 'website_contact manage', 'admin', 'website_contact', 'active'),
(0, 'website_contact manage', 'admin', 'website_contact', 'disable'),
(1, 'website_contact view', 'admin', 'website_contact', 'search'),
(1, 'website_contact view', 'admin', 'website_contact', 'show'),
(1, 'website_contact view', 'admin', 'website_contact', 'sort'),
(0, 'website_contact view', 'admin', 'website_contact', 'filter'),

(1, 'module_banner view', 'admin', 'module_banner', 'index'),
(1, 'module_banner manage', 'admin', 'module_banner', 'add'),
(1, 'module_banner manage', 'admin', 'module_banner', 'edit'),
(1, 'module_banner manage', 'admin', 'module_banner', 'multi_copy'),
(1, 'module_banner manage', 'admin', 'module_banner', 'delete'),
(1, 'module_banner manage', 'admin', 'module_banner', 'multi_delete'),
(1, 'module_banner manage', 'admin', 'module_banner', 'save'),
(1, 'module_banner manage', 'admin', 'module_banner', 'status'),
(1, 'module_banner manage', 'admin', 'module_banner', 'active'),
(1, 'module_banner manage', 'admin', 'module_banner', 'disable'),
(1, 'module_banner view', 'admin', 'module_banner', 'search'),
(1, 'module_banner view', 'admin', 'module_banner', 'show'),
(1, 'module_banner view', 'admin', 'module_banner', 'sort'),
(1, 'module_banner view', 'admin', 'module_banner', 'filter')

;

-- --------------------------------------------------------

--
-- Table structure for table `website_contact`
--

DROP TABLE IF EXISTS `website_contact`;
CREATE TABLE IF NOT EXISTS `website_contact` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `nation`
--

DROP TABLE IF EXISTS `nation`;
CREATE TABLE IF NOT EXISTS `nation` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `nation`
--

INSERT INTO `nation` (`status`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(1, 'AL', 'Albania'),
(1, 'DZ', 'Algeria'),
(1, 'AS', 'American Samoa'),
(1, 'AD', 'Andorra'),
(1, 'AO', 'Angola'),
(1, 'AI', 'Anguilla'),
(1, 'AG', 'Antigua And Barbuda'),
(1, 'AR', 'Argentina'),
(1, 'AW', 'Aruba'),
(1, 'AU', 'Australia'),
(1, 'AT', 'Austria'),
(1, 'AZ', 'Azerbaijan'),
(1, 'BS', 'Bahamas'),
(1, 'BH', 'Bahrain'),
(1, 'BD', 'Bangladesh'),
(1, 'BB', 'Barbados'),
(1, 'BY', 'Belarus'),
(1, 'BE', 'Belgium'),
(1, 'BZ', 'Belize'),
(1, 'BJ', 'Benin'),
(1, 'BM', 'Bermuda'),
(1, 'BT', 'Bhutan'),
(1, 'BO', 'Bolivia'),
(1, 'BA', 'Bosnia and Herzegovina'),
(1, 'BW', 'Botswana'),
(1, 'BR', 'Brazil'),
(1, 'VG', 'British Virgin Islands'),
(1, 'BN', 'Brunei Darussalam'),
(1, 'BG', 'Bulgaria'),
(1, 'BF', 'Burkina Faso'),
(1, 'BI', 'Burundi'),
(1, 'KH', 'Cambodia'),
(1, 'CM', 'Cameroon'),
(1, 'CA', 'Canada'),
(1, 'CV', 'Cape Verde'),
(1, 'KY', 'Cayman Islands'),
(1, 'CF', 'Central African Republic'),
(1, 'TD', 'Chad'),
(1, 'CL', 'Chile'),
(1, 'CN', 'China'),
(1, 'CO', 'Colombia'),
(1, 'KM', 'Comoros'),
(1, 'CD', 'Congo, Democratic Republic'),
(1, 'CG', 'Congo-Brazzaville'),
(1, 'CK', 'Cook Islands'),
(1, 'CR', 'Costa Rica'),
(1, 'HR', 'Croatia'),
(1, 'CU', 'Cuba'),
(1, 'AN', 'Curacao'),
(1, 'CY', 'Cyprus'),
(1, 'C2', 'Cyprus (Northern)'),
(1, 'CZ', 'Czech Republic'),
(1, 'DK', 'Denmark'),
(1, 'DJ', 'Djibouti'),
(1, 'DM', 'Dominica'),
(1, 'DO', 'Dominican Republic'),
(1, 'TP', 'East Timor'),
(1, 'EC', 'Ecuador'),
(1, 'EG', 'Egypt'),
(1, 'SV', 'El Salvador'),
(1, 'GQ', 'Equatorial Guinea'),
(1, 'ER', 'Eritrea'),
(1, 'EE', 'Estonia'),
(1, 'ET', 'Ethiopia'),
(1, 'FK', 'Falkland Islands (Malvinas)'),
(1, 'FJ', 'Fiji'),
(1, 'FI', 'Finland'),
(1, 'FR', 'France'),
(1, 'GF', 'French Guiana'),
(1, 'PF', 'French Polynesia'),
(1, 'GA', 'Gabon'),
(1, 'GM', 'Gambia'),
(1, 'GE', 'Georgia'),
(1, 'DE', 'Germany'),
(1, 'GH', 'Ghana'),
(1, 'GI', 'Gibraltar'),
(1, 'GR', 'Greece'),
(1, 'GD', 'Grenada'),
(1, 'GP', 'Guadeloupe'),
(1, 'GU', 'Guam'),
(1, 'GT', 'Guatemala'),
(1, 'GN', 'Guinea'),
(1, 'GW', 'Guinea-Bissau'),
(1, 'GY', 'Guyana'),
(1, 'HT', 'Haiti'),
(1, 'HN', 'Honduras'),
(1, 'HK', 'Hong Kong'),
(1, 'HU', 'Hungary'),
(1, 'IS', 'Iceland'),
(1, 'IN', 'India'),
(1, 'ID', 'Indonesia'),
(1, 'IQ', 'Iraq'),
(1, 'IE', 'Ireland'),
(1, 'IL', 'Israel'),
(1, 'IT', 'Italy'),
(1, 'CI', 'Ivory Coast'),
(1, 'JM', 'Jamaica'),
(1, 'JP', 'Japan'),
(1, 'JO', 'Jordan'),
(1, 'KZ', 'Kazakhstan'),
(1, 'KE', 'Kenya'),
(1, 'KI', 'Kiribati'),
(1, 'KR', 'Korea'),
(1, 'K1', 'Kosovo'),
(1, 'KW', 'Kuwait'),
(1, 'KG', 'Kyrghyz Republic'),
(1, 'LA', 'Laos'),
(1, 'LV', 'Latvia'),
(1, 'LB', 'Lebanon'),
(1, 'LS', 'Lesotho'),
(1, 'LR', 'Liberia'),
(1, 'LY', 'Libya'),
(1, 'LI', 'Liechtenstein'),
(1, 'LT', 'Lithuania'),
(1, 'LU', 'Luxembourg'),
(1, 'MO', 'Macau'),
(1, 'MK', 'Macedonia'),
(1, 'MG', 'Madagascar'),
(1, 'MW', 'Malawi'),
(1, 'MY', 'Malaysia'),
(1, 'MV', 'Maldives'),
(1, 'ML', 'Mali'),
(1, 'MT', 'Malta'),
(1, 'MH', 'Marshall Islands'),
(1, 'MQ', 'Martinique'),
(1, 'MR', 'Mauritania'),
(1, 'MU', 'Mauritius'),
(1, 'YT', 'Mayotte'),
(1, 'MX', 'Mexico'),
(1, 'FM', 'Micronesia'),
(1, 'MD', 'Moldova'),
(1, 'MC', 'Monaco'),
(1, 'MN', 'Mongolia'),
(1, 'ME', 'Montenegro'),
(1, 'MS', 'Montserrat'),
(1, 'MA', 'Morocco'),
(1, 'MZ', 'Mozambique'),
(1, 'NA', 'Namibia'),
(1, 'NR', 'Nauru'),
(1, 'NP', 'Nepal'),
(1, 'NL', 'Netherlands'),
(1, 'NC', 'New Caledonia'),
(1, 'NZ', 'New Zealand'),
(1, 'NI', 'Nicaragua'),
(1, 'NE', 'Niger'),
(1, 'NG', 'Nigeria'),
(1, 'NU', 'Niue'),
(1, 'MP', 'Northern Mariana Islands'),
(1, 'NO', 'Norway'),
(1, 'OM', 'Oman'),
(1, 'PK', 'Pakistan'),
(1, 'PW', 'Palau'),
(1, 'PS', 'Palestinian Authority'),
(1, 'PA', 'Panama'),
(1, 'PG', 'Papua New Guinea'),
(1, 'PY', 'Paraguay'),
(1, 'PE', 'Peru'),
(1, 'PH', 'Philippines'),
(1, 'PL', 'Poland'),
(1, 'PT', 'Portugal'),
(1, 'PR', 'Puerto Rico'),
(1, 'QA', 'Qatar'),
(1, 'RE', 'Reunion Island'),
(1, 'RO', 'Romania'),
(1, 'RU', 'Russia'),
(1, 'RW', 'Rwanda'),
(1, 'BL', 'Saint Barthelemy'),
(1, 'KN', 'Saint Kitts And Nevis'),
(1, 'LC', 'Saint Lucia'),
(1, 'VC', 'Saint Vincent And The Grenadines'),
(1, 'WS', 'Samoa'),
(1, 'ST', 'Sao Tome And Principe'),
(1, 'SA', 'Saudi Arabia'),
(1, 'SN', 'Senegal'),
(1, 'YU', 'Serbia'),
(1, 'SL', 'Sierra Leone'),
(1, 'SG', 'Singapore'),
(1, 'SK', 'Slovakia'),
(1, 'SI', 'Slovenia'),
(1, 'SB', 'Solomon Islands'),
(1, 'ZA', 'South Africa'),
(1, 'ES', 'Spain'),
(1, 'LK', 'Sri Lanka'),
(1, 'S1', 'St. Maarten'),
(1, 'MF', 'St. Martin'),
(1, 'SD', 'Sudan'),
(1, 'SR', 'Suriname'),
(1, 'SE', 'Sweden'),
(1, 'CH', 'Switzerland'),
(1, 'SY', 'Syria'),
(1, 'TW', 'Taiwan'),
(1, 'TJ', 'Tajikistan'),
(1, 'TZ', 'Tanzania'),
(1, 'TH', 'Thailand'),
(1, 'TG', 'Togo'),
(1, 'TO', 'Tonga'),
(1, 'TT', 'Trinidad and Tobago'),
(1, 'TN', 'Tunisia'),
(1, 'TR', 'Turkey'),
(1, 'TM', 'Turkmenistan'),
(1, 'TC', 'Turks and Caicos Islands'),
(1, 'TV', 'Tuvalu'),
(1, 'UG', 'Uganda'),
(1, 'UA', 'Ukraine'),
(1, 'AE', 'United Arab Emirates'),
(1, 'GB', 'United Kingdom'),
(1, 'US', 'United States'),
(1, 'UY', 'Uruguay'),
(1, 'UZ', 'Uzbekistan'),
(1, 'VU', 'Vanuatu'),
(1, 'VE', 'Venezuela'),
(1, 'VN', 'Vietnam'),
(1, 'VI', 'Virgin Islands (US)'),
(1, 'YE', 'Yemen'),
(1, 'ZM', 'Zambia'),
(1, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `user_security_question`
--

DROP TABLE IF EXISTS `user_security_question`;
CREATE TABLE IF NOT EXISTS `user_security_question` (
  `id` int(11) NOT NULL auto_increment,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `user_security_question`
--

INSERT INTO `user_security_question` (`id`, `status`) VALUES
(1001, 1),
(1002, 1),
(1003, 1),
(1004, 1),
(1005, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_security_question_description`
--

DROP TABLE IF EXISTS `user_security_question_description`;
CREATE TABLE IF NOT EXISTS `user_security_question_description` (
  `security_question_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`security_question_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `user_security_question_description`
--

INSERT INTO `user_security_question_description` (`security_question_id`, `language_id`, `name`) VALUES
(1001, 1, 'Họ của mẹ bạn'),
(1001, 2, 'Mother\'s maiden name'),
(1002, 1, 'Nơi sinh của bạn'),
(1002, 2, 'Place of birth'),
(1003, 1, 'Trường tiểu học của bạn'),
(1003, 2, 'Elementary School'),
(1004, 1, 'Tên thú cưng của bạn'),
(1004, 2, 'Pet\'s name'),
(1005, 1, 'Món ăn yêu thích của bạn'),
(1005, 2, 'The most favorite food');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `payment` text NOT NULL,
  `data` longtext NOT NULL,
  `created_time` datetime NOT NULL,
  `completed_time` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=11001 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

DROP TABLE IF EXISTS `transaction_type`;
CREATE TABLE IF NOT EXISTS `transaction_type` (
  `id` int(11) NOT NULL auto_increment,
  `type_code` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1101 ;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `type_code`, `status`) VALUES
(1101, 'PW1101', 1),
(1102, 'PW1102', 1),
(1103, 'PW1103', 1),
(1104, 'PW1104', 1),
(1105, 'PW1105', 1),
(1106, 'PW1106', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type_description`
--

DROP TABLE IF EXISTS `transaction_type_description`;
CREATE TABLE IF NOT EXISTS `transaction_type_description` (
  `type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`type_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `transaction_type_description`
--

INSERT INTO `transaction_type_description` (`type_id`, `language_id`, `name`) VALUES
(1101, 1, 'Di động trả trước'),
(1101, 2, 'Di động trả trước'),
(1102, 1, 'Di động trả sau'),
(1102, 2, 'Di động trả sau'),
(1103, 1, 'Mua thẻ điện thoại'),
(1103, 2, 'Mua thẻ điện thoại'),
(1104, 1, 'Mua thẻ game'),
(1104, 2, 'Mua thẻ game'),
(1105, 1, 'Nạp tiền game'),
(1105, 2, 'Nạp tiền game'),
(1106, 1, 'Nạp tiền tài khoản'),
(1106, 2, 'Nạp tiền tài khoản');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
CREATE TABLE IF NOT EXISTS `transaction_log` (
  `id` int(11) NOT NULL auto_increment,
  `transaction_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `data` text NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_bnvn_paygate`
--

DROP TABLE IF EXISTS `log_bnvn_paygate`;
CREATE TABLE IF NOT EXISTS `log_bnvn_paygate` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `data` text NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_vnpay_paygate`
--

DROP TABLE IF EXISTS `log_vnpay_paygate`;
CREATE TABLE IF NOT EXISTS `log_vnpay_paygate` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `data` text NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_vtc_pay`
--

DROP TABLE IF EXISTS `log_vtc_pay`;
CREATE TABLE IF NOT EXISTS `log_vtc_pay` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `data` text NOT NULL,
  `result` longtext NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
